RED := '\033[0;31m'
RESET := '\033[0m'

@_default:
  just --list

_echo_err +msg:
  #!/usr/bin/env bash
  echo -e >&2 "{{ RED }}ERROR{{ RESET }}: {{ msg }}"

# Create a new folder for the provided year
create year:
  #!/usr/bin/env bash
  [[ ! "{{ year }}" =~ ^20(1[5-9]|[2-9][0-9])$ ]] && just _echo_err "Invalid year: {{ year }}" && exit
  [[ -e {{ year }} ]] && just _echo_err "File or folder already exists at ./{{ year }}. Aborting." && exit

  # Enable for recursive globbing
  shopt -s globstar

  echo "Creating folder for {{year}}..."

  #mkdir -p "{{ year }}/template"
  cp -r template "{{ year }}"

  #for origin in template/*; do
  #  # target dir is origin but with the specified year instead of 'template'
  #  target="${origin/#template/{{ year }}}"

  #  if [[ -d "$origin" && ! -L "$origin" ]]; then
  #    mkdir -p "$target"

      #if [[ -d "${origin}/day" ]]; then
      #  for day in {01..25}; do
      #    mkdir -p "${target}/day${day}"

      #    for day_origin in "${origin}"/day/**/*; do
      #      day_target="${day_origin/#template/{{ year }}}"
      #      day_target="${day_target/\/day\///day${day}/}"

      #      if [[ -d "$day_origin" ]]; then
      #        mkdir "$day_target"
      #      fi
      #    done

      #    for day_origin in "${origin}"/day/**/*; do
      #      day_target="${day_origin/#template/{{ year }}}"
      #      day_target="${day_target/\/day\///day${day}/}"

      #      if [[ ! -d "$day_origin" ]]; then
      #        sed 's/dayDAY/day'"$day"'/g' "$day_origin" > "$day_target"
      #      fi
      #    done
      #  done
      #fi

      #if [[ -d "${origin}/year" ]]; then
      #  cp -R "${origin}/year/." "${target}/"
      #fi
  #  fi
  #done
