# Instructions

Requires
- Rust 1.56.0 or greater
- [Just](https://github.com/casey/just)


## 1. Create daily solution files

To start working on day 1, you can run:

```shell
just create 1
```

This will result in the following files and directories:

```
2015
├── day01/
│  ├── src/
│  │  ├── main.rs
│  │  ├── solution1.rs
│  │  └── solution2.rs
│  ├── input/
│  │  ├── input.txt
│  │  └── test_input.txt
│  ├── Cargo.lock
│  └── Cargo.toml
├── .justfile
├── README.md
└── template/
```

## 2. Add inputs and test inputs

Edit `../inputs/day01/test_input.txt` and `../inputs/day01/input.txt`.


## 3. Write some code

`day01/src/solution.rs` is probably a good place for this.

I'll wait.


## 4. Run the tests

To run the tests for only the first solution of day 1:

```shell
just test 1 1
```

To run for both solutions:

```shell
just test 1
```

## 5. Run the solution

To run the solution for day 1 part 1:

```shell
just day 1 1
```

## 6. Other useful commands

Run solutions in release mode:

```shell
just rrun 1
just rrun 1 1
```

Format code with `rustfmt`:

```shell
just format 1
```

Check code with `clippy`:

```shell
just check 1
```
