fn maybe_solve(input: &str) -> Option<i32> {
    let mut floor = 0;

    for (i, c) in input.chars().enumerate() {
        if c == '(' {
            floor += 1;
        } else if c == ')' {
            floor -= 1;
        }
        if floor < 0 {
            return Some(i as i32 + 1);
        }
    }

    None
}

pub fn solve(input: &str) -> i32 {
    maybe_solve(input).unwrap()
}

#[cfg(test)]
mod tests {
    use super::solve;

    fn get_test_input() -> &'static str {
        include_str!("../inputs/test_input.txt")
    }

    #[test]
    fn test_solve() {
        assert_eq!(solve(get_test_input()), 3);
    }

    #[test]
    fn test_from_str() {
        #[rustfmt::skip]
        let to_test = [
            (1, ")"),
            (1, "))"),
            (3, "())"),
            (5, "()())"),
        ];

        for (expected, input) in to_test.iter() {
            assert_eq!(solve(&input), *expected);
        }
    }

    #[test]
    fn test_no_solution() {
        assert!(&super::maybe_solve("").is_none());
    }
}
