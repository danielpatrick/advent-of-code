use crate::present::Present;

pub fn solve(input: &str) -> i32 {
    input
        .lines()
        .map(Present::new)
        .map(|present| present.wrapping_paper_required())
        .sum()
}

#[cfg(test)]
mod tests {
    use super::solve;

    fn get_test_input() -> &'static str {
        include_str!("../inputs/test_input.txt")
    }

    #[test]
    fn test_solve() {
        assert_eq!(solve(get_test_input()), 101);
    }
}
