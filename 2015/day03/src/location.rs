#[derive(Debug, PartialEq, Eq, Hash)]
pub struct Location {
    x: i32,
    y: i32,
}

impl Location {
    pub const fn new(x: i32, y: i32) -> Self {
        Self { x, y }
    }

    pub fn moved(&self, arrow: char) -> Self {
        let mut x = self.x;
        let mut y = self.y;

        match arrow {
            '>' => x += 1,
            'v' => y += 1,
            '<' => x -= 1,
            '^' => y -= 1,
            _ => panic!("You fool"),
        }

        Self::new(x, y)
    }

    pub const fn values(&self) -> (i32, i32) {
        (self.x, self.y)
    }
}

#[cfg(test)]
mod tests {
    use super::Location;

    #[test]
    fn test_move_loc() {
        let start_loc = Location::new(1, 1);
        #[rustfmt::skip]
        let test_input = [
            ('>', 2, 1),
            ('v', 1, 2),
            ('<', 0, 1),
            ('^', 1, 0)
        ];

        for (d, x, y) in test_input.iter() {
            let new_loc = start_loc.moved(*d);
            assert_eq!(*x, new_loc.x);
            assert_eq!(*y, new_loc.y);
        }
    }
}
