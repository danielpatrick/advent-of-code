use crate::location::Location;
use std::collections::HashSet;

pub fn solve(input: &str) -> i32 {
    let mut loc = Location::new(0, 0);
    let mut houses = HashSet::new();
    houses.insert(loc.values());

    for d in input.chars() {
        loc = loc.moved(d);
        houses.insert(loc.values());
    }

    houses.len() as i32
}

#[cfg(test)]
mod tests {
    use super::solve;

    #[test]
    fn test_solve() {
        #[rustfmt::skip]
        let test_input = [
            (">", 2),
            ("^>v<", 4),
            ("^v^v^v^v^v", 2),
        ];

        for (dirs, n) in test_input.iter() {
            assert_eq!(solve(dirs), *n);
        }
    }
}
