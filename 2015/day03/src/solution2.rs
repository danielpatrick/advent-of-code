use crate::location::Location;
use std::collections::HashSet;

pub fn solve(input: &str) -> i32 {
    let mut loc1 = Location::new(0, 0);
    let mut loc2 = Location::new(0, 0);
    let mut houses = HashSet::new();
    houses.insert(loc1.values());

    for (i, d) in input.chars().enumerate() {
        if i % 2 == 0 {
            loc1 = loc1.moved(d);
            houses.insert(loc1.values());
        } else {
            loc2 = loc2.moved(d);
            houses.insert(loc2.values());
        }
    }

    houses.len() as i32
}

#[cfg(test)]
mod tests {
    use super::solve;

    #[test]
    fn test_solve() {
        #[rustfmt::skip]
        let test_input = [
            (">", 2),
            ("^v", 3),
            ("^>v<", 3),
            ("^v^v^v^v^v", 11),
        ];

        for (dirs, n) in test_input.iter() {
            assert_eq!(solve(dirs), *n);
        }
    }
}
