extern crate md5;

fn starts_with_n_zeroes(input: &str, n: usize) -> bool {
    input.len() >= n && input[0..n].chars().all(|c| c == '0')
}

pub fn finder(puzzle_input: &str, num_zeroes: usize) -> i32 {
    let mut n = 0;

    loop {
        n += 1;
        let to_hash = format!("{}{}", puzzle_input, n);
        let digest = md5::compute(to_hash);

        if starts_with_n_zeroes(&format!("{:x}", digest), num_zeroes) {
            return n;
        }
    }
}

#[cfg(test)]
mod tests {
    use super::{finder, starts_with_n_zeroes};

    #[test]
    fn test_starts_with_n_zeroes() {
        #[rustfmt::skip]
        let should_be_false = [
            ("", 5),
            ("0", 5),
            ("0000", 5),
            ("0000-", 5),
            ("100000", 5),
            ("00000", 6),
        ];
        for (input, n) in should_be_false.iter() {
            assert!(!starts_with_n_zeroes(&input, *n));
        }

        #[rustfmt::skip]
        let should_be_true = [
            ("00000", 5),
            ("000001", 5),
            ("000000", 6),
            ("0000001", 6),
        ];
        for (input, n) in should_be_true.iter() {
            assert!(starts_with_n_zeroes(&input, *n));
        }
    }

    #[test]
    fn test_finder() {
        #[rustfmt::skip]
        let to_test = [
            ("abcdef", 609043),
            ("pqrstuv", 1048970),
        ];
        for (input, expected) in to_test.iter() {
            assert_eq!(*expected, finder(&input, 5));
        }
    }
}
