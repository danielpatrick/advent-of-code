use crate::finder::finder;

pub fn solve(input: &str) -> i32 {
    finder(input, 5)
}

#[cfg(test)]
mod tests {
    use super::solve;

    #[test]
    fn test_solve() {
        #[rustfmt::skip]
        let to_test = [
            ("abcdef", 609043),
            ("pqrstuv", 1048970),
        ];
        for (input, expected) in to_test.iter() {
            assert_eq!(solve(input), *expected);
        }
    }
}
