use crate::finder::finder;

pub fn solve(input: &str) -> i32 {
    finder(input, 6)
}
