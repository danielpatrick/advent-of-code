extern crate itertools;
use itertools::Itertools;

fn has_three_vowels(s: &str) -> bool {
    s.chars().filter(|c| "aeiou".contains(*c)).count() >= 3
}

fn has_repeated_char(s: &str) -> bool {
    s.chars().tuple_windows::<(_, _)>().any(|(a, b)| a == b)
}

fn has_illegal_strings(s: &str) -> bool {
    s.contains("ab") | s.contains("cd") | s.contains("pq") | s.contains("xy")
}

fn is_nice(s: &str) -> bool {
    has_three_vowels(s) & has_repeated_char(s) & !has_illegal_strings(s)
}

pub fn solve(input: &str) -> usize {
    input.lines().filter(|s| is_nice(s)).count()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_has_three_vowels() {
        let test_input = [
            ("aeiou", true),
            ("aei", true),
            ("xazegov", true),
            ("aeiouaeiouaeiou", true),
            ("xx", false),
            ("iijjkk", false),
            ("arrgh", false),
            ("abcdde", false),
            ("aabbccdd", false),
            ("ooppqq", false),
            ("wwxxyyzz", false),
        ];

        for (s, expected) in test_input.iter() {
            assert_eq!(has_three_vowels(s), *expected);
        }
    }

    #[test]
    fn test_has_repeated_char() {
        let test_input = [
            ("aeiou", false),
            ("aei", false),
            ("xazegov", false),
            ("aeiouaeiouaeiou", false),
            ("xx", true),
            ("iijjkk", true),
            ("arrgh", true),
            ("abcdde", true),
            ("aabbccdd", true),
            ("ooppqq", true),
            ("wwxxyyzz", true),
        ];

        for (s, expected) in test_input.iter() {
            assert_eq!(has_repeated_char(s), *expected);
        }
    }

    #[test]
    fn test_has_illegal_strings() {
        let test_input = [
            ("aeiou", false),
            ("aei", false),
            ("xazegov", false),
            ("aeiouaeiouaeiou", false),
            ("xx", false),
            ("iijjkk", false),
            ("arrgh", false),
            ("abcdde", true),
            ("aabbccdd", true),
            ("ooppqq", true),
            ("wwxxyyzz", true),
        ];

        for (s, expected) in test_input.iter() {
            assert_eq!(has_illegal_strings(s), *expected);
        }
    }

    #[test]
    fn test_is_nice() {
        #[rustfmt::skip]
        let test_input = [
            ("ugknbfddgicrmopn", true),
            ("aaa", true),
            ("jchzalrnumimnmhp", false),
            ("haegwjzuvuyypxyu", false),
            ("dvszwmarrgswjxmb", false),
        ];

        for (s, expected) in test_input.iter() {
            assert_eq!(is_nice(s), *expected);
        }
    }

    #[test]
    fn test_solve() {
        #[rustfmt::skip]
        let test_input = "ugknbfddgicrmopn\n\
                          aaa\n\
                          jchzalrnumimnmhp\n\
                          haegwjzuvuyypxyu\n\
                          dvszwmarrgswjxmb";

        assert_eq!(solve(test_input), 2 as usize);
    }
}
