use std::collections::HashMap;

extern crate itertools;
use itertools::Itertools;

fn has_two_pairs(s: &str) -> bool {
    let mut pairs: HashMap<(char, char), Vec<usize>> = HashMap::new();

    for ((i, c1), (_, c2)) in s.chars().enumerate().tuple_windows::<(_, _)>() {
        let positions = pairs.entry((c1, c2)).or_insert_with(Vec::new);
        positions.push(i);
    }

    pairs.values().any(|val| {
        val.iter()
            .tuple_combinations()
            .any(|(a, b)| (*a as i32 - *b as i32).abs() > 1)
    })
}

fn has_split_repeat(s: &str) -> bool {
    s.chars()
        .tuple_windows::<(_, _, _)>()
        .any(|(a, _b, c)| a == c)
}

fn is_nice(s: &str) -> bool {
    has_two_pairs(s) & has_split_repeat(s)
}

pub fn solve(input: &str) -> usize {
    input.lines().filter(|s| is_nice(s)).count()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_has_two_pairs() {
        #[rustfmt::skip]
        let test_input = [
            ("xyxy", true),
            ("aabcdefgaa", true),
            ("baaa", false),
        ];

        for (s, expected) in test_input.iter() {
            assert_eq!(has_two_pairs(s), *expected);
        }
    }

    #[test]
    fn test_has_split_repeat() {
        #[rustfmt::skip]
        let test_input = [
            ("xyx", true),
            ("abcdefeghi", true),
            ("aaa", true),
            ("baab", false),
        ];

        for (s, expected) in test_input.iter() {
            assert_eq!(has_split_repeat(s), *expected);
        }
    }

    #[test]
    fn test_is_nice() {
        #[rustfmt::skip]
        let test_input = [
            ("qjhvhtzxzqqjkmpb", true),
            ("xxyxx", true),
            ("uurcxstgmygtbstg", false),
            ("ieodomkazucvgmuy", false),
        ];

        for (s, expected) in test_input.iter() {
            assert_eq!(is_nice(s), *expected);
        }
    }

    #[test]
    fn test_solve() {
        #[rustfmt::skip]
        let test_input = "qjhvhtzxzqqjkmpb\n\
                          xxyxx\n\
                          uurcxstgmygtbstg\n\
                          ieodomkazucvgmuy";

        assert_eq!(solve(test_input), 2 as usize);
    }
}
