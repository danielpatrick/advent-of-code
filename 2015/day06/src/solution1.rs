extern crate itertools;
extern crate regex;

use crate::instruction::parse_instructions;

use itertools::Itertools;

pub fn solve(input: &str) -> i32 {
    let instructions = parse_instructions(input);

    (0..1000)
        .cartesian_product(0..1000)
        .filter(|(x, y)| {
            instructions
                .iter()
                .fold(false, |acc, instr| instr.act(*x, *y, acc))
        })
        .count() as i32
}

#[cfg(test)]
mod tests {
    use super::*;

    fn get_test_input() -> &'static str {
        include_str!("../inputs/test_input.txt")
    }

    #[test]
    fn test_solve() {
        #[rustfmt::skip]
        assert_eq!(solve(get_test_input()), 10_799);
    }
}
