use crate::instructions::{parse_instructions, process_instructions};

pub fn solve(input: &str) -> u16 {
    let instructions = parse_instructions(input);
    let registers = process_instructions(&instructions);

    *registers.get("a").unwrap()
}

#[cfg(test)]
mod tests {
    use super::*;

    fn get_test_input() -> &'static str {
        include_str!("../inputs/test_input1.txt")
    }

    #[test]
    fn test_solve() {
        #[rustfmt::skip]
        assert_eq!(solve(get_test_input()), 123u16);
    }
}
