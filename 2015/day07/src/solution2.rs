use crate::instructions::{parse_instructions, process_instructions, Instruction};

pub fn solve(input: &str) -> u16 {
    let mut instructions = parse_instructions(input);
    let registers = process_instructions(&instructions);

    let a = registers["a"].to_string();
    instructions.insert("b", Instruction::Noop(&a));
    let new_registers = process_instructions(&instructions);

    *new_registers.get("a").unwrap()
}

#[cfg(test)]
mod tests {
    use super::*;

    fn get_test_input() -> &'static str {
        include_str!("../inputs/test_input2.txt")
    }

    #[test]
    fn test_solve() {
        assert_eq!(solve(get_test_input()), 456);
    }
}
