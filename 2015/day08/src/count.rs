pub fn count_chars(test_input: &str) -> i32 {
    test_input
        .split('\n')
        .map(|s| s.chars().count() as i32)
        .sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    fn get_test_input() -> &'static str {
        include_str!("../inputs/test_input.txt")
    }

    #[test]
    fn test_count_chars() {
        let num_chars = count_chars(get_test_input());

        assert_eq!(num_chars, 36);
    }

    #[test]
    fn test_count_chars_with_example_input() {
        let test_input = "\"\"\n\
                         \"abc\"\n\
                         \"aaa\\\"aaa\"\n\
                         \"\\x27\"\n";
        let num_chars = count_chars(test_input);

        assert_eq!(num_chars, 23);
    }
}
