use crate::count::count_chars;

fn unescape(input: &str) -> String {
    let escape_chars = ["\\", "\""];

    input
        .lines()
        .map(|s| {
            if s.len() < 2 {
                "".to_string()
            } else {
                let end = s.len() - 1;
                let new_s = &s[1..end].replace("\\\\", "..");
                new_s
                    .split('\\')
                    .enumerate()
                    .map(|(i, word)| {
                        if i == 0 || escape_chars.contains(&word) {
                            word.to_string()
                        } else if word.starts_with('x') {
                            let ord = u8::from_str_radix(&word[1..3], 16).unwrap();
                            let c = ord as char;
                            let mut v = word.chars().collect::<Vec<char>>();
                            v.splice(0..3, c.to_string().chars());
                            v.iter().collect::<String>()
                        } else {
                            word.to_string()
                        }
                    })
                    .collect::<Vec<String>>()
                    .join("")
                    .replace("..", "\\")
            }
        })
        .collect::<Vec<String>>()
        .join("\n")
}

pub fn solve(input: &str) -> i32 {
    count_chars(input) - count_chars(&unescape(input))
}

#[cfg(test)]
mod tests {
    use super::*;

    fn get_test_input() -> &'static str {
        include_str!("../inputs/test_input.txt")
    }

    #[test]
    fn test_unescape() {
        let unescaped = unescape(get_test_input());

        assert_eq!(unescaped, "\na\nabc\naaa\"aaa\nbbb\\bbb\n'");
        assert_eq!(count_chars(&unescaped), 19);
    }

    #[test]
    fn test_solve() {
        assert_eq!(solve(get_test_input()), 17);
    }

    #[test]
    fn test_solve_with_example_input() {
        let test_input = "\"\"\n\
                         \"abc\"\n\
                         \"aaa\\\"aaa\"\n\
                         \"\\x27\"\n";

        assert_eq!(solve(test_input), 12);
    }
}
