use crate::count::count_chars;
use regex::Regex;

fn escape(input: &str) -> String {
    let re = Regex::new(r"\\(?P<hex>x[0-9a-f]{2})").unwrap();

    input
        .lines()
        .map(|s| {
            if s.is_empty() {
                s.to_string()
            } else {
                let mut s1 = s
                    .replace("\\\\", "--------")
                    .replace("\\\"", "----....")
                    .replace("\\\\", "--------")
                    .replace("\\\\", "--------");

                s1 = re.replace_all(&s1, "----$hex").to_string();

                let mut s2 = String::from("\"....");
                s2.push_str(&s1[1..s1.len() - 1]);
                s2.push_str("....\"");

                s2 = s2.replace("----", "\\\\").replace("....", "\\\"");
                s2.to_string()
            }
        })
        .collect::<Vec<String>>()
        .join("\n")
}

pub fn solve(input: &str) -> i32 {
    count_chars(&escape(input)) - count_chars(input)
}

#[cfg(test)]
mod tests {
    use super::*;

    fn get_test_input() -> &'static str {
        include_str!("../inputs/test_input.txt")
    }

    #[test]
    fn test_part2() {
        assert_eq!(solve(get_test_input()), 29);
    }

    #[test]
    fn test_part2_with_example_input() {
        let test_input = "\"\"\n\
                         \"abc\"\n\
                         \"aaa\\\"aaa\"\n\
                         \"\\x27\"\n";

        assert_eq!(solve(test_input), 19);
    }
}
