use bimap::BiHashMap;
use itertools::Itertools;
use std::collections::{HashMap, HashSet};
use std::fmt;

type Graph = HashMap<usize, Vec<(usize, u32)>>;

pub struct PathFinder {
    graph: Graph,
    cities: BiHashMap<usize, String>,
}

impl PathFinder {
    pub fn best_distance<F>(&self, f: F) -> u32
    where
        F: Fn(u32, u32) -> u32 + Copy,
    {
        let mut cities: HashSet<usize> = self.cities.left_values().copied().collect();

        let to_cities: Vec<_> = self.cities.left_values().map(|c| (*c, 0)).collect();
        self.best_remaining(&mut cities, &to_cities, 0, f).unwrap()
    }

    fn best_remaining<F>(
        &self,
        cities: &mut HashSet<usize>,
        to_cities: &[(usize, u32)],
        distance: u32,
        f: F,
    ) -> Option<u32>
    where
        F: Fn(u32, u32) -> u32 + Copy,
    {
        if cities.is_empty() {
            return Some(distance);
        }

        let mut shortest = None;

        for (to_city, d) in to_cities {
            if cities.remove(to_city) {
                if let Some(result) =
                    self.best_remaining(cities, &self.graph[to_city], distance + d, f)
                {
                    shortest = shortest.map_or(Some(result), |n| Some(f(result, n)));
                }
                cities.insert(*to_city);
            }
        }

        shortest
    }

    fn get_or_add_city(cities_map: &mut BiHashMap<String, usize>, s: &str) -> usize {
        if let Some(i) = cities_map.get_by_left(s) {
            *i
        } else {
            let i = cities_map.len();
            cities_map.insert(s.into(), i);
            i
        }
    }
}

impl From<&str> for PathFinder {
    fn from(s: &str) -> Self {
        let mut cities_map = BiHashMap::<String, usize>::new();
        let graph = s.lines().fold(Graph::new(), |mut acc, line| {
            let mut iter = line.split_whitespace();
            let from_city = iter.next().unwrap();
            let to_city = iter.nth(1).unwrap();
            let distance: u32 = iter.nth(1).unwrap().parse().unwrap();

            let from_i = Self::get_or_add_city(&mut cities_map, from_city);
            let to_i = Self::get_or_add_city(&mut cities_map, to_city);

            acc.entry(from_i).or_default().push((to_i, distance));
            acc.entry(to_i).or_default().push((from_i, distance));

            acc
        });

        let cities = cities_map
            .iter()
            .map(|(s, i)| (*i, s.into()))
            .sorted()
            .collect();

        Self { graph, cities }
    }
}

impl fmt::Display for PathFinder {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let distances = self
            .graph
            .iter()
            .flat_map(|(from_i, to_cities)| {
                to_cities.iter().map(|(to_i, distance)| {
                    format!(
                        "{} to {} = {}\n",
                        self.cities.get_by_left(from_i).unwrap(),
                        self.cities.get_by_left(to_i).unwrap(),
                        distance
                    )
                })
            })
            .collect::<String>();

        write!(f, "{}", distances)
    }
}
