use crate::pathfinder::PathFinder;

pub fn solve(input: &str) -> u32 {
    let pf = PathFinder::from(input);

    pf.best_distance(u32::max)
}

#[cfg(test)]
mod tests {
    use super::*;

    fn get_test_input() -> &'static str {
        include_str!("../inputs/test_input.txt")
    }

    #[test]
    fn test_solve() {
        assert_eq!(solve(get_test_input()), 982);
    }
}
