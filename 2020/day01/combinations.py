#!/usr/bin/env python
# -*- coding: utf-8 -*-

import itertools
from math import prod


def get_product_for_matching_combination(inputs, n):
    entries = (int(entry) for entry in inputs)
    combinations = itertools.combinations(entries, n)

    for combination in combinations:
        if sum(combination) == 2020:
            return prod(combination)

    raise Exception("No combination summed to 2020")
