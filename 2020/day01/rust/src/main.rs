use itertools::Itertools;
use std::fs;

fn parse_file(filename: &str) -> Vec<u32> {
    fs::read_to_string(filename)
        .expect("problem reading file")
        .split('\n')
        .filter_map(|line| match line.len() {
            0 => None,
            _ => line.parse().ok(),
        })
        .collect()
}

fn get_product_for_matching_combination(inputs: Vec<u32>, n: usize) -> u32 {
    inputs
        .iter()
        .combinations(n)
        .find_map(|numbers| match numbers.iter().copied().sum() {
            2020 => Some(numbers.iter().copied().product()),
            _ => None,
        })
        .unwrap()
}

fn solve_part1(filename: &str) -> u32 {
    let numbers = parse_file(&filename);
    get_product_for_matching_combination(numbers, 2)
}

fn solve_part2(filename: &str) -> u32 {
    let numbers = parse_file(&filename);
    get_product_for_matching_combination(numbers, 3)
}

fn main() {
    println!("{}", solve_part1("../input.txt"));
    println!("{}", solve_part2("../input.txt"));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_solve_part1() {
        assert_eq!(solve_part1("../test_input.txt"), 514579);
    }

    #[test]
    fn test_solve_part2() {
        assert_eq!(solve_part2("../test_input.txt"), 241861950);
    }
}
