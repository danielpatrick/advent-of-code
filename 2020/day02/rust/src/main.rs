use regex::Regex;
use std::fs;

#[derive(Debug)]
struct PolicyAndPassword {
    num1: usize,
    num2: usize,
    letter: char,
    password: String,
}

impl PolicyAndPassword {
    pub fn is_valid1(&self) -> bool {
        let count = self.password.chars().filter(|c| *c == self.letter).count();
        self.num1 <= count && count <= self.num2
    }

    pub fn is_valid2(&self) -> bool {
        let char1 = self
            .password
            .chars()
            .nth(self.num1 - 1)
            .filter(|c| *c == self.letter);
        let char2 = self
            .password
            .chars()
            .nth(self.num2 - 1)
            .filter(|c| *c == self.letter);

        match (char1, char2) {
            (Some(_), None) => true,
            (None, Some(_)) => true,
            _ => false,
        }
    }
}

fn parse_file(filename: &str) -> Vec<PolicyAndPassword> {
    let re = Regex::new(r"(?m)^(\d+)-(\d+) (\w): (\w+)$").unwrap();
    let text = fs::read_to_string(filename).unwrap();

    re.captures_iter(&text)
        .map(|capture| PolicyAndPassword {
            num1: capture.get(1).unwrap().as_str().parse().unwrap(),
            num2: capture.get(2).unwrap().as_str().parse().unwrap(),
            letter: capture.get(3).unwrap().as_str().chars().next().unwrap(),
            password: capture.get(4).unwrap().as_str().to_owned(),
        })
        .collect()
}

fn solve_part1(filename: &str) -> usize {
    let passwords = parse_file(&filename);
    passwords.iter().filter(|p| p.is_valid1()).count()
}

fn solve_part2(filename: &str) -> usize {
    let passwords = parse_file(&filename);
    passwords.iter().filter(|p| p.is_valid2()).count()
}

fn main() {
    println!("{}", solve_part1("../input.txt"));
    println!("{}", solve_part2("../input.txt"));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_solve_part1() {
        assert_eq!(solve_part1("../test_input.txt"), 2);
    }

    #[test]
    fn test_solve_part2() {
        assert_eq!(solve_part2("../test_input.txt"), 1);
    }
}
