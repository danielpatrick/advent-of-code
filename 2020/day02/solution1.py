#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pathlib import Path

def parse(inputs):
    for line in inputs:
        range_, letter, password = line.split()
        min_, max_ = range_.split("-")
        letter = letter.strip(":")

        yield int(min_), int(max_), letter, password


def is_valid(min_, max_, letter, password):
    return min_ <= password.count(letter) <= max_


def solve(input_text):
    return sum(
        1 for min_, max_, letter, password in parse(input_text)
        if is_valid(min_, max_, letter, password)
    )


if __name__ == '__main__':
    from shared.utils import get_input
    from timeit import default_timer as timer

    start = timer()

    input_path = Path(__file__).parent.joinpath("input.txt")
    input_text = get_input(input_path)
    solution = solve(input_text)
    print(solution)

    end = timer()
    print()
    print("-" * 80)
    print("Time elapsed: {:.3f}s".format(end - start))
