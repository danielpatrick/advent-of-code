#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pathlib import Path


def parse(inputs):
    for line in inputs:
        positions, letter, password = line.split()
        pos1, pos2 = positions.split("-")
        letter = letter.strip(":")

        yield int(pos1) - 1, int(pos2) - 1, letter, password


def index_match(string, index, letter):
    return len(string) > index and string[index] == letter


def is_valid(pos1, pos2, letter, password):
    first = index_match(password, pos1, letter)
    second = index_match(password, pos2, letter)
    return (first and not second) or (second and not first)


def solve(input_text):
    return sum(
        1 for pos1, pos2, letter, password in parse(input_text)
        if is_valid(pos1, pos2, letter, password)
    )


if __name__ == '__main__':
    from shared.utils import get_input
    from timeit import default_timer as timer

    start = timer()

    input_path = Path(__file__).parent.joinpath("input.txt")
    input_text = get_input(input_path)
    solution = solve(input_text)
    print(solution)

    end = timer()
    print()
    print("-" * 80)
    print("Time elapsed: {:.3f}s".format(end - start))
