use std::fs;

#[derive(Debug)]
struct Slope {
    width: usize,
    height: usize,
    cells: Vec<bool>,
}

impl Slope {
    pub fn from_file(filename: &str) -> Slope {
        let rows: Vec<String> = fs::read_to_string(filename)
            .expect("problem reading file")
            .split("\n")
            .filter_map(|line| match line.len() {
                0 => None,
                _ => Some(line.to_owned()),
            })
            .collect();

        Slope {
            width: rows[0].len(),
            height: rows.len(),
            cells: rows
                .iter()
                .flat_map(|s| s.chars())
                .map(|c| c == '#')
                .collect(),
        }
    }

    pub fn is_tree(&self, row: usize, col: usize) -> bool {
        self.cells[row * self.width + col % self.width]
    }

    pub fn traverse(&self, dy: usize, dx: usize) -> usize {
        let mut row = 0;
        let mut col = 0;
        let mut tree_count = 0;

        while row < self.height {
            if self.is_tree(row, col) {
                tree_count += 1
            }
            row += dy;
            col += dx;
        }

        tree_count
    }
}

fn solve_part1(filename: &str) -> usize {
    let slope = Slope::from_file(&filename);

    slope.traverse(1, 3)
}

fn solve_part2(filename: &str) -> usize {
    let movements = vec![(1, 1), (1, 3), (1, 5), (1, 7), (2, 1)];
    let slope = Slope::from_file(&filename);

    movements.iter().map(|(dy, dx)| slope.traverse(*dy, *dx)).product()
}

fn main() {
    println!("{}", solve_part1("../input.txt"));
    println!("{}", solve_part2("../input.txt"));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_solve_part1() {
        assert_eq!(solve_part1("../test_input.txt"), 7);
    }

    #[test]
    fn test_solve_part2() {
        assert_eq!(solve_part2("../test_input.txt"), 336);
    }
}
