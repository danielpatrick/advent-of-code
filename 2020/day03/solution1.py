#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pathlib import Path


class Slope:
    def __init__(self, rows):
        self._width = len(rows[0])
        self._cells = [list(row) for row in rows]

    def get_cell(self, x, y):
        return self._cells[y][x % self._width]

    def is_tree(self, x, y):
        return self.get_cell(x, y) == "#"


def solve(input_text):
    num_rows = len(input_text)
    slope = Slope(input_text)
    row = 0
    col = 0
    tree_count = 0

    while row < num_rows:
        if slope.is_tree(col, row):
            tree_count += 1
        row += 1
        col += 3

    return tree_count


if __name__ == '__main__':
    from shared.utils import get_input
    from timeit import default_timer as timer

    start = timer()

    input_path = Path(__file__).parent.joinpath("input.txt")
    input_text = get_input(input_path)
    solution = solve(input_text)
    print(solution)

    end = timer()
    print()
    print("-" * 80)
    print("Time elapsed: {:.3f}s".format(end - start))
