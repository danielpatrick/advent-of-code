use std::collections::{HashMap, HashSet};
use std::fs;

struct Passport {
    byr: u32,
    iyr: u32,
    eyr: u32,
    hgt: String,
    hcl: String,
    ecl: String,
    pid: String,
}

impl Passport {
    pub fn from_string(s: &str) -> Option<Passport> {
        let kwargs: HashMap<&str, &str> = s
            .split_whitespace()
            .map(|s| {
                let mut split = s.split(":");
                (split.next().unwrap(), split.next().unwrap())
            })
            .collect();

        Passport::from_hashmap(kwargs)
    }

    fn from_hashmap(h: HashMap<&str, &str>) -> Option<Passport> {
        let keys = h.keys().map(|s| s.to_string()).collect::<HashSet<String>>();
        let required = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"]
            .iter()
            .map(|s| s.to_string())
            .collect::<HashSet<String>>();

        if (&required - &keys).len() == 0 {
            Some(Passport {
                byr: h.get("byr").unwrap().parse().unwrap(),
                iyr: h.get("iyr").unwrap().parse().unwrap(),
                eyr: h.get("eyr").unwrap().parse().unwrap(),
                hgt: h.get("hgt").unwrap().to_string(),
                hcl: h.get("hcl").unwrap().to_string(),
                ecl: h.get("ecl").unwrap().to_string(),
                pid: h.get("pid").unwrap().to_string(),
            })
        } else {
            None
        }
    }

    fn valid_years(&self) -> bool {
        (1920..=2002).contains(&self.byr)
            && (2010..=2020).contains(&self.iyr)
            && (2020..=2030).contains(&self.eyr)
    }

    fn valid_height(&self) -> bool {
        if self.hgt.ends_with("cm") {
            let height = self.hgt.trim_end_matches("cm").parse::<u32>().unwrap_or(0);
            (150..=193).contains(&height)
        } else if self.hgt.ends_with("in") {
            let height = self.hgt.trim_end_matches("in").parse::<u32>().unwrap_or(0);
            (59..=76).contains(&height)
        } else {
            false
        }
    }

    fn valid_hair_colour(&self) -> bool {
        self.hcl.starts_with("#")
            && u32::from_str_radix(self.hcl.trim_start_matches("#"), 16).is_ok()
    }

    fn valid_eye_colour(&self) -> bool {
        ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"].contains(&self.ecl.as_str())
    }

    fn valid_passport_id(&self) -> bool {
        self.pid.len() == 9 && self.pid.parse::<u32>().is_ok()
    }

    pub fn validate(&self) -> bool {
        self.valid_years()
            && self.valid_height()
            && self.valid_hair_colour()
            && self.valid_eye_colour()
            && self.valid_passport_id()
    }
}

fn parse_passports(filename: &str) -> Vec<Passport> {
    fs::read_to_string(filename)
        .expect("problem reading file")
        .split("\n\n")
        .filter_map(|s| Passport::from_string(s))
        .collect()
}

fn solve_part1(filename: &str) -> usize {
    let passports = parse_passports(&filename);
    passports.len()
}

fn solve_part2(filename: &str) -> usize {
    let passports = parse_passports(&filename);
    passports.iter().filter(|p| p.validate()).count()
}

fn main() {
    println!("{}", solve_part1("../input.txt"));
    println!("{}", solve_part2("../input.txt"));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_solve_part1() {
        assert_eq!(solve_part1("../test_input.txt"), 2);
    }

    #[test]
    fn test_solve_part2() {
        assert_eq!(solve_part2("../test_input.txt"), 2);
    }
}
