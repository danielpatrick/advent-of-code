#!/usr/bin/env python
# -*- coding: utf-8 -*-


from dataclasses import dataclass
from pathlib import Path
from typing import Optional


@dataclass
class Passport:
    byr: str
    iyr: str
    eyr: str
    hgt: str
    hcl: str
    ecl: str
    pid: str
    cid: Optional[int] = None


def valid_passport(passport_text):
    passport_attrs = dict(kv.split(":") for kv in passport_text.split())
    try:
        Passport(**passport_attrs)
    except:
        return False

    return True


def solve(input_text):
    passport_texts = "\n".join(input_text).split("\n\n")
    return len(list(filter(valid_passport, passport_texts)))


if __name__ == '__main__':
    from shared.utils import get_input
    from timeit import default_timer as timer

    start = timer()

    input_path = Path(__file__).parent.joinpath("input.txt")
    input_text = get_input(input_path)
    solution = solve(input_text)
    print(solution)

    end = timer()
    print()
    print("-" * 80)
    print("Time elapsed: {:.3f}s".format(end - start))
