#!/usr/bin/env python
# -*- coding: utf-8 -*-

from dataclasses import dataclass
from pathlib import Path
from typing import Optional


@dataclass
class Passport:
    byr: str
    iyr: str
    eyr: str
    hgt: str
    hcl: str
    ecl: str
    pid: str
    cid: Optional[int] = None


def is_valid_height(height):
    if height.endswith("cm"):
        return 150 <= int(height[:-2]) <= 193

    if height.endswith("in"):
        return 59 <= int(height[:-2]) <= 76

    return False


def is_valid_hair_colour(colour):
    if colour.startswith("#"):
        try:
            int(colour[1:], 16)
        except:
            return False
        return True

    return False


def validate_passport(passport):
    assert len(passport.byr) == 4
    assert 1920 <= int(passport.byr) <= 2002

    assert 2010 <= int(passport.iyr) <= 2020

    assert 2020 <= int(passport.eyr) <= 2030

    assert is_valid_height(passport.hgt)

    assert is_valid_hair_colour(passport.hcl)

    assert passport.ecl in ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"]

    assert len(passport.pid) == 9
    assert passport.pid.isnumeric()


def valid_passport(passport_text):
    passport_attrs = dict(kv.split(":") for kv in passport_text.split())

    try:
        validate_passport(Passport(**passport_attrs))
    except:
        return False

    return True


def solve(input_text):
    passport_texts = "\n".join(input_text).split("\n\n")
    return len(list(filter(valid_passport, passport_texts)))



if __name__ == '__main__':
    from shared.utils import get_input
    from timeit import default_timer as timer

    start = timer()

    input_path = Path(__file__).parent.joinpath("input.txt")
    input_text = get_input(input_path)
    solution = solve(input_text)
    print(solution)

    end = timer()
    print()
    print("-" * 80)
    print("Time elapsed: {:.3f}s".format(end - start))
