use std::fs;

fn parse_input(filename: &str) -> Vec<usize> {
    return fs::read_to_string(filename)
        .expect("problem reading file")
        .split("\n")
        .filter(|line| line.len() != 0)
        .map(|line| {
            line.chars()
                .filter_map(|c| match c {
                    'F' | 'L' => Some('0'),
                    'B' | 'R' => Some('1'),
                    _ => None, // definitely not the correct thing to do
                })
                .collect::<String>()
        })
        .map(|s| usize::from_str_radix(&s, 2).unwrap())
        .collect();
}

fn solve_part1(filename: &str) -> usize {
    *parse_input(&filename).iter().max().unwrap()
}

fn solve_part2(filename: &str) -> usize {
    let mut pass_ids = parse_input(&filename);
    pass_ids.sort();

    let first = *pass_ids.first().unwrap();
    let last = *pass_ids.last().unwrap();

    (first..=last)
        .find_map(|i| if pass_ids.contains(&i) { None } else { Some(i) })
        .unwrap()
}

fn main() {
    println!("{}", solve_part1("../input.txt"));
    println!("{}", solve_part2("../input.txt"));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_solve_part1() {
        assert_eq!(solve_part1("../test_input.txt"), 820);
    }

    #[test]
    fn test_solve_part2() {
        assert_eq!(solve_part2("../test_input.txt"), 123);
    }
}
