#!/usr/bin/env python
# -*- coding: utf-8 -*-

from math import ceil, floor


def binary_partition(chars, start, end, start_char, end_char):
    if start == end:
        return start

    if not chars:
        raise Exception(
            "Ran out of characters - maybe boarding pass is too short?"
        )

    if chars[0] == start_char:
        return binary_partition(
            chars[1:], start, floor((start + end) / 2), start_char, end_char
        )
    elif chars[0] == end_char:
        return binary_partition(
            chars[1:], ceil((start + end) / 2), end, start_char, end_char
        )

    raise Exception(
        f"'{chars[0]}' is invalid: expected '{start_char}' or '{end_char}'"
    )


def get_col(boarding_pass):
    return binary_partition(boarding_pass[7:], 0, 7, "L", "R")


def get_row(boarding_pass):
    return binary_partition(boarding_pass[:7], 0, 127, "F", "B")


def get_seat_id(pass_id):
    return get_row(pass_id) * 8 + get_col(pass_id)


def only_an_idiot_would_get_seat_id_the_other_way(pass_id):
    table = pass_id.maketrans("FBLR", "0101")

    return int(pass_id.translate(table), 2)
