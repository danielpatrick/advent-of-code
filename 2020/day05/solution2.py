#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pathlib import Path

from .shared import only_an_idiot_would_get_seat_id_the_other_way as get_seat_id


def solve(input_text):
    seats = sorted(get_seat_id(line) for line in input_text)
    prev = min(seats) - 1

    for seat in seats:
        if seat > (prev + 1):
            return seat - 1

        prev = seat


if __name__ == '__main__':
    from shared.utils import get_input
    from timeit import default_timer as timer

    start = timer()

    input_path = Path(__file__).parent.joinpath("input.txt")
    input_text = get_input(input_path)
    solution = solve(input_text)
    print(solution)

    end = timer()
    print()
    print("-" * 80)
    print("Time elapsed: {:.3f}s".format(end - start))
