#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pathlib import Path
from unittest.mock import patch
import unittest

from shared.utils import get_input
from . import solution1, solution2, shared


SOLUTION_DIR = Path(__file__).parent


class TestShared(unittest.TestCase):
    module = shared

    def test_get_col(self):
        input_and_expected = (
            ("FBFBBFFRLR", 5),
            ("BFFFBBFRRR", 7),
            ("FFFBBBFRRR", 7),
            ("BBFFBBFRLL", 4),
        )

        for i, e in input_and_expected:
            self.assertEqual(self.module.get_col(i), e)

    def test_get_row(self):
        input_and_expected = (
            ("FBFBBFFRLR", 44),
            ("BFFFBBFRRR", 70),
            ("FFFBBBFRRR", 14),
            ("BBFFBBFRLL", 102),
        )

        for i, e in input_and_expected:
            self.assertEqual(self.module.get_row(i), e)

    def test_get_seat_id(self):
        input_and_expected = (
            ("FBFBBFFRLR", 357),
            ("BFFFBBFRRR", 567),
            ("FFFBBBFRRR", 119),
            ("BBFFBBFRLL", 820),
        )

        for i, e in input_and_expected:
            self.assertEqual(self.module.get_seat_id(i), e)


class TestSolution(unittest.TestCase):
    module = None
    input_filename = "test_input.txt"
    expected = None

    def setUp(self):
        if self.module is None:
            raise NotImplementedError(
                "subclasses of TestSolution must provide module to test"
            )
        if self.expected is None:
            raise NotImplementedError(
                "subclasses of TestSolution must provide expected value"
            )
        self.input_path = SOLUTION_DIR.joinpath(self.input_filename)
        self.input_text = get_input(self.input_path)


class TestSolution1(TestSolution):
    module = solution1
    expected = 820

    def test_solver(self):
        solution = self.module.solve(self.input_text)
        self.assertEqual(self.expected, solution)


class TestSolution2(TestSolution):
    module = solution2
    expected = 123

    def test_solver(self):
        with patch("day05.solution2.get_seat_id") as get_seat_id:
            get_seat_id.side_effect = (120, 121, 122, 124, 125, 126)

            solution = self.module.solve(self.input_text)

            self.assertEqual(self.expected, solution)


if __name__ == '__main__':
    unittest.main()
