use std::collections::HashSet;
use std::fs;

fn solve_part1(filename: &str) -> usize {
    fs::read_to_string(filename)
        .expect("problem reading file")
        .split("\n\n")
        .map(|lines| {
            lines
                .chars()
                .filter(|c| *c != '\n')
                .collect::<HashSet<char>>()
                .len()
        })
        .sum()
}

fn solve_part2(filename: &str) -> usize {
    fs::read_to_string(filename)
        .expect("problem reading file")
        .split("\n\n")
        .map(|lines| {
            let group = lines
                .lines()
                .map(|line| line.chars().collect::<HashSet<char>>());
            let mut iter = group.into_iter();
            let acc = iter.next().unwrap();
            iter.fold(acc, |acc, x| &acc & &x).len()
        })
        .sum()
}

fn main() {
    println!("{}", solve_part1("../input.txt"));
    println!("{}", solve_part2("../input.txt"));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_solve_part1() {
        assert_eq!(solve_part1("../test_input.txt"), 11);
    }

    #[test]
    fn test_solve_part2() {
        assert_eq!(solve_part2("../test_input.txt"), 6);
    }
}
