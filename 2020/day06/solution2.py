#!/usr/bin/env python
# -*- coding: utf-8 -*-

from functools import reduce
from operator import and_
from pathlib import Path


def all_yes(group):
    return reduce(and_, group)


def get_group_yeses(group_str):
    return all_yes(set(line) for line in group_str.split("\n"))


def get_groups(input_text):
    return (
        get_group_yeses(group) for group in "\n".join(input_text).split("\n\n")
    )


def solve(input_text):
    return sum(len(group) for group in get_groups(input_text))


if __name__ == '__main__':
    from shared.utils import get_input
    from timeit import default_timer as timer

    start = timer()

    input_path = Path(__file__).parent.joinpath("input.txt")
    input_text = get_input(input_path)
    solution = solve(input_text)
    print(solution)

    end = timer()
    print()
    print("-" * 80)
    print("Time elapsed: {:.3f}s".format(end - start))
