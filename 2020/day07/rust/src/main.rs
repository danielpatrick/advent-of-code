use std::collections::HashMap;
use std::fs;

type BagRules = HashMap<String, HashMap<String, usize>>;

fn read_rules(filename: &str) -> BagRules {
    fs::read_to_string(filename)
        .expect("problem reading file")
        .lines()
        .map(|line| {
            let mut parts = line.split(" bags contain ");

            (
                parts.next().unwrap().to_string(),
                parse_bag_contents(parts.next().unwrap()),
            )
        })
        .collect()
}

fn parse_bag_contents(s: &str) -> HashMap<String, usize> {
    if s == "no other bags." {
        HashMap::new()
    } else {
        s.trim_end_matches(".")
            .split(", ")
            .map(|bag_rule| {
                let mut num_ang_bag = bag_rule
                    .trim_end_matches(" bag")
                    .trim_end_matches(" bags")
                    .splitn(2, " ");

                let num = num_ang_bag.next().unwrap().parse::<usize>().unwrap();
                let bag = num_ang_bag.next().unwrap().to_string();

                (bag, num)
            })
            .collect()
    }
}

fn can_hold(rules: &BagRules, bag: &str, bag_type: &str, memo: &mut HashMap<String, bool>) -> bool {
    if memo.contains_key(bag) {
        *memo.get(bag).unwrap()
    } else {
        let result = rules[bag]
            .keys()
            .filter(|subbag| match subbag {
                b if *b == bag_type => true,
                _ => can_hold(&rules, subbag, bag_type, memo),
            })
            .count()
            > 0;
        memo.insert(bag.to_string(), result);
        result
    }
}

fn count_can_hold(rules: &BagRules, bag_type: &str) -> usize {
    let mut memo = HashMap::new();
    rules
        .keys()
        .filter(|bag| can_hold(rules, bag, bag_type, &mut memo))
        .count()
}

fn count_bags(rules: &BagRules, rule: &str, memo: &mut HashMap<String, usize>) -> usize {
    if memo.contains_key(rule) {
        *memo.get(rule).unwrap()
    } else {
        let bag_rules = rules.get(rule).unwrap();
        let count = match bag_rules.len() {
            0 => 1,
            _ => {
                bag_rules
                    .iter()
                    .map(|(bag, num)| count_bags(rules, bag, memo) * num)
                    .sum::<usize>()
                    + 1
            }
        };
        memo.insert(rule.to_string(), count);
        count
    }
}

fn solve_part1(rules: &BagRules) -> usize {
    count_can_hold(rules, "shiny gold")
}

fn solve_part2(rules: &BagRules) -> usize {
    let mut memo = HashMap::new();

    count_bags(&rules, "shiny gold", &mut memo) - 1
}

fn main() {
    let rules = read_rules("../input.txt");

    println!("{}", solve_part1(&rules));
    println!("{}", solve_part2(&rules));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_solve_part1() {
        let rules = read_rules("../input.txt");
        assert_eq!(solve_part1(&rules), 4);
    }

    #[test]
    fn test_solve_part2() {
        let rules = read_rules("../input.txt");
        assert_eq!(solve_part2(&rules), 32);
        assert_eq!(solve_part2(&rules), 126);
    }
}
