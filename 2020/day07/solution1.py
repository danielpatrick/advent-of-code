#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
from collections import defaultdict
from functools import reduce
from operator import or_
from pathlib import Path


def bag_and_num(bag_str):
    l, _, r = bag_str.partition(" ")
    return re.sub(" bags?.?", "", r), int(l)


def get_rules(input_text):
    rules_by_contents = defaultdict(set)

    for line in input_text:
        bag_type, contents_str = line.split(" bags contain ")

        if contents_str == "no other bags.":
            continue

        bag_types = (
            bag_and_num(bag_str)[0] for bag_str in contents_str.split(", ")
        )

        for bt in bag_types:
            rules_by_contents[bt].add(bag_type)

    return rules_by_contents


def get_matching_rules(rules, rule_name):
    if rule_name not in rules:
        return set()

    matching_rules = rules[rule_name]
    subrules = reduce(
        or_,
        (
            get_matching_rules(rules, new_rule_name)
            for new_rule_name in matching_rules
        ),
    )
    return matching_rules | subrules


def solve(input_text):
    rules = get_rules(input_text)

    return len(get_matching_rules(rules, "shiny gold"))


if __name__ == '__main__':
    from shared.utils import get_input
    from timeit import default_timer as timer

    start = timer()

    input_path = Path(__file__).parent.joinpath("input.txt")
    input_text = get_input(input_path)
    solution = solve(input_text)
    print(solution)

    end = timer()
    print()
    print("-" * 80)
    print("Time elapsed: {:.3f}s".format(end - start))
