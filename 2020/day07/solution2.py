#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
from pathlib import Path


def bag_and_num(bag_str):
    l, _, r = bag_str.partition(" ")
    return re.sub(" bags?.?", "", r), int(l)


def parse_rule(line):
    key, contents_str = line.split(" bags contain ")

    if contents_str == "no other bags.":
        return (key, {})

    return (
        key,
        dict(bag_and_num(bag_str) for bag_str in contents_str.split(", ")),
    )


def get_rules(lines):
    return dict(
        parse_rule(line) for line in lines
    )


def num_required_bags(rules, rule_name):
    bag_count = 1 + sum(
        num_required_bags(rules, rn) * n
        for rn, n in rules[rule_name].items()
    )

    return bag_count


def solve(input_text):
    rules = get_rules(input_text)

    return num_required_bags(rules, "shiny gold") - 1


if __name__ == '__main__':
    from shared.utils import get_input
    from timeit import default_timer as timer

    start = timer()

    input_path = Path(__file__).parent.joinpath("input.txt")
    input_text = get_input(input_path)
    solution = solve(input_text)
    print(solution)

    end = timer()
    print()
    print("-" * 80)
    print("Time elapsed: {:.3f}s".format(end - start))
