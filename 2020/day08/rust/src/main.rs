use std::fs;
use std::collections::HashSet;

#[derive(Debug, PartialEq, Eq, Hash)]
enum Instruction {
    Nop(i32),
    Acc(i32),
    Jmp(i32),
}

impl Instruction {
    pub fn new(s: &str) -> Option<Instruction> {
        match s.split_at(4) {
            ("nop ", i) => Some(Instruction::Nop(i.parse::<i32>().unwrap())),
            ("acc ", i) => Some(Instruction::Acc(i.parse::<i32>().unwrap())),
            ("jmp ", i) => Some(Instruction::Jmp(i.parse::<i32>().unwrap())),
            _ => None,
        }
    }
}

fn parse_file(filename: &str) -> Vec<Instruction> {
    fs::read_to_string(filename)
        .expect("problem reading file")
        .split('\n')
        .filter_map(|line| match line.len() {
            0 => None,
            _ => Instruction::new(line),
        })
        .collect()
}

fn solve_part1(filename: &str) -> Result<i32, &str> {
    let program = parse_file(filename);
    let mut visited = HashSet::new();
    let mut i = 0;
    let mut acc = 0;

    while !visited.contains(&i) {
        visited.insert(i);

        match program[i] {
            Instruction::Nop(_) => { i += 1; },
            Instruction::Acc(n) => {
                acc += n;
                i += 1;
            },
            Instruction::Jmp(n) => {
                let new_i = i as i32 + n;
                if new_i < 0 {
                    return Err("Oh noes");
                }
                i = new_i as usize;
            },
        }
    }

    Ok(acc)
}


fn execute<'a>(program: &'a Vec<Instruction>, visited: &mut HashSet<usize>, acc: i32, i: usize, modified: bool) -> Result<i32, &'a str> {
    if i >= program.len() {
        return Ok(acc);
    }

    if visited.contains(&i) {
        return Err("Sad times");
    }

    visited.insert(i);
    let mut new_i = i as i32;

    if !modified {
        match program[i] {
            Instruction::Nop(n) => {
                new_i += n;
                if new_i < 0 {
                    return Err("Oh noes");
                }
            },
            Instruction::Jmp(_) => { new_i += 1; },
            _ => (),
        }

        let result = execute(program, visited, acc, new_i as usize, true);
        if result.is_ok() {
            return result;
        }
    }

    let mut new_acc = acc;
    new_i = i as i32;

    match program[i] {
        Instruction::Nop(_) => { new_i += 1; },
        Instruction::Acc(n) => {
            new_acc += n;
            new_i += 1;
        },
        Instruction::Jmp(n) => {
            new_i += n;
            if new_i < 0 {
                return Err("Oh noes");
            }
        },
    }

    let result = execute(program, visited, new_acc, new_i as usize, modified);

    if result.is_err() {
        visited.remove(&i);
    }

    result
}


fn solve_part2(filename: &str) -> i32 {
    let program = parse_file(filename);
    let mut visited = HashSet::new();
    let i = 0;
    let acc = 0;

    execute(&program, &mut visited, acc, i, false).unwrap()
}

fn main() {
    println!("{}", solve_part1("../input.txt").unwrap());
    println!("{}", solve_part2("../input.txt"));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_file() {
        assert_eq!(
            parse_file("../test_input.txt"),
            vec![
                Instruction::Nop(0),
                Instruction::Acc(1),
                Instruction::Jmp(4),
                Instruction::Acc(3),
                Instruction::Jmp(-3),
                Instruction::Acc(-99),
                Instruction::Acc(1),
                Instruction::Jmp(-4),
                Instruction::Acc(6),
            ]
        );
    }

    #[test]
    fn test_solve_part1() {
        assert_eq!(solve_part1("../test_input.txt").unwrap(), 5);
    }

    #[test]
    fn test_solve_part2() {
        assert_eq!(solve_part2("../test_input.txt"), 8);
    }
}
