#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pathlib import Path


def perform(instruction_text, acc, i):
    instruction, num = instruction_text.split()

    if instruction == "nop":
        i += 1
    elif instruction == "acc":
        acc += int(num)
        i += 1
    elif instruction == "jmp":
        i += int(num)
    else:
        raise Exception(f"Dodgy instruction found 😬: {instruction}")

    return acc, i


def solve(input_text):
    executed = set()
    acc = 0
    i = 0

    while i not in executed:
        if i < 0 or i >= len(input_text):
            raise Exception("That wasn't supposed to happen 💩")

        executed.add(i)
        acc, i = perform(input_text[i], acc, i)

    return acc


if __name__ == '__main__':
    from shared.utils import get_input
    from timeit import default_timer as timer

    start = timer()

    input_path = Path(__file__).parent.joinpath("input.txt")
    input_text = get_input(input_path)
    solution = solve(input_text)
    print(solution)

    end = timer()
    print()
    print("-" * 80)
    print("Time elapsed: {:.3f}s".format(end - start))
