#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pathlib import Path


def perform(instruction_text, acc, i):
    instruction, num = instruction_text.split()

    if instruction == "nop":
        i += 1
    elif instruction == "acc":
        acc += int(num)
        i += 1
    elif instruction == "jmp":
        i += int(num)
    else:
        raise Exception(f"Dodgy instruction found 😬: {instruction}")

    return acc, i


def execute(program, tried=None, acc=0, i=0):
    executed = tried or set()

    while i < len(program):
        if i in executed:
            raise Exception("Another failed attempt 💩")
        if i < 0:
            raise Exception("That's unexpected also 💩")

        executed.add(i)
        acc, i = perform(program[i], acc, i)

    return acc


def memoize(program):
    memo = {}
    executed = set()
    acc = 0
    i = 0

    while i not in executed:
        if i < 0 or i >= len(program):
            raise Exception("That wasn't supposed to happen 💩")

        executed.add(i)
        acc, i = perform(program[i], acc, i)

        memo[i] = (set(executed), acc)

    return memo


def solve(input_text):
    memo = memoize(input_text)

    for i, instruction in enumerate(input_text):
        if i not in memo:
            # We can't reach this instruction without modifying the code
            # elsewhere, so we might as well skip this one
            continue

        tried, acc = memo[i]

        program = input_text[:]

        if "jmp" in instruction:
            program[i] = instruction.replace("jmp", "nop")
        elif "nop" in instruction:
            program[i] = instruction.replace("nop", "jmp")
        else:
            continue

        try:
            return execute(program, tried, acc, i)
        except:
            pass

    raise Exception("None of the programs terminated correctly 😿")


if __name__ == '__main__':
    from shared.utils import get_input
    from timeit import default_timer as timer

    start = timer()

    input_path = Path(__file__).parent.joinpath("input.txt")
    input_text = get_input(input_path)
    solution = solve(input_text)
    print(solution)

    end = timer()
    print()
    print("-" * 80)
    print("Time elapsed: {:.3f}s".format(end - start))
