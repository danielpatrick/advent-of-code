#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pathlib import Path


def execute(program, history, acc, i, modified=False):
    if i >= len(program):
        return acc

    if i in history:
        return None

    history.add(i)
    instruction, num = program[i].split()

    if not modified:
        new_i = i
        if instruction == "nop":
            new_i += int(num)
        if instruction == "jmp":
            new_i += 1

        result = execute(program, history, acc, new_i, True)

        if result is not None:
            return result

    new_i = i

    if instruction == "nop":
        new_i += 1
    elif instruction == "acc":
        acc += int(num)
        new_i += 1
    elif instruction == "jmp":
        new_i += int(num)

    result = execute(program, history, acc, new_i, modified)

    if result is None:
        history.remove(i)

    return result



def solve(input_text):
    return execute(input_text, set(), 0, 0, False)


if __name__ == '__main__':
    from shared.utils import get_input
    from timeit import default_timer as timer

    start = timer()

    input_path = Path(__file__).parent.joinpath("input.txt")
    input_text = get_input(input_path)
    solution = solve(input_text)
    print(solution)

    end = timer()
    print()
    print("-" * 80)
    print("Time elapsed: {:.3f}s".format(end - start))
