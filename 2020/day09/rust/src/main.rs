use std::fs;

fn parse_file(filename: &str) -> Vec<u64> {
    fs::read_to_string(filename)
        .expect("problem reading file")
        .split('\n')
        .filter_map(|line| match line.len() {
            0 => None,
            _ => line.parse().ok(),
        })
        .collect()
}

fn has_sum_numbers(numbers: &[u64], n: u64) -> bool {
    for i in 0..(numbers.len() - 1) {
        for j in (i + 1)..numbers.len() {
            if (numbers[i] + numbers[j]) == n {
                return true;
            }
        }
    }
    false
}

fn get_invalid_number(numbers: &Vec<u64>, preamble_length: usize) -> Option<u64> {
    for end in preamble_length..numbers.len() {
        let start = end - preamble_length;
        match has_sum_numbers(&numbers[start..end], numbers[end]) {
            true => (),
            false => return Some(numbers[end]),
        }
    }
    None
}

fn solve_part1(filename: &str, preamble_length: usize) -> Result<u64, &str> {
    let numbers = parse_file(&filename);

    get_invalid_number(&numbers, preamble_length).ok_or("No invalid num found")
}

fn solve_part2(filename: &str, preamble_length: usize) -> Result<u64, &str> {
    let numbers = parse_file(filename);
    let invalid = get_invalid_number(&numbers, preamble_length).unwrap();

    let mut i = 0;
    let mut j = 1;
    let mut rolling_sum = numbers[i] + numbers[j];
    let length = numbers.len();

    while rolling_sum != invalid {
        match rolling_sum {
            rs if rs > invalid => {
                rolling_sum -= numbers[i];
                i += 1;

                if i == j {
                    j += 1;

                    if j >= length {
                        return Err("We got to the end without finding a solution");
                    }

                    rolling_sum += numbers[j];
                }
            }
            rs if rs < invalid => {
                j += 1;
                rolling_sum += numbers[j];
            }
            _ => (),
        }
    }

    return Ok(
        numbers[i..(j + 1)].iter().min().unwrap() + numbers[i..(j + 1)].iter().max().unwrap()
    );
}

fn main() {
    println!("{}", solve_part1("../input.txt", 25).unwrap());
    println!("{}", solve_part2("../input.txt", 25).unwrap());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_file() {
        assert_eq!(
            parse_file("../test_input.txt"),
            vec![
                35, 20, 15, 25, 47, 40, 62, 55, 65, 95, 102, 117, 150, 182, 127, 219, 299, 277,
                309, 576,
            ]
        );
    }

    #[test]
    fn test_solve_part1() {
        assert_eq!(solve_part1("../test_input.txt", 5).unwrap(), 127);
    }

    #[test]
    fn test_solve_part2() {
        assert_eq!(solve_part2("../test_input.txt", 5).unwrap(), 62);
    }
}
