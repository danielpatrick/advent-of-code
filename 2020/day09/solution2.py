#!/usr/bin/env python
# -*- coding: utf-8 -*-

from itertools import combinations
from pathlib import Path


def get_sum_numbers(numbers, n):
    for a, b in combinations(numbers, 2):
        if (a + b) == n:
            return a, b

    raise Exception(f"No pair adds up to {n}")


def get_invalid_number(preamble, numbers):
    for n in numbers:
        try:
            a, b = get_sum_numbers(preamble, n)
        except:
            return n

        index_a = preamble.index(a)
        index_b = preamble.index(b)

        preamble.pop(0)
        preamble.append(n)

    raise Exception("Is there an infinite preamble or something?")


def solve(input_text, preamble_length=25):
    numbers = [int(n) for n in input_text]

    invalid = get_invalid_number(
        numbers[:preamble_length],
        numbers[preamble_length:]
    )

    length = len(numbers)

    i = 0
    j = 1
    rolling_sum = numbers[i] + numbers[j]

    while rolling_sum != invalid:
        if rolling_sum > invalid:
            rolling_sum -= numbers[i]
            i += 1

            if i == j:
                j += 1

                if j > length:
                    raise Exception("We got to the end without finding a solution")

                rolling_sum += numbers[j]
        elif rolling_sum < invalid:
            j += 1
            rolling_sum += numbers[j]

    sub = numbers[i:j+1]
    return min(sub) + max(sub)


if __name__ == '__main__':
    from shared.utils import get_input
    from timeit import default_timer as timer

    start = timer()

    input_path = Path(__file__).parent.joinpath("input.txt")
    input_text = get_input(input_path)
    solution = solve(input_text)
    print(solution)

    end = timer()
    print()
    print("-" * 80)
    print("Time elapsed: {:.3f}s".format(end - start))
