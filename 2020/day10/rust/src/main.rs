use std::cmp::max;
use std::collections::HashMap;
use std::fs;

fn parse_file(filename: &str) -> Vec<u128> {
    fs::read_to_string(filename)
        .expect("problem reading file")
        .split('\n')
        .filter_map(|line| match line.len() {
            0 => None,
            _ => line.parse().ok(),
        })
        .collect()
}

fn get_differences(adapters: &Vec<u128>) -> (u128, u128) {
    let mut ones = 0;
    let mut threes = 0;
    let mut prev = 0;

    for j in adapters {
        match j - prev {
            1 => ones += 1,
            3 => threes += 1,
            _ => (),
        }

        prev = *j;
    }

    (ones, threes)
}

fn solve_part1(filename: &str) -> u128 {
    let mut adapters = parse_file(&filename);
    adapters.sort();
    adapters.push(adapters.last().unwrap() + 3);
    adapters.insert(0, 0);

    let (ones, threes) = get_differences(&adapters);

    ones * threes
}

fn number_of_solutions(adapters: &Vec<u128>) -> u128 {
    let mut solutions_at_jolt = HashMap::new();
    solutions_at_jolt.insert(0, 1);

    for jolts in adapters[1..].iter() {
        for j in (max(*jolts, 3) - 3)..*jolts {
            let contains = solutions_at_jolt.contains_key(&j);
            if contains {
                let inc = *solutions_at_jolt.get(&j).unwrap();
                let count = solutions_at_jolt.entry(*jolts).or_insert(0);
                *count += inc;
            }
        }
    }

    *solutions_at_jolt.get(&adapters.last().unwrap()).unwrap()
}

fn solve_part2(filename: &str) -> u128 {
    let mut adapters = parse_file(&filename);
    adapters.sort();
    adapters.push(adapters.last().unwrap() + 3);
    adapters.insert(0, 0);

    number_of_solutions(&adapters)
}

fn main() {
    println!("{}", solve_part1("../input.txt"));
    println!("{}", solve_part2("../input.txt"));
}

#[cfg(test)]
mod tests {
    use super::*;

    fn adapters1() -> Vec<u128> {
        vec![0, 1, 4, 5, 6, 7, 10, 11, 12, 15, 16, 19, 22]
    }

    fn adapters2() -> Vec<u128> {
        vec![
            0, 1, 2, 3, 4, 7, 8, 9, 10, 11, 14, 17, 18, 19, 20, 23, 24, 25, 28, 31, 32, 33, 34, 35,
            38, 39, 42, 45, 46, 47, 48, 49, 52,
        ]
    }

    #[test]
    fn test_parse_file() {
        assert_eq!(
            parse_file("../test_input.txt"),
            vec![16, 10, 15, 5, 1, 11, 7, 19, 6, 12, 4]
        );
    }

    #[test]
    fn test_get_differences() {
        assert_eq!(get_differences(&adapters1()), (7, 5));
        assert_eq!(get_differences(&adapters2()), (22, 10));
    }

    #[test]
    fn test_solve_part1() {
        assert_eq!(solve_part1("../test_input.txt"), 35);
    }

    #[test]
    fn test_number_of_solutions() {
        assert_eq!(number_of_solutions(&adapters1()), 8);
        assert_eq!(number_of_solutions(&adapters2()), 19208);
    }

    #[test]
    fn test_solve_part2() {
        assert_eq!(solve_part2("../test_input.txt"), 8);
    }
}
