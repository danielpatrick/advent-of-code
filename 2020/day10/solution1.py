#!/usr/bin/env python
# -*- coding: utf-8 -*-

from collections import defaultdict
from pathlib import Path


def parse_adapters(input_text):
    return [int(n) for n in input_text]


def get_differences(adapters):
    differences = defaultdict(int)
    prev = 0

    for j in adapters:
        differences[j - prev] += 1
        prev = j

    return differences


def solve(input_text):
    adapters = sorted(parse_adapters(input_text))
    adapters = [0] + adapters + [adapters[-1] + 3]

    differences = get_differences(adapters)

    return differences[1] * differences[3]


if __name__ == '__main__':
    from shared.utils import get_input
    from timeit import default_timer as timer

    start = timer()

    input_path = Path(__file__).parent.joinpath("input.txt")
    input_text = get_input(input_path)
    solution = solve(input_text)
    print(solution)

    end = timer()
    print()
    print("-" * 80)
    print("Time elapsed: {:.3f}s".format(end - start))
