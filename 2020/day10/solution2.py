#!/usr/bin/env python
# -*- coding: utf-8 -*-

from collections import defaultdict
from pathlib import Path


def parse_adapters(input_text):
    return [int(n) for n in input_text]


def number_of_solutions(adapters):
    solutions_at_jolt = defaultdict(int)
    solutions_at_jolt[0] = 1

    for jolts in adapters[1:]:
        for j in range(jolts - 3, jolts):
            if j >= 0 and j in solutions_at_jolt:
                solutions_at_jolt[jolts] += solutions_at_jolt[j]

    return solutions_at_jolt[max(adapters)]


def solve(input_text):
    adapters = sorted(parse_adapters(input_text))
    adapters = [0] + adapters + [adapters[-1] + 3]

    return number_of_solutions(adapters)


if __name__ == '__main__':
    from shared.utils import get_input
    from timeit import default_timer as timer

    start = timer()

    input_path = Path(__file__).parent.joinpath("input.txt")
    input_text = get_input(input_path)
    solution = solve(input_text)
    print(solution)

    end = timer()
    print()
    print("-" * 80)
    print("Time elapsed: {:.3f}s".format(end - start))
