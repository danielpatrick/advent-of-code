#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pathlib import Path
import unittest

from shared.utils import get_input
from . import solution1, solution2


SOLUTION_DIR = Path(__file__).parent


class TestSolution(unittest.TestCase):
    module = None
    input_filename = "test_input.txt"
    expected = None

    def setUp(self):
        if self.module is None:
            raise NotImplementedError(
                "subclasses of TestSolution must provide module to test"
            )
        if self.expected is None:
            raise NotImplementedError(
                "subclasses of TestSolution must provide expected value"
            )
        self.input_path = SOLUTION_DIR.joinpath(self.input_filename)
        self.input_text = get_input(self.input_path)


class TestSolution1(TestSolution):
    module = solution1
    adapters = sorted([16, 10, 15, 5, 1, 11, 7, 19, 6, 12, 4])
    adapters2 = sorted([
        28, 33, 18, 42, 31, 14, 46, 20, 48, 47,
        24, 23, 49, 45, 19, 38, 39, 11, 1, 32,
        25, 35, 8, 17, 7, 9, 4, 2, 34, 10, 3,
    ])
    expected = 35

    def test_solver(self):
        solution = self.module.solve(self.input_text)
        self.assertEqual(self.expected, solution)

    def test_get_differences(self):
        differences1 = self.module.get_differences(
            [0] + self.adapters + [max(self.adapters) + 3]
        )
        self.assertEqual(differences1[1], 7)
        self.assertEqual(differences1[3], 5)

        differences2 = self.module.get_differences(
            [0] + self.adapters2 + [max(self.adapters2) + 3]
        )
        self.assertEqual(differences2[1], 22)
        self.assertEqual(differences2[3], 10)


class TestSolution2(TestSolution):
    module = solution2
    adapters = sorted([16, 10, 15, 5, 1, 11, 7, 19, 6, 12, 4])
    adapters2 = sorted([
        28, 33, 18, 42, 31, 14, 46, 20, 48, 47,
        24, 23, 49, 45, 19, 38, 39, 11, 1, 32,
        25, 35, 8, 17, 7, 9, 4, 2, 34, 10, 3,
    ])
    expected = 8

    def test_solver(self):
        solution = self.module.solve(self.input_text)
        self.assertEqual(self.expected, solution)

    def test_number_of_solutions(self):
        self.assertEqual(
            self.module.number_of_solutions(
                [0] + self.adapters + [max(self.adapters) + 3]
            ),
            8,
        )
        self.assertEqual(
            self.module.number_of_solutions(
                [0] + self.adapters2 + [max(self.adapters2) + 3]
            ),
            19208,
        )


if __name__ == '__main__':
    unittest.main()
