#!/usr/bin/env python
# -*- coding: utf-8 -*-

from collections import Counter
from itertools import product


class Layout:
    occupied_threshold = 4
    extended = False

    def __init__(self, lines):
        initial = "".join(lines)
        self.num_cells = len(initial)
        self.height = len(lines)
        self.height_less_one = self.height - 1
        self.width = len(lines[0])
        self.width_less_one = self.width - 1
        self.chairs = {pos for (pos, c) in enumerate(initial) if c != "."}
        self.coords = tuple(
            (pos % self.width, pos // self.width)
            for pos in range(self.num_cells)
        )
        self.cells = [0] * self.num_cells

        UP = -self.width
        DOWN = self.width
        LEFT = -1
        RIGHT = 1
        UP_LEFT = UP + LEFT
        UP_RIGHT = UP + RIGHT
        DOWN_LEFT = DOWN + LEFT
        DOWN_RIGHT = DOWN + RIGHT

        self.UPWARDS = [UP, UP_LEFT, UP_RIGHT]
        self.DOWNWARDS = [DOWN, DOWN_LEFT, DOWN_RIGHT]
        self.LEFTWARDS = [LEFT, UP_LEFT, DOWN_LEFT]
        self.RIGHTWARDS = [RIGHT, UP_RIGHT, DOWN_RIGHT]

        self.DIRS = [
            UP, UP_RIGHT, RIGHT, DOWN_RIGHT, DOWN, DOWN_LEFT, LEFT, UP_LEFT,
        ]

    def get_pos(self, row, col):
        return row * self.width + col

    def is_blank(self, pos):
        return pos not in self.chairs

    def is_occupied(self, pos):
        return self.cells[pos] == 1

    def get_char(self, row, col):
        pos = self.get_pos(row, col)

        if self.is_blank(pos):
            return "."
        elif self.is_occupied(pos):
            return "#"
        else:
            return "L"

    def in_bounds(self, pos, direction):
        col, row = self.coords[pos]

        if col == 0 and direction in self.LEFTWARDS:
            return False
        if col == self.width_less_one and direction in self.RIGHTWARDS:
            return False
        if row == 0 and direction in self.UPWARDS:
            return False
        if row == self.height_less_one and direction in self.DOWNWARDS:
            return False

        return True

    def count_occupied_neighbours(self, pos):
        count = 0

        for direction in self.DIRS:
            if not self.in_bounds(pos, direction):
                continue

            new = pos + direction

            if self.extended:
                while self.is_blank(new) and self.in_bounds(new, direction):
                    new += direction

            if self.is_occupied(new):
                count += 1

        return count

    def has_flipped(self, pos):
        count = self.count_occupied_neighbours(pos)

        if self.is_occupied(pos):
            return count >= self.occupied_threshold
        else:
            return count == 0

    def tick(self):
        flipped = [pos for pos in self.chairs if self.has_flipped(pos)]

        for pos in flipped:
            self.cells[pos] = self.cells[pos] * -1 + 1

        return bool(flipped)

    def count_occupied(self):
        return sum(self.cells[pos] for pos in self.chairs)

    def __str__(self):
        return "\n".join(
            "".join(self.get_char(row, col) for col in range(self.width))
            for row in range(self.height)
        )


class ExtendedLayout(Layout):
    occupied_threshold = 5
    extended = True
