use std::fmt;
use std::fs;

struct Layout {
    chairs: Vec<usize>,
    cells: Vec<char>,
    width: usize,
    width_less_one: usize,
    height: usize,
    height_less_one: usize,
    extended: bool,
    neighbour_threshold: usize,
    dirs: Vec<i32>,
    upwards: Vec<i32>,
    downwards: Vec<i32>,
    leftwards: Vec<i32>,
    rightwards: Vec<i32>,
}

impl Layout {
    pub fn new(v: Vec<String>, extended: bool, threshold: usize) -> Layout {
        let width = v[0].len();
        let height = v.len();
        let up = 0 - width as i32;
        let down = width as i32;
        let left = -1;
        let right = 1;
        let up_left = up + left;
        let up_right = up + right;
        let down_left = down + left;
        let down_right = down + right;
        let chairs = v.iter().flat_map(|s| s.chars()).enumerate().filter_map(|(i, c)| match c {
            '.' => None,
            _ => Some(i),
        }).collect();
        let cells = v.iter().flat_map(|s| s.chars()).collect();

        Layout {
            chairs: chairs,
            cells: cells,
            width: width,
            width_less_one: width - 1,
            height: height,
            height_less_one: height - 1,
            extended: extended,
            neighbour_threshold: threshold,
            dirs: vec![
                up, up_right, right, down_right,
                down, down_left, left, up_left,
            ],
            upwards: vec![up, up_right, up_left],
            downwards: vec![down, down_right, down_left],
            leftwards: vec![left, up_left, down_left],
            rightwards: vec![right, up_right, down_right],
        }
    }

    fn is_blank(&self, pos: usize) -> bool{
        self.cells[pos] == '.'
    }

    fn is_occupied(&self, pos: usize) -> bool{
        self.cells[pos] == '#'
    }

    fn is_empty(&self, pos: usize) -> bool{
        self.cells[pos] == 'L'
    }

    fn get_char(&self, row: usize, col: usize) -> char {
        self.cells[row * self.width + col]
    }

    fn get_col_row(&self, pos: usize) -> (usize, usize) {
        (pos % self.width, pos / self.width)
    }

    fn in_bounds(&self, pos: usize, dir: i32) -> bool {
        let (col, row) = self.get_col_row(pos);

        if col == 0 && self.leftwards.contains(&dir) {
            false
        } else if col == self.width_less_one && self.rightwards.contains(&dir) {
            false
        } else if row == 0 && self.upwards.contains(&dir) {
            false
        } else if row == self.height_less_one && self.downwards.contains(&dir) {
            false
        } else {
            true
        }
    }

    fn count_occupied_neighbours(&self, pos: usize) -> usize {
        let mut count = 0;

        for dir in self.dirs.iter() {
            if !self.in_bounds(pos, *dir) {
                continue;
            }

            let mut new = pos as i32 + *dir;

            if self.extended {
                while self.is_blank(new as usize) && self.in_bounds(new as usize, *dir) {
                    new += *dir;
                }
            }

            if self.is_occupied(new as usize) {
                count += 1;
            }
        }

        count
    }

    fn has_flipped(&self, pos: usize) -> bool {
        if self.is_blank(pos) {
            return false;
        }

        let count = self.count_occupied_neighbours(pos);

        (self.is_empty(pos) && count == 0) || (self.is_occupied(pos) && count >= self.neighbour_threshold)
    }

    fn tick(&mut self) -> bool {
        let flipped: Vec<usize> = self.chairs.iter()
            .filter(|pos| self.has_flipped(**pos)).copied().collect();

        for pos in flipped.iter() {
            if self.cells[*pos] == '#' {
                self.cells[*pos] = 'L';
            } else {
                self.cells[*pos] = '#';
            }
        }

        flipped.len() > 0
    }

    fn count_occupied(&self) -> usize {
        self.cells.iter().filter(|c| **c == '#').count()
    }
}

impl fmt::Display for Layout {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let result = (0..self.height)
            .map(|row| {
                (0..self.width)
                    .map(|col| self.get_char(row, col))
                    .collect::<String>()
                    + "\n"
            })
            .collect::<String>();
        write!(f, "{}", result)
    }
}

fn parse_file(filename: &str) -> Vec<String> {
    fs::read_to_string(filename)
        .expect("problem reading file")
        .split('\n')
        .filter_map(|line| match line.len() {
            0 => None,
            _ => Some(line.to_owned()),
        })
        .collect()
}

fn solve_part1(filename: &str) -> usize {
    let lines = parse_file(&filename);
    let mut l = Layout::new(lines, false, 4);

    while l.tick() {}

    l.count_occupied()
}

fn solve_part2(filename: &str) -> usize {
    let lines = parse_file(&filename);
    let mut l = Layout::new(lines, true, 5);

    while l.tick() {}

    l.count_occupied()
}

fn main() {
    println!("{}", solve_part1("../input.txt"));
    println!("{}", solve_part2("../input.txt"));
}

#[cfg(test)]
mod tests {
    use super::*;

    fn step0() -> String {
        String::from(
            "L.LL.LL.LL\n\
             LLLLLLL.LL\n\
             L.L.L..L..\n\
             LLLL.LL.LL\n\
             L.LL.LL.LL\n\
             L.LLLLL.LL\n\
             ..L.L.....\n\
             LLLLLLLLLL\n\
             L.LLLLLL.L\n\
             L.LLLLL.LL\n\
            ",
        )
    }

    #[test]
    fn test_in_bounds() {
        let l = Layout::new(parse_file("../test_input.txt"), false, 4);
        assert_eq!(l.in_bounds(0, -10), false); // up
        assert_eq!(l.in_bounds(0, -1), false); // left
        assert_eq!(l.in_bounds(0, -11), false); // up left
        assert_eq!(l.in_bounds(1, -10), false); // up
        assert_eq!(l.in_bounds(1, -1), true); // left
        assert_eq!(l.in_bounds(10, -10), true); // up
        assert_eq!(l.in_bounds(99, 10), false); // down
        assert_eq!(l.in_bounds(99, 1), false); // right
        assert_eq!(l.in_bounds(99, 11), false); // down right
        assert_eq!(l.in_bounds(98, 1), true); // right
        assert_eq!(l.in_bounds(89, 10), true); // down
    }

    #[test]
    fn test_display() {
        let l = Layout::new(parse_file("../test_input.txt"), false, 4);
        assert_eq!(format!("{}", l), step0());
    }

    #[test]
    fn test_solve_part1() {
        assert_eq!(solve_part1("../test_input.txt"), 37);
    }

    #[test]
    fn test_solve_part2() {
        assert_eq!(solve_part2("../test_input.txt"), 26);
    }
}
