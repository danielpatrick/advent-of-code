#!/usr/bin/env python
# -*- coding: utf-8 -*-

import curses
import sys
from pathlib import Path

from .layout import ExtendedLayout


def solve(input_text):
    l = ExtendedLayout(input_text)

    while l.tick():
        pass

    return l.count_occupied()


def play(input_text):
    l = ExtendedLayout(input_text)

    yield str(l)

    while l.tick():
        yield str(l)


if __name__ == '__main__':
    from shared.utils import get_input
    from timeit import default_timer as timer

    start = timer()

    input_path = Path(__file__).parent.joinpath("input.txt")
    input_text = get_input(input_path)

    if "visual" in sys.argv:
        def visualise(stdscr):
            stdscr.clear()
            curses.start_color()
            curses.use_default_colors()
            curses.init_pair(1, 34, 34)
            curses.init_pair(2, 240, 240)
            curses.init_pair(3, 0, 0)

            colour_map = {
                "#": curses.color_pair(1),
                "L": curses.color_pair(2),
                ".": curses.color_pair(3),
            }

            for visual in play(input_text):
                visual = visual.split("\n")

                for i in range(len(visual)):
                    for j in range(len(visual[i])):
                        char = visual[i][j]
                        stdscr.addstr(i, j, " ", colour_map[char])

                stdscr.refresh()  # display new image

            stdscr.getkey()  # wait for key press before exiting

        curses.wrapper(visualise)
    else:
        solution = solve(input_text)
        print(solution)

    end = timer()
    print()
    print("-" * 80)
    print("Time elapsed: {:.3f}s".format(end - start))
