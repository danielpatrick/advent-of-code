#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pathlib import Path
import unittest

from shared.utils import get_input
from . import solution1, solution2
from .layout import Layout, ExtendedLayout


SOLUTION_DIR = Path(__file__).parent

tick0 = (
    "L.LL.LL.LL\nLLLLLLL.LL\nL.L.L..L..\nLLLL.LL.LL\nL.LL.LL.LL\n"
    "L.LLLLL.LL\n..L.L.....\nLLLLLLLLLL\nL.LLLLLL.L\nL.LLLLL.LL"
)

tick1 = (
    "#.##.##.##\n#######.##\n#.#.#..#..\n####.##.##\n#.##.##.##\n"
    "#.#####.##\n..#.#.....\n##########\n#.######.#\n#.#####.##"
)
tick2 = (
    "#.LL.L#.##\n#LLLLLL.L#\nL.L.L..L..\n#LLL.LL.L#\n#.LL.LL.LL\n"
    "#.LLLL#.##\n..L.L.....\n#LLLLLLLL#\n#.LLLLLL.L\n#.#LLLL.##"
)
tick3 = (
    "#.##.L#.##\n#L###LL.L#\nL.#.#..#..\n#L##.##.L#\n#.##.LL.LL\n"
    "#.###L#.##\n..#.#.....\n#L######L#\n#.LL###L.L\n#.#L###.##"
)
tick4 = (
    "#.#L.L#.##\n#LLL#LL.L#\nL.L.L..#..\n#LLL.##.L#\n#.LL.LL.LL\n"
    "#.LL#L#.##\n..L.L.....\n#L#LLLL#L#\n#.LLLLLL.L\n#.#L#L#.##"
)
tick5 = (
    "#.#L.L#.##\n#LLL#LL.L#\nL.#.L..#..\n#L##.##.L#\n#.#L.LL.LL\n"
    "#.#L#L#.##\n..L.L.....\n#L#L##L#L#\n#.LLLLLL.L\n#.#L#L#.##"
)

tick1_extended = (
    "#.##.##.##\n#######.##\n#.#.#..#..\n####.##.##\n#.##.##.##\n"
    "#.#####.##\n..#.#.....\n##########\n#.######.#\n#.#####.##"
)
tick2_extended = (
    "#.LL.LL.L#\n#LLLLLL.LL\nL.L.L..L..\nLLLL.LL.LL\nL.LL.LL.LL\n"
    "L.LLLLL.LL\n..L.L.....\nLLLLLLLLL#\n#.LLLLLL.L\n#.LLLLL.L#"
)
tick3_extended = (
    "#.L#.##.L#\n#L#####.LL\nL.#.#..#..\n##L#.##.##\n#.##.#L.##\n"
    "#.#####.#L\n..#.#.....\nLLL####LL#\n#.L#####.L\n#.L####.L#"
)
tick7_extended = (
    "#.L#.L#.L#\n#LLLLLL.LL\nL.L.L..#..\n##L#.#L.L#\nL.L#.LL.L#\n"
    "#.LLLL#.LL\n..#.L.....\nLLL###LLL#\n#.LLLLL#.L\n#.L#LL#.L#"
)

class TestSolution(unittest.TestCase):
    module = None
    input_filename = "test_input.txt"
    expected = None

    def setUp(self):
        if self.module is None:
            raise NotImplementedError(
                "subclasses of TestSolution must provide module to test"
            )
        if self.expected is None:
            raise NotImplementedError(
                "subclasses of TestSolution must provide expected value"
            )
        self.input_path = SOLUTION_DIR.joinpath(self.input_filename)
        self.input_text = get_input(self.input_path)


class TestLayout(unittest.TestCase):
    def test_count_occupied_neighbours(self):
        l = Layout(tick0.split("\n"))
        l.tick()
        l.tick()

        self.assertEqual(l.count_occupied_neighbours(11), 2)
        self.assertEqual(l.count_occupied_neighbours(99), 1)
        self.assertEqual(l.count_occupied_neighbours(70), 1)

    def test_layout_str(self):
        l = Layout(tick0.split("\n"))
        self.assertEqual(str(l), tick0)

    def test_layout_tick(self):
        l = Layout(tick0.split("\n"))

        l.tick()
        self.assertEqual(str(l), tick1)

        l.tick()
        self.assertEqual(str(l), tick2)

        l.tick()
        self.assertEqual(str(l), tick3)

        l.tick()
        self.assertEqual(str(l), tick4)

        l.tick()
        self.assertEqual(str(l), tick5)

    def test_count_occupied(self):
        l = Layout(tick0.split("\n"))
        self.assertEqual(l.count_occupied(), 0)
        l.tick()
        self.assertEqual(l.count_occupied(), 71)
        l.tick()
        self.assertEqual(l.count_occupied(), 20)


class TestExtendedLayout(unittest.TestCase):
    def test_count_occupied_neighbours(self):
        l = ExtendedLayout(tick0.split("\n"))
        l.tick()
        l.tick()

        self.assertEqual(l.count_occupied_neighbours(11), 2)
        self.assertEqual(l.count_occupied_neighbours(99), 0)
        self.assertEqual(l.count_occupied_neighbours(70), 1)

    def test_layout_tick(self):
        l = ExtendedLayout(tick0.split("\n"))

        l.tick()
        self.assertEqual(str(l), tick1_extended)

        l.tick()
        self.assertEqual(str(l), tick2_extended)


class TestSolution1(TestSolution):
    module = solution1
    expected = 37

    def test_solver(self):
        solution = self.module.solve(self.input_text)
        self.assertEqual(self.expected, solution)


class TestSolution2(TestSolution):
    module = solution2
    expected = 26

    def test_solver(self):
        solution = self.module.solve(self.input_text)
        self.assertEqual(self.expected, solution)


if __name__ == '__main__':
    unittest.main()
