use std::fs;

#[derive(Clone, Copy)]
enum Instruction {
    North(i32),
    South(i32),
    East(i32),
    West(i32),
    Left(i32),
    Right(i32),
    Forward(i32),
}

struct Ship<'a> {
    instructions: &'a Vec<Instruction>,
    current_instruction: usize,
    position: (i32, i32),
    velocity: (i32, i32),
    use_waypoints: bool,
}

impl<'a> Ship<'a> {
    fn new(instructions: &'a Vec<Instruction>, velocity: (i32, i32), use_waypoints: bool) -> Ship {
        Ship {
            instructions,
            current_instruction: 0,
            position: (0, 0),
            velocity,
            use_waypoints,
        }
    }

    fn distance_from_origin(&self) -> usize {
        self.position.0.abs() as usize + self.position.1.abs() as usize
    }

    fn tick(&mut self) -> bool {
        use Instruction::*;

        match self.next_instruction() {
            North(n) => self.move_north_south(n),
            South(n) => self.move_north_south(-n),
            East(n) => self.move_east_west(n),
            West(n) => self.move_east_west(-n),
            Left(n) => self.rotate(n),
            Right(n) => self.rotate(-n),
            Forward(n) => self.move_forward(n),
        }

        self.current_instruction += 1;
        self.current_instruction < self.instructions.len()
    }

    fn next_instruction(&mut self) -> Instruction {
        self.instructions[self.current_instruction]
    }

    fn get_position(&self) -> (i32, i32) {
        self.position
    }

    fn set_position(&mut self, x: i32, y: i32) {
        self.position = (x, y);
    }

    fn get_velocity(&self) -> (i32, i32) {
        self.velocity
    }

    fn set_velocity(&mut self, x: i32, y: i32) {
        self.velocity = (x, y)
    }

    fn move_north_south(&mut self, n: i32) {
        if self.use_waypoints {
            let (x, y) = self.get_velocity();
            self.set_velocity(x, y + n);
        } else {
            let (x, y) = self.get_position();
            self.set_position(x, y + n);
        }
    }

    fn move_east_west(&mut self, n: i32) {
        if self.use_waypoints {
            let (x, y) = self.get_velocity();
            self.set_velocity(x + n, y);
        } else {
            let (x, y) = self.get_position();
            self.set_position(x + n, y);
        }
    }

    fn rotate(&mut self, n: i32) {
        let (mut x, mut y) = self.get_velocity();

        for _ in 0..((n / 90 + 4) % 4) {
            let tmp = -y;
            y = x;
            x = tmp;
        }

        self.set_velocity(x, y);
    }

    fn move_forward(&mut self, n: i32) {
        let (x, y) = self.get_position();
        let (dx, dy) = self.get_velocity();
        self.set_position(x + dx * n, y + dy * n);
    }
}

fn parse_instructions(filename: &str) -> Vec<Instruction> {
    use Instruction::*;

    fs::read_to_string(filename)
        .expect("problem reading file")
        .split('\n')
        .filter_map(|line| match line.len() {
            0 => None,
            _ => {
                let mut iter = line.chars();
                let c = iter.next().unwrap();
                let n = iter.collect::<String>().parse().unwrap();

                match c {
                    'N' => Some(North(n)),
                    'S' => Some(South(n)),
                    'E' => Some(East(n)),
                    'W' => Some(West(n)),
                    'L' => Some(Left(n)),
                    'R' => Some(Right(n)),
                    'F' => Some(Forward(n)),
                    _ => None,
                }
            }
        })
        .collect()
}

fn solve_part1(instructions: &Vec<Instruction>) -> usize {
    let mut ship = Ship::new(instructions, (1, 0), false);

    while ship.tick() {}

    ship.distance_from_origin()
}

fn solve_part2(instructions: &Vec<Instruction>) -> usize {
    let mut ship = Ship::new(instructions, (10, 1), true);

    while ship.tick() {}

    ship.distance_from_origin()
}

fn main() {
    let instructions = parse_instructions("../input.txt");
    println!("{}", solve_part1(&instructions));
    println!("{}", solve_part2(&instructions));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_solve_part1() {
        let instructions = parse_instructions("../test_input.txt");
        assert_eq!(solve_part1(&instructions), 25);
    }

    #[test]
    fn test_solve_part2() {
        let instructions = parse_instructions("../test_input.txt");
        assert_eq!(solve_part2(&instructions), 286);
    }
}
