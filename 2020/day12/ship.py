#!/usr/bin/env python
# -*- coding: utf-8 -*-


class Ship:
    def __init__(self, instructions, starting_velocity):
        if not instructions:
            raise Exception("Instructions must not be empty")

        self.instructions = instructions
        self.current = 0
        self.pos = [0, 0]
        self.vel = [starting_velocity[0], starting_velocity[1]]

        self.move = {
            "N": self.move_north,
            "S": self.move_south,
            "E": self.move_east,
            "W": self.move_west,
            "L": self.rotate_left,
            "R": self.rotate_right,
            "F": self.move_forward,
        }

    def tick(self):
        instruction = self.instructions[self.current]
        self.move[instruction[0]](int(instruction[1:]))

        self.current += 1

        if self.current >= len(self.instructions):
            return False

        return True

    def move_north(self, n):
        self.pos[1] += n

    def move_south(self, n):
        self.pos[1] -= n

    def move_east(self, n):
        self.pos[0] += n

    def move_west(self, n):
        self.pos[0] -= n

    def rotate_left(self, n):
        if n % 90 != 0:
            raise Exception(f"Invalid rotation: {n} degrees")

        for _ in range(n // 90):
            self.vel = [-self.vel[1], self.vel[0]]

    def rotate_right(self, n):
        if n % 90 != 0:
            raise Exception(f"Invalid rotation: {n} degrees")

        for _ in range(n // 90):
            self.vel = [self.vel[1], -self.vel[0]]

    def move_forward(self, n):
        self.pos[0] += self.vel[0] * n
        self.pos[1] += self.vel[1] * n
