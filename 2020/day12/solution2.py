#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pathlib import Path

from .ship import Ship


class WaypointShip(Ship):
    def move_north(self, n):
        self.vel[1] += n

    def move_south(self, n):
        self.vel[1] -= n

    def move_east(self, n):
        self.vel[0] += n

    def move_west(self, n):
        self.vel[0] -= n


def solve(input_text):
    s = WaypointShip(input_text, [10, 1])

    while s.tick():
        pass

    return abs(s.pos[0]) + abs(s.pos[1])


if __name__ == '__main__':
    from shared.utils import get_input
    from timeit import default_timer as timer

    start = timer()

    input_path = Path(__file__).parent.joinpath("input.txt")
    input_text = get_input(input_path)
    solution = solve(input_text)
    print(solution)

    end = timer()
    print()
    print("-" * 80)
    print("Time elapsed: {:.3f}s".format(end - start))
