use std::collections::HashMap;
use std::fs;

fn parse_file1(filename: &str) -> (i128, Vec<i128>) {
    let lines = fs::read_to_string(filename)
        .expect("problem reading file")
        .split('\n')
        .filter_map(|line| match line.len() {
            0 => None,
            _ => Some(line.to_owned()),
        })
        .collect::<Vec<String>>();

    let t = lines[0].parse().unwrap();
    let v = lines[1]
        .split(",")
        .filter_map(|n| match n {
            "x" => None,
            n => n.parse().ok(),
        })
        .collect();

    (t, v)
}

fn parse_file2(filename: &str) -> HashMap<i128, i128> {
    fs::read_to_string(filename)
        .expect("problem reading file")
        .split('\n')
        .enumerate()
        .find_map(|(i, line)| match i {
            1 => Some(
                line.split(",")
                    .enumerate()
                    .filter_map(|(i, n)| match n {
                        "x" => None,
                        n => {
                            let b = n.parse().unwrap();
                            Some((b, b - i as i128))
                        }
                    })
                    .collect::<HashMap<i128, i128>>(),
            ),
            _ => None,
        })
        .unwrap()
}

fn solve_part1(filename: &str) -> i128 {
    let (start_time, buses) = parse_file1(&filename);
    let mut t = 0;

    loop {
        for bus in buses.iter() {
            if (start_time + t) % bus == 0 {
                return bus * t;
            }
        }

        t += 1
    }
}

fn solve_part2(filename: &str) -> i128 {
    let buses = parse_file2(&filename);
    let product: i128 = buses.keys().product();

    buses
        .iter()
        .map(|(divisor, remainder)| {
            let n = product / divisor;
            let mut nu = n;

            while nu % divisor != 1 {
                nu += n;
            }
            remainder * nu
        })
        .sum::<i128>() % product
}

fn main() {
    println!("{}", solve_part1("../input.txt"));
    println!("{}", solve_part2("../input.txt"));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_file1() {
        assert_eq!(
            parse_file1("../test_input.txt"),
            (939, vec![7, 13, 59, 31, 19]),
        );
    }

    #[test]
    fn test_parse_file2() {
        assert_eq!(
            parse_file2("../test_input.txt"),
            [(7, 7), (13, 12), (59, 55), (31, 25), (19, 12)]
                .iter()
                .cloned()
                .collect(),
        );
    }

    #[test]
    fn test_solve_part1() {
        assert_eq!(solve_part1("../test_input.txt"), 295);
    }

    #[test]
    fn test_solve_part2() {
        assert_eq!(solve_part2("../test_input.txt"), 1068781);
    }
}
