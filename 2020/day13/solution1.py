#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pathlib import Path


def parse_input(input_text):
    start_time = int(input_text[0])
    buses = [int(s) for s in input_text[1].split(",") if s != "x"]

    return start_time, buses


def solve(input_text):
    start_time, buses = parse_input(input_text)
    t = 0

    while True:
        for bus in buses:
            if (start_time + t) % bus == 0:
                return bus * t

        t += 1


if __name__ == '__main__':
    from shared.utils import get_input
    from timeit import default_timer as timer

    start = timer()

    input_path = Path(__file__).parent.joinpath("input.txt")
    input_text = get_input(input_path)
    solution = solve(input_text)
    print(solution)

    end = timer()
    print()
    print("-" * 80)
    print("Time elapsed: {:.3f}s".format(end - start))
