#!/usr/bin/env python
# -*- coding: utf-8 -*-

from math import prod
from pathlib import Path


def parse_input(input_text):
    result = {}

    for i, bus in enumerate(input_text[1].split(",")):
        if not bus == "x":
            b = int(bus)
            result[b] = b - i

    return result


def solve(input_text):
    buses = parse_input(input_text)
    product = prod(buses.keys())
    total = 0

    for divisor, remainder in buses.items():
        n = product // divisor
        nu = n

        while nu % divisor != 1:
            nu += n

        total += remainder * nu

    return total % product


if __name__ == '__main__':
    from shared.utils import get_input
    from timeit import default_timer as timer

    start = timer()

    input_path = Path(__file__).parent.joinpath("input.txt")
    input_text = get_input(input_path)
    solution = solve(input_text)
    print(solution)

    end = timer()
    print()
    print("-" * 80)
    print("Time elapsed: {:.3f}s".format(end - start))
