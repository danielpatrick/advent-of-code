use std::fs;
use std::collections::HashMap;

enum Instruction {
    Mask(String),
    Mem(u64, u64),
}

fn parse_instructions(filename: &str) -> Vec<Instruction> {
    fs::read_to_string(filename)
        .expect("problem reading file")
        .lines()
        .map(|line| match line.starts_with("mask") {
            true => Instruction::Mask(line.trim_start_matches("mask = ").to_string()),
            false => {
                let mut iter = line.split(" = ");
                let mem = iter
                    .next()
                    .unwrap()
                    .trim_start_matches("mem[")
                    .trim_end_matches("]")
                    .parse::<u64>()
                    .unwrap();
                let num = iter
                    .next()
                    .unwrap()
                    .parse::<u64>()
                    .unwrap();
                Instruction::Mem(mem, num)
            },
        })
        .collect()
}

fn get_or_mask(mask: &str) -> u64 {
    u64::from_str_radix(&mask.replace("X", "0"), 2).unwrap()
}

fn get_and_mask(mask: &str) -> u64 {
    u64::from_str_radix(&mask.replace("X", "1"), 2).unwrap()
}

fn get_x_masks(mask: &str) -> (u64, Vec<u64>) {
    let len = mask.len();
    if len == 0 {
        return (0, vec![0]);
    }

    let (mut zero_mask, masks) = get_x_masks(&mask[..(len - 1)]);

    let mut new_masks = Vec::new();
    zero_mask <<= 1;

    if mask.chars().last().unwrap() == 'X' {
        zero_mask += 1;

        for m in masks.iter() {
            new_masks.push(m << 1);
            new_masks.push((m << 1) + 1);
        }
        (zero_mask, new_masks)
    } else {
        (zero_mask, masks.iter().map(|m| m << 1).collect())
    }
}

fn solve_part1(instructions: &Vec<Instruction>) -> u64 {
    let mut registers: HashMap<u64, u64> = HashMap::new();
    let mut or_mask = 0;
    let mut and_mask = 1;

    for i in instructions.iter() {
        match i {
            Instruction::Mask(mask) => {
                or_mask = get_or_mask(mask);
                and_mask = get_and_mask(mask);
            }
            Instruction::Mem(mem, num) => {
                registers.insert(*mem, (*num | or_mask) & and_mask);
            },
        }
    }

    registers.values().sum()
}

fn solve_part2(instructions: &Vec<Instruction>) -> u64 {
    let mut registers: HashMap<u64, u64> = HashMap::new();
    let mut or_mask = 0;
    let mut zero_mask = 0;
    let mut x_masks: Vec<u64> = Vec::new();

    for i in instructions.iter() {
        match i {
            Instruction::Mask(mask) => {
                or_mask = get_or_mask(mask);
                let masks = get_x_masks(mask);
                zero_mask = masks.0;
                x_masks = masks.1;
            }
            Instruction::Mem(mem, num) => {
                let mut reg = *mem;
                reg |= or_mask;
                reg &= !zero_mask;

                for x_mask in &x_masks {
                    let masked_reg = reg | x_mask;
                    registers.insert(masked_reg, *num);
                }
            },
        }
    }

    registers.values().sum()
}

fn main() {
    let instructions = parse_instructions("../input.txt");
    println!("{}", solve_part1(&instructions));
    println!("{}", solve_part2(&instructions));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_solve_part1() {
        let instructions = parse_instructions("../test_input.txt");
        assert_eq!(solve_part1(&instructions), 165);
    }

    #[test]
    fn test_solve_part2() {
        let instructions = parse_instructions("../test_input2.txt");
        assert_eq!(solve_part2(&instructions), 208);
    }
}
