#!/usr/bin/env python
# -*- coding: utf-8 -*-

from collections import defaultdict
from pathlib import Path


def parse_input(input_text):
    instructions = []

    for instruction in input_text:
        if instruction.startswith("mask"):
            mask = instruction.split(" = ")[1]

            or_mask = int(mask.replace("X", "0"), 2)
            and_mask = int(mask.replace("X", "1"), 2)

            instructions.append(("mask", or_mask, and_mask))
        else:
            mem, num = instruction.split(" = ")
            mem = mem.replace("mem[", "").replace("]", "")

            instructions.append(("mem", int(mem), int(num)))

    return instructions


def solve(input_text):
    instructions = parse_input(input_text)
    or_mask = 0
    and_mask = 1

    registers = defaultdict(int)

    for instruction in instructions:
        if instruction[0] == "mask":
            or_mask, and_mask = instruction[1:]
        else:
            num = instruction[2]
            num |= or_mask
            num &= and_mask
            registers[instruction[1]] = num

    return sum(registers.values())


if __name__ == '__main__':
    from shared.utils import get_input
    from timeit import default_timer as timer

    start = timer()

    input_path = Path(__file__).parent.joinpath("input.txt")
    input_text = get_input(input_path)
    solution = solve(input_text)
    print(solution)

    end = timer()
    print()
    print("-" * 80)
    print("Time elapsed: {:.3f}s".format(end - start))
