#!/usr/bin/env python
# -*- coding: utf-8 -*-

from collections import defaultdict
from pathlib import Path


def get_or_mask(mask):
    return int(mask.replace("X", "0"), 2)


def get_x_masks(mask):
    if len(mask) == 0:
        return (0, [0])

    zero_mask, masks = get_x_masks(mask[:-1])

    if mask[-1] == "X":
        new_masks = []
        zero_mask <<= 1
        zero_mask += 1

        for mask in masks:
            new_masks.append(mask << 1)
            new_masks.append((mask << 1) + 1)
    else:
        zero_mask <<= 1
        new_masks = [mask << 1 for mask in masks]

    return (zero_mask, new_masks)


def parse_input(input_text):
    instructions = []

    for instruction in input_text:
        if instruction.startswith("mask"):
            mask = instruction.split(" = ")[1]

            or_mask = get_or_mask(mask)
            zero_mask, x_masks = get_x_masks(mask)

            instructions.append(("mask", or_mask, zero_mask, x_masks))
        else:
            mem, num = instruction.split(" = ")
            mem = mem.replace("mem[", "").replace("]", "")

            instructions.append(("mem", int(mem), int(num)))

    return instructions


def solve(input_text):
    instructions = parse_input(input_text)
    or_mask = None
    zero_mask = None
    x_masks = []

    registers = defaultdict(int)

    for instruction in instructions:
        if instruction[0] == "mask":
            or_mask, zero_mask, x_masks = instruction[1:]
        else:
            reg = instruction[1]
            reg |= or_mask
            reg &= ~zero_mask

            for x_mask in x_masks:
                masked_reg = reg | x_mask
                registers[masked_reg] = instruction[2]

    return sum(registers.values())


if __name__ == '__main__':
    from shared.utils import get_input
    from timeit import default_timer as timer

    start = timer()

    input_path = Path(__file__).parent.joinpath("input.txt")
    input_text = get_input(input_path)
    solution = solve(input_text)
    print(solution)

    end = timer()
    print()
    print("-" * 80)
    print("Time elapsed: {:.3f}s".format(end - start))
