#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pathlib import Path
import unittest

from shared.utils import get_input
from . import solution1, solution2


SOLUTION_DIR = Path(__file__).parent


class TestSolution(unittest.TestCase):
    module = None
    input_filename = "test_input.txt"
    expected = None

    def setUp(self):
        if self.module is None:
            raise NotImplementedError(
                "subclasses of TestSolution must provide module to test"
            )
        if self.expected is None:
            raise NotImplementedError(
                "subclasses of TestSolution must provide expected value"
            )
        self.input_path = SOLUTION_DIR.joinpath(self.input_filename)
        self.input_text = get_input(self.input_path)


class TestSolution1(TestSolution):
    module = solution1
    expected = 165

    def test_solver(self):
        solution = self.module.solve(self.input_text)
        self.assertEqual(self.expected, solution)


class TestSolution2(TestSolution):
    module = solution2
    input_filename = "test_input2.txt"
    expected = 208

    def test_x_masks(self):
        zero_mask, masks = self.module.get_x_masks(
            "000000000000000000000000000000X1001X"
        )
        self.assertEqual(zero_mask, 33)
        self.assertEqual(len(masks), 4)
        self.assertTrue(0 in masks)
        self.assertTrue(1 in masks)
        self.assertTrue(32 in masks)
        self.assertTrue(33 in masks)

    def test_x_masks2(self):
        zero_mask, masks = self.module.get_x_masks(
            "00000000000000000000000000000000X0XX"
        )
        self.assertEqual(zero_mask, 11)
        self.assertEqual(len(masks), 8)
        self.assertTrue(0 in masks)
        self.assertTrue(1 in masks)
        self.assertTrue(2 in masks)
        self.assertTrue(3 in masks)
        self.assertTrue(8 in masks)
        self.assertTrue(9 in masks)
        self.assertTrue(10 in masks)
        self.assertTrue(11 in masks)

    def test_solver(self):
        solution = self.module.solve(self.input_text)
        self.assertEqual(self.expected, solution)


if __name__ == '__main__':
    unittest.main()
