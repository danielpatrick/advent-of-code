use std::collections::HashMap;
use std::fs;

fn parse_file(filename: &str) -> Vec<u32> {
    fs::read_to_string(filename)
        .expect("problem reading file")
        .split('\n')
        .nth(0)
        .unwrap()
        .split(",")
        .map(|s| s.parse::<u32>().unwrap())
        .collect()
}

fn get_nth_number(v: &mut Vec<u32>, n: u32) -> u32 {
    let initial = v.len() as u32;
    let mut prev_num;
    let mut last_num = v.pop().unwrap();
    let mut spoken = v
        .iter()
        .enumerate()
        .map(|(i, n)| (*n, i as u32 + 1))
        .collect::<HashMap<u32, u32>>();

    for i in initial..n {
        prev_num = last_num;

        if let Some(prev) = spoken.get_mut(&prev_num) {
            last_num = i - *prev;
            *prev = i;
        } else {
            last_num = 0;
            spoken.insert(prev_num, i);
        }
    }

    last_num
}

fn solve_part1(filename: &str) -> u32 {
    let mut numbers = parse_file(&filename);
    get_nth_number(&mut numbers, 2020)
}

fn solve_part2(filename: &str) -> u32 {
    let mut numbers = parse_file(&filename);
    get_nth_number(&mut numbers, 30000000)
}

fn main() {
    println!("{}", solve_part1("../input.txt"));
    println!("{}", solve_part2("../input.txt"));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_get_nth_number() {
        assert_eq!(get_nth_number(vec![1, 3, 2], 2020), 1);
        assert_eq!(get_nth_number(vec![2, 1, 3], 2020), 10);
        assert_eq!(get_nth_number(vec![1, 2, 3], 2020), 27);
        assert_eq!(get_nth_number(vec![2, 3, 1], 2020), 78);
        assert_eq!(get_nth_number(vec![3, 2, 1], 2020), 438);
        assert_eq!(get_nth_number(vec![3, 1, 2], 2020), 1836);

        let n = 30000000;
        assert_eq!(get_nth_number(vec![1, 3, 2], n), 2578);
        assert_eq!(get_nth_number(vec![2, 1, 3], n), 3544142);
        assert_eq!(get_nth_number(vec![1, 2, 3], n), 261214);
        assert_eq!(get_nth_number(vec![2, 3, 1], n), 6895259);
        assert_eq!(get_nth_number(vec![3, 2, 1], n), 18);
        assert_eq!(get_nth_number(vec![3, 1, 2], n), 362);
    }

    #[test]
    fn test_solve_part1() {
        assert_eq!(solve_part1("../test_input.txt"), 436);
    }

    #[test]
    fn test_solve_part2() {
        assert_eq!(solve_part2("../test_input.txt"), 175594);
    }
}
