#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pathlib import Path


def solve(input_text):
    starting_numbers = [int(s) for s in input_text[0].split(",")]
    spoken = {n: i for (i, n) in enumerate(starting_numbers[:-1], 1)}
    last_num = starting_numbers[-1]

    for i in range(len(starting_numbers), 30000000):
        prev_num = last_num

        if prev_num in spoken:
            prev_spoken = spoken[prev_num]
            last_num = i - prev_spoken
        else:
            last_num = 0

        spoken[prev_num] = i

    return last_num


if __name__ == '__main__':
    from shared.utils import get_input
    from timeit import default_timer as timer

    start = timer()

    input_path = Path(__file__).parent.joinpath("input.txt")
    input_text = get_input(input_path)
    solution = solve(input_text)
    print(solution)

    end = timer()
    print()
    print("-" * 80)
    print("Time elapsed: {:.3f}s".format(end - start))
