#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pathlib import Path
import unittest

from shared.utils import get_input
from . import solution1, solution2


SOLUTION_DIR = Path(__file__).parent


class TestSolution(unittest.TestCase):
    module = None
    input_filename = "test_input.txt"
    expected = None

    def setUp(self):
        if self.module is None:
            raise NotImplementedError(
                "subclasses of TestSolution must provide module to test"
            )
        if self.expected is None:
            raise NotImplementedError(
                "subclasses of TestSolution must provide expected value"
            )
        self.input_path = SOLUTION_DIR.joinpath(self.input_filename)
        self.input_text = get_input(self.input_path)


class TestSolution1(TestSolution):
    module = solution1
    expected = 436

    def test_solver(self):
        solution = self.module.solve(self.input_text)
        self.assertEqual(self.expected, solution)

    def test_more_values(self):
        self.assertEqual(self.module.solve(["1,3,2"]), 1)
        self.assertEqual(self.module.solve(["2,1,3"]), 10)
        self.assertEqual(self.module.solve(["1,2,3"]), 27)
        self.assertEqual(self.module.solve(["2,3,1"]), 78)
        self.assertEqual(self.module.solve(["3,2,1"]), 438)
        self.assertEqual(self.module.solve(["3,1,2"]), 1836)


class TestSolution2(TestSolution):
    module = solution2
    expected = 175594

    def test_solver(self):
        solution = self.module.solve(self.input_text)
        self.assertEqual(self.expected, solution)

    def test_more_values(self):
        self.assertEqual(self.module.solve(["1,3,2"]), 2578)
        self.assertEqual(self.module.solve(["2,1,3"]), 3544142)
        self.assertEqual(self.module.solve(["1,2,3"]), 261214)
        self.assertEqual(self.module.solve(["2,3,1"]), 6895259)
        self.assertEqual(self.module.solve(["3,2,1"]), 18)
        self.assertEqual(self.module.solve(["3,1,2"]), 362)


if __name__ == '__main__':
    unittest.main()
