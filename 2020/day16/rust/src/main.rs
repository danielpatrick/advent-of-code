use std::collections::{HashMap, HashSet};
use std::fs;
use std::iter::Iterator;
use std::ops::RangeInclusive;

use regex::Regex;

type Fields = HashMap<String, (RangeInclusive<u32>, RangeInclusive<u32>)>;
type Ticket = Vec<u32>;

fn parse_input(filename: &str) -> (Fields, Ticket, Vec<Ticket>, HashSet<u32>) {
    let fields_re = Regex::new(r"(?m)^([\w\s]+): (\d+)-(\d+) or (\d+)-(\d+)$").unwrap();

    let mut fields = HashMap::new();
    let mut range_list: Vec<RangeInclusive<u32>> = Vec::new();

    let sections: Vec<String> = fs::read_to_string(filename)
        .expect("problem reading file")
        .split("\n\nyour ticket:\n")
        .map(|s| s.to_string())
        .flat_map(|s| {
            s.split("\n\nnearby tickets:\n")
                .map(|s| s.to_string())
                .collect::<Vec<String>>()
        })
        .collect();

    for capture in fields_re.captures_iter(&sections[0]) {
        let field_name = capture.get(1).unwrap().as_str();
        let range1_start = capture.get(2).unwrap().as_str().parse().unwrap();
        let range1_end = capture.get(3).unwrap().as_str().parse().unwrap();
        let range2_start = capture.get(4).unwrap().as_str().parse().unwrap();
        let range2_end = capture.get(5).unwrap().as_str().parse().unwrap();

        fields.insert(
            field_name.to_string(),
            (range1_start..=range1_end, range2_start..=range2_end),
        );
        range_list.push(range1_start..=range1_end);
        range_list.push(range2_start..=range2_end);
    }

    let ticket = sections[1]
        .lines()
        .flat_map(|line| line.split(",").map(|s| s.parse().unwrap()))
        .collect();

    let other_tickets = sections[2]
        .lines()
        .map(|line| line.split(",").map(|s| s.parse::<u32>().unwrap()).collect())
        .collect();

    let ranges = range_list
        .iter()
        .flat_map(|range| range.to_owned().collect::<Vec<u32>>())
        .collect();

    (fields, ticket, other_tickets, ranges)
}

fn map_fields(
    fields: &Fields,
    ticket: &Ticket,
    other_tickets: &Vec<Ticket>,
) -> HashMap<String, usize> {
    let num_fields = ticket.len();
    let mut known_fields = HashMap::new();
    let mut known_indexes = HashSet::new();
    let mut field_candidates: HashMap<String, HashSet<usize>> = HashMap::new();
    let other_ticket_fields: Vec<Vec<u32>> = (0..num_fields)
        .map(|i| other_tickets.iter().map(|t| t[i]).collect::<Vec<u32>>())
        .collect();

    for i in 0..num_fields {
        for (field_name, (r1, r2)) in fields.iter() {
            if other_ticket_fields[i]
                .iter()
                .all(|value| r1.contains(&value) || r2.contains(&value))
            {
                field_candidates
                    .entry(field_name.to_string())
                    .or_insert(HashSet::new())
                    .insert(i);
            }
        }
    }

    while known_indexes.len() < num_fields {
        for (k, v) in &mut field_candidates {
            if !known_fields.contains_key(k) {
                *v = v.difference(&known_indexes).copied().collect();

                if v.len() == 1 {
                    let i = v.iter().next().unwrap();
                    known_fields.insert(k.to_string(), *i);
                    known_indexes.insert(*i);
                }
            }
        }
    }

    known_fields
}

fn solve_part1(other_tickets: &Vec<Ticket>, ranges: &HashSet<u32>) -> u32 {
    other_tickets
        .iter()
        .flat_map(|t| t.iter().filter(|value| !ranges.contains(&value)))
        .sum()
}

fn solve_part2(
    fields: &Fields,
    ticket: &Ticket,
    other_tickets: &Vec<Ticket>,
    ranges: &HashSet<u32>,
    target_fields: &Vec<String>,
) -> u64 {
    let valid_other_tickets = other_tickets
        .iter()
        .filter(|t| t.iter().all(|n| ranges.contains(&n)))
        .cloned()
        .collect();

    let field_map = map_fields(&fields, &ticket, &valid_other_tickets);

    target_fields
        .iter()
        .map(|field_name| ticket[field_map[&field_name.to_string()]] as u64)
        .product()
}

fn main() {
    let (fields, ticket, other_tickets, ranges) = parse_input("../input.txt");
    let target_fields = vec![
        "departure location".to_string(),
        "departure station".to_string(),
        "departure platform".to_string(),
        "departure track".to_string(),
        "departure date".to_string(),
        "departure time".to_string(),
    ];

    println!("{}", solve_part1(&other_tickets, &ranges));
    println!(
        "{}",
        solve_part2(&fields, &ticket, &other_tickets, &ranges, &target_fields)
    );
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_solve_part1() {
        let (_, _, other_tickets, ranges) = parse_input("../test_input.txt");
        assert_eq!(solve_part1(&other_tickets, &ranges), 71);
    }

    #[test]
    fn test_solve_part2() {
        let (fields, ticket, other_tickets, ranges) = parse_input("../test_input2.txt");
        let target_fields = vec!["row".to_string(), "seat".to_string()];
        assert_eq!(
            solve_part2(&fields, &ticket, &other_tickets, &ranges, &target_fields),
            143
        );
    }
}
