#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
from collections import defaultdict
from pathlib import Path


field_regex = re.compile("([\w ]+): ([0-9]+)\-([0-9]+) or ([0-9]+)\-([0-9]+)")


def parse_input(input_text):
    mode = 0
    ranges = set()
    nearby_tickets = defaultdict(int)

    for line in input_text:
        if line == "":
            continue

        if "your ticket" in line:
            mode = 1
        elif "nearby tickets" in line:
            mode = 2
        elif mode == 0:
            field, r1a, r1b, r2a, r2b = field_regex.match(line).groups()
            ranges |= set(range(int(r1a), int(r1b) + 1))
            ranges |= set(range(int(r2a), int(r2b) + 1))
        elif mode == 1:
            continue
        elif mode == 2:
            for n in line.split(","):
                nearby_tickets[int(n)] += 1

    return ranges, nearby_tickets


def solve(input_text):
    ranges, nearby = parse_input(input_text)

    return sum(k * v for k, v in nearby.items() if not k in ranges)


if __name__ == '__main__':
    from shared.utils import get_input
    from timeit import default_timer as timer

    start = timer()

    input_path = Path(__file__).parent.joinpath("input.txt")
    input_text = get_input(input_path)
    solution = solve(input_text)
    print(solution)

    end = timer()
    print()
    print("-" * 80)
    print("Time elapsed: {:.3f}s".format(end - start))
