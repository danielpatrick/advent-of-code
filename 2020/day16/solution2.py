#!/usr/bin/env python
# -*- coding: utf-8 -*-
import re
from math import prod
from pathlib import Path


field_regex = re.compile("([\w ]+): ([0-9]+)\-([0-9]+) or ([0-9]+)\-([0-9]+)")
target_fields = [
    "departure location",
    "departure station",
    "departure platform",
    "departure track",
    "departure date",
    "departure time",
]


def parse_input(input_text):
    mode = 0
    fields = {}
    ticket = None
    nearby_tickets = []
    ranges = set()

    for line in input_text:
        if line == "":
            continue

        if "your ticket" in line:
            mode = 1
        elif "nearby tickets" in line:
            mode = 2
        elif mode == 0:
            field, r1a, r1b, r2a, r2b = field_regex.match(line).groups()
            range1 = range(int(r1a), int(r1b) + 1)
            range2 = range(int(r2a), int(r2b) + 1)
            fields[field] = [range1, range2]
            ranges |= set(range1)
            ranges |= set(range2)
        elif mode == 1:
            ticket = [int(n) for n in line.split(",")]
        elif mode == 2:
            nearby_tickets.append([int(n) for n in line.split(",")])

    return (
        fields,
        ticket,
        nearby_tickets,
        lambda ticket: all(value in ranges for value in ticket),
    )


def map_fields(fields, ticket, nearby):
    num_fields = len(ticket)
    known = {}
    known_indexes = set()

    field_candidates = {}

    # Get initial list of candidates
    for i in range(num_fields):
        for f, (r1, r2) in fields.items():
            if all(n in r1 or n in r2 for n in nearby[i]):
                if f not in field_candidates:
                    field_candidates[f] = set()
                field_candidates[f].add(i)

    # Whittle down candidates to known
    while len(known_indexes) < num_fields:
        for k in field_candidates:
            if k in known:
                continue

            field_candidates[k] -= known_indexes

            if len(field_candidates[k]) == 1:
                i = field_candidates[k].pop()
                known[k] = i
                known_indexes.add(i)

    return known


def solve(input_text, target_fields):
    fields, ticket, nearby, in_range = parse_input(input_text)

    # remove invalid tickets
    nearby = list(map(list, zip(*filter(in_range, nearby))))

    field_map = map_fields(fields, ticket, nearby)

    return prod(ticket[field_map[field_name]] for field_name in target_fields)


if __name__ == '__main__':
    from shared.utils import get_input
    from timeit import default_timer as timer

    start = timer()

    input_path = Path(__file__).parent.joinpath("input.txt")
    input_text = get_input(input_path)
    solution = solve(input_text, target_fields)
    print(solution)

    end = timer()
    print()
    print("-" * 80)
    print("Time elapsed: {:.3f}s".format(end - start))
