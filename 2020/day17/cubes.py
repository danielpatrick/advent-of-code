#!/usr/bin/env python
# -*- coding: utf-8 -*-

from collections import defaultdict
from itertools import product


class Cubes:
    def __init__(self, n, lines):
        self.active = set()
        self.num_dimensions = n

        self.NEIGHBOURING = set(product(*[[-1, 0, 1]] * n)) - {(0,) * n}

        for i, line in enumerate(lines):
            for j, col in enumerate(line):
                if col == "#":
                    self.active.add((*[0] * (n - 2), i, j))

    def get_neighbours(self):
        """
        This method is quite slow due to the `tuple(map, zip(cell, offsets))`
        and can be implemented more efficiently if we know the length of cell
        and offsets.
        """
        neighboured = defaultdict(int)

        for cell in self.active:
            for offsets in self.NEIGHBOURING:
                neighboured[tuple(map(sum, zip(cell, offsets)))] += 1

        return neighboured

    def tick(self):
        neighboured = self.get_neighbours()
        to_activate = set()
        to_decativate = set()

        for cell in neighboured.keys() - self.active:
            if neighboured[cell] == 3:
                to_activate.add(cell)

        for cell in self.active:
            if cell not in neighboured or neighboured[cell] not in (2, 3):
                to_decativate.add(cell)

        self.active |= to_activate
        self.active -= to_decativate

    def count_active(self):
        return len(self.active)
