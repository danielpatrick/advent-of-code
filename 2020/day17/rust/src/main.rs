use itertools::{zip, Itertools};
use std::collections::{HashMap, HashSet};
use std::fs;

struct Cubes3 {
    active: HashSet<Vec<i32>>,
    neighbours: HashSet<Vec<i32>>,
}

impl Cubes3 {
    pub fn new(v: HashSet<Vec<i32>>, n: usize) -> Cubes3 {
        let origin = vec![0; n];
        Cubes3 {
            active: v,
            neighbours: (0..n)
                .map(|_| -1..2)
                .multi_cartesian_product()
                .filter(|pos| *pos != origin)
                .collect(),
        }
    }

    fn get_neighbours(&self) -> HashMap<Vec<i32>, usize> {
        self.active
            .iter()
            .cartesian_product(self.neighbours.iter())
            .fold(HashMap::new(), |mut acc, (cell, offsets)| {
                let newpos = zip(cell, offsets).map(|(v, offset)| v + offset).collect();
                let count = acc.entry(newpos).or_insert(0);
                *count += 1;
                acc
            })
    }

    pub fn tick(&mut self) {
        let neighbours = self.get_neighbours();
        let to_activate = neighbours
            .iter()
            .filter_map(|(pos, n)| {
                if *n == 3 && !self.active.contains(pos) {
                    Some(pos.clone())
                } else {
                    None
                }
            })
            .collect::<HashSet<Vec<i32>>>();
        let to_deactivate = self
            .active
            .iter()
            .filter_map(|pos| match neighbours.get(pos).unwrap_or(&0) {
                &2 | &3 => None,
                _ => Some(pos.clone()),
            })
            .collect::<HashSet<Vec<i32>>>();

        self.active = &self.active | &to_activate;
        self.active = &self.active - &to_deactivate;
    }

    pub fn count_active(&self) -> usize {
        self.active.len()
    }
}

fn parse_file(filename: &str, n: usize) -> HashSet<Vec<i32>> {
    fs::read_to_string(filename)
        .expect("problem reading file")
        .split('\n')
        .enumerate()
        .map(|(i, s)| (i, s.chars().enumerate()))
        .flat_map(|(i, s)| {
            s.filter_map(move |(j, c)| match c {
                '#' => Some([i as i32, j as i32].iter().fold(
                    vec![0 as i32; n - 2],
                    |mut acc, i| {
                        acc.push(*i);
                        acc
                    },
                )),
                _ => None,
            })
        })
        .collect()
}

fn solve_part1(filename: &str) -> usize {
    let active = parse_file(&filename, 3);
    let mut cubes = Cubes3::new(active, 3);

    for _ in 0..6 {
        cubes.tick();
    }

    cubes.count_active()
}

fn solve_part2(filename: &str) -> usize {
    let active = parse_file(&filename, 4);
    let mut cubes = Cubes3::new(active, 4);

    for _ in 0..6 {
        cubes.tick();
    }

    cubes.count_active()
}

fn main() {
    println!("{}", solve_part1("../input.txt"));
    println!("{}", solve_part2("../input.txt"));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_solve_part1() {
        assert_eq!(solve_part1("../test_input.txt"), 112);
    }

    #[test]
    fn test_solve_part2() {
        assert_eq!(solve_part2("../test_input.txt"), 848);
    }
}
