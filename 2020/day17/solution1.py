#!/usr/bin/env python
# -*- coding: utf-8 -*-

from collections import defaultdict
from pathlib import Path

from .cubes import Cubes


class ThreeDimensionalCubes(Cubes):
    def get_neighbours(self):
        neighboured = defaultdict(int)

        for x, y, z in self.active:
            for dx, dy, dz in self.NEIGHBOURING:
                neighboured[(x + dx, y + dy, z + dz)] += 1

        return neighboured


def solve(input_text):
    cubes = ThreeDimensionalCubes(3, input_text)

    for _ in range(6):
        cubes.tick()

    return cubes.count_active()


if __name__ == '__main__':
    from shared.utils import get_input
    from timeit import default_timer as timer

    start = timer()

    input_path = Path(__file__).parent.joinpath("input.txt")
    input_text = get_input(input_path)
    solution = solve(input_text)
    print(solution)

    end = timer()
    print()
    print("-" * 80)
    print("Time elapsed: {:.3f}s".format(end - start))
