#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pathlib import Path
import unittest

from shared.utils import get_input
from . import solution1, solution2


SOLUTION_DIR = Path(__file__).parent


class TestSolution(unittest.TestCase):
    module = None
    input_filename = "test_input.txt"
    expected = None

    def setUp(self):
        if self.module is None:
            raise NotImplementedError(
                "subclasses of TestSolution must provide module to test"
            )
        if self.expected is None:
            raise NotImplementedError(
                "subclasses of TestSolution must provide expected value"
            )
        self.input_path = SOLUTION_DIR.joinpath(self.input_filename)
        self.input_text = get_input(self.input_path)


class TestSolution1(TestSolution):
    module = solution1
    expected = 112

    def test_solver(self):
        solution = self.module.solve(self.input_text)
        self.assertEqual(self.expected, solution)

    def test_step0(self):
        cubes = self.module.Cubes(self.input_text)
        self.assertEqual(cubes.count_active(), 5)

    def test_step1(self):
        cubes = self.module.Cubes(self.input_text)
        cubes.tick()
        self.assertEqual(cubes.count_active(), 11)

    def test_step2(self):
        cubes = self.module.Cubes(self.input_text)
        cubes.tick()
        cubes.tick()
        self.assertEqual(cubes.count_active(), 21)

    def test_step3(self):
        cubes = self.module.Cubes(self.input_text)
        for _ in range(3):
            cubes.tick()
        self.assertEqual(cubes.count_active(), 38)


class TestSolution2(TestSolution):
    module = solution2
    expected = 848

    def test_solver(self):
        solution = self.module.solve(self.input_text)
        self.assertEqual(self.expected, solution)


if __name__ == '__main__':
    unittest.main()
