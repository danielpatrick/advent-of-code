use std::default::Default;
use std::fs;
use std::ops;

use regex::Regex;

#[derive(Debug, Clone, Copy)]
enum Operator {
    Add,
    Mul,
}

#[derive(Debug)]
enum ExpressionItem<T: ops::Mul + ops::Add> {
    Int(T),
    Operation(Operator),
    OpenBracket,
    CloseBracket,
}

type CalculatorExpression<T> = Vec<ExpressionItem<T>>;

trait Calculatable: ops::Add<Output = Self> + ops::Mul<Output = Self> + Default + Copy
where
    Self: Sized,
{
}

impl<T> Calculatable for T where T: ops::Add<Output = T> + ops::Mul<Output = T> + Default + Copy {}

trait Calculator<T: Calculatable> {
    fn calculate(&self, e: &CalculatorExpression<T>) -> T {
        self.rec_calculate(e, 0, T::default(), Some(Operator::Add))
            .1
    }

    fn rec_calculate(
        &self,
        e: &CalculatorExpression<T>,
        i: usize,
        val: T,
        op: Option<Operator>,
    ) -> (usize, T) {
        use ExpressionItem::*;

        if i == e.len() {
            (i, val)
        } else {
            match &e[i] {
                CloseBracket => (i + 1, val),
                OpenBracket => self.open_bracket(e, i + 1, val, op.unwrap()),
                Int(n) => self.int(e, i + 1, val, op.unwrap(), *n),
                Operation(Operator::Add) => self.add(e, i + 1, val),
                Operation(Operator::Mul) => self.mul(e, i + 1, val),
            }
        }
    }

    fn operate(&self, lhs: T, rhs: T, op: Operator) -> T {
        match op {
            Operator::Add => lhs + rhs,
            Operator::Mul => lhs * rhs,
        }
    }

    fn open_bracket(
        &self,
        e: &CalculatorExpression<T>,
        i: usize,
        val: T,
        op: Operator,
    ) -> (usize, T) {
        let (i, sub) = self.rec_calculate(e, i, T::default(), Some(Operator::Add));
        let val = self.operate(val, sub, op);
        self.rec_calculate(e, i, val, None)
    }

    fn int(&self, e: &CalculatorExpression<T>, i: usize, val: T, op: Operator, n: T) -> (usize, T) {
        let val = self.operate(val, n, op);
        self.rec_calculate(e, i, val, None)
    }

    fn add(&self, e: &CalculatorExpression<T>, i: usize, val: T) -> (usize, T) {
        self.rec_calculate(e, i, val, Some(Operator::Add))
    }

    fn mul(&self, e: &CalculatorExpression<T>, i: usize, val: T) -> (usize, T) {
        self.rec_calculate(e, i, val, Some(Operator::Mul))
    }
}

struct BasicCalculator {}

impl<T: Calculatable> Calculator<T> for BasicCalculator {}

struct AddPrecedenceCalculator {}

impl<T: Calculatable> Calculator<T> for AddPrecedenceCalculator {
    fn mul(&self, e: &CalculatorExpression<T>, i: usize, val: T) -> (usize, T) {
        let (i, sub) = self.rec_calculate(e, i, T::default(), Some(Operator::Add));
        let val = self.operate(val, sub, Operator::Mul);
        (i, val)
    }
}

fn parse_input(filename: &str) -> Vec<CalculatorExpression<u64>> {
    let calc_ex_re = Regex::new(r"[\d+]|[\*\+]|[\(\)]").unwrap();

    fs::read_to_string(filename)
        .expect("problem reading file")
        .lines()
        .map(|line| {
            calc_ex_re
                .find_iter(line)
                .map(|m| match m.as_str() {
                    "+" => ExpressionItem::Operation(Operator::Add),
                    "*" => ExpressionItem::Operation(Operator::Mul),
                    "(" => ExpressionItem::OpenBracket,
                    ")" => ExpressionItem::CloseBracket,
                    s => ExpressionItem::Int(s.parse().unwrap()),
                })
                .collect()
        })
        .collect()
}

fn solve_part1(calculator_expressions: &Vec<CalculatorExpression<u64>>) -> u64 {
    let calculator = BasicCalculator {};

    calculator_expressions
        .iter()
        .map(|e| calculator.calculate(e))
        .sum()
}

fn solve_part2(calculator_expressions: &Vec<CalculatorExpression<u64>>) -> u64 {
    let calculator = AddPrecedenceCalculator {};

    calculator_expressions
        .iter()
        .map(|e| calculator.calculate(e))
        .sum()
}

fn main() {
    let calculator_expressions = parse_input("../input.txt");
    println!("{}", solve_part1(&calculator_expressions));
    println!("{}", solve_part2(&calculator_expressions));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_solve_part1() {
        let calculator_expressions = parse_input("../test_input.txt");
        assert_eq!(solve_part1(&calculator_expressions), 26386);
    }

    #[test]
    fn test_calculator2() {
        let calculator_expressions = parse_input("../test_input.txt");
        let calculator = AddPrecedenceCalculator {};
        assert_eq!(calculator.calculate(&calculator_expressions[0]), 51);
        assert_eq!(calculator.calculate(&calculator_expressions[1]), 46);
        assert_eq!(calculator.calculate(&calculator_expressions[2]), 1445);
        assert_eq!(calculator.calculate(&calculator_expressions[3]), 669060);
        assert_eq!(calculator.calculate(&calculator_expressions[4]), 23340);
    }

    #[test]
    fn test_solve_part2() {
        let calculator_expressions = parse_input("../test_input.txt");
        assert_eq!(solve_part2(&calculator_expressions), 693942);
    }
}
