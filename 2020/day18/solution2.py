#!/usr/bin/env python
# -*- coding: utf-8 -*-

import operator
import re
from pathlib import Path


calc_regex = re.compile(r"[0-9]+|[\*\+]|[\(\)]")

operations = {
    "+": operator.add,
    "*": operator.mul,
}


def operate(lhs, op, rhs):
    return operations[op](int(lhs), int(rhs))


def _calculate(text, pos, current, op=None):
    if pos == len(text):
        return current, pos

    if text[pos] == ")":
        return current, pos + 1

    if text[pos] == "(":
        sub, pos = _calculate(text, pos + 1, 0, "+")
        current = operate(current, op, sub)
        return _calculate(text, pos, current)

    if text[pos].isnumeric():
        current = operate(current, op, text[pos])
        return _calculate(text, pos + 1, current)

    if text[pos] == "*":
        sub, pos = _calculate(text, pos + 1, 0, "+")
        current = operate(current, "*", sub)
        return current, pos

    if text[pos] == "+":
        return _calculate(text, pos + 1, current, text[pos])


def calculate(text):
    return _calculate(text, 0, 0, "+")[0]


def parse_line(line):
    return calc_regex.findall(line)


def solve(input_text):
    return sum(calculate(parse_line(line)) for line in input_text)


if __name__ == '__main__':
    from shared.utils import get_input
    from timeit import default_timer as timer

    start = timer()

    input_path = Path(__file__).parent.joinpath("input.txt")
    input_text = get_input(input_path)
    solution = solve(input_text)
    print(solution)

    end = timer()
    print()
    print("-" * 80)
    print("Time elapsed: {:.3f}s".format(end - start))
