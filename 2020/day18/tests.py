#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pathlib import Path
import unittest

from shared.utils import get_input
from . import solution1, solution2


SOLUTION_DIR = Path(__file__).parent


class TestSolution(unittest.TestCase):
    module = None
    input_filename = "test_input.txt"
    expected = None

    def setUp(self):
        if self.module is None:
            raise NotImplementedError(
                "subclasses of TestSolution must provide module to test"
            )
        if self.expected is None:
            raise NotImplementedError(
                "subclasses of TestSolution must provide expected value"
            )
        self.input_path = SOLUTION_DIR.joinpath(self.input_filename)
        self.input_text = get_input(self.input_path)


class TestSolution1(TestSolution):
    module = solution1
    expected = 51 + 26 + 437 + 12240 + 13632

    def test_calculate_simple(self):
        self.assertEqual(self.module.calculate(["3", "+", "2"]), 5)
        self.assertEqual(self.module.calculate(["3", "*", "2"]), 6)

    def test_calculate_braces(self):
        self.assertEqual(self.module.calculate(["(", "3", ")"]), 3)
        self.assertEqual(self.module.calculate(["(", "3", "+", "2", ")"]), 5)
        self.assertEqual(self.module.calculate(["(", "3", "*", "2", ")"]), 6)

    def test_calculate_multiple(self):
        self.assertEqual(self.module.calculate(["3", "+", "2", "*", "5"]), 25)
        self.assertEqual(self.module.calculate(["3", "*", "2", "+", "4"]), 10)

    def test_braces_and_simple(self):
        self.assertEqual(self.module.calculate(["(", "3", ")", "+", "1"]), 4)
        self.assertEqual(self.module.calculate(["1", "+", "(", "3", ")"]), 4)
        self.assertEqual(self.module.calculate(
            ["(", "3", "+", "2", ")", "*", "5"]
        ), 25)
        self.assertEqual(self.module.calculate(
            ["5", "*", "(", "3", "+", "2", ")"]
        ), 25)
        self.assertEqual(self.module.calculate(
            ["(", "3", "*", "2", ")", "+", "4"]
        ), 10)
        self.assertEqual(self.module.calculate(
            ["4", "+", "(", "3", "*", "2", ")"]
        ), 10)

    def test_calculate(self):
        self.assertEqual(
            self.module.calculate(self.module.parse_line(self.input_text[0])),
            51,
        )
        self.assertEqual(
            self.module.calculate(self.module.parse_line(self.input_text[1])),
            26,
        )
        self.assertEqual(
            self.module.calculate(self.module.parse_line(self.input_text[2])),
            437,
        )
        self.assertEqual(
            self.module.calculate(self.module.parse_line(self.input_text[3])),
            12240,
        )
        self.assertEqual(
            self.module.calculate(self.module.parse_line(self.input_text[4])),
            13632,
        )

    def test_solver(self):
        solution = self.module.solve(self.input_text)
        self.assertEqual(self.expected, solution)


class TestSolution2(TestSolution):
    module = solution2
    expected = 51 + 46 + 1445 + 669060 + 23340

    def test_calculate(self):
        self.assertEqual(
            self.module.calculate(self.module.parse_line(self.input_text[0])),
            51,
        )
        self.assertEqual(
            self.module.calculate(self.module.parse_line(self.input_text[1])),
            46,
        )
        self.assertEqual(
            self.module.calculate(self.module.parse_line(self.input_text[2])),
            1445,
        )
        self.assertEqual(
            self.module.calculate(self.module.parse_line(self.input_text[3])),
            669060,
        )
        self.assertEqual(
            self.module.calculate(self.module.parse_line(self.input_text[4])),
            23340,
        )

    def test_solver(self):
        solution = self.module.solve(self.input_text)
        self.assertEqual(self.expected, solution)


if __name__ == '__main__':
    unittest.main()
