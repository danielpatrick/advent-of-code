use std::collections::{HashMap, HashSet};
use std::fs;

type ValidatorSequence = Vec<u8>;

enum ValidatorRule {
    Char(char),
    Sequence(ValidatorSequence),
    Either(ValidatorSequence, ValidatorSequence),
}

struct Validator {
    rules: HashMap<u8, ValidatorRule>,
}

impl Validator {
    fn new(s: &str) -> Validator {
        let rules = s.lines().map(|line| Validator::parse_rule(line)).collect();

        Validator { rules }
    }

    fn parse_rule(line: &str) -> (u8, ValidatorRule) {
        let mut line_iter = line.splitn(2, ": ");
        let key = line_iter.next().unwrap().parse().unwrap();
        let value_str = line_iter.next().unwrap();

        let value = if value_str.contains('|') {
            let mut sequences = value_str.splitn(2, " | ").map(|s| validator_sequence(s));
            ValidatorRule::Either(sequences.next().unwrap(), sequences.next().unwrap())
        } else {
            if value_str.contains('"') {
                ValidatorRule::Char(value_str.chars().nth(1).unwrap())
            } else {
                ValidatorRule::Sequence(validator_sequence(value_str))
            }
        };

        (key, value)
    }

    fn update_rule(&mut self, line: &str) {
        let (k, v) = Validator::parse_rule(line);
        self.rules.insert(k, v);
    }

    fn apply_rule(&self, message: &str, rule_number: u8) -> HashSet<String> {
        if message.len() == 0 {
            return HashSet::new();
        }

        match &self.rules[&rule_number] {
            ValidatorRule::Char(c) => {
                let mc = message.chars().next().unwrap();
                if mc == *c {
                    vec![mc.to_string()].into_iter().collect()
                } else {
                    HashSet::new()
                }
            }
            ValidatorRule::Sequence(s) => self.validate_sequence(message, s),
            ValidatorRule::Either(a, b) => {
                &self.validate_sequence(message, a) | &self.validate_sequence(message, b)
            }
        }
    }

    fn validate_sequence(&self, message: &str, sequence: &ValidatorSequence) -> HashSet<String> {
        let mut sub_matches: HashSet<String> = HashSet::new();
        sub_matches.insert("".to_string());

        sequence.iter().fold(sub_matches, |acc, sub_rule| {
            acc.iter().fold(HashSet::new(), |acc, sub_match| {
                &acc | &self
                    .apply_rule(&message[sub_match.len()..], *sub_rule)
                    .iter()
                    .map(|m| format!("{}{}", sub_match, m))
                    .collect()
            })
        })
    }

    fn validate(&self, message: &str) -> bool {
        self.apply_rule(message, 0).contains(&message.to_string())
    }
}

fn parse_input(filename: &str) -> (String, Vec<String>) {
    let sections: Vec<String> = fs::read_to_string(filename)
        .expect("problem reading file")
        .split("\n\n")
        .map(|s| s.to_string())
        .collect();

    let messages = sections[1].lines().map(|s| s.to_string()).collect();

    (sections[0].to_owned(), messages)
}
fn validator_sequence(s: &str) -> ValidatorSequence {
    s.split(" ").map(|n| n.parse().unwrap()).collect()
}

fn solve_part1(validator: &Validator, messages: &Vec<String>) -> usize {
    messages.iter().filter(|m| validator.validate(m)).count()
}

fn solve_part2(validator: &mut Validator, messages: &Vec<String>) -> usize {
    validator.update_rule("8: 42 | 42 8");
    validator.update_rule("11: 42 31 | 42 11 31");

    messages.iter().filter(|m| validator.validate(m)).count()
}

fn main() {
    let (ruleset, messages) = parse_input("../input.txt");
    let mut validator = Validator::new(&ruleset);
    println!("{}", solve_part1(&validator, &messages));
    println!("{}", solve_part2(&mut validator, &messages));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_validate_sequence() {
        let validator = Validator::new("4: \"a\"\n5: \"b\"");

        assert_eq!(
            validator.validate_sequence("aba", &vec![4, 5, 4]),
            vec!["aba".to_string()].iter().cloned().collect()
        );
        assert_eq!(
            validator.validate_sequence("abb", &vec![4, 5, 4]),
            Vec::new().iter().cloned().collect()
        );
    }

    #[test]
    fn test_validate() {
        let (ruleset, messages) = parse_input("../test_input.txt");
        let validator = Validator::new(&ruleset);

        assert_eq!(validator.validate(&messages[0]), true);
        assert_eq!(validator.validate(&messages[1]), false);
        assert_eq!(validator.validate(&messages[2]), true);
        assert_eq!(validator.validate(&messages[3]), false);
        assert_eq!(validator.validate(&messages[4]), false);
    }

    #[test]
    fn test_solve_part1() {
        let (ruleset, messages) = parse_input("../test_input.txt");
        let validator = Validator::new(&ruleset);

        assert_eq!(solve_part1(&validator, &messages), 2);
    }

    #[test]
    fn test_solve_part2() {
        let (ruleset, messages) = parse_input("../test_input2.txt");
        let mut validator = Validator::new(&ruleset);

        assert_eq!(solve_part2(&mut validator, &messages), 12);
    }
}
