#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pathlib import Path

from .validator import Validator


def solve(input_text):
    i = input_text.index("")
    rules = input_text[:i]
    messages = input_text[i + 1:]

    validator = Validator(rules)
    validator.update_rule("8: 42 | 42 8")
    validator.update_rule("11: 42 31 | 42 11 31")

    return sum(1 for m in messages if validator.check_message(m))


if __name__ == '__main__':
    from shared.utils import get_input
    from timeit import default_timer as timer

    start = timer()

    input_path = Path(__file__).parent.joinpath("input.txt")
    input_text = get_input(input_path)
    solution = solve(input_text)
    print(solution)

    end = timer()
    print()
    print("-" * 80)
    print("Time elapsed: {:.3f}s".format(end - start))
