#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pathlib import Path
import unittest

from shared.utils import get_input
from . import solution1, solution2, validator


SOLUTION_DIR = Path(__file__).parent


class TestSolution(unittest.TestCase):
    module = None
    input_filename = "test_input.txt"
    expected = None

    def setUp(self):
        if self.module is None:
            raise NotImplementedError(
                "subclasses of TestSolution must provide module to test"
            )
        if self.expected is None:
            raise NotImplementedError(
                "subclasses of TestSolution must provide expected value"
            )
        self.input_path = SOLUTION_DIR.joinpath(self.input_filename)
        self.input_text = get_input(self.input_path)


class TestValidator(unittest.TestCase):
    module = validator

    def setUp(self):
        input1 = get_input(SOLUTION_DIR / "test_input.txt")
        self.validator1 = self.module.Validator(
            input1[:input1.index("")]
        )
        input2 = get_input(SOLUTION_DIR / "test_input2.txt")
        self.validator2 = self.module.Validator(
            input2[:input2.index("")]
        )
        self.validator2.update_rule("8: 42 | 42 8")
        self.validator2.update_rule("11: 42 31 | 42 11 31")

    def test_check_message(self):
        self.assertEqual(self.validator1.check_message("ababbb"), True)
        self.assertEqual(self.validator1.check_message("bababa"), False)
        self.assertEqual(self.validator1.check_message("abbbab"), True)
        self.assertEqual(self.validator1.check_message("aaabbb"), False)
        self.assertEqual(self.validator1.check_message("aaaabbb"), False)

    def test_apply_rule(self):
        self.assertEqual(self.validator2.apply_rule("b", 14), {"b"})
        self.assertEqual(self.validator2.apply_rule("b", 15), {"b"})
        self.assertEqual(self.validator2.apply_rule("bb", 20), {"bb"})
        self.assertIn(
            "bbabbbbaabaabba",
            self.validator2.apply_rule("bbabbbbaabaabba", 0)
        )

    def test_rule_checker_valid(self):
        valid = [
            "bbabbbbaabaabba",
            "babbbbaabbbbbabbbbbbaabaaabaaa",
            "aaabbbbbbaaaabaababaabababbabaaabbababababaaa",
            "bbbbbbbaaaabbbbaaabbabaaa",
            "bbbababbbbaaaaaaaabbababaaababaabab",
            "ababaaaaaabaaab",
            "ababaaaaabbbaba",
            "baabbaaaabbaaaababbaababb",
            "abbbbabbbbaaaababbbbbbaaaababb",
            "aaaaabbaabaaaaababaa",
            "aaaabbaabbaaaaaaabbbabbbaaabbaabaaa",
            "aabbbbbaabbbaaaaaabbbbbababaaaaabbaaabba",
        ]

        for message in valid:
            self.assertEqual(self.validator2.check_message(message), True)

    def test_rule_checker_invalid(self):
        invalid = [
            "abbbbbabbbaaaababbaabbbbabababbbabbbbbbabaaaa",
            "aaaabbaaaabbaaa",
            "babaaabbbaaabaababbaabababaaab",
        ]

        for message in invalid:
            self.assertEqual(self.validator2.check_message(message), False)


class TestSolution1(TestSolution):
    module = solution1
    expected = 2

    def test_solver(self):
        solution = self.module.solve(self.input_text)
        self.assertEqual(self.expected, solution)


class TestSolution2(TestSolution):
    module = solution2
    input_filename = "test_input2.txt"
    expected = 12

    def test_solver(self):
        solution = self.module.solve(self.input_text)
        self.assertEqual(self.expected, solution)


if __name__ == '__main__':
    unittest.main()
