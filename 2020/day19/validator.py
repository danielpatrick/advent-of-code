#!/usr/bin/env python
# -*- coding: utf-8 -*-


class Validator:
    def __init__(self, rules):
        self.rules = dict(self.parse_rule(s) for s in rules)

    def update_rule(self, line):
        k, v = self.parse_rule(line)
        self.rules[k] = v

    def parse_rule(self, line):
        k, v = line.split(": ")
        if '"' in v:
            v = v.replace('"', "")
        else:
            if "|" in v:
                v = v.split("|")
            else:
                v = [v]
            for i in range(len(v)):
                v[i] = v[i].strip().split()
                v[i] = [int(n) for n in v[i]]

        return int(k), v

    def apply_rule(self, message, n=0):
        if len(message) == 0:
            return set()

        rule = self.rules[n]

        if isinstance(rule, str):
            return {message[0]} if message[0] == rule else set()

        results = set()

        for or_rule in rule:
            or_matches = {""}

            for sub_rule in or_rule:
                sub_matches = set()

                for sub_match in or_matches:
                    matches = self.apply_rule(message[len(sub_match):], sub_rule)

                    sub_matches |= set(sub_match + match for match in matches)

                or_matches = sub_matches

            results |= or_matches

        return results

    def check_message(self, message):
        return message in self.apply_rule(message, 0)
