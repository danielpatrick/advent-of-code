#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .image import Image


class Grid:
    def __init__(self, tiles):
        self.tiles = tiles
        self.size = len(self.tiles)
        self.width = int(self.size ** 0.5)
        self.directions = {
            0: (-1, 0),
            1: (0, 1),
            2: (1, 0),
            3: (0, -1),
        }
        self.grid = {}
        self.placed = set()

    def solve(self):
        first_tile = self.tiles[0]
        self.grid[(0, 0)] = first_tile
        self.placed.add(first_tile.tile_number)

        return self.add_next_tile()

    def get_ranges(self):
        ys, xs = zip(*self.grid)
        ys = sorted(ys)
        xs = sorted(xs)
        return (ys[0], ys[-1]), (xs[0], xs[-1])

    def within_bounds(self):
        (y0, y1), (x0, x1) = self.get_ranges()

        return (y1 - y0 + 1) == (x1 - x0 + 1) == self.width

    def get_corners(self):
        (y0, y1), (x0, x1) = self.get_ranges()

        return (
            self.grid[(y0, x0)],
            self.grid[(y1, x0)],
            self.grid[(y0, x1)],
            self.grid[(y1, x1)],
        )

    def neighbour_tiles(self):
        options = {}

        for pos, tile in self.grid.items():
            for d, (dy, dx) in self.directions.items():
                y, x = pos[0] - dy, pos[1] - dx
                if (y, x) in self.grid:
                    continue

                if (y, x) not in options:
                    options[(y, x)] = []

                options[(y, x)].append((d, tile))

        return sorted(options.items(), key=lambda item: -len(item[1]))

    def add_next_tile(self):
        if len(self.placed) == len(self.tiles):
            return self.within_bounds()

        for tile in self.tiles:
            if tile.tile_number in self.placed:
                continue

            for pos, neighbours in self.neighbour_tiles():
                direction, existing_tile = neighbours.pop()

                num_positions = tile.count_positions(direction, existing_tile)
                for _ in range(num_positions):
                    fits = True

                    tile.rotate_to_fit(direction, existing_tile)

                    for direction, neighbour_tile in neighbours:
                        num_positions = tile.count_positions(
                            direction, neighbour_tile, False
                        )
                        if num_positions == 0:
                            fits = False
                            break

                    if not fits:
                        tile.reset()
                        continue  # try next rotation/flip

                    self.grid[pos] = tile
                    self.placed.add(tile.tile_number)
                    result = self.add_next_tile()

                    if result:
                        return True

                    tile.reset()
                    self.placed.remove(tile.tile_number)
                    del self.grid[pos]

        return False

    def get_completed_grid(self):
        (y0, y1), (x0, x1) = self.get_ranges()
        y_range = range(y0, y1 + 1)
        x_range = range(x0, x1 + 1)

        rows = []

        for i, y in enumerate(y_range):
            rows.append([])
            for x in x_range:
                if (y, x) not in self.grid:
                    raise Exception(
                        f"Unable to draw grid: missing tile {(y, x)}"
                    )

                rows[i].append(self.grid[(y, x)])

        return rows

    def create_image(self):
        image = []

        for row in self.get_completed_grid():
            image_rows = []

            for tile in row:
                for i, line in enumerate(tile.trimmed()):
                    if i > len(image_rows) - 1:
                        image_rows.append(line)
                    else:
                        image_rows[i] += line

            image.extend(image_rows)

        return Image(image)
