#!/usr/bin/env python
# -*- coding: utf-8 -*-


class Image:
    def __init__(self, rows):
        self.rows = rows
        self.pixels = [pixel for row in rows for pixel in row]
        self.size = len(self.pixels)
        self.width = len(rows[0])
        self.height = len(rows)
        self.sea_monster = [
            "..................#.",
            "#....##....##....###",
            ".#..#..#..#..#..#...",
        ]
        self.sea_monster_width = len(self.sea_monster[0])
        self.sea_monster_height = len(self.sea_monster)
        self.rotation = 0

    def is_sea_monster(self, i_offset, j_offset):
        matched = []

        for i in range(self.sea_monster_height):
            for j in range(self.sea_monster_width):
                if self.sea_monster[i][j] == "#":
                    if self.pixels[(i_offset + i) * self.width + j_offset + j] == "#":
                        matched.append((i_offset + i, j_offset + j))
                    else:
                        return []

        return matched

    def remove(self, pixels):
        for i, j in pixels:
            self.pixels[i * self.width + j] = "."

    def search(self):
        found = False

        for i in range(self.height - self.sea_monster_height):
            for j in range(self.width - self.sea_monster_width):
                matched = self.is_sea_monster(i, j)

                if matched:
                    found = True

                self.remove(matched)

        return found

    def flip(self):
        new_pixels = []

        for i in range(self.width):
            new_pixels.extend(
                self.pixels[i * self.width:i * self.width + self.width][::-1]
            )

        self.pixels = new_pixels

    def rotate(self):
        new_pixels = []

        for i in range(self.width):
            new_pixels.extend(self.pixels[i:self.size:self.width][::-1])

        self.pixels = new_pixels

    def rotate_and_flip(self):
        self.rotation += 1
        if self.rotation == 4:
            self.flip()
        elif self.rotation == 8:
            raise Exception("Tried all variants")
        else:
            self.rotate()

    def rough_water_count(self):
        return sum(1 for pixel in self.pixels if pixel == "#")

    def __str__(self):
        return "\n".join(
            "".join(self.pixels[i:i + self.width])
            for i in range(0, self.size, self.width)
        ) + "\n"
