use std::fmt;
use std::mem;
use std::ops::Index;

use crate::point::Point;
use bitvec::prelude::*;

pub trait BoolGrid: Index<usize, Output = bool> {
    fn len(&self) -> usize;
    fn height(&self) -> usize;
    fn width(&self) -> usize;
    fn bits(&self) -> &BitVec;
    fn true_char(&self) -> char;
    fn false_char(&self) -> char;

    fn format(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let true_char = self.true_char();
        let false_char = self.false_char();

        write!(
            f,
            "{}",
            (0..self.len())
                .flat_map(|i| {
                    if i != 0 && i % self.width() == 0 {
                        Some('\n')
                    } else {
                        None
                    }
                    .into_iter()
                    .chain(std::iter::once(match self[i] {
                        true => true_char,
                        false => false_char,
                    }))
                })
                .collect::<String>()
        )
    }
}

pub trait BoolGridMut: BoolGrid {
    fn rotations(&self) -> usize;
    fn is_flipped(&self) -> bool;
    fn rotate(&mut self, number_of_rotations: usize);
    fn flip(&mut self);
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct BitVecGrid {
    bits: BitVec,
    height: usize,
    width: usize,
    len: usize,
    true_char: char,
    false_char: char,
    rotations: usize,
    is_flipped: bool,
}

impl BitVecGrid {
    pub fn new(
        bits: BitVec,
        height: usize,
        width: usize,
        true_char: char,
        false_char: char,
    ) -> BitVecGrid {
        BitVecGrid {
            bits,
            height,
            width,
            len: height * width,
            true_char,
            false_char,
            rotations: 0,
            is_flipped: false,
        }
    }

    pub fn from_str(
        s: &str,
        height: usize,
        width: usize,
        true_char: char,
        false_char: char,
    ) -> BitVecGrid {
        let bits: BitVec = s
            .chars()
            .filter_map(|c| match c {
                c if c == true_char => Some(true),
                c if c == false_char => Some(false),
                _ => panic!(
                    "Invalid character {} does not match {} or {}",
                    c, true_char, false_char
                ),
            })
            .collect();

        Self::new(bits, height, width, true_char, false_char)
    }

    fn clone_from_bits(&self, new_bits: BitVec, swap_axes: bool) -> BitVecGrid {
        BitVecGrid::new(
            new_bits,
            if swap_axes { self.width } else { self.height },
            if swap_axes { self.height } else { self.width },
            self.true_char,
            self.false_char,
        )
    }

    pub fn rotated(&self, number_of_rotations: usize) -> BitVecGrid {
        let mut bits = self.bits.clone();
        let mut n = number_of_rotations;

        while n >= 2 {
            n -= 2;
            bits.reverse();
        }

        if n == 1 {
            bits = (0..self.len())
                .map(|i| {
                    bits[self.len() - ((i % self.height()) + 1) * self.width() + i / self.height()]
                })
                .collect();
        }
        self.clone_from_bits(bits, true)
    }

    pub fn flipped(&self) -> BitVecGrid {
        let bits = (0..self.len())
            .map(|i| {
                self.bits[(i / self.width()) * self.width() + self.width() - i % self.width() - 1]
            })
            .collect();
        self.clone_from_bits(bits, false)
    }

    pub fn get_bit(&self, index: usize) -> &bool {
        &self.bits[index]
    }

    pub fn set_bit(&mut self, index: usize, value: bool) {
        self.bits.set(index, value)
    }
}

impl BoolGrid for BitVecGrid {
    fn len(&self) -> usize {
        self.len
    }

    fn height(&self) -> usize {
        self.height
    }

    fn width(&self) -> usize {
        self.width
    }

    fn bits(&self) -> &BitVec {
        &self.bits
    }

    fn true_char(&self) -> char {
        self.true_char
    }

    fn false_char(&self) -> char {
        self.false_char
    }
}

impl BoolGridMut for BitVecGrid {
    fn rotate(&mut self, number_of_rotations: usize) {
        if number_of_rotations % 2 == 1 {
            mem::swap(&mut self.height, &mut self.width);
        }
        self.rotations += number_of_rotations;
        self.rotations %= 4;
    }

    fn flip(&mut self) {
        self.is_flipped = !self.is_flipped;
    }

    fn rotations(&self) -> usize {
        self.rotations
    }

    fn is_flipped(&self) -> bool {
        self.is_flipped
    }
}

impl fmt::Display for BitVecGrid {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.format(f)
    }
}

impl Index<usize> for BitVecGrid {
    type Output = bool;

    fn index(&self, index: usize) -> &Self::Output {
        self.index(Point::new(index / self.width(), index % self.width()))
    }
}

impl Index<Point> for BitVecGrid {
    type Output = bool;

    fn index(&self, index: Point) -> &Self::Output {
        let mut n = self.rotations();

        let (mut y, mut x, height, width) = if n % 2 == 1 {
            (
                self.width() - index.x - 1,
                index.y,
                self.width(),
                self.height(),
            )
        } else {
            (index.y, index.x, self.height(), self.width())
        };

        while n >= 2 {
            n -= 2;
            x = width - x - 1;
            y = height - y - 1;
        }

        if self.is_flipped() {
            x = width - x - 1;
        }

        &self.get_bit(y * width + x)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_index_square() {
        // ###
        // .##
        // .#.
        let mut g = BitVecGrid::from_str("###.##.#.", 3, 3, '#', '.');

        g.flip();
        // ###
        // ##.
        // .#.
        assert_eq!(format!("{}", g), "###\n##.\n.#.");

        g.rotate(1);
        // .##
        // ###
        // ..#
        assert_eq!(format!("{}", g), ".##\n###\n..#");
    }

    #[test]
    fn test_index_rect() {
        // ###.
        // ##.#
        // #.##
        let mut g = BitVecGrid::from_str("###.##.##.##", 3, 4, '#', '.');

        g.flip();
        // .###
        // #.##
        // ##.#
        assert_eq!(format!("{}", g), ".###\n#.##\n##.#");
        println!();

        g.rotate(1);
        // ##.
        // #.#
        // .##
        // ###
        assert_eq!(format!("{}", g), "##.\n#.#\n.##\n###");
    }
}
