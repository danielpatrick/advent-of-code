use std::cmp::Ordering;

#[derive(Debug, Hash, PartialEq, Eq, Clone, Copy)]
pub enum Direction {
    Up = 0,
    Right = 1,
    Down = 2,
    Left = 3,
}

impl Direction {
    pub fn opposite(&self) -> Direction {
        match &self {
            Direction::Up => Direction::Down,
            Direction::Right => Direction::Left,
            Direction::Down => Direction::Up,
            Direction::Left => Direction::Right,
        }
    }

    fn from_num(n: usize) -> Direction {
        match n % 4 {
            0 => Direction::Up,
            1 => Direction::Right,
            2 => Direction::Down,
            _ => Direction::Left,
        }
    }

    pub fn rotate_anticlockwise(&self, n: usize) -> Direction {
        Self::from_num(*self as usize + 4 - n)
    }

    pub fn iter<'a>() -> DirectionIter<'a> {
        DirectionIter {
            iter: [
                Direction::Up,
                Direction::Right,
                Direction::Down,
                Direction::Left,
            ]
            .iter(),
        }
    }
}

impl Ord for Direction {
    fn cmp(&self, other: &Self) -> Ordering {
        (*self as usize).cmp(&(*other as usize))
    }
}

impl PartialOrd for Direction {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

pub struct DirectionIter<'a> {
    iter: std::slice::Iter<'a, Direction>,
}

impl<'a> Iterator for DirectionIter<'a> {
    type Item = &'a Direction;

    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next()
    }
}
