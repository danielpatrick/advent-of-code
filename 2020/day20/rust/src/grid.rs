use std::cmp::Ordering;
use std::collections::HashMap;

use bitvec::prelude::*;

use crate::direction::Direction;
use crate::image::{Image, ToImage};
use crate::point::Point;
use crate::tile::{PlacedTile, Tile, TileEdge, TileNumber, Tiled};

pub struct Grid {
    width: usize,
    grid: HashMap<Point, PlacedTile>,
    tile_numbers: Vec<TileNumber>,
    tiles: HashMap<TileNumber, Tile>,
}

impl Grid {
    pub fn new(mut tiles: Vec<Tile>) -> Grid {
        let size = tiles.len();
        let tiles: HashMap<TileNumber, Tile> =
            tiles.drain(..).map(|t| (t.get_number(), t)).collect();
        let mut tile_numbers: Vec<TileNumber> = tiles.keys().cloned().collect();
        tile_numbers.sort_unstable();

        Grid {
            width: (size as f64).sqrt() as usize,
            grid: HashMap::new(),
            tile_numbers,
            tiles,
        }
    }

    pub fn solve(&mut self) {
        self.add_next_tile();
    }

    fn add_next_tile(&mut self) -> bool {
        if self.tiles.is_empty() {
            true
        } else {
            for idx in 0..self.tile_numbers.len() {
                let tile_number = self.tile_numbers[idx];

                if self.place_tile(tile_number) {
                    return true;
                }
            }

            false
        }
    }

    fn place_tile(&mut self, tile_number: TileNumber) -> bool {
        if self.grid.is_empty() {
            let pos = Point::new(0, 0);

            for _ in 0..8 {
                self.tiles
                    .get_mut(&tile_number)
                    .unwrap()
                    .rotate_and_or_flip();

                if self.try_to_place_tile(pos, tile_number) {
                    return true;
                }
            }
        } else {
            let neighbour_tiles = self.neighbour_tiles();

            for (pos, dirs_and_edges) in neighbour_tiles.iter() {
                if !self.tiles.contains_key(&tile_number) {
                    continue;
                }

                let (d, edge) = &dirs_and_edges[0];

                let num_positions = self.tiles[&tile_number].count_positions(*edge);

                for _ in 0..num_positions {
                    let mut fits = true;
                    self.tiles
                        .get_mut(&tile_number)
                        .unwrap()
                        .rotate_to_fit(*d, *edge);

                    for (d, edge) in &dirs_and_edges[1..] {
                        if !self.tiles[&tile_number].can_fit(*d, *edge) {
                            fits = false;
                            break;
                        }
                    }

                    if fits && self.try_to_place_tile(*pos, tile_number) {
                        return true;
                    }
                }
            }
        }

        false
    }

    fn try_to_place_tile(&mut self, pos: Point, tile_number: TileNumber) -> bool {
        self.grid
            .insert(pos, self.tiles.remove(&tile_number).unwrap().place_tile());

        if self.add_next_tile() {
            true
        } else {
            self.tiles
                .insert(tile_number, self.grid.remove(&pos).unwrap().reset());
            false
        }
    }

    pub fn get_corners(&self) -> [TileNumber; 4] {
        let end = self.width - 1;

        [
            self.grid[&Point::new(0, 0)].get_number(),
            self.grid[&Point::new(0, end)].get_number(),
            self.grid[&Point::new(end, 0)].get_number(),
            self.grid[&Point::new(end, end)].get_number(),
        ]
    }

    fn neighbour_tiles(&self) -> Vec<(Point, Vec<(Direction, TileEdge)>)> {
        let neighbours = self
            .grid
            .iter()
            .fold(HashMap::new(), |mut acc, (pos, tile)| {
                Direction::iter()
                    .filter_map(|d| pos.checked_sub(*d).map(|pos| (d, pos)))
                    .filter(|(_, pos)| {
                        pos.y < self.width && pos.x < self.width && !self.grid.contains_key(pos)
                    })
                    .for_each(|(d, pos)| {
                        acc.entry(pos)
                            .or_insert_with(Vec::new)
                            .push((*d, tile.get_edge(d.opposite())));
                    });
                acc
            });

        let mut neighbours_vec = neighbours
            .into_iter()
            .map(|(k, mut v)| {
                v.sort();
                (k, v)
            })
            .collect::<Vec<(Point, Vec<(Direction, TileEdge)>)>>();

        neighbours_vec.sort_by(|(a_pos, a_tile_dirs), (b_pos, b_tile_dirs)| {
            match b_tile_dirs.len().cmp(&a_tile_dirs.len()) {
                Ordering::Equal => a_pos.cmp(b_pos),
                lt_or_gt => lt_or_gt,
            }
        });

        neighbours_vec
    }
}

impl ToImage for Grid {
    fn to_image(&self) -> Image {
        let width = self.width * self.grid.get(&Point::new(0, 0)).unwrap().cropped_size();
        let mut bits = BitVec::new();

        for y in 0..self.width {
            let cropped_tiles: Vec<Vec<BitVec>> = (0..self.width)
                .map(move |x| self.grid.get(&Point::new(y, x)).unwrap().cropped())
                .collect();

            for i in 0..cropped_tiles[0].len() {
                for t in cropped_tiles.iter() {
                    bits.extend(t.get(i).unwrap());
                }
            }
        }

        Image::from_bits(bits, width, width, '#', '.')
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_add_next_tile() {
        let tiles = vec![
            Tile::from_str(1, "#######.."),
            Tile::from_str(2, "#...#..##"),
            Tile::from_str(3, "##..#...#"),
            Tile::from_str(4, "..###.#.."),
        ];
        let mut g = Grid::new(tiles);
        // We should get the following
        //
        // ### #..
        // ##. .#.
        // ##. .##
        //
        // ##. .##
        // .#. .#.
        // ..# #..
        g.add_next_tile();
        assert_eq!(g.grid[&Point::new(0, 0)].get_number(), 1);
        assert_eq!(g.grid[&Point::new(0, 1)].get_number(), 2);
        assert_eq!(g.grid[&Point::new(1, 0)].get_number(), 3);
        assert_eq!(g.grid[&Point::new(1, 1)].get_number(), 4);
    }

    #[test]
    fn test_neighbour_tiles() {
        let tiles = vec![
            Tile::from_str(1, "..#.#.#.."),
            Tile::from_str(2, "#...#...#"),
        ];
        let mut g = Grid::new(tiles);
        g.width = 3;

        g.grid
            .insert(Point::new(0, 0), g.tiles.remove(&1).unwrap().place_tile());
        g.grid
            .insert(Point::new(1, 1), g.tiles.remove(&2).unwrap().place_tile());

        let expected = vec![
            (
                Point::new(0, 1),
                vec![(Direction::Down, 4), (Direction::Left, 4)],
            ),
            (
                Point::new(1, 0),
                vec![(Direction::Up, 1), (Direction::Right, 1)],
            ),
            (Point::new(1, 2), vec![(Direction::Left, 1)]),
            (Point::new(2, 1), vec![(Direction::Up, 4)]),
        ];
        let result = g.neighbour_tiles();

        assert_eq!(result.len(), 4);
        assert_eq!(result, expected);
    }

    #[test]
    fn test_get_corners() {
        let mut g = Grid::new(Vec::new());
        g.width = 3;

        let t1 = Tile::from_str(1, "#").place_tile();
        let t2 = Tile::from_str(2, "#").place_tile();
        let t3 = Tile::from_str(3, "#").place_tile();
        let t4 = Tile::from_str(4, "#").place_tile();

        let t11 = Tile::from_str(11, "#").place_tile();
        let t12 = Tile::from_str(12, "#").place_tile();
        let t13 = Tile::from_str(13, "#").place_tile();
        let t14 = Tile::from_str(14, "#").place_tile();

        g.grid.insert(Point::new(0, 0), t1);
        g.grid.insert(Point::new(0, 2), t2);
        g.grid.insert(Point::new(2, 0), t3);
        g.grid.insert(Point::new(2, 2), t4);

        g.grid.insert(Point::new(1, 2), t11);
        g.grid.insert(Point::new(2, 1), t12);
        g.grid.insert(Point::new(1, 0), t13);
        g.grid.insert(Point::new(0, 1), t14);

        assert_eq!(
            g.get_corners().iter().cloned().collect::<Vec<TileNumber>>(),
            vec![1, 2, 3, 4]
        );
    }
}
