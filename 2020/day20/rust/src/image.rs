use std::fmt;

use bitvec::prelude::*;

use crate::bitvecgrid::{BitVecGrid, BoolGrid};
use crate::point::Point;

#[derive(Debug)]
pub struct Image {
    pixels: BitVecGrid,
}

impl Image {
    pub fn new(pixels: BitVecGrid) -> Image {
        Image { pixels }
    }

    pub fn from_bits(
        bits: BitVec,
        height: usize,
        width: usize,
        true_char: char,
        false_char: char,
    ) -> Image {
        Self::new(BitVecGrid::new(bits, height, width, true_char, false_char))
    }

    pub fn from_str(
        s: &str,
        height: usize,
        width: usize,
        true_char: char,
        false_char: char,
    ) -> Image {
        Self::new(BitVecGrid::from_str(
            s, height, width, true_char, false_char,
        ))
    }

    fn height(&self) -> usize {
        self.pixels.height()
    }

    fn width(&self) -> usize {
        self.pixels.width()
    }

    fn get_pixel(&self, x: usize, y: usize) -> bool {
        self.pixels[Point::new(y, x)]
    }

    fn set_pixel(&mut self, x: usize, y: usize, value: bool) {
        let pos = self.width() * y + x;

        self.pixels.set_bit(pos, value);
    }

    fn matching_pixels_at(&self, x: usize, y: usize, sub_image: &Image) -> Vec<(usize, usize)> {
        sub_image
            .pixels
            .bits()
            .iter_ones()
            .map(|i| (x + (i % sub_image.width()), y + (i / sub_image.width())))
            .filter(|(x, y)| self.get_pixel(*x, *y))
            .collect()
    }

    fn set_pixels_to_zero(&mut self, pixels: Vec<(usize, usize)>) {
        for (x, y) in pixels.iter() {
            self.set_pixel(*x, *y, false);
        }
    }

    pub fn count_ones(&self) -> usize {
        self.pixels.bits().count_ones()
    }

    fn remove_matches(&mut self, sub_image: &Image) -> bool {
        let num_ones = sub_image.count_ones();
        let vertical_pixels = self.height() - sub_image.height();
        let horizontal_pixels = self.width() - sub_image.width();

        let mut count = 0;

        for x in 0..horizontal_pixels {
            for y in 0..vertical_pixels {
                let matching_pixels = self.matching_pixels_at(x, y, &sub_image);
                if matching_pixels.len() == num_ones {
                    self.set_pixels_to_zero(matching_pixels);
                    count += 1;
                }
            }
        }

        count > 0
    }

    pub fn apply_filter(&mut self, sub_image: &Image) {
        sub_image
            .rotate_flip_iter()
            .any(|image| self.remove_matches(&image));
    }

    fn rotate_flip_iter(&self) -> ImageFlipRotateIter {
        ImageFlipRotateIter {
            next_image: Some(Image::new(self.pixels.clone())),
            variant_number: 0,
        }
    }

    fn flipped(&self) -> Image {
        Image::new(self.pixels.flipped())
    }

    fn rotated_clockwise(&self) -> Image {
        Image::new(self.pixels.rotated(1))
    }
}

pub struct ImageFlipRotateIter {
    next_image: Option<Image>,
    variant_number: usize,
}

impl Iterator for ImageFlipRotateIter {
    type Item = Image;

    fn next(&mut self) -> Option<Self::Item> {
        self.variant_number += 1;

        match self.variant_number {
            9 => None,
            8 => self.next_image.take(),
            4 => self
                .next_image
                .replace(self.next_image.as_ref().unwrap().flipped()),
            _ => self
                .next_image
                .replace(self.next_image.as_ref().unwrap().rotated_clockwise()),
        }
    }
}

pub trait ToImage {
    fn to_image(&self) -> Image;
}

impl fmt::Display for Image {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.pixels.format(f)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_iter() {
        // ###.
        // ##.#
        // #.##
        let image = Image::from_str("###.##.##.##", 3, 4, '#', '.');
        let mut iter = image.rotate_flip_iter();

        assert_eq!(format!("{}", iter.next().unwrap()), "###.\n##.#\n#.##");
        assert_eq!(format!("{}", iter.next().unwrap()), "###\n.##\n#.#\n##.");
        assert_eq!(format!("{}", iter.next().unwrap()), "##.#\n#.##\n.###");
        assert_eq!(format!("{}", iter.next().unwrap()), ".##\n#.#\n##.\n###");

        assert_eq!(format!("{}", iter.next().unwrap()), "##.\n#.#\n.##\n###");
        assert_eq!(format!("{}", iter.next().unwrap()), "#.##\n##.#\n###.");
        assert_eq!(format!("{}", iter.next().unwrap()), "###\n##.\n#.#\n.##");
        assert_eq!(format!("{}", iter.next().unwrap()), ".###\n#.##\n##.#");

        assert!(iter.next().is_none());
    }

    #[test]
    fn test_display() {
        let image = Image::from_str("##..#......#..##", 4, 4, '#', '.');

        assert_eq!(format!("{}", image), "##..\n#...\n...#\n..##");
    }
}
