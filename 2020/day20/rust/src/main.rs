use std::fs;

mod bitvecgrid;
mod direction;
mod grid;
mod image;
mod point;
mod tile;

use grid::Grid;
use image::{Image, ToImage};
use tile::{Tile, TileNumber};

fn parse_input(filename: &str) -> Vec<Tile> {
    fs::read_to_string(filename)
        .expect("problem reading file")
        .split("\n\n")
        .map(|s| {
            let mut s_iter = s.lines();
            let num: TileNumber = s_iter
                .next()
                .unwrap()
                .trim_start_matches("Tile ")
                .trim_end_matches(':')
                .parse()
                .unwrap();
            Tile::from_str(num, &s_iter.collect::<String>())
        })
        .collect()
}

fn solve_part1(grid: &mut Grid) -> usize {
    grid.solve();

    grid.get_corners().iter().map(|n| *n as usize).product()
}

fn solve_part2(grid: &Grid) -> usize {
    let mut image = grid.to_image();

    let sea_monster = Image::from_str(
        "..................#.#....##....##....###.#..#..#..#..#..#...",
        3,
        20,
        '#',
        '.',
    );

    image.apply_filter(&sea_monster);

    image.count_ones()
}

fn main() {
    let tiles = parse_input("../input.txt");
    let mut grid = Grid::new(tiles);

    println!("{}", solve_part1(&mut grid));
    println!("{}", solve_part2(&grid));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_solve_part1() {
        let tiles = parse_input("../test_input.txt");
        let mut grid = Grid::new(tiles);

        assert_eq!(solve_part1(&mut grid), 20899048083289);
    }

    #[test]
    fn test_solve_part2() {
        let tiles = parse_input("../test_input.txt");
        let mut grid = Grid::new(tiles);
        grid.solve();

        assert_eq!(solve_part2(&grid), 273);
    }
}
