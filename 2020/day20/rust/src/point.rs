use std::ops::{Add, Sub};

use crate::direction::{Direction, Direction::*};

#[derive(Debug, Hash, PartialEq, Eq, Clone, Copy, Ord, PartialOrd)]
pub struct Point {
    pub y: usize,
    pub x: usize,
}

impl Point {
    pub fn new(y: usize, x: usize) -> Point {
        Point { y, x }
    }

    pub fn checked_sub(&self, rhs: Direction) -> Option<Point> {
        let min_y = if rhs == Down { 1 } else { 0 };
        let max_y = if rhs == Up {
            usize::MAX - 1
        } else {
            usize::MAX
        };
        let min_x = if rhs == Right { 1 } else { 0 };
        let max_x = if rhs == Left {
            usize::MAX - 1
        } else {
            usize::MAX
        };

        if self.y >= min_y && self.y <= max_y && self.x >= min_x && self.x <= max_x {
            Some(*self - rhs)
        } else {
            None
        }
    }
}

impl Add<Direction> for Point {
    type Output = Point;

    fn add(self, other: Direction) -> Point {
        let x = match other {
            Up | Down => self.x,
            Left => self.x + 1,
            Right => self.x - 1,
        };
        let y = match other {
            Left | Right => self.y,
            Up => self.y + 1,
            Down => self.y - 1,
        };
        Point { y, x }
    }
}

impl Sub<Direction> for Point {
    type Output = Point;

    fn sub(self, other: Direction) -> Point {
        let x = match other {
            Up | Down => self.x,
            Left => self.x + 1,
            Right => self.x - 1,
        };
        let y = match other {
            Left | Right => self.y,
            Up => self.y + 1,
            Down => self.y - 1,
        };
        Point { y, x }
    }
}

impl Sub<Point> for Point {
    type Output = Point;

    fn sub(self, other: Point) -> Point {
        Point::new(self.y - other.y, self.x - other.x)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_cmp() {
        assert!(Point::new(0, 0) < Point::new(0, 1));
        assert!(Point::new(0, 1) == Point::new(0, 1));
        assert!(Point::new(0, 1) < Point::new(1, 0));
        assert!(Point::new(1, 2) > Point::new(1, 1));
    }
}
