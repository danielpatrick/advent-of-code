use std::cmp::Ordering;
use std::fmt;
use std::ops::Index;

use bitvec::prelude::*;

use crate::bitvecgrid::{BitVecGrid, BoolGrid, BoolGridMut};
use crate::direction::Direction;
use crate::point::Point;

pub type TileNumber = u16;
pub type TileEdge = u16;

pub trait Tiled {
    fn get_number(&self) -> TileNumber;
    fn get_edge(&self, direction: Direction) -> TileEdge;
}

trait SquareEdges: Index<usize> {
    const TOP_FN: fn(usize, usize) -> usize = |i, _| i;
    const RIGHT_FN: fn(usize, usize) -> usize = |i, width| i * width + width - 1;
    const BOTTOM_FN: fn(usize, usize) -> usize = |i, width| i + width * (width - 1); // needs to be reversed for regular bottom edge
    const LEFT_FN: fn(usize, usize) -> usize = |i, width| i * width; // needs to be reversed for regular left edge

    fn edge_bits(&self, reverse: bool, f: fn(usize, usize) -> usize) -> TileEdge;

    fn top_edge(&self) -> TileEdge;
    fn right_edge(&self) -> TileEdge;
    fn bottom_edge(&self) -> TileEdge;
    fn left_edge(&self) -> TileEdge;

    fn reversed_top_edge(&self) -> TileEdge;
    fn reversed_right_edge(&self) -> TileEdge;
    fn reversed_bottom_edge(&self) -> TileEdge;
    fn reversed_left_edge(&self) -> TileEdge;
}

impl SquareEdges for BitVecGrid {
    fn edge_bits(&self, reverse: bool, f: fn(usize, usize) -> usize) -> TileEdge {
        let mut edge: BitVec = (0..self.width())
            .map(|i| self[f(i, self.width())])
            .collect();

        if reverse {
            edge.reverse();
        }

        edge.load()
    }

    fn top_edge(&self) -> TileEdge {
        self.edge_bits(false, Self::TOP_FN)
    }

    fn right_edge(&self) -> TileEdge {
        self.edge_bits(false, Self::RIGHT_FN)
    }

    fn bottom_edge(&self) -> TileEdge {
        self.edge_bits(true, Self::BOTTOM_FN)
    }

    fn left_edge(&self) -> TileEdge {
        self.edge_bits(true, Self::LEFT_FN)
    }

    fn reversed_top_edge(&self) -> TileEdge {
        self.edge_bits(true, Self::TOP_FN)
    }

    fn reversed_right_edge(&self) -> TileEdge {
        self.edge_bits(true, Self::RIGHT_FN)
    }

    fn reversed_bottom_edge(&self) -> TileEdge {
        self.edge_bits(false, Self::BOTTOM_FN)
    }

    fn reversed_left_edge(&self) -> TileEdge {
        self.edge_bits(false, Self::LEFT_FN)
    }
}

#[derive(Debug, PartialEq, Eq)]
pub struct InnerTile {
    cells: BitVecGrid,
    number: TileNumber,
    top_edge: TileEdge,
    right_edge: TileEdge,
    bottom_edge: TileEdge,
    left_edge: TileEdge,
    reversed_top_edge: TileEdge,
    reversed_right_edge: TileEdge,
    reversed_bottom_edge: TileEdge,
    reversed_left_edge: TileEdge,
    rotations: usize,
    is_flipped: bool,
}

impl Index<usize> for InnerTile {
    type Output = bool;

    fn index(&self, index: usize) -> &Self::Output {
        &self.cells[index]
    }
}

impl BoolGrid for InnerTile {
    fn len(&self) -> usize {
        self.cells.len()
    }

    fn width(&self) -> usize {
        self.cells.width()
    }

    fn height(&self) -> usize {
        self.cells.height()
    }

    fn bits(&self) -> &BitVec {
        &self.cells.bits()
    }

    fn true_char(&self) -> char {
        '#'
    }

    fn false_char(&self) -> char {
        '.'
    }
}

impl BoolGridMut for InnerTile {
    fn rotations(&self) -> usize {
        self.rotations
    }

    fn is_flipped(&self) -> bool {
        self.is_flipped
    }

    fn rotate(&mut self, number_of_rotations: usize) {
        self.rotations += number_of_rotations;
        self.rotations %= 4;

        self.cells.rotate(number_of_rotations);
    }

    fn flip(&mut self) {
        self.is_flipped = !self.is_flipped;

        self.cells.flip();
    }
}

#[derive(Debug, PartialEq, Eq)]
pub struct Tile {
    inner: InnerTile,
}

impl Tile {
    pub fn new(n: TileNumber, cells: BitVecGrid, rotations: usize, is_flipped: bool) -> Self {
        Self {
            inner: InnerTile {
                number: n,
                top_edge: cells.top_edge(),
                right_edge: cells.right_edge(),
                bottom_edge: cells.bottom_edge(),
                left_edge: cells.left_edge(),
                reversed_top_edge: cells.reversed_top_edge(),
                reversed_right_edge: cells.reversed_right_edge(),
                reversed_bottom_edge: cells.reversed_bottom_edge(),
                reversed_left_edge: cells.reversed_left_edge(),
                cells,
                rotations,
                is_flipped,
            },
        }
    }

    pub fn from_str(n: TileNumber, s: &str) -> Self {
        let width = (s.len() as f64).sqrt() as usize;

        let cells = BitVecGrid::from_str(s, width, width, '#', '.');

        Self::new(n, cells, 0, false)
    }

    pub fn can_fit(&self, direction: Direction, other_edge: TileEdge) -> bool {
        self.get_edge(direction) == other_edge
    }

    pub fn count_positions(&self, other_edge: TileEdge) -> usize {
        Direction::iter()
            .map(|d| {
                [self.get_edge(*d), self.get_reversed_edge(*d)]
                    .iter()
                    .filter(|&edge| edge == &other_edge)
                    .count()
            })
            .sum()
    }

    pub fn rotate_and_or_flip(&mut self) {
        if self.inner.rotations() == 0 {
            self.inner.flip();
        }
        self.inner.rotate(1);
    }

    pub fn rotate_to_fit(&mut self, direction: Direction, other_edge: TileEdge) {
        self.rotate_and_or_flip();

        while self.get_edge(direction) != other_edge {
            self.rotate_and_or_flip();
        }
    }

    fn get_reversed_edge(&self, direction: Direction) -> TileEdge {
        match (
            direction.rotate_anticlockwise(self.inner.rotations()),
            self.inner.is_flipped(),
        ) {
            (Direction::Up, false) => self.inner.reversed_top_edge,
            (Direction::Right, false) => self.inner.reversed_right_edge,
            (Direction::Down, false) => self.inner.reversed_bottom_edge,
            (Direction::Left, false) => self.inner.reversed_left_edge,
            (Direction::Up, true) => self.inner.top_edge,
            (Direction::Right, true) => self.inner.left_edge,
            (Direction::Down, true) => self.inner.bottom_edge,
            (Direction::Left, true) => self.inner.right_edge,
        }
    }

    pub fn place_tile(self) -> PlacedTile {
        PlacedTile { inner: self.inner }
    }
}

impl Tiled for Tile {
    fn get_number(&self) -> TileNumber {
        self.inner.number
    }

    fn get_edge(&self, direction: Direction) -> TileEdge {
        match (
            direction.rotate_anticlockwise(self.inner.rotations()),
            self.inner.is_flipped(),
        ) {
            (Direction::Up, false) => self.inner.top_edge,
            (Direction::Right, false) => self.inner.right_edge,
            (Direction::Down, false) => self.inner.bottom_edge,
            (Direction::Left, false) => self.inner.left_edge,
            (Direction::Up, true) => self.inner.reversed_top_edge,
            (Direction::Right, true) => self.inner.reversed_left_edge,
            (Direction::Down, true) => self.inner.reversed_bottom_edge,
            (Direction::Left, true) => self.inner.reversed_right_edge,
        }
    }
}

impl fmt::Display for Tile {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.inner.cells.format(f)
    }
}

impl Ord for Tile {
    fn cmp(&self, other: &Self) -> Ordering {
        self.inner.number.cmp(&other.inner.number)
    }
}

impl PartialOrd for Tile {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

#[derive(Debug, PartialEq, Eq)]
pub struct PlacedTile {
    inner: InnerTile,
}

impl PlacedTile {
    pub fn reset(self) -> Tile {
        Tile { inner: self.inner }
    }

    pub fn cropped_size(&self) -> usize {
        self.inner.width() - 2
    }

    pub fn cropped(&self) -> Vec<BitVec> {
        (1..(self.inner.height() - 1))
            .map(|y| {
                (1..(self.inner.width() - 1))
                    .map(|x| self.inner.cells[Point::new(y, x)])
                    .collect()
            })
            .collect()
    }
}

impl Tiled for PlacedTile {
    fn get_number(&self) -> TileNumber {
        self.inner.number
    }

    fn get_edge(&self, direction: Direction) -> TileEdge {
        match (
            direction.rotate_anticlockwise(self.inner.rotations()),
            self.inner.is_flipped(),
        ) {
            (Direction::Up, false) => self.inner.reversed_top_edge,
            (Direction::Right, false) => self.inner.reversed_right_edge,
            (Direction::Down, false) => self.inner.reversed_bottom_edge,
            (Direction::Left, false) => self.inner.reversed_left_edge,
            (Direction::Up, true) => self.inner.top_edge,
            (Direction::Right, true) => self.inner.left_edge,
            (Direction::Down, true) => self.inner.bottom_edge,
            (Direction::Left, true) => self.inner.right_edge,
        }
    }
}

impl fmt::Display for PlacedTile {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.inner.cells.format(f)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_rotate_anticlockwise() {
        assert_eq!(Direction::Up.rotate_anticlockwise(1), Direction::Left);
        assert_eq!(Direction::Up.rotate_anticlockwise(2), Direction::Down);
        assert_eq!(Direction::Up.rotate_anticlockwise(3), Direction::Right);
        assert_eq!(Direction::Up.rotate_anticlockwise(4), Direction::Up);
    }

    #[test]
    fn test_rotate() {
        let mut t = Tile::from_str(1, "....###.#");
        t.inner.rotate(1);
        assert_eq!(t.to_string(), "#..\n.#.\n##.");
    }

    #[test]
    fn test_flip() {
        let mut t = Tile::from_str(1, "....###.#");
        t.inner.flip();
        assert_eq!(t.to_string(), "...\n##.\n#.#");
    }

    #[test]
    fn test_get_edge() {
        let t1 = Tile::from_str(1, ".##...#.#");
        assert_eq!(t1.get_edge(Direction::Up), 6);
        assert_eq!(t1.get_edge(Direction::Right), 5);
        assert_eq!(t1.get_edge(Direction::Down), 5);
        assert_eq!(t1.get_edge(Direction::Left), 1);
    }

    #[test]
    fn test_get_edge_rotated() {
        let mut t1 = Tile::from_str(1, ".##...#.#");
        t1.inner.rotate(1);
        assert_eq!(t1.get_edge(Direction::Up), 1);
        assert_eq!(t1.get_edge(Direction::Right), 6);
        assert_eq!(t1.get_edge(Direction::Down), 5);
        assert_eq!(t1.get_edge(Direction::Left), 5);
    }

    #[test]
    fn test_get_edge_flipped() {
        let mut t1 = Tile::from_str(1, ".##...#.#");
        t1.inner.flip();
        assert_eq!(t1.get_edge(Direction::Up), 3);
        assert_eq!(t1.get_edge(Direction::Right), 4);
        assert_eq!(t1.get_edge(Direction::Down), 5);
        assert_eq!(t1.get_edge(Direction::Left), 5);
    }

    #[test]
    fn test_get_edge_flipped_rotated() {
        let mut t1 = Tile::from_str(1, ".##...#.#");
        t1.inner.flip();
        t1.inner.rotate(1);
        assert_eq!(t1.get_edge(Direction::Up), 5);
        assert_eq!(t1.get_edge(Direction::Right), 3);
        assert_eq!(t1.get_edge(Direction::Down), 4);
        assert_eq!(t1.get_edge(Direction::Left), 5);
    }

    #[test]
    fn test_rotate_to_fit() {
        // t1
        // .##
        // ...
        // #.#
        //
        // t2
        // .##
        // ###
        // #.#
        let mut t1 = Tile::from_str(1, ".##...#.#");
        let t2 = Tile::from_str(2, ".######.#").place_tile();
        t1.rotate_to_fit(Direction::Right, t2.get_edge(Direction::Left));
        assert_eq!(t1.to_string(), "#..\n..#\n#.#");
    }

    #[test]
    fn test_rotate_to_fit_against_reversed_tile() {
        // t1
        // .##
        // ...
        // #.#
        //
        // t1 after flip & 3 x rotate
        // ..#
        // #..
        // #.#
        //
        // t2
        // ##.
        // .##
        // ###
        //
        // t2 after flip & rotate
        // ##.
        // ###
        // #.#
        let mut t1 = Tile::from_str(1, ".##...#.#");
        let mut t2 = Tile::from_str(2, "##..#####");
        t2.rotate_and_or_flip();
        let t2_placed = t2.place_tile();
        t1.rotate_to_fit(Direction::Left, t2_placed.get_edge(Direction::Right));
        assert_eq!(t1.to_string(), "..#\n#..\n#.#");
    }

    #[test]
    fn test_rotate_to_fit_reverse() {
        let mut t1 = Tile::from_str(1, ".##...#.#");
        let t2 = Tile::from_str(2, "##.######").place_tile();
        t1.rotate_to_fit(Direction::Left, t2.get_edge(Direction::Right));
        assert_eq!(t1.to_string(), "..#\n#..\n#.#");
    }

    #[test]
    fn test_placed_rotated_tile() {
        // #..
        // ..#
        // #.#
        let mut tile = Tile::from_str(1, ".##...#.#");
        tile.inner.rotate(1);
        let placed_tile = tile.place_tile();

        assert_eq!(placed_tile.to_string(), "#..\n..#\n#.#");
        assert_eq!(placed_tile.get_edge(Direction::Up), 4);
        assert_eq!(placed_tile.get_edge(Direction::Right), 3);
        assert_eq!(placed_tile.get_edge(Direction::Down), 5);
        assert_eq!(placed_tile.get_edge(Direction::Left), 5);
    }

    #[test]
    fn test_placed_flipped_tile() {
        // ##.
        // ...
        // #.#
        let mut tile = Tile::from_str(1, ".##...#.#");
        tile.inner.flip();
        let placed_tile = tile.place_tile();

        assert_eq!(placed_tile.to_string(), "##.\n...\n#.#");
        assert_eq!(placed_tile.get_edge(Direction::Up), 6);
        assert_eq!(placed_tile.get_edge(Direction::Right), 1);
        assert_eq!(placed_tile.get_edge(Direction::Down), 5);
        assert_eq!(placed_tile.get_edge(Direction::Left), 5);
    }

    #[test]
    fn test_placed_flipped_rotated_tile() {
        // #.#
        // ..#
        // #..
        let mut tile = Tile::from_str(1, ".##...#.#");
        tile.inner.flip();
        tile.inner.rotate(1);
        let placed_tile = tile.place_tile();

        assert_eq!(placed_tile.to_string(), "#.#\n..#\n#..");
        assert_eq!(placed_tile.get_edge(Direction::Up), 5);
        assert_eq!(placed_tile.get_edge(Direction::Right), 6);
        assert_eq!(placed_tile.get_edge(Direction::Down), 1);
        assert_eq!(placed_tile.get_edge(Direction::Left), 5);
    }

    #[test]
    fn test_cropped_placed_tile() {
        // #..#
        // ..#.
        // .##.
        // #..#
        let tile = Tile::from_str(1, "#..#..#..##.#..#").place_tile();
        let cropped = tile.cropped();

        assert_eq!(cropped.len(), 2);
        assert_eq!(cropped[0], bitvec![0, 1]);
        assert_eq!(cropped[1], bitvec![1, 1]);
    }
}
