#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pathlib import Path
from math import prod

from .tiles import create_tiles
from .grid import Grid


def solve(input_text):
    tiles = create_tiles(input_text)
    grid = Grid(tiles)
    grid.solve()
    corner_tiles = grid.get_corners()

    return prod(t.tile_number for t in corner_tiles)


if __name__ == '__main__':
    from shared.utils import get_input
    from timeit import default_timer as timer

    start = timer()

    input_path = Path(__file__).parent.joinpath("input.txt")
    input_text = get_input(input_path)
    solution = solve(input_text)
    print(solution)

    end = timer()
    print()
    print("-" * 80)
    print("Time elapsed: {:.3f}s".format(end - start))
