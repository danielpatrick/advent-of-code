#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pathlib import Path

from .grid import Grid
from .tiles import create_tiles


def solve(input_text):
    tiles = create_tiles(input_text)
    grid = Grid(tiles)
    grid.solve()

    image = grid.create_image()

    while not image.search():
        image.rotate_and_flip()

    return image.rough_water_count()


if __name__ == '__main__':
    from shared.utils import get_input
    from timeit import default_timer as timer

    start = timer()

    input_path = Path(__file__).parent.joinpath("input.txt")
    input_text = get_input(input_path)
    solution = solve(input_text)
    print(solution)

    end = timer()
    print()
    print("-" * 80)
    print("Time elapsed: {:.3f}s".format(end - start))
