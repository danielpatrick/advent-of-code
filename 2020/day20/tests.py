#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pathlib import Path
import unittest

from shared.utils import get_input
from . import solution1, solution2
from .tiles import Tile
from .grid import Grid


SOLUTION_DIR = Path(__file__).parent


class TestSolution(unittest.TestCase):
    module = None
    input_filename = "test_input.txt"
    expected = None

    def setUp(self):
        if self.module is None:
            raise NotImplementedError(
                "subclasses of TestSolution must provide module to test"
            )
        if self.expected is None:
            raise NotImplementedError(
                "subclasses of TestSolution must provide expected value"
            )
        self.input_path = SOLUTION_DIR.joinpath(self.input_filename)
        self.input_text = get_input(self.input_path)


class TestTile(unittest.TestCase):
    def test_rotate(self):
        t = Tile(["Tile 1:", "...", ".##", "#.#"])
        t.rotate()
        self.assertEqual("".join(t.cells), "#...#.##.")
        t.rotate(2)
        self.assertEqual("".join(t.cells), ".##.#...#")
        t.rotate(1)
        self.assertEqual("".join(t.cells), "....###.#")
        t.rotate(3)
        self.assertEqual("".join(t.cells), ".##.#...#")

    def test_flip(self):
        t = Tile(["Tile 1:", "...", ".##", "#.#"])
        t.flip()
        self.assertEqual("".join(t.cells), "...##.#.#")

    def test_rotate_to_fit(self):
        text1 = ["Tile 1:", ".##", "...", "#.#"]
        text2 = ["Tile 2:", ".##", "###", "#.#"]

        t1 = Tile(text1)
        t2 = Tile(text2)

        t1.rotate_to_fit(1, t2)
        self.assertEqual(str(t1), "Tile 1:\n#..\n..#\n#.#\n")

    def test_rotate_to_fit_reverse(self):
        text1 = ["Tile 1:", ".##", "...", "#.#"]
        text2 = ["Tile 2:", "##.", "###", "###"]

        t1 = Tile(text1)
        t2 = Tile(text2)

        t1.rotate_to_fit(3, t2)
        self.assertEqual(str(t1), "Tile 1:\n..#\n#..\n#.#\n")


class TestGrid(unittest.TestCase):
    def test_neighbour_tiles(self):
        #    INITIAL GRID
        #
        #     -1  0  1  2
        #    -------------
        # -1 | .  .  .  .
        #  0 | .  a  .  .
        #  1 | .  .  b  .
        #  2 | .  .  .  .

        grid = Grid([])
        grid.grid = {
            (0, 0): "a",
            (1, 1): "b",
        }
        result = grid.neighbour_tiles()

        self.assertEqual(len(result), 6)
        self.assertIn(((0, -1), [(1, "a")]), result)
        self.assertIn(((-1, 0), [(2, "a")]), result)
        self.assertIn(((1, 2), [(3, "b")]), result)
        self.assertIn(((2, 1), [(0, "b")]), result)
        self.assertIn(((0, 1), [(3, "a"), (2, "b")]), result)
        self.assertIn(((1, 0), [(0, "a"), (1, "b")]), result)


class TestSolution1(TestSolution):
    module = solution1
    expected = 20899048083289

    def test_solver(self):
        solution = self.module.solve(self.input_text)
        self.assertEqual(self.expected, solution)


class TestSolution2(TestSolution):
    module = solution2
    expected = 273

    def test_solver(self):
        solution = self.module.solve(self.input_text)
        self.assertEqual(self.expected, solution)


if __name__ == '__main__':
    unittest.main()
