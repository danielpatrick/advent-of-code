class Tile:
    def __init__(self, lines):
        self.original = [c for line in lines[1:] for c in line]
        self.size = len(self.original)
        self.width = int(self.size ** 0.5)
        self.tile_number = int(lines[0].split()[1].replace(":", ""))
        self.reset()

    def reset(self):
        self.fixed = False
        self._edges = None
        self.cells = self.original[:]

    @property
    def top_edge(self):
        return "".join(self.cells[0:self.width])

    @property
    def right_edge(self):
        return "".join(self.cells[self.width - 1::self.width])

    @property
    def bottom_edge(self):
        return "".join(
            self.cells[self.size:self.size - self.width - 1:-1]
        )

    @property
    def left_edge(self):
        return "".join(
            self.cells[self.size - self.width::-self.width]
        )

    @property
    def edges(self):
        if self._edges is None:
            self._edges = [
                self.top_edge,
                self.right_edge,
                self.bottom_edge,
                self.left_edge,
            ]
            self._edges += [
                edge[::-1] for edge in self._edges
            ][::-1]

        return self._edges

    def rotate(self, n=1):
        self._edges = None

        while n >= 2:
            n -= 2
            self.cells = self.cells[::-1]

        if n == 1:
            new_cells = []
            for i in range(self.width):
                new_cells.extend(self.cells[i:self.size:self.width][::-1])

            self.cells = new_cells

    def flip(self):
        self._edges = None

        new_cells = []
        for i in range(self.width):
            new_cells.extend(
                self.cells[i * self.width:i * self.width + self.width][::-1]
            )

        self.cells = new_cells

    def count_positions(self, direction, other, can_mutate=True):
        other_edge_dir = (direction + 2) % 4
        other_edge = other.edges[other_edge_dir][::-1]

        if not can_mutate:
            return 1 if self.edges[direction] == other_edge else 0

        return sum(1 for edge in self.edges if edge == other_edge)

    def get_edge_dir(self, edge):
        try:
            i = self.edges[:4].index(edge)
        except ValueError:
            return None

        return i

    def rotate_to_fit(self, direction, other):
        other_edge_dir = (direction + 2) % 4
        other_edge = other.edges[other_edge_dir][::-1]

        if other_edge in self.edges[4:]:
            self.flip()

        edge_dir = self.get_edge_dir(other_edge)

        rotations_needed = (direction + 4 - edge_dir) % 4
        self.rotate(rotations_needed)

    def trimmed(self):
        return [
            "".join(self.cells[i + 1:i + self.width - 1])
            for i in range(self.width, self.size - self.width, self.width)
        ]

    def __str__(self):
        return f"Tile {self.tile_number}:\n" + "\n".join(
            "".join(self.cells[i:i + self.width])
            for i in range(0, self.size, self.width)
        ) + "\n"


def create_tiles(input_text):
    tiles = []
    prev = 0

    while prev < len(input_text):
        try:
            i = input_text.index("", prev)
        except:
            i = len(input_text)

        t = Tile(input_text[prev:i])
        tiles.append(t)
        prev = i + 1

    return tiles
