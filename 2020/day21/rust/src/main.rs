use std::collections::{HashMap, HashSet};
use std::fs;

use itertools::Itertools;

type IngredientInfo = (Vec<String>, Vec<String>);

fn parse_input(filename: &str) -> (Vec<IngredientInfo>, Vec<String>) {
    let s = fs::read_to_string(filename).expect("problem reading file");

    let info: Vec<IngredientInfo> = s
        .lines()
        .map(|s| {
            let mut iter = s.splitn(2, " (contains ");
            (
                iter.next()
                    .unwrap()
                    .split_whitespace()
                    .map(|s| s.to_string())
                    .collect(),
                iter.next()
                    .unwrap()
                    .trim_end_matches(')')
                    .split(", ")
                    .map(|s| s.to_string())
                    .collect(),
            )
        })
        .collect();

    let ingredients = info
        .iter()
        .flat_map(|(ingredients, _)| ingredients.iter().map(|s| s.to_string()))
        .collect();

    (info, ingredients)
}

fn get_allergens(ing_info: &[IngredientInfo], ingredients: &[String]) -> HashMap<String, String> {
    let ingredients = ingredients
        .iter()
        .sorted()
        .dedup()
        .cloned()
        .collect::<Vec<String>>();

    let mut candidates = ing_info.iter().fold(HashMap::new(), |mut acc, (ing, all)| {
        for allergen in all.iter() {
            let entry = acc.entry(allergen).or_insert_with(|| ingredients.to_vec());
            *entry = entry.drain(..).filter(|s| ing.contains(s)).collect();
        }
        acc
    });

    let mut known = HashMap::new();
    let mut known_ingredients = HashSet::new();

    // This would probably have been much nicer with the unstable hash_drain_filter feature...
    while !candidates.is_empty() {
        candidates.values_mut().for_each(|v| {
            v.retain(|ing| !known_ingredients.contains(ing));
        });

        let mut keys_to_remove: Vec<String> = candidates
            .iter()
            .filter_map(|(k, v)| match v.len() {
                1 => Some(k.to_string()),
                _ => None,
            })
            .collect();

        for allergen in keys_to_remove.drain(..) {
            let ingredient = candidates.remove(&allergen).unwrap()[0].to_string();
            known.insert(ingredient.to_string(), allergen);
            known_ingredients.insert(ingredient);
        }
    }

    known
}

fn solve_part1(ingredients: &[String], allergens_map: &HashMap<String, String>) -> usize {
    ingredients
        .iter()
        .filter(|&ingredient| !allergens_map.contains_key(ingredient))
        .count()
}

fn solve_part2(allergens_map: &HashMap<String, String>) -> String {
    allergens_map
        .iter()
        .sorted_by(|(_, v1), (_, v2)| v1.cmp(&v2))
        .map(|(k, _)| k)
        .join(",")
}

fn main() {
    let (ing_info, ingredients) = parse_input("../input.txt");
    let allergens_map = get_allergens(&ing_info, &ingredients);

    println!("{}", solve_part1(&ingredients, &allergens_map));
    println!("{}", solve_part2(&allergens_map));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_solve_part1() {
        let (ing_info, ingredients) = parse_input("../test_input.txt");
        let allergens_map = get_allergens(&ing_info, &ingredients);
        assert_eq!(solve_part1(&ingredients, &allergens_map), 5);
    }

    #[test]
    fn test_solve_part2() {
        let (ing_info, ingredients) = parse_input("../test_input.txt");
        let allergens_map = get_allergens(&ing_info, &ingredients);
        assert_eq!(
            solve_part2(&allergens_map),
            "mxmxvkd,sqjhc,fvjkl".to_string()
        );
    }
}
