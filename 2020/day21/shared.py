#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
from itertools import combinations


def get_ingredients(lines):
    ingredients_regex = re.compile("^([a-z ]+) \(contains ([a-z, ]+)\)$")

    ingredients_lists = []

    for line in lines:
        ingredients, allergens = ingredients_regex.findall(line)[0]
        ingredients_lists.append(
            (
                ingredients.split(" "),
                allergens.split(", "),
            )
        )

    return ingredients_lists


def get_allergens(ingredients_lists):
    allergen_candidates = {}

    for ingredients, allergens in ingredients_lists:
        for allergen in allergens:
            if allergen not in allergen_candidates:
                allergen_candidates[allergen] = set(ingredients)
            else:
                allergen_candidates[allergen] &= set(ingredients)

    while any(len(s) > 1 for s in allergen_candidates.values()):
        for a1, a2 in combinations(allergen_candidates.keys(), 2):
            if len(allergen_candidates[a1]) == 1:
                allergen_candidates[a2] -= allergen_candidates[a1]
            elif len(allergen_candidates[a2]) == 1:
                allergen_candidates[a1] -= allergen_candidates[a2]

    return {k: next(iter(v)) for k, v in allergen_candidates.items()}
