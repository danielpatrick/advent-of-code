#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pathlib import Path

from .shared import get_ingredients, get_allergens


def solve(input_text):
    ingredients_lists = get_ingredients(input_text)
    allergens = get_allergens(ingredients_lists)

    return ",".join(allergens[k] for k in sorted(list(allergens.keys())))


if __name__ == '__main__':
    from shared.utils import get_input
    from timeit import default_timer as timer

    start = timer()

    input_path = Path(__file__).parent.joinpath("input.txt")
    input_text = get_input(input_path)
    solution = solve(input_text)
    print(solution)

    end = timer()
    print()
    print("-" * 80)
    print("Time elapsed: {:.3f}s".format(end - start))
