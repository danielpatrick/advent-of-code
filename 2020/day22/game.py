#!/usr/bin/env python
# -*- coding: utf-8 -*-


class Game:
    def __init__(self, hand1, hand2):
        self.players = (hand1, hand2)
        self.winner = None

    @classmethod
    def from_stringlist(cls, lines):
        player1 = []
        player2 = []
        current = player1

        for line in lines:
            if line.startswith("Player"):
                continue
            elif line == "":
                current = player2
            else:
                current.append(int(line))

        return cls(player1, player2)

    def play(self):
        while self.winner is None:
            self.play_round()

        return self.winner

    def play_round(self):
        cards = (self.players[0].pop(0), self.players[1].pop(0))

        round_winner = self.get_round_winner(cards)

        self.players[round_winner].extend(
            (cards[round_winner], cards[abs(round_winner - 1)])
        )

        if len(self.players[1]) == 0:
            self.winner = 0

        if len(self.players[0]) == 0:
            self.winner = 1

    def get_round_winner(self, cards):
        return cards.index(max(cards))

    def calculate_score(self, player):
        return sum(i * n for i, n in enumerate(self.players[player][::-1], 1))
