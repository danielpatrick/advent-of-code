use std::collections::{HashSet, VecDeque};
use std::fmt;
use std::fs;

type Card = usize;

struct Hand {
    cards: VecDeque<Card>,
}

impl Hand {
    fn from_str(s: &str) -> Hand {
        Hand {
            cards: s
                .lines()
                .map(|line| line.parse::<Card>().unwrap())
                .collect(),
        }
    }

    fn hand_size(&self) -> usize {
        self.cards.len()
    }

    fn next_card(&self) -> Option<Card> {
        self.cards.front().copied()
    }

    fn play_card(&mut self) -> Card {
        self.cards.pop_front().unwrap()
    }

    fn win_hand(&mut self, winning_card: Card, losing_card: Card) {
        self.cards.push_back(winning_card);
        self.cards.push_back(losing_card);
    }

    fn score(&self) -> usize {
        self.cards
            .iter()
            .rev()
            .enumerate()
            .map(|(i, card)| card * (i + 1))
            .sum()
    }

    fn get_cards(&self, num_cards: usize) -> Hand {
        let mut cards = self.cards.clone();
        cards.truncate(num_cards);

        Hand { cards }
    }
}

enum Player {
    One = 1,
    Two = 2,
}

impl fmt::Debug for Hand {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.cards.fmt(f)
    }
}

trait CardGame {
    fn get_hand(&self, player: Player) -> &Hand;
    fn get_mut_hand(&mut self, player: Player) -> &mut Hand;

    fn play(&mut self) -> Player {
        loop {
            if let Some(winner) = self.play_round() {
                return winner;
            }
        }
    }

    fn next_cards(&self) -> (Option<Card>, Option<Card>) {
        (
            self.get_hand(Player::One).next_card(),
            self.get_hand(Player::Two).next_card(),
        )
    }

    fn play_cards(&mut self) -> (usize, usize) {
        (
            self.get_mut_hand(Player::One).play_card(),
            self.get_mut_hand(Player::Two).play_card(),
        )
    }

    fn win_hand(&mut self, winner: Player, card1: Card, card2: Card) {
        match winner {
            Player::One => self.get_mut_hand(Player::One).win_hand(card1, card2),
            Player::Two => self.get_mut_hand(Player::Two).win_hand(card2, card1),
        }
    }

    fn play_round(&mut self) -> Option<Player> {
        match self.next_cards() {
            (_, None) => return Some(Player::One),
            (None, _) => return Some(Player::Two),
            _ => {
                let (c1, c2) = self.play_cards();
                let winner = self.decide_round_winner(c1, c2);
                self.win_hand(winner, c1, c2);
            }
        }

        None
    }

    fn decide_round_winner(&mut self, card1: Card, card2: Card) -> Player {
        if card1 > card2 {
            Player::One
        } else {
            Player::Two
        }
    }

    fn calculate_score(&self, player: Player) -> usize {
        self.get_hand(player).score()
    }
}

struct Game {
    player1: Hand,
    player2: Hand,
}

impl Game {
    fn new(player1: Hand, player2: Hand) -> Game {
        Game { player1, player2 }
    }
}

impl CardGame for Game {
    fn get_hand(&self, player: Player) -> &Hand {
        match player {
            Player::One => &self.player1,
            Player::Two => &self.player2,
        }
    }

    fn get_mut_hand(&mut self, player: Player) -> &mut Hand {
        match player {
            Player::One => &mut self.player1,
            Player::Two => &mut self.player2,
        }
    }
}

struct RecursiveGame {
    player1: Hand,
    player2: Hand,
    past_hands: HashSet<String>,
}

impl RecursiveGame {
    fn new(player1: Hand, player2: Hand) -> RecursiveGame {
        RecursiveGame {
            player1,
            player2,
            past_hands: HashSet::new(),
        }
    }
}

impl CardGame for RecursiveGame {
    fn get_hand(&self, player: Player) -> &Hand {
        match player {
            Player::One => &self.player1,
            Player::Two => &self.player2,
        }
    }

    fn get_mut_hand(&mut self, player: Player) -> &mut Hand {
        match player {
            Player::One => &mut self.player1,
            Player::Two => &mut self.player2,
        }
    }

    fn play_round(&mut self) -> Option<Player> {
        let hand_hash = format!("{:?}{:?}", self.player1, self.player2);

        if self.past_hands.contains(&hand_hash) {
            Some(Player::One)
        } else {
            self.past_hands.insert(hand_hash);

            match self.next_cards() {
                (_, None) => Some(Player::One),
                (None, _) => Some(Player::Two),
                _ => {
                    let (c1, c2) = self.play_cards();
                    let winner = if self.player1.hand_size() >= c1 && self.player2.hand_size() >= c2
                    {
                        let mut subgame = RecursiveGame::new(
                            self.player1.get_cards(c1),
                            self.player2.get_cards(c2),
                        );

                        subgame.play()
                    } else {
                        self.decide_round_winner(c1, c2)
                    };

                    self.win_hand(winner, c1, c2);

                    None
                }
            }
        }
    }
}

fn parse_input(filename: &str) -> (Hand, Hand) {
    let input = fs::read_to_string(filename).expect("problem reading file");
    let mut iter = input.splitn(2, "\n\nPlayer 2:\n");

    (
        Hand::from_str(iter.next().unwrap().trim_start_matches("Player 1:\n")),
        Hand::from_str(iter.next().unwrap()),
    )
}

fn solve_part1(filename: &str) -> usize {
    let (hand1, hand2) = parse_input(filename);

    let mut game = Game::new(hand1, hand2);

    let winner = game.play();

    game.calculate_score(winner)
}

fn solve_part2(filename: &str) -> usize {
    let (hand1, hand2) = parse_input(filename);

    let mut game = RecursiveGame::new(hand1, hand2);

    let winner = game.play();

    game.calculate_score(winner)
}

fn main() {
    println!("{}", solve_part1("../input.txt"));
    println!("{}", solve_part2("../input.txt"));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_solve_part1() {
        assert_eq!(solve_part1("../test_input.txt"), 306);
    }

    #[test]
    fn test_solve_part2() {
        assert_eq!(solve_part2("../test_input.txt"), 291);
    }
}
