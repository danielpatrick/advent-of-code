#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pathlib import Path

from .game import Game


class RecursiveGame(Game):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.round_hands = set()

    def play_round(self):
        hand_hash = str(self.players)

        if hand_hash in self.round_hands:
            self.winner = 0
        else:
            self.round_hands.add(hand_hash)

            super().play_round()

    def get_round_winner(self, cards):
        if (
            len(self.players[0]) >= cards[0] and
            len(self.players[1]) >= cards[1]
        ):
            subgame = self.__class__(
                self.players[0][:cards[0]],
                self.players[1][:cards[1]],
            )

            return subgame.play()
        else:
            return super().get_round_winner(cards)


def solve(input_text):
    game = RecursiveGame.from_stringlist(input_text)

    winner = game.play()
    return game.calculate_score(winner)


if __name__ == '__main__':
    from shared.utils import get_input
    from timeit import default_timer as timer

    start = timer()

    input_path = Path(__file__).parent.joinpath("input.txt")
    input_text = get_input(input_path)
    solution = solve(input_text)
    print(solution)

    end = timer()
    print()
    print("-" * 80)
    print("Time elapsed: {:.3f}s".format(end - start))
