#!/usr/bin/env python
# -*- coding: utf-8 -*-


class CupsGame:
    def __init__(self, cups, length=None):
        self.current = int(cups[0]) - 1
        self.picked_up = []

        self.length = len(cups)
        self.cups = [0] * self.length

        prev_cup = int(cups[-1]) - 1

        for c in cups:
            next_cup = int(c) - 1
            self.cups[prev_cup] = next_cup
            prev_cup = next_cup

        if length is not None:
            temp = self.cups[prev_cup]
            self.cups[prev_cup] = self.length
            self.cups.extend(range(self.length + 1, length + 1))
            self.cups[-1] = temp

            self.length = length

    def play_round(self):
        i = self.cups[self.current]

        for _ in range(3):
            self.picked_up.append(i)
            i = self.cups[i]

        self.cups[self.current] = i

        destination = (self.current - 1) % self.length

        while destination in self.picked_up:
            destination -= 1
            destination %= self.length

        original = self.cups[destination]
        self.cups[destination] = self.picked_up[0]
        self.cups[self.picked_up[2]] = original

        self.picked_up = []
        self.current = self.cups[self.current]

    def __str__(self):
        i = 0
        result = [1]

        for _ in range(self.length - 1):
            i = self.cups[i]
            result.append(i + 1)

        return "".join(str(n) for n in result)

    def get_cups_after(self, value, num_cups):
        value -= 1
        result = []

        for _ in range(num_cups):
            value = self.cups[value]
            result.append(value + 1)

        return result
