use std::fmt;
use std::fs;

struct Game {
    cups: Vec<usize>,
    length: usize,
    current: usize,
    picked_up: Vec<usize>,
}

impl Game {
    fn from_file(filename: &str, min_length: Option<usize>) -> Game {
        let input = fs::read_to_string(filename)
            .expect("problem reading file")
            .trim()
            .chars()
            .map(|c| c.to_digit(10).unwrap() as usize)
            .collect::<Vec<usize>>();

        let current = input[0] - 1;
        let mut cups = input.clone();
        let mut length = input.len();
        let mut prev_cup = input[length - 1] - 1;

        for c in input.iter() {
            let next_cup = c - 1;
            cups[prev_cup] = next_cup;
            prev_cup = next_cup;
        }

        if let Some(l) = min_length {
            let temp = cups[prev_cup];
            cups[prev_cup] = length;
            cups.extend((length + 1)..(l + 1));
            cups[l - 1] = temp;

            length = l;
        }

        Game {
            cups,
            length,
            current,
            picked_up: Vec::new(),
        }
    }

    fn wrapped_sub(&self, lhs: &mut usize, rhs: usize) {
        while rhs > *lhs {
            *lhs += self.length;
        }

        *lhs = (*lhs - rhs) % self.length;
    }

    fn play_round(&mut self) {
        let mut i = self.cups[self.current];

        for _ in 0..3 {
            self.picked_up.push(i);
            i = self.cups[i];
        }

        let mut destination = self.current;
        self.wrapped_sub(&mut destination, 1);

        self.cups[self.current] = i;

        while self.picked_up.contains(&destination) {
            self.wrapped_sub(&mut destination, 1);
        }

        let original = self.cups[destination];
        self.cups[destination] = self.picked_up[0];
        self.cups[self.picked_up[2]] = original;

        self.picked_up.clear();

        self.current = self.cups[self.current];
    }

    fn get_two_cups_after(&self, value: usize) -> (usize, usize) {
        let v1 = self.cups[value - 1];
        let v2 = self.cups[v1];

        (v1 + 1, v2 + 1)
    }
}

impl fmt::Display for Game {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let numbers = (0..(self.length - 1)).fold(Vec::new(), |mut acc, _| {
            acc.push(self.cups[*acc.last().unwrap_or(&0)]);

            acc
        });

        write!(
            f,
            "{}",
            numbers
                .iter()
                .map(|n| (n + 1).to_string())
                .collect::<String>()
        )
    }
}

fn solve_part1(filename: &str, num_rounds: usize) -> String {
    let mut game = Game::from_file(filename, None);

    for _ in 0..num_rounds {
        game.play_round();
    }

    game.to_string()
}

fn solve_part2(filename: &str) -> usize {
    let mut game = Game::from_file(filename, Some(1_000_000));

    for _ in 0..10_000_000 {
        game.play_round();
    }

    let (one, two) = game.get_two_cups_after(1);

    one * two
}

fn main() {
    println!("{}", solve_part1("../input.txt", 100));
    println!("{}", solve_part2("../input.txt"));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_solve_part1() {
        assert_eq!(solve_part1("../test_input.txt", 10), "92658374".to_string());
        assert_eq!(
            solve_part1("../test_input.txt", 100),
            "67384529".to_string()
        );
    }

    #[test]
    fn test_solve_part2() {
        assert_eq!(solve_part2("../test_input.txt"), 149245887792);
    }
}
