#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pathlib import Path

from .game import CupsGame


def solve(input_text, num_rounds=10_000_000):
    cups = CupsGame(input_text[0], 1_000_000)

    for _ in range(num_rounds):
        cups.play_round()

    one, two = cups.get_cups_after(1, 2)

    return one * two


if __name__ == '__main__':
    from shared.utils import get_input
    from timeit import default_timer as timer

    start = timer()

    input_path = Path(__file__).parent.joinpath("input.txt")
    input_text = get_input(input_path)
    solution = solve(input_text)
    print(solution)

    end = timer()
    print()
    print("-" * 80)
    print("Time elapsed: {:.3f}s".format(end - start))
