use std::collections::{HashMap, HashSet};
use std::fs;

type Tile = (i32, i32);

struct TiledFloor {
    tiles: HashSet<Tile>,
}

impl TiledFloor {
    const DIRECTIONS: [Tile; 6] = [(-1, -1), (0, -1), (1, 0), (1, 1), (0, 1), (-1, 0)];

    fn parse_line(line: &str) -> Tile {
        let mut prev = None;
        let mut x = 0;
        let mut y = 0;

        line.chars().for_each(|c| match (prev, c) {
            (None, 'e') => x += 1,
            (None, 'w') => x -= 1,
            (None, c) => prev = Some(c),
            (Some('n'), 'w') => {
                x -= 1;
                y -= 1;
                prev = None;
            }
            (Some('n'), 'e') => {
                y -= 1;
                prev = None;
            }
            (Some('s'), 'e') => {
                x += 1;
                y += 1;
                prev = None;
            }
            (Some('s'), 'w') => {
                y += 1;
                prev = None;
            }
            (Some(c1), c2) => panic!("Invalid character combination {} + {}", c1, c2),
        });

        (x, y)
    }

    fn from_file(filename: &str) -> TiledFloor {
        let mut t = TiledFloor {
            tiles: HashSet::new(),
        };

        fs::read_to_string(filename)
            .expect("problem reading file")
            .lines()
            .for_each(|line| t.flip_tile(Self::parse_line(line)));

        t
    }

    fn count_black_tiles(&self) -> usize {
        self.tiles.len()
    }

    fn flip_tile(&mut self, t: Tile) {
        if self.tiles.contains(&t) {
            self.tiles.remove(&t);
        } else {
            self.tiles.insert(t);
        }
    }

    fn day(&mut self) {
        let mut flip_to_white = HashSet::new();
        let mut potentially_flip_to_black = HashMap::new();

        for (x, y) in self.tiles.iter() {
            let mut neighbour_count = 0;

            for (dx, dy) in Self::DIRECTIONS.iter() {
                let t = (x + dx, y + dy);
                if self.tiles.contains(&t) {
                    neighbour_count += 1;
                }

                potentially_flip_to_black
                    .entry(t)
                    .or_insert_with(HashSet::new)
                    .insert((*x, *y));
            }

            if ![1, 2].contains(&neighbour_count) {
                flip_to_white.insert((*x, *y));
            }
        }

        self.tiles = &self.tiles - &flip_to_white;

        for (tile, neighbours) in potentially_flip_to_black.iter() {
            if neighbours.len() == 2 {
                self.tiles.insert(*tile);
            }
        }
    }
}

fn solve_part1(filename: &str) -> usize {
    let floor = TiledFloor::from_file(filename);

    floor.count_black_tiles()
}

fn solve_part2(filename: &str) -> usize {
    let mut floor = TiledFloor::from_file(filename);

    for _ in 0..100 {
        floor.day();
    }

    floor.count_black_tiles()
}

fn main() {
    println!("{}", solve_part1("../input.txt"));
    println!("{}", solve_part2("../input.txt"));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_line() {
        assert_eq!(TiledFloor::parse_line("nwwswee"), (0, 0));
        assert_eq!(TiledFloor::parse_line("esew"), (1, 1));
    }

    #[test]
    fn test_solve_part1() {
        assert_eq!(solve_part1("../test_input.txt"), 10);
    }

    #[test]
    fn test_solve_part2() {
        assert_eq!(solve_part2("../test_input.txt"), 2208);
    }
}
