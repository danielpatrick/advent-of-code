#!/usr/bin/env python
# -*- coding: utf-8 -*-

from collections import defaultdict


class TiledFloor:
    def __init__(self, lines):
        self.directions = {
            "nw": (-1, -1),
            "ne": (0, -1),
            "e": (1, 0),
            "se": (1, 1),
            "sw": (0, 1),
            "w": (-1, 0),
        }
        self.tiles = set()

        for line in lines:
            self.parse_tiles(line)

    def parse_tiles(self, line):
        x = y = 0
        i = 0

        while i < len(line):
            direction = line[i]
            if direction in ("n", "s"):
                i += 1
                direction += line[i]

            x, y = self.get_next_tile(x, y, direction)

            i += 1

        self.flip_tile(x, y)

    def get_next_tile(self, x, y, direction):
        dx, dy = self.directions[direction]

        return x + dx, y + dy

    def flip_tile(self, x, y):
        if (x, y) in self.tiles:
            self.tiles.remove((x, y))
        else:
            self.tiles.add((x, y))

    def count_black_tiles(self):
        return len(self.tiles)

    def day(self):
        flip_to_white = set()
        potentially_flip_to_black = defaultdict(set)

        for x, y in self.tiles:
            neighbour_count = 0

            for dx, dy in self.directions.values():
                if (x + dx, y + dy) in self.tiles:
                    neighbour_count += 1

                potentially_flip_to_black[(x + dx, y + dy)].add((x, y))

            if neighbour_count not in (1, 2):
                flip_to_white.add((x, y))

        for x, y in flip_to_white:
            self.tiles.remove((x, y))

        for (x, y), neighbours in potentially_flip_to_black.items():
            if len(neighbours) == 2:
                self.tiles.add((x, y))
