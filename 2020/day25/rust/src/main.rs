use std::fs;

const DIVISOR: usize = 20201227;

struct TransformGenerator {
    prev: usize,
    divisor: usize,
    subject_number: usize,
}

impl TransformGenerator {
    fn new(subject_number: usize) -> TransformGenerator {
        TransformGenerator {
            prev: 1,
            divisor: DIVISOR,
            subject_number,
        }
    }
}

impl Iterator for TransformGenerator {
    type Item = usize;

    fn next(&mut self) -> Option<Self::Item> {
        self.prev = (self.subject_number * self.prev) % self.divisor;
        Some(self.prev)
    }
}

enum PublicKeySource {
    Card,
    Door,
}

fn get_loop_size(
    card_public_key: usize,
    door_public_key: usize,
    subject_number: usize,
) -> (PublicKeySource, usize) {
    TransformGenerator::new(subject_number)
        .enumerate()
        .find_map(|(i, n)| match n % DIVISOR {
            p if p == card_public_key => Some((PublicKeySource::Card, i + 1)),
            p if p == door_public_key => Some((PublicKeySource::Door, i + 1)),
            _ => None,
        })
        .unwrap()
}

fn transform(loop_size: usize, subject_number: usize) -> usize {
    TransformGenerator::new(subject_number)
        .nth(loop_size - 1)
        .unwrap()
}

fn solve(filename: &str) -> usize {
    let s = fs::read_to_string(filename).expect("problem reading file");
    let mut iter = s.lines();

    let card_public_key = iter.next().unwrap().parse::<usize>().unwrap();
    let door_public_key = iter.next().unwrap().parse::<usize>().unwrap();

    match get_loop_size(card_public_key, door_public_key, 7) {
        (PublicKeySource::Card, loop_size) => transform(loop_size, door_public_key),
        (PublicKeySource::Door, loop_size) => transform(loop_size, card_public_key),
    }
}

fn main() {
    println!("{}", solve("../input.txt"));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_solve() {
        assert_eq!(solve("../test_input.txt"), 14897079);
    }
}
