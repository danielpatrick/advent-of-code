#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pathlib import Path

DIVISOR = 20201227
DEFAULT_SUBJECT_NUMBER = 7


def transform_generator(subject_number):
    n = 1

    while True:
        n = (n * subject_number) % DIVISOR

        yield n


def transform(loop_size, subject_number):
    for i, n in enumerate(transform_generator(subject_number), 1):
        if i == loop_size:
            return n


def get_loop_sizes(public_key1, public_key2, subject_number):
    n = 0

    for p in transform_generator(subject_number):
        n += 1

        candidate = p % DIVISOR

        if candidate == public_key1:
            return (n, None)
        elif candidate == public_key2:
            return (None, n)

    raise Exception("Wah wah")


def solve(input_text):
    card_public_key = int(input_text[0])
    door_public_key = int(input_text[1])

    loop_size1, loop_size2 = get_loop_sizes(
        card_public_key, door_public_key, DEFAULT_SUBJECT_NUMBER
    )

    if loop_size1 is not None:
        return transform(loop_size1, door_public_key)
    else:
        return transform(loop_size2, card_public_key)



if __name__ == '__main__':
    from shared.utils import get_input
    from timeit import default_timer as timer

    start = timer()

    input_path = Path(__file__).parent.joinpath("input.txt")
    input_text = get_input(input_path)
    solution = solve(input_text)
    print(solution)

    end = timer()
    print()
    print("-" * 80)
    print("Time elapsed: {:.3f}s".format(end - start))
