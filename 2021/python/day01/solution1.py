#!/usr/bin/env python
# -*- coding: utf-8 -*-


def solve(input_text):
    count = 0
    prev = None

    for line in input_text:
        n = int(line)

        if prev is not None and n > prev:
            count += 1

        prev = n

    return count


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
