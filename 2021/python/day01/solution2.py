#!/usr/bin/env python
# -*- coding: utf-8 -*-


def solve(input_text):
    count = 0
    prev = (None, None, None)

    for line in input_text:
        n = int(line)

        if prev[0] is not None and n > prev[0]:
            count += 1

        prev = (prev[1], prev[2], n)

    return count


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
