#!/usr/bin/env python
# -*- coding: utf-8 -*-


def solve(input_text):
    x = 0
    y = 0

    for instruction in input_text:
        d, n = instruction.split()
        n = int(n)

        if d == "forward":
            x += n
        elif d == "down":
            y += n
        elif d == "up":
            y -= n
        else:
            raise Exception(f"Not a valid direction: '{d}'")

    return x * y


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
