#!/usr/bin/env python
# -*- coding: utf-8 -*-


def solve(input_text):
    x = 0
    y = 0
    aim = 0

    for line in input_text:
        d, n = line.split()
        n = int(n)

        if d == "forward":
            x += n
            y += aim * n
        elif d == "down":
            aim += n
        elif d == "up":
            aim -= n
        else:
            raise Exception(f"Not a valid direction: '{d}'")

    return x * y


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
