#!/usr/bin/env python
# -*- coding: utf-8 -*-

from collections import Counter


def solve(input_text):
    input_str = "".join(input_text)
    gamma = ""
    epsilon = ""
    num_bits = len(input_text[0])

    for i in range(num_bits):
        counter = Counter(input_str[i::num_bits])
        gamma += counter.most_common()[0][0]
        epsilon += counter.most_common()[-1][0]

    return int(gamma, 2) * int(epsilon, 2)


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
