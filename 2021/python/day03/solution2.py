#!/usr/bin/env python
# -*- coding: utf-8 -*-

from collections import Counter


def oxygen_rating_rule(counts):
    if len(counts) == 1 or counts[0][1] > counts[1][1]:
        return counts[0][0]

    return "1"


def scrubber_rating_rule(counts):
    if len(counts) == 1 or counts[0][1] > counts[1][1]:
        return counts[-1][0]

    return "0"


def calculate_rating(lines, rule):
    if not lines or not lines[0]:
        return ""

    c = rule(Counter([line[0] for line in lines]).most_common())

    return c + calculate_rating(
        [line[1:] for line in lines if line[0] == c],
        rule,
    )


def solve(input_text):
    oxygen_rating = calculate_rating(input_text, oxygen_rating_rule)
    scrubber_rating = calculate_rating(input_text, scrubber_rating_rule)

    return int(oxygen_rating, 2) * int(scrubber_rating, 2)


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
