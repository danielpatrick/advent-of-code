#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest

from shared import get_input

from .. import solution2
from ..solution2 import (
    calculate_rating,
    oxygen_rating_rule,
    scrubber_rating_rule,
    solve,
)


class TestSolution:
    @pytest.fixture
    def input_text(self):
        yield get_input(solution2.__file__, "test_input.txt")

    def test_calculate_oxygen_rating(self, input_text):
        assert calculate_rating(input_text, oxygen_rating_rule) == "10111"

    def test_calculate_scrubber_rating(self, input_text):
        assert calculate_rating(input_text, scrubber_rating_rule) == "01010"

    def test_solver(self, input_text):
        solution = solve(input_text)

        assert solution == 230
