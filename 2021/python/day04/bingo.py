#!/usr/bin/env python
# -*- coding: utf-8 -*-

from itertools import chain
from typing import List


class BingoCard:
    def __init__(self, input: List[str]):
        self.original_input = [[int(s) for s in row.split()] for row in input]

        self.rows = [set(row) for row in self.original_input]

        length = len(self.rows[0])

        self.cols = [
            set(row[i] for row in self.original_input) for i in range(length)
        ]

        self.has_won = False

    def remove_any_matches(self, n):
        for line in chain(self.rows, self.cols):
            line.discard(n)

            if len(line) == 0:
                self.has_won = True

        return self.has_won

    def remaining_numbers(self):
        return [n for line in self.rows for n in line]

    def __str__(self):
        s = f" Has won: {self.has_won}\n"

        for i, row in enumerate(self.original_input):
            s += (
                "".join(
                    f" {n:>2} " if n in self.rows[i] else f"[{n:>2}]"
                    for n in row
                )
                + "\n"
            )

        return s


def parse_game_input(input: List[str]):
    numbers = [int(n) for n in input[0].split(",")]
    cards = []
    section = []

    for line in input[2:]:
        if line == "":
            cards.append(BingoCard(section))
            section = []
        else:
            section.append(line)

    cards.append(BingoCard(section))

    return (numbers, cards)
