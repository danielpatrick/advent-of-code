#!/usr/bin/env python
# -*- coding: utf-8 -*-

from typing import List

from .bingo import parse_game_input


def solve(input_text: List[str]):
    numbers, cards = parse_game_input(input_text)

    for n in numbers:
        for card in cards:
            if card.remove_any_matches(n):
                return sum(card.remaining_numbers()) * n


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
