#!/usr/bin/env python
# -*- coding: utf-8 -*-

from typing import List

from .bingo import parse_game_input


def solve(input_text: List[str]):
    numbers, cards = parse_game_input(input_text)
    winning_cards = set()

    for n in numbers:
        for i, card in enumerate(cards):
            if i in winning_cards:
                continue

            if card.remove_any_matches(n):
                winning_cards.add(i)

                if len(winning_cards) == len(cards):
                    return sum(card.remaining_numbers()) * n


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
