#!/usr/bin/env python
# -*- coding: utf-8 -*-

from unittest.mock import call, patch

import pytest

from shared import get_input

from .. import bingo
from ..bingo import BingoCard, parse_game_input


@pytest.fixture
def card_one_args():
    yield [
        "22 13 17 11  0",
        " 8  2 23  4 24",
        "21  9 14 16  7",
        " 6 10  3 18  5",
        " 1 12 20 15 19",
    ]


@pytest.fixture
def card_two_args():
    yield [
        " 3 15  0  2 22",
        " 9 18 13 17  5",
        "19  8  7 25 23",
        "20 11 10 24  4",
        "14 21 16 12  6",
    ]


@pytest.fixture
def card_three_args():
    yield [
        "14 21 17 24  4",
        "10 16 15  9 19",
        "18  8 23 26 20",
        "22 11 13  6  5",
        " 2  0 12  3  7",
    ]


class TestBingoCard:
    def test_remove_matches_horizontal_win(self, card_one_args):
        card = BingoCard(card_one_args)

        assert card.remove_any_matches(1) is False
        assert card.remove_any_matches(12) is False
        assert card.remove_any_matches(20) is False
        assert card.remove_any_matches(15) is False
        assert card.remove_any_matches(19) is True

    def test_remove_matches_vertical_win(self, card_two_args):
        card = BingoCard(card_two_args)

        assert card.remove_any_matches(22) is False
        assert card.remove_any_matches(5) is False
        assert card.remove_any_matches(23) is False
        assert card.remove_any_matches(4) is False
        assert card.remove_any_matches(6) is True

    def test_remove_matches_diagonal_no_effect(self, card_three_args):
        card = BingoCard(card_three_args)

        assert card.remove_any_matches(14) is False
        assert card.remove_any_matches(16) is False
        assert card.remove_any_matches(23) is False
        assert card.remove_any_matches(6) is False
        assert card.remove_any_matches(7) is False

    def test_remove_matches_still_won_on_next_remove(self, card_one_args):
        card = BingoCard(card_one_args)

        card.remove_any_matches(1)
        card.remove_any_matches(12)
        card.remove_any_matches(20)
        card.remove_any_matches(15)
        card.remove_any_matches(19)

        assert card.remove_any_matches(19) is True
        assert card.remove_any_matches(100) is True

    def test_remaining_numbers(self, card_two_args):
        expected_result = [15, 0, 2, 9, 18, 13, 17, 19, 8, 7, 25, 20, 11]
        card = BingoCard(card_two_args)

        for n in [22, 5, 23, 4, 6, 3, 12, 16, 21, 14, 24, 10]:
            card.remove_any_matches(n)

        assert sorted(card.remaining_numbers()) == sorted(expected_result)

    def test_str(self, card_three_args):
        expected_output1 = (
            " Has won: False\n"
            "[14] 21  17  24   4 \n"
            " 10 [16] 15   9 [19]\n"
            " 18 [ 8][23][26][20]\n"
            " 22  11  13 [ 6][ 5]\n"
            "  2   0  12   3 [ 7]\n"
        )
        expected_output2 = (
            " Has won: True\n"
            "[14] 21  17  24 [ 4]\n"
            " 10 [16] 15   9 [19]\n"
            " 18 [ 8][23][26][20]\n"
            " 22  11  13 [ 6][ 5]\n"
            "  2   0  12   3 [ 7]\n"
        )
        card = BingoCard(card_three_args)

        for n in [14, 16, 19, 8, 23, 26, 20, 6, 5, 7]:
            card.remove_any_matches(n)

        assert str(card) == expected_output1

        card.remove_any_matches(4)

        assert str(card) == expected_output2


class TestParseGameInput:
    @pytest.fixture
    def input_text(self):
        yield get_input(bingo.__file__, "test_input.txt")

    @pytest.fixture
    def mock_bingo_card(self):
        with patch("day04.bingo.BingoCard", wraps=BingoCard) as mock:
            yield mock

    def test_parser(
        self,
        input_text,
        mock_bingo_card,
        card_one_args,
        card_two_args,
        card_three_args,
    ):
        # fmt: off
        expected_numbers = [
            7, 4, 9, 5, 11,
            17, 23, 2, 0, 14,
            21, 24, 10, 16, 13,
            6, 15, 25, 12, 22,
            18, 20, 8, 19, 3,
            26, 1,
        ]
        # fmt: on

        expected_bingo_card_args = [
            call(card_one_args),
            call(card_two_args),
            call(card_three_args),
        ]

        numbers, cards = parse_game_input(input_text)

        assert numbers == expected_numbers

        for card in cards:
            assert isinstance(card, BingoCard)

        assert mock_bingo_card.call_args_list == expected_bingo_card_args
