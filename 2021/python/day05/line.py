#!/usr/bin/env python
# -*- coding: utf-8 -*-

from itertools import repeat


def get_range(one, two):
    if two > one:
        return range(one, two + 1)
    if one > two:
        return range(one, two - 1, -1)

    return repeat(one)


class Line:
    def __init__(self, s):
        one, two = s.split(" -> ")
        self.x1, self.y1 = (int(n) for n in one.split(","))
        self.x2, self.y2 = (int(n) for n in two.split(","))

    def is_point(self):
        return self.x1 == self.x2 and self.y1 == self.y2

    def is_diagonal(self):
        return self.x1 != self.x2 and self.y1 != self.y2

    def points_list(self):
        if self.is_point():
            return [(self.x1, self.y1)]

        xs = get_range(self.x1, self.x2)
        ys = get_range(self.y1, self.y2)

        return list(zip(xs, ys))
