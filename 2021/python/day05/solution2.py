#!/usr/bin/env python
# -*- coding: utf-8 -*-

from collections import Counter
from itertools import chain

from .line import Line


def solve(input_text):
    counts = Counter(
        chain.from_iterable(Line(line).points_list() for line in input_text)
    )

    return sum(1 for _, v in counts.items() if v >= 2)


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
