#!/usr/bin/env python
# -*- coding: utf-8 -*-

from ..line import Line


class TestSolution:
    def test_is_point(self):
        assert Line("1,1 -> 1,1").is_point()

        assert not Line("1,1 -> 2,2").is_point()
        assert not Line("2,2 -> 1,1").is_point()
        assert not Line("2,1 -> 1,2").is_point()
        assert not Line("1,2 -> 2,1").is_point()

        assert not Line("1,1 -> 1,2").is_point()
        assert not Line("1,2 -> 1,1").is_point()

        assert not Line("1,1 -> 2,1").is_point()
        assert not Line("2,1 -> 1,1").is_point()

    def test_is_diagonal(self):
        assert Line("1,1 -> 2,2").is_diagonal()
        assert Line("2,2 -> 1,1").is_diagonal()
        assert Line("2,1 -> 1,2").is_diagonal()
        assert Line("1,2 -> 2,1").is_diagonal()

        assert not Line("1,1 -> 1,1").is_diagonal()

        assert not Line("1,1 -> 1,2").is_diagonal()
        assert not Line("1,2 -> 1,1").is_diagonal()

        assert not Line("1,1 -> 2,1").is_diagonal()
        assert not Line("2,1 -> 1,1").is_diagonal()

    def test_points_list_point(self):
        assert Line("1,1 -> 1,1").points_list() == [(1, 1)]

    def test_points_list_vertical(self):
        assert Line("1,1 -> 1,3").points_list() == [(1, 1), (1, 2), (1, 3)]
        assert Line("1,3 -> 1,1").points_list() == [(1, 3), (1, 2), (1, 1)]

    def test_points_list_horizontal(self):
        assert Line("1,1 -> 3,1").points_list() == [(1, 1), (2, 1), (3, 1)]
        assert Line("3,1 -> 1,1").points_list() == [(3, 1), (2, 1), (1, 1)]

    def test_points_list_diagonal(self):
        assert Line("1,1 -> 3,3").points_list() == [(1, 1), (2, 2), (3, 3)]
        assert Line("3,3 -> 1,1").points_list() == [(3, 3), (2, 2), (1, 1)]
        assert Line("9,7 -> 7,9").points_list() == [(9, 7), (8, 8), (7, 9)]
        assert Line("7,9 -> 9,7").points_list() == [(7, 9), (8, 8), (9, 7)]
