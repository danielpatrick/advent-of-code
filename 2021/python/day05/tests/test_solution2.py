#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest

from shared import get_input

from .. import solution2
from ..solution2 import Line, solve


class TestSolution:
    @pytest.fixture
    def input_text(self):
        yield get_input(solution2.__file__, "test_input.txt")

    def test_points_list_diagonal(self):
        assert Line("1,1 -> 3,3").points_list() == [(1, 1), (2, 2), (3, 3)]
        assert Line("3,3 -> 1,1").points_list() == [(3, 3), (2, 2), (1, 1)]
        assert Line("9,7 -> 7,9").points_list() == [(9, 7), (8, 8), (7, 9)]
        assert Line("7,9 -> 9,7").points_list() == [(7, 9), (8, 8), (9, 7)]

    def test_solver(self, input_text):
        solution = solve(input_text)

        assert solution == 12
