#!/usr/bin/env python
# -*- coding: utf-8 -*-

from collections import Counter


def count_after(input_text, n):
    counts = Counter(input_text.split(","))
    state = [counts[str(n)] for n in range(9)]

    for offset in range(n):
        state[(offset + 7) % 9] += state[offset % 9]

    return sum(state)
