#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .lanternfish import count_after


def solve(input_text):
    return count_after(input_text, 256)


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
