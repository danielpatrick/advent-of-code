#!/usr/bin/env python
# -*- coding: utf-8 -*-

from ..lanternfish import count_after


def test_counts_after():
    assert count_after("3,4,3,1,2", 80) == 5934
    assert count_after("3,4,3,1,2", 256) == 26984457539
