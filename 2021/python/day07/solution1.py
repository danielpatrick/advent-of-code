#!/usr/bin/env python
# -*- coding: utf-8 -*-


from statistics import median


def solve(input_text):
    positions = [int(s) for s in input_text.split(",")]

    avg = int(median(positions))

    return sum(abs(avg - n) for n in positions)


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
