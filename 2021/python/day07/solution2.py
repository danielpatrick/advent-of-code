#!/usr/bin/env python
# -*- coding: utf-8 -*-

import statistics
from math import ceil, floor


def calculate_fuel(distance):
    return distance * (distance + 1) // 2


def total_fuel(positions, target):
    return sum(calculate_fuel(abs(target - pos)) for pos in positions)


def solve(input_text):
    positions = [int(s) for s in input_text.split(",")]
    median = statistics.median(positions)
    mean = statistics.mean(positions)
    low = floor(min(median, mean))
    high = ceil(max(median, mean))

    return min(total_fuel(positions, t) for t in range(low, high + 1))


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
