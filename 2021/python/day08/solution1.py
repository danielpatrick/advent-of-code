#!/usr/bin/env python
# -*- coding: utf-8 -*-


def parse(line):
    a, b = line.split("|")

    return a.split(), b.split()


def solve(input_text):
    lines = [parse(line) for line in input_text]

    return sum(
        sum(1 for item in line[1] if len(item) in (2, 4, 3, 7))
        for line in lines
    )


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
