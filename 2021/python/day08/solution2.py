#!/usr/bin/env python
# -*- coding: utf-8 -*-

from collections import defaultdict
from functools import reduce


def create_mapping(patterns):
    patterns = reduce(
        lambda acc, p: acc[len(p)].append(set(p)) or acc,
        patterns,
        defaultdict(list),
    )

    # 2 => [1]
    # 3 => [7]
    # 4 => [4]
    # 5 => [2, 3, 5]
    # 6 => [0, 6, 9]
    # 7 => [8]

    matched = {}

    # "Easy"
    matched[1] = patterns[2][0]
    matched[7] = patterns[3][0]
    matched[4] = patterns[4][0]
    matched[8] = patterns[7][0]

    # Depend on above
    matched[3] = next(p for p in patterns[5] if p.issuperset(matched[1]))
    matched[6] = next(p for p in patterns[6] if not p.issuperset(matched[1]))
    matched[9] = next(p for p in patterns[6] if p.issuperset(matched[4]))

    # Depend on all the above
    matched[5] = next(p for p in patterns[5] if p.issubset(matched[6]))
    matched[0] = next(p for p in patterns[6] if not p.issuperset(matched[5]))
    matched[2] = next(p for p in patterns[5] if not p.issubset(matched[9]))

    return {"".join(sorted(v)): k for k, v in matched.items()}


def determine_value(line):
    mapping = create_mapping(set(chars for chars in line[0] + line[1]))

    return sum(mapping[s] * 10 ** i for i, s in enumerate(reversed(line[1])))


def parse(line):
    a, b = line.split("|")

    return [
        ["".join(sorted(s)) for s in a.split()],
        ["".join(sorted(s)) for s in b.split()],
    ]


def solve(input_text):
    lines = [parse(line) for line in input_text]

    return sum(determine_value(line) for line in lines)


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
