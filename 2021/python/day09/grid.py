#!/usr/bin/env python
# -*- coding: utf-8 -*-


class Grid:
    def __init__(self, s):
        self.height = len(s)
        self.width = len(s[0])
        self.length = self.height * self.width

        self.heights = "".join(s)

    def adjacent_positions(self, pos):
        if pos >= self.width:  # Up
            yield pos - self.width
        if pos % self.width < (self.width - 1):  # Right
            yield pos + 1
        if pos < (self.length - self.width):  # Down
            yield pos + self.width
        if pos % self.width >= 1:  # Left
            yield pos - 1

    def adjacent_heights(self, pos):
        for pos in self.adjacent_positions(pos):
            yield self.heights[pos]

    def get_low_points(self):
        for i, height in enumerate(self.heights):
            if all(h > height for h in self.adjacent_heights(i)):
                yield int(height)

    def map_basin(self, not_checked, start):
        not_checked.discard(start)

        if self.heights[start] == "9":
            return 0

        count = 1

        for pos in self.adjacent_positions(start):
            if pos in not_checked:
                count += self.map_basin(not_checked, pos)

        return count

    def find_basin_sizes(self):
        not_checked = set(range(self.length))
        basins = []

        while not_checked:
            pos = not_checked.pop()
            basin_size = self.map_basin(not_checked, pos)

            if basin_size != 0:
                basins.append(basin_size)

        return basins
