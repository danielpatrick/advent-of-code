#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .grid import Grid


def solve(input_text):
    grid = Grid(input_text)

    return sum(n + 1 for n in grid.get_low_points())


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
