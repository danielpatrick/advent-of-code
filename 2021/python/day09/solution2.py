#!/usr/bin/env python
# -*- coding: utf-8 -*-

from math import prod

from .grid import Grid


def solve(input_text):
    grid = Grid(input_text)
    basin_lengths = sorted(grid.find_basin_sizes())

    return prod(basin_lengths[-3:])


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
