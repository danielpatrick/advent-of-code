#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest

from shared import get_input

from .. import grid
from ..grid import Grid


class TestGrid:
    @pytest.fixture
    def grid(self):
        yield Grid(get_input(grid.__file__, "test_input.txt"))

    def test_adjacent_heights(self, grid):
        # Corners
        assert list(grid.adjacent_heights(0)) == ["1", "3"]
        assert list(grid.adjacent_heights(9)) == ["1", "1"]
        assert list(grid.adjacent_heights(40)) == ["8", "8"]
        assert list(grid.adjacent_heights(49)) == ["9", "7"]

        # Edges
        assert list(grid.adjacent_heights(1)) == ["9", "9", "2"]
        assert list(grid.adjacent_heights(19)) == ["0", "2", "2"]
        assert list(grid.adjacent_heights(30)) == ["9", "7", "9"]
        assert list(grid.adjacent_heights(48)) == ["8", "8", "6"]

        # Somewhere in the middle
        assert list(grid.adjacent_heights(24)) == ["8", "8", "8", "6"]

    def test_get_low_points(self, grid):
        assert list(grid.get_low_points()) == [1, 0, 5, 5]

    def test_map_basin(self, grid):
        not_checked = set(range(50))
        expected_not_checked1 = not_checked - {0, 1, 2, 10, 11, 20}
        expected_not_checked2 = (
            expected_not_checked1
            - {5, 6, 7, 8, 9, 16, 18, 19, 29}
            - {3, 4, 15, 17, 26, 28, 39}
        )

        assert grid.map_basin(not_checked, 0) == 3
        assert not_checked == expected_not_checked1

        assert grid.map_basin(not_checked, 3) == 0
        assert grid.map_basin(not_checked, 4) == 0

        assert grid.map_basin(not_checked, 5) == 9
        assert not_checked == expected_not_checked2

    def test_find_basins(self, grid):
        expected_basins = [3, 9, 14, 9]

        assert grid.find_basin_sizes() == expected_basins
