#!/usr/bin/env python
# -*- coding: utf-8 -*-

pairs = {
    ")": "(",
    "]": "[",
    "}": "{",
    ">": "<",
}

points = {
    ")": 3,
    "]": 57,
    "}": 1197,
    ">": 25137,
}


def chunk_validator(s: str) -> int:
    stack = []

    for char in s:
        if char in pairs:
            prev = stack.pop()

            if prev != pairs[char]:
                return points[char]

        else:
            stack.append(char)

    return 0


def solve(input_text):
    return sum(chunk_validator(line) for line in input_text)


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
