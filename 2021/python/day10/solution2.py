#!/usr/bin/env python
# -*- coding: utf-8 -*-

pairs = {
    ")": "(",
    "]": "[",
    "}": "{",
    ">": "<",
}

points = {
    "(": 1,
    "[": 2,
    "{": 3,
    "<": 4,
}


def chunk_validator(s: str) -> int:
    stack = []

    for char in s:
        if char in pairs:
            prev = stack.pop()

            if prev != pairs[char]:
                return None
        else:
            stack.append(char)

    score = 0

    while stack:
        char = stack.pop()
        score *= 5
        score += points[char]

    return score


def solve(input_text):
    scores = []

    for line in input_text:
        score = chunk_validator(line)

        if score is not None:
            scores.append(score)

    scores = sorted(scores)

    return scores[len(scores) // 2]


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
