#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest

from shared import get_input

from .. import solution1
from ..solution1 import chunk_validator, solve


class TestSolution:
    @pytest.fixture
    def input_text(self):
        yield get_input(solution1.__file__, "test_input.txt")

    def test_solver(self, input_text):
        solution = solve(input_text)

        assert solution == 26397

    def test_chunk_validator(self):
        assert chunk_validator("{([(<{}[<>[]}>{[]{[(<()>") == 1197
        assert chunk_validator("[[<[([]))<([[{}[[()]]]") == 3
        assert chunk_validator("[{[{({}]{}}([{[{{{}}([]") == 57
        assert chunk_validator("[<(<(<(<{}))><([]([]()") == 3
        assert chunk_validator("<{([([[(<>()){}]>(<<{{") == 25137
