#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest

from shared import get_input

from .. import solution2
from ..solution2 import chunk_validator, solve


class TestSolution:
    @pytest.fixture
    def input_text(self):
        yield get_input(solution2.__file__, "test_input.txt")

    def test_solver(self, input_text):
        solution = solve(input_text)

        assert solution == 288957

    def test_chunk_validator_invalid(self):
        assert chunk_validator("{([(<{}[<>[]}>{[]{[(<()>") is None
        assert chunk_validator("[[<[([]))<([[{}[[()]]]") is None
        assert chunk_validator("[{[{({}]{}}([{[{{{}}([]") is None
        assert chunk_validator("[<(<(<(<{}))><([]([]()") is None
        assert chunk_validator("<{([([[(<>()){}]>(<<{{") is None

    def test_chunk_validator_valid(self):
        assert chunk_validator("[({(<(())[]>[[{[]{<()<>>") == 288957
        assert chunk_validator("[(()[<>])]({[<{<<[]>>(") == 5566
        assert chunk_validator("(((({<>}<{<{<>}{[]{[]{}") == 1480781
        assert chunk_validator("{<[[]]>}<{[{[{[]{()[[[]") == 995444
        assert chunk_validator("<{([{{}}[<[[[<>{}]]]>[]]") == 294
