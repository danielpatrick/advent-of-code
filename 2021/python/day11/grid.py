class Grid:
    def __init__(self, input_text):
        self.zero = 0
        self.nine = 9
        self.width = len(input_text[0])
        self.height = len(input_text)
        self.size = self.width * self.height
        self.octopuses = [int(c) for c in "".join(input_text)]

    def step(self):
        self.zero -= 1
        self.nine -= 1

        flashed = set()
        count = 0

        count = sum(
            self.apply_flash(i, flashed)
            for i in range(self.size)
            if self.octopuses[i] > self.nine and i not in flashed
        )

        for pos in flashed:
            self.octopuses[pos] = self.zero

        return count

    def apply_flash(self, pos, flashed):
        count = 0

        if self.octopuses[pos] > self.nine and pos not in flashed:
            flashed.add(pos)
            count = 1

            for pos in self.neighbours(pos):
                if self.octopuses[pos] <= self.nine:
                    self.octopuses[pos] += 1
                    count += self.apply_flash(pos, flashed)

        return count

    def neighbours(self, pos):
        x = pos % self.width
        y = pos // self.width

        north = y >= 1
        east = x < self.width - 1
        south = y < self.height - 1
        west = x >= 1

        if north:
            yield pos - self.width

            if east:
                yield pos - self.width + 1
        if east:
            yield pos + 1

            if south:
                yield pos + self.width + 1
        if south:
            yield pos + self.width

            if west:
                yield pos + self.width - 1
        if west:
            yield pos - 1

            if north:
                yield pos - self.width - 1

    def __str__(self):
        return "".join(
            "".join(
                str(self.octopuses[y * self.width + x] - self.zero)
                for x in range(self.width)
            )
            + "\n"
            for y in range(self.height)
        )
