#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .grid import Grid


def solve(input_text):
    grid = Grid(input_text)

    flashes = 0

    for _ in range(100):
        flashes += grid.step()

    return flashes


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
