#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .grid import Grid


def solve(input_text):
    grid = Grid(input_text)

    steps = 0
    flashes = 0

    while flashes < 100:
        steps += 1
        flashes = grid.step()

    return steps


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
