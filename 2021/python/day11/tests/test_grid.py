#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest

from shared import get_input

from .. import grid
from ..grid import Grid


class TestGrid:
    @pytest.fixture
    def input_text(self):
        yield get_input(grid.__file__, "test_input.txt")

    def test_one_step(self, input_text):
        grid = Grid(input_text)

        assert grid.step() == 0

    def test_two_steps(self, input_text):
        grid = Grid(input_text)
        grid.step()

        assert grid.step() == 35

    def test_three_steps(self, input_text):
        grid = Grid(input_text)
        grid.step()
        grid.step()

        assert grid.step() == 45

    def test_four_steps(self, input_text):
        grid = Grid(input_text)
        grid.step()
        grid.step()
        grid.step()

        assert grid.step() == 16

    def test_ten_steps(self, input_text):
        grid = Grid(input_text)

        assert sum(grid.step() for _ in range(10)) == 204

    def test_str(self, input_text):
        expected = (
            "5483143223\n"
            "2745854711\n"
            "5264556173\n"
            "6141336146\n"
            "6357385478\n"
            "4167524645\n"
            "2176841721\n"
            "6882881134\n"
            "4846848554\n"
            "5283751526\n"
        )

        assert str(Grid(input_text)) == expected
