#!/usr/bin/env python
# -*- coding: utf-8 -*-


def get_rules(input_text):
    rules = {}

    for line in input_text:
        a, b = line.split("-")

        if a != "end" and b != "start":
            if a not in rules:
                rules[a] = []

            rules[a].append(b)

        if a != "start" and b != "end":
            if b not in rules:
                rules[b] = []

            rules[b].append(a)

    return rules


def count_routes(rules, path, current, allowed_duplicates=0):
    if current == "end":
        return 1

    if current not in rules:
        return 0

    count = 0

    for target in rules[current]:
        remaining_duplicates = allowed_duplicates

        if not target.isupper() and target in path:
            if remaining_duplicates == 0:
                continue

            remaining_duplicates -= 1

        count += count_routes(
            rules, path + [target], target, remaining_duplicates
        )

    return count
