#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .caves import count_routes, get_rules


def solve(input_text):
    rules = get_rules(input_text)

    return count_routes(rules, ["start"], "start", 1)


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
