#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest

from shared import get_input

from .. import solution2
from ..solution2 import solve


class TestSolution:
    @pytest.fixture
    def input_text1(self):
        yield get_input(solution2.__file__, "test_input1.txt")

    @pytest.fixture
    def input_text2(self):
        yield get_input(solution2.__file__, "test_input2.txt")

    @pytest.fixture
    def input_text3(self):
        yield get_input(solution2.__file__, "test_input3.txt")

    def test_solver(self, input_text1, input_text2, input_text3):
        assert solve(input_text1) == 36
        assert solve(input_text2) == 103
        assert solve(input_text3) == 3509
