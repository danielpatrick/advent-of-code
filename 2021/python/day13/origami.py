#!/usr/bin/env python
# -*- coding: utf-8 -*-

from typing import List, Tuple


class OrigamiDots:
    def __init__(self, dots: List[Tuple[int, int]]):
        self.dots = set(dots)

    def folded_x(self, dot: Tuple[int, int], n: int):
        return (n * 2 - dot[0], dot[1])

    def folded_y(self, dot: Tuple[int, int], n: int):
        return (dot[0], n * 2 - dot[1])

    def fold(self, axis: str, n: int):
        dots = list(self.dots)

        for i in range(len(dots)):
            if axis == "x":
                if dots[i][0] > n:
                    self.dots.discard(dots[i])
                    self.dots.add(self.folded_x(dots[i], n))
            elif axis == "y":
                if dots[i][1] > n:
                    self.dots.discard(dots[i])
                    self.dots.add(self.folded_y(dots[i], n))
            else:
                raise Exception(f"Invalid axis: {axis}")

    def count_dots(self):
        return len(self.dots)

    def as_rows(self):
        x1 = min(dot[0] for dot in self.dots)
        x2 = max(dot[0] for dot in self.dots)
        y1 = min(dot[1] for dot in self.dots)
        y2 = max(dot[1] for dot in self.dots)

        return [
            "".join(
                "#" if (x, y) in self.dots else "." for x in range(x1, x2 + 1)
            )
            for y in range(y1, y2 + 1)
        ]

    def __str__(self):
        return "\n".join(self.as_rows()) + "\n"


def parse(input_text: List[str]):
    parsing_folds = False
    dots = set()
    folds = []

    for line in input_text:
        if parsing_folds:
            line = line.split()
            axis, n = line[-1].split("=")
            folds.append((axis, int(n)))
        elif line:
            dots.add(tuple(int(s) for s in line.split(",")))
        else:
            parsing_folds = True

    return (OrigamiDots(dots), folds)
