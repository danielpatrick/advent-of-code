#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .origami import parse


def solve(input_text):
    origami, folds = parse(input_text)

    axis, n = folds[0]
    origami.fold(axis, n)

    return origami.count_dots()


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
