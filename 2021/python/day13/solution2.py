#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .origami import parse


def solve(input_text):
    origami, folds = parse(input_text)

    for axis, n in folds:
        origami.fold(axis, n)

    return str(origami)


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
