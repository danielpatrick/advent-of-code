#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest

from shared import get_input

from .. import solution2
from ..solution2 import solve


class TestSolution:
    @pytest.fixture
    def input_text(self):
        yield get_input(solution2.__file__, "test_input.txt")

    def test_solver(self, input_text):
        # fmt: off
        expected = (
            "#####\n"
            "#...#\n"
            "#...#\n"
            "#...#\n"
            "#####\n"
        )
        # fmt: on

        assert solve(input_text) == expected
