#!/usr/bin/env python
# -*- coding: utf-8 -*-

from collections import Counter, defaultdict


def create_rules(lines):
    return dict(line.split(" -> ") for line in lines)


def count_occurences(pair, rules, cache, n):
    if n == 0:
        cache[0][pair] = Counter(pair[1])
        return cache[0][pair]

    if pair in cache[n]:
        return cache[n][pair]

    counts = Counter()

    if pair in rules:
        c = rules[pair]
        counts += count_occurences(pair[0] + c, rules, cache, n - 1)
        counts += count_occurences(c + pair[1], rules, cache, n - 1)

    cache[n][pair] = counts

    return counts


def min_and_max_occurences_after(polymer, rules, n):
    cache = defaultdict(dict)

    counts = Counter(polymer[0])

    for i in range(len(polymer) - 1):
        counts += count_occurences(polymer[i : i + 2], rules, cache, n)

    frequencies = counts.most_common()

    return (frequencies[-1][1], frequencies[0][1])
