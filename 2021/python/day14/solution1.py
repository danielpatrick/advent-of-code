#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .polymers import min_and_max_occurences_after


def solve(input_text):
    polymer = input_text[0]
    rules = dict(line.split(" -> ") for line in input_text[2:])

    least, most = min_and_max_occurences_after(polymer, rules, 10)

    return most - least


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
