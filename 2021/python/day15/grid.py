#!/usr/bin/env python
# -*- coding: utf-8 -*-

import heapq


class Grid:
    def __init__(self, s):
        self.width = len(s[0])
        self.height = len(s)

        self.start = 0
        self.target = self.idx(self.width - 1, self.height - 1)

        self.grid = [
            int(s[y][x])
            for (x, y) in (
                self.point(pos) for pos in range(self.width * self.height)
            )
        ]

    def tile_risk(self, pos):
        return self.grid[pos]

    def idx(self, x, y):
        return self.width * y + x

    def point(self, pos):
        return (pos % self.width, pos // self.width)

    def neighbours(self, pos):
        x, y = self.point(pos)

        if y < self.height - 1:
            yield pos + self.width
        if x < self.height - 1:
            yield pos + 1
        if y > 0:
            yield pos - self.width
        if x > 0:
            yield pos - 1

    def neighbours_with_risk(self, pos):
        return ((p, self.tile_risk(p)) for p in self.neighbours(pos))

    def find_path(self):
        risks = {}
        route = {}
        queue = []

        risks[self.start] = 0
        route[self.start] = None
        heapq.heappush(queue, (0, self.start))

        while queue:
            risk, current = heapq.heappop(queue)

            if current == self.target:
                break

            if current in risks and risk > risks[current]:
                continue

            for next_pos, next_risk in self.neighbours_with_risk(current):
                next_risk += risk

                if next_pos not in risks or next_risk < risks[next_pos]:
                    risks[next_pos] = next_risk
                    heapq.heappush(queue, (next_risk, next_pos))
                    route[next_pos] = current

        current = self.target
        path = []

        while current != self.start:
            path.append(current)
            current = route[current]

        # self.draw_path(path)
        return path

    def path_risk(self, path):
        return sum(self.tile_risk(p) for p in path)

    def draw_path(self, path):
        BOLD = "\033[1m"
        RESET = "\033[0m"
        path = set(path)

        for y in range(self.target[1] + 1):
            for x in range(self.target[0] + 1):
                pos = self.idx(x, y)
                risk = self.tile_risk(pos)

                if pos == (0, 0) or pos in path:
                    print(f"{BOLD}{risk}{RESET}", end="")
                else:
                    print(risk, end="")
            print()
        print()
