#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .grid import Grid


def solve(input_text):
    grid = Grid(input_text)

    return grid.path_risk(grid.find_path())


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
