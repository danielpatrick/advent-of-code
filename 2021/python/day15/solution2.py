#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .grid import Grid as BaseGrid


class Grid(BaseGrid):
    def __init__(self, s):
        w = len(s[0])
        h = len(s)
        self.width = w * 5
        self.height = h * 5

        self.start = 0
        self.target = self.idx(self.width - 1, self.height - 1)

        self.grid = [
            self.calc_risk(s, x, y, w, h)
            for (x, y) in (
                self.point(pos) for pos in range(self.width * self.height)
            )
        ]

    def calc_risk(self, s, x, y, w, h):
        if x < w and y < h:
            return int(s[y][x])

        risk = int(s[y % h][x % w])
        risk += x // w
        risk += y // h

        if risk > 9:
            risk -= 9

        return risk


def solve(input_text):
    grid = Grid(input_text)

    return grid.path_risk(grid.find_path())


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
