import math


def sub(bits, i, num):
    start = i
    end = i - num

    i -= num

    mask = (1 << start) - (1 << end)
    return ((bits & mask) >> end, i)


class Packet:
    def __init__(self, version, packet_type, value):
        self.version = version
        self.packet_type = packet_type
        self.value = value

    @classmethod
    def from_bits(cls, bits, i):
        version, i = sub(bits, i, 3)
        packet_type, i = sub(bits, i, 3)

        if packet_type == 4:
            value, i = cls.parse_literal(bits, i)
            return cls(version, packet_type, value), i
        else:
            length_type, i = sub(bits, i, 1)

            if length_type == 0:
                packets, i = cls.parse_num_bits(bits, i)
                return cls(version, packet_type, packets), i
            else:
                packets, i = cls.parse_num_packets(bits, i)
                return cls(version, packet_type, packets), i

    @classmethod
    def parse_literal(cls, bits, i):
        buf = 0

        while True:
            sentinel, i = sub(bits, i, 1)
            b, i = sub(bits, i, 4)

            buf <<= 4
            buf += b

            if sentinel == 0:
                break

        return buf, i

    @classmethod
    def parse_num_bits(cls, bits, i):
        packets = []
        num_bits, i = sub(bits, i, 15)
        limit = i - num_bits

        while i > limit:
            p, i = Packet.from_bits(bits, i)
            packets.append(p)

        return packets, i

    @classmethod
    def parse_num_packets(cls, bits, i):
        packets = []
        num_packets, i = sub(bits, i, 11)

        while num_packets > 0:
            p, i = Packet.from_bits(bits, i)
            packets.append(p)
            num_packets -= 1

        return packets, i

    def compute(self):
        if self.packet_type == 0:
            return sum(p.compute() for p in self.value)
        elif self.packet_type == 1:
            return math.prod(p.compute() for p in self.value)
        elif self.packet_type == 2:
            return min(p.compute() for p in self.value)
        elif self.packet_type == 3:
            return max(p.compute() for p in self.value)
        elif self.packet_type == 4:
            return self.value
        elif self.packet_type == 5:
            return int(self.value[0].compute() > self.value[1].compute())
        elif self.packet_type == 6:
            return int(self.value[0].compute() < self.value[1].compute())
        elif self.packet_type == 7:
            return int(self.value[0].compute() == self.value[1].compute())

    def __str__(self):
        common = f"Version: {self.version}, Type: {self.packet_type}"

        if self.packet_type == 4:
            return f"{common}, Value: {self.value}"
        else:
            return f"{common}, Sub packets: {len(self.value)}"


def parse_packets(input_text):
    return Packet.from_bits(int(input_text, 16), len(input_text) * 4)[0]
