#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .packet import parse_packets


def add_versions(packet):
    if packet.packet_type == 4:
        return packet.version
    else:
        return packet.version + sum(add_versions(p) for p in packet.value)


def solve(input_text):
    return add_versions(parse_packets(input_text))


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
