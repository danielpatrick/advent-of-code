#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .packet import parse_packets


def solve(input_text):
    return parse_packets(input_text).compute()


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
