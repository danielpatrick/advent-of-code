#!/usr/bin/env python
# -*- coding: utf-8 -*-

from ..packet import parse_packets


class TestSolution:
    def test_parse_literal(self):
        packet = parse_packets("D2FE28")

        assert packet.version == 6
        assert packet.packet_type == 4
        assert packet.value == 2021

    def test_parse_sub_packets_by_bit_limit(self):
        packet = parse_packets("38006F45291200")

        assert packet.version == 1
        assert packet.packet_type == 6

        sub_packets = packet.value
        assert len(sub_packets) == 2
        assert sub_packets[0].version == 6
        assert sub_packets[0].packet_type == 4
        assert sub_packets[0].value == 10
        assert sub_packets[1].version == 2
        assert sub_packets[1].packet_type == 4
        assert sub_packets[1].value == 20

    def test_parse_sub_packets_by_packet_count(self):
        packet = parse_packets("EE00D40C823060")

        assert packet.version == 7
        assert packet.packet_type == 3

        sub_packets = packet.value
        assert len(sub_packets) == 3
        assert sub_packets[0].version == 2
        assert sub_packets[0].packet_type == 4
        assert sub_packets[0].value == 1
        assert sub_packets[1].version == 4
        assert sub_packets[1].packet_type == 4
        assert sub_packets[1].value == 2
        assert sub_packets[2].version == 1
        assert sub_packets[2].packet_type == 4
        assert sub_packets[2].value == 3

    def test_parse_nested(self):
        packet = parse_packets("9C0141080250320F1802104A08")

        assert packet.version == 4
        assert packet.packet_type == 7

        sub_packets = packet.value

        assert sub_packets[0].version == 2
        assert sub_packets[0].packet_type == 0
        assert len(sub_packets[0].value) == 2
        assert sub_packets[0].value[0].version == 2
        assert sub_packets[0].value[0].packet_type == 4
        assert sub_packets[0].value[0].value == 1
        assert sub_packets[0].value[1].version == 4
        assert sub_packets[0].value[1].packet_type == 4
        assert sub_packets[0].value[1].value == 3

        assert sub_packets[1].version == 6
        assert sub_packets[1].packet_type == 1
        assert len(sub_packets[1].value) == 2
        assert sub_packets[1].value[0].version == 0
        assert sub_packets[1].value[0].packet_type == 4
        assert sub_packets[1].value[0].value == 2
        assert sub_packets[1].value[1].version == 2
        assert sub_packets[1].value[1].packet_type == 4
        assert sub_packets[1].value[1].value == 2

    def test_compute(self):
        assert parse_packets("C200B40A82").compute() == 3
        assert parse_packets("04005AC33890").compute() == 54
        assert parse_packets("880086C3E88112").compute() == 7
        assert parse_packets("CE00C43D881120").compute() == 9
        assert parse_packets("D8005AC2A8F0").compute() == 1
        assert parse_packets("F600BC2D8F").compute() == 0
        assert parse_packets("9C005AC2F8F0").compute() == 0
        assert parse_packets("9C0141080250320F1802104A08").compute() == 1
        assert parse_packets("02008180210420C4200").compute() == 10
