#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest

from shared import get_input

from .. import solution1
from ..packet import parse_packets
from ..solution1 import add_versions, solve


class TestSolution:
    @pytest.fixture
    def input_text(self):
        yield get_input(solution1.__file__, "test_input1.txt")

    def test_solver(self, input_text):
        solution = solve(input_text)

        assert solution == 31

    def test_add_versions1(self):
        raw = "8A004A801A8002F478"
        assert add_versions(parse_packets(raw)) == 16

    def test_add_versions2(self):
        raw = "620080001611562C8802118E34"
        assert add_versions(parse_packets(raw)) == 12

    def test_add_versions3(self):
        raw = "C0015000016115A2E0802F182340"
        assert add_versions(parse_packets(raw)) == 23

    def test_add_versions4(self):
        raw = "A0016C880162017C3686B18A3D4780"
        assert add_versions(parse_packets(raw)) == 31
