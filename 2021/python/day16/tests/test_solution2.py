#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest

from shared import get_input

from .. import solution2
from ..solution2 import solve


class TestSolution:
    @pytest.fixture
    def input_text(self):
        yield get_input(solution2.__file__, "test_input2.txt")

    def test_solver(self, input_text):
        solution = solve(input_text)

        assert solution == 54

    def test_more(self):
        assert solve("C200B40A82") == 3
        assert solve("04005AC33890") == 54
        assert solve("880086C3E88112") == 7
        assert solve("CE00C43D881120") == 9
        assert solve("D8005AC2A8F0") == 1
        assert solve("F600BC2D8F") == 0
        assert solve("9C005AC2F8F0") == 0
        assert solve("9C0141080250320F1802104A08") == 1
        assert solve("02008180210420C4200") == 10
