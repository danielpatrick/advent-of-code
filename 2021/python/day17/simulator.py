#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
from enum import Enum

REGEX = re.compile(
    "^target area: x=([0-9-]+)..([0-9]+), y=([0-9-]+)..([0-9-]+)$"
)


class Ordering(Enum):
    LESS = 1
    EQUAL = 2
    GREATER = 3
    UNKNOWN = 100


def in_bounds(x1, x2, y1, y2, x, y):
    return x1 <= x <= x2 and y1 <= y <= y2


def does_hit(x1, x2, y1, y2, dx, dy):
    #
    #  (x1, y2)  (x2, y2)
    #      +--------+
    #      |        |
    #      |        |
    #      |        |
    #      +--------+
    #  (x1, y1)  (x2, y1)
    #
    x = 0
    y = 0
    highest_y = 0

    while y >= y1:
        if dx == 0:
            if x < x1:
                # Falling short of the target
                return (Ordering.LESS, None)
            if x > x2:
                # Falling further than the target
                return (Ordering.LESS, None)
        if x > x2 and y > y2:
            # Above and further than the target
            return (Ordering.GREATER, None)

        highest_y = max(highest_y, y)

        if in_bounds(x1, x2, y1, y2, x, y):
            return (Ordering.EQUAL, highest_y)

        x += dx
        y += dy
        dx = max(dx - 1, 0)
        dy -= 1

    if x < x1 and y < y1:
        # Lower than and short of the target
        return (Ordering.LESS, None)

    return (Ordering.UNKNOWN, None)


def parse(input_text):
    groups = REGEX.match(input_text).groups()
    x1, x2, y1, y2 = [int(s) for s in groups]

    if x1 > x2:
        x1, x2 = x2, x1
    if y1 > y2:
        y1, y2 = y2, y1

    return (x1, x2, y1, y2)


def highest_y(input_text):
    _, _, y1, _ = parse(input_text)

    y = abs(y1)

    return y * (y - 1) // 2


def run(input_text):
    x1, x2, y1, y2 = parse(input_text)

    # Assume y1 and y2 negative:
    #  - positive y1 and y2 requires bound of [int((2 * y1) ** 0.5), y2]
    #  - negative y1 and positive y2 has an infinite number of solutions
    # (because any dy greater than 0 will eventually reach zero)
    start_dy = y1
    end_dy = abs(y1) + 1

    # Assume x1 positive, because we can't move to the left
    end_dx = int((2 * x1) ** 0.5) - 1
    num_solutions = 0

    for dx in range(x2, end_dx, -1):
        for dy in range(start_dy, end_dy):
            order, result = does_hit(x1, x2, y1, y2, dx, dy)

            if order == Ordering.LESS:
                start_dy += 1
            elif order == Ordering.EQUAL:
                num_solutions += 1
            elif order == Ordering.GREATER:
                break

    return num_solutions
