#!/usr/bin/env python
# -*- coding: utf-8 -*-


from .simulator import highest_y


def solve(input_text):
    return highest_y(input_text)


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
