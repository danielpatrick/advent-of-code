#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .simulator import run


def solve(input_text):
    num_solutions = run(input_text)

    return num_solutions


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
