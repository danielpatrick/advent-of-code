#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest

from shared import get_input

from .. import simulator
from ..simulator import Ordering, does_hit, highest_y, run


class TestSolution:
    @pytest.fixture
    def input_text(self):
        yield get_input(simulator.__file__, "test_input.txt")

    def test_highest_y(self, input_text):
        assert highest_y(input_text) == 45

    def test_run(self, input_text):
        assert run(input_text) == 112

    def test_does_hit(self):
        assert does_hit(20, 30, -10, -5, 7, 2) == (Ordering.EQUAL, 3)
        assert does_hit(20, 30, -10, -5, 6, 3) == (Ordering.EQUAL, 6)
        assert does_hit(20, 30, -10, -5, 9, 0) == (Ordering.EQUAL, 0)
        assert does_hit(20, 30, -10, -5, 17, -4) == (Ordering.UNKNOWN, None)
        assert does_hit(20, 30, -10, -5, 9, 2) == (Ordering.GREATER, None)
