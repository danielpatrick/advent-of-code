#!/usr/bin/env python
# -*- coding: utf-8 -*-


def dumps(number):
    result = "".join(number[:1])

    for i in range(1, len(number)):
        prev = number[i - 1]
        s = number[i]

        if (prev == "]" or isinstance(prev, int)) and s != "]":
            result += f",{s}"
        else:
            result += str(s)

    return result


def add_right(number, to_add):
    if isinstance(number[1], int):
        number[1] += to_add
        return None
    else:
        to_add = add_right(number[1], to_add)


def add_left(number, to_add):
    if isinstance(number[0], int):
        number[0] += to_add
        return None
    else:
        return add_left(number[0], to_add)


def explode_recursive(number, depth=1):
    left = None
    right = None
    has_modified = False

    if depth > 4 and isinstance(number[0], int) and isinstance(number[1], int):
        return (number[0], number[1], True)

    if not isinstance(number[0], int):
        left, right, has_modified = explode_recursive(number[0], depth + 1)

        if left is not None and right is not None:
            number[0] = 0

        if right is not None:
            if isinstance(number[1], int):
                number[1] += right
                right = None
            else:
                right = add_left(number[1], right)

    if not has_modified and not isinstance(number[1], int):
        left, right, has_modified = explode_recursive(number[1], depth + 1)

        if left is not None and right is not None:
            number[1] = 0

        if left is not None:
            if isinstance(number[0], int):
                number[0] += left
                left = None
            else:
                left = add_right(number[0], left)

    return (left, right, has_modified)


def explode(number):
    _, _, has_modified = explode_recursive(number)

    return (number, has_modified)


def split_recursive(number):
    has_modified = False

    if isinstance(number[0], int):
        if number[0] >= 10:
            has_modified = True
            n = number[0] // 2
            number[0] = [n, number[0] - n]
    else:
        has_modified = split_recursive(number[0])

    if not has_modified:
        if isinstance(number[1], int):
            if number[1] >= 10:
                has_modified = True
                n = number[1] // 2
                number[1] = [n, number[1] - n]
        else:
            has_modified = split_recursive(number[1])

    return has_modified


def split(number):
    has_modified = split_recursive(number)

    return (number, has_modified)


def add(number1, number2):
    return [number1, number2]


def reduce_numbers(*numbers):
    number = None

    for n in numbers:
        has_modified = True

        if number is None:
            number = n
        else:
            number = add(number, n)

        while has_modified:
            number, has_modified = explode(number)

            if not has_modified:
                number, has_modified = split(number)

    return number


def magnitude(number):
    if isinstance(number, int):
        return number

    return magnitude(number[0]) * 3 + magnitude(number[1]) * 2
