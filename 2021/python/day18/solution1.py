#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json

from .snailfish import magnitude, reduce_numbers


def solve(input_text):
    number = reduce_numbers(*(json.loads(line) for line in input_text))

    return magnitude(number)


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
