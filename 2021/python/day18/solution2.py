#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
from itertools import permutations

from .snailfish import magnitude, reduce_numbers


def solve(input_text):
    highest = 0

    for a, b in permutations(input_text, 2):
        result = magnitude(reduce_numbers(json.loads(a), json.loads(b)))

        highest = max(result, highest)

    return highest


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
