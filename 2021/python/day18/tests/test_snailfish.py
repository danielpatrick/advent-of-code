#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json

import pytest

from shared import get_input

from .. import snailfish
from ..snailfish import add, explode, magnitude, reduce_numbers, split


class TestSnailfish:
    @pytest.fixture
    def input_text1(self):
        yield get_input(snailfish.__file__, "test_input1.txt")

    @pytest.fixture
    def input_text2(self):
        yield get_input(snailfish.__file__, "test_input2.txt")

    def test_explode1(self):
        number = [[[[[9, 8], 1], 2], 3], 4]
        expected = ([[[[0, 9], 2], 3], 4], True)
        assert explode(number) == expected

    def test_explode2(self):
        number = [7, [6, [5, [4, [3, 2]]]]]
        expected = ([7, [6, [5, [7, 0]]]], True)
        assert explode(number) == expected

    def test_explode3(self):
        number = [[6, [5, [4, [3, 2]]]], 1]
        expected = ([[6, [5, [7, 0]]], 3], True)
        assert explode(number) == expected

    def test_explode4(self):
        number = [[3, [2, [1, [7, 3]]]], [6, [5, [4, [3, 2]]]]]
        expected = ([[3, [2, [8, 0]]], [9, [5, [4, [3, 2]]]]], True)
        assert explode(number) == expected

    def test_explode5(self):
        number = [[3, [2, [8, 0]]], [9, [5, [4, [3, 2]]]]]
        expected = ([[3, [2, [8, 0]]], [9, [5, [7, 0]]]], True)
        assert explode(number) == expected

    def test_explode_not_changed(self):
        assert explode([[[[0, 9], 2], 3], 4]) == ([[[[0, 9], 2], 3], 4], False)

    def test_split1(self):
        number = [[[[0, 7], 4], [15, [0, 13]]], [1, 1]]
        expected = ([[[[0, 7], 4], [[7, 8], [0, 13]]], [1, 1]], True)
        assert split(number) == expected

    def test_split2(self):
        number = [[[[0, 7], 4], [[7, 8], [0, 13]]], [1, 1]]
        expected = ([[[[0, 7], 4], [[7, 8], [0, [6, 7]]]], [1, 1]], True)
        assert split(number) == expected

    def test_split_not_changed(self):
        number = [[[[1, 7], 4], [[7, 8], [0, [6, 7]]]], [1, 1]]
        expected = ([[[[1, 7], 4], [[7, 8], [0, [6, 7]]]], [1, 1]], False)
        assert split(number) == expected

    def test_add1(self):
        assert add([1, 2], [[3, 4], 5]) == [[1, 2], [[3, 4], 5]]

    def test_add2(self):
        number1 = [[[[4, 3], 4], 4], [7, [[8, 4], 9]]]
        number2 = [1, 1]
        expected = [[[[[4, 3], 4], 4], [7, [[8, 4], 9]]], [1, 1]]

        assert add(number1, number2) == expected

    def test_reduce_numbers_simple(self):
        numbers = ([[[[4, 3], 4], 4], [7, [[8, 4], 9]]], [1, 1])
        expected = [[[[0, 7], 4], [[7, 8], [6, 0]]], [8, 1]]

        assert reduce_numbers(*numbers) == expected

    def test_reduce_numbers_with_example_input1(self, input_text1):
        expected = [
            [[[6, 6], [7, 6]], [[7, 7], [7, 0]]],
            [[[7, 7], [7, 7]], [[7, 8], [9, 9]]],
        ]
        numbers = [json.loads(line) for line in input_text1]

        assert reduce_numbers(*numbers) == expected

    def test_reduce_numbers_with_example_input2(self, input_text2):
        expected = [
            [[[8, 7], [7, 7]], [[8, 6], [7, 7]]],
            [[[0, 7], [6, 6]], [8, 7]],
        ]
        numbers = [json.loads(line) for line in input_text2]

        assert reduce_numbers(*numbers) == expected

    def test_magnitude1(self):
        assert magnitude([9, 1]) == 29
        assert magnitude([1, 9]) == 21
        assert magnitude([[9, 1], [1, 9]]) == 129
        assert magnitude([[1, 2], [[3, 4], 5]]) == 143
        assert magnitude([[[[0, 7], 4], [[7, 8], [6, 0]]], [8, 1]]) == 1384
        assert magnitude([[[[1, 1], [2, 2]], [3, 3]], [4, 4]]) == 445
        assert magnitude([[[[3, 0], [5, 3]], [4, 4]], [5, 5]]) == 791
        assert magnitude([[[[5, 0], [7, 4]], [5, 5]], [6, 6]]) == 1137

    def test_magnitude2(self):
        number = [
            [[[8, 7], [7, 7]], [[8, 6], [7, 7]]],
            [[[0, 7], [6, 6]], [8, 7]],
        ]

        assert magnitude(number) == 3488
