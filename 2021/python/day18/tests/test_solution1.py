#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest

from shared import get_input

from .. import solution1
from ..solution1 import solve


class TestSolution:
    @pytest.fixture
    def input_text1(self):
        yield get_input(solution1.__file__, "test_input1.txt")

    @pytest.fixture
    def input_text2(self):
        yield get_input(solution1.__file__, "test_input2.txt")

    def test_solver(self, input_text1, input_text2):
        assert solve(input_text1) == 4140
        assert solve(input_text2) == 3488
