#!/usr/bin/env python
# -*- coding: utf-8 -*-

from collections import deque

mappings = [
    lambda x, y, z: (x, y, z),
    lambda x, y, z: (x, z, -y),
    lambda x, y, z: (x, -z, y),
    lambda x, y, z: (x, -y, -z),
    lambda x, y, z: (-x, z, y),
    lambda x, y, z: (-x, y, -z),
    lambda x, y, z: (-x, -y, z),
    lambda x, y, z: (-x, -z, -y),
    lambda x, y, z: (y, z, x),
    lambda x, y, z: (y, x, -z),
    lambda x, y, z: (y, -x, z),
    lambda x, y, z: (y, -z, -x),
    lambda x, y, z: (-y, x, z),
    lambda x, y, z: (-y, z, -x),
    lambda x, y, z: (-y, -z, x),
    lambda x, y, z: (-y, -x, -z),
    lambda x, y, z: (z, x, y),
    lambda x, y, z: (z, y, -x),
    lambda x, y, z: (z, -y, x),
    lambda x, y, z: (z, -x, -y),
    lambda x, y, z: (-z, y, x),
    lambda x, y, z: (-z, x, -y),
    lambda x, y, z: (-z, -x, y),
    lambda x, y, z: (-z, -y, -x),
]


def sub(pos1, pos2):
    return (pos1[0] - pos2[0], pos1[1] - pos2[1], pos1[2] - pos2[2])


def add(pos1, pos2):
    return (pos1[0] + pos2[0], pos1[1] + pos2[1], pos1[2] + pos2[2])


class FixedScanner:
    def __init__(self, number, positions, location=None):
        self.number = number
        self.location = location or (0, 0, 0)
        self.positions = sorted(
            (x + self.location[0], y + self.location[1], z + self.location[2])
            for x, y, z in positions
        )

    def find_overlap(self, scanner):
        for positions in scanner.generate_positions():
            offset = self.get_offset(positions)

            if offset is not None:
                return FixedScanner(scanner.number, positions, offset)

    def get_offset(self, positions):
        # This method seems naive but works for the given inputs.
        counts = {}

        for p1 in self.positions:
            for p2 in positions:
                diff = sub(p1, p2)
                n = counts.get(diff, 0) + 1

                if n >= 12:
                    return diff

                counts[diff] = n


class Scanner:
    def __init__(self, s):
        lines = s.split("\n")

        self.number = int(lines[0].split()[2])

        self.positions = [[]]

        for line in lines[1:]:
            self.positions[0].append(tuple(int(s) for s in line.split(",")))

        self.positions[0].sort()

    def generate_positions(self):
        for i in range(24):
            if i + 1 > len(self.positions):
                mapping = mappings[i]

                self.positions.append(
                    sorted(mapping(*p) for p in self.positions[0])
                )

            yield self.positions[i]


def locate_scanners(input_text):
    input_text = "\n".join(input_text)
    scanners = deque(Scanner(s.strip()) for s in input_text.split("\n\n"))

    scanner = scanners.popleft()
    overlapping = [FixedScanner(scanner.number, scanner.positions[0])]

    added = [0]

    while scanners:
        new_added = []

        for i in added:
            fixed = overlapping[i]

            for _ in range(len(scanners)):
                scanner = scanners.popleft()
                overlap = fixed.find_overlap(scanner)

                if overlap is not None:
                    overlapping.append(overlap)
                    new_added.append(len(overlapping) - 1)
                else:
                    scanners.append(scanner)

        added = new_added

    return overlapping
