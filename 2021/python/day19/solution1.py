#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .scanners import locate_scanners


def solve(input_text):
    scanners = locate_scanners(input_text)

    return len(set(pos for s in scanners for pos in s.positions))


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
