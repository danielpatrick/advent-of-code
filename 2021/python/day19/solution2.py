#!/usr/bin/env python
# -*- coding: utf-8 -*-

from itertools import combinations

from .scanners import locate_scanners


def distance(a, b):
    return abs(a[0] - b[0]) + abs(a[1] - b[1]) + abs(a[2] - b[2])


def solve(input_text):
    scanners = locate_scanners(input_text)
    pairs = combinations((s.location for s in scanners), 2)

    return max(distance(a, b) for a, b in pairs)


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
