#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest

from shared import get_input

from .. import scanners
from ..scanners import Scanner, locate_scanners
from .fixtures import expected_positions

scanner_lines = (
    "--- scanner 0 ---\n"
    "-1,-1,1\n"
    "-2,-2,2\n"
    "-3,-3,3\n"
    "-2,-3,1\n"
    "5,6,-4\n"
    "8,0,7"
)


class TestSolution:
    @pytest.fixture
    def input_text(self):
        yield get_input(scanners.__file__, "test_input.txt")

    def test_locate_scanners(self, input_text):
        scanners = locate_scanners(input_text)

        positions = set(pos for s in scanners for pos in s.positions)

        assert len(positions) == 79

    def test_generate_positions(self):

        scanner = Scanner(scanner_lines)

        positions_list = list(scanner.generate_positions())
        unique_positions_lists = set(tuple(p) for p in positions_list)

        assert sorted(positions_list) == sorted(expected_positions)
        assert len(unique_positions_lists) == 24
