#!/usr/bin/env python
# -*- coding: utf-8 -*-


class ImageEnhancer:
    decode = str.maketrans({".": "0", "#": "1"})
    encode = str.maketrans({"0": ".", "1": "#"})

    def __init__(self, s):
        self.algorithm = s[0].translate(self.decode)
        self.image = []

        for line in s[2:]:
            self.image.append(line.translate(self.decode))

        self.height = len(self.image)
        self.width = len(self.image[0])
        self.default = "0"

    def enhance(self):
        new_image = []

        for y in range(-1, self.height + 1):
            new_image.append(
                "".join(
                    self.get_new_pixel(x, y) for x in range(-1, self.width + 1)
                )
            )

        self.image = new_image
        self.height = len(self.image)
        self.width = len(self.image[0])

        if self.algorithm[0] == "1":
            if self.default == "0":
                self.default = "1"
            else:
                self.default = "0"

    def get_new_pixel(self, x, y):
        return self.algorithm[self.get_enhanced_value(x, y)]

    def get_enhanced_value(self, x, y):
        return int(self.get_nine(x, y), 2)

    def get_nine(self, x, y):
        return (
            self.get_three(x, y - 1)
            + self.get_three(x, y)
            + self.get_three(x, y + 1)
        )

    def get_three(self, x, y):
        if 0 <= y < self.height:
            if x == -1:
                return f"{self.default}{self.default}{self.image[y][x + 1]}"
            if x == 0:
                return f"{self.default}{self.image[y][x : x + 2]}"
            if 0 <= x < self.width - 1:
                return self.image[y][x - 1 : x + 2]
            if x == self.width - 1:
                return f"{self.image[y][x - 1 : x + 1]}{self.default}"
            if x == self.width:
                return f"{self.image[y][x - 1]}{self.default}{self.default}"

        return self.default * 3

    def lit_pixel_count(self):
        return sum(row.count("1") for row in self.image)

    def __str__(self):
        return "".join(row.translate(self.encode) + "\n" for row in self.image)
