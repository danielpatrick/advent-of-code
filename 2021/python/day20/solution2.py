#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .enhancer import ImageEnhancer


def solve(input_text):
    image = ImageEnhancer(input_text)

    for _ in range(50):
        image.enhance()

    return image.lit_pixel_count()


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
