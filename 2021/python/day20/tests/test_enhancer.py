#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest

from shared import get_input

from .. import enhancer
from ..enhancer import ImageEnhancer


class TestEnhancer:
    @pytest.fixture
    def input_text(self):
        yield get_input(enhancer.__file__, "test_input.txt")

    def test_get_three(self, input_text):
        to_test = (
            (-1, -1, "000"),
            (-1, 0, "001"),
            (0, 0, "010"),
            (1, 1, "100"),
            (2, 2, "100"),
            (3, 3, "100"),
            (4, 4, "110"),
            (5, 4, "100"),
            (5, 5, "000"),
            (3, 0, "010"),
            (4, 0, "100"),
            (5, 0, "000"),
        )
        image = ImageEnhancer(input_text)

        for x, y, expected in to_test:
            assert image.get_three(x, y) == expected

    def test_get_nine(self, input_text):
        to_test = (
            (0, 0, "000010010"),
            (1, 1, "100100110"),
            (2, 2, "000100010"),
        )
        image = ImageEnhancer(input_text)

        for x, y, expected in to_test:
            assert image.get_nine(x, y) == expected
