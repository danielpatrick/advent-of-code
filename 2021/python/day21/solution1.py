#!/usr/bin/env python
# -*- coding: utf-8 -*-

from itertools import islice


def parse_player(s):
    words = s.split()
    return (int(words[-1]), 0)


def move(position, score, n):
    position += n
    position %= 10

    if position == 0:
        position = 10

    return (position, score + position)


def deterministic_die(num_sides):
    prev_num = num_sides

    while True:
        if prev_num == num_sides:
            prev_num = 1
        else:
            prev_num += 1

        yield prev_num


def solve(input_text):
    pos1, score1 = parse_player(input_text[0])
    pos2, score2 = parse_player(input_text[1])
    die_rolls = deterministic_die(100)

    num_rolls = 0
    losing_score = None

    while True:
        num_rolls += 3
        pos1, score1 = move(pos1, score1, sum(islice(die_rolls, 3)))

        if score1 >= 1000:
            losing_score = score2
            break

        num_rolls += 3
        pos2, score2 = move(pos2, score2, sum(islice(die_rolls, 3)))

        if score2 >= 1000:
            losing_score = score1
            break

    return losing_score * num_rolls


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
