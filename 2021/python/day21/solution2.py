#!/usr/bin/env python
# -*- coding: utf-8 -*-

POSSIBLE_ROLLS = ((3, 1), (4, 3), (5, 6), (6, 7), (7, 6), (8, 3), (9, 1))


def parse_player(s):
    words = s.split()
    return (int(words[-1]), 0)


def move(position, score, n):
    position += n
    position %= 10

    if position == 0:
        position = 10

    return (position, score + position)


def roll(player1, player2, cache={}):  # noqa: B006
    if player1[1] >= 21:
        return (1, 0)
    if player2[1] >= 21:
        return (0, 1)

    key = (*player1, *player2)

    if key in cache:
        return cache[key]

    num_wins = [0, 0]

    for n, count in POSSIBLE_ROLLS:
        two, one = roll(player2, move(*player1, n))

        num_wins[0] += one * count
        num_wins[1] += two * count

    num_wins = tuple(num_wins)
    cache[key] = num_wins

    return num_wins


def solve(input_text):
    player1 = parse_player(input_text[0])
    player2 = parse_player(input_text[1])

    result = roll(player1, player2)

    return max(result)


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
