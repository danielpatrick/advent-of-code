#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re

REGEX = re.compile(
    r"^(?P<instruction>on|off) "
    r"x=(?P<x1>-?\d+)\.\.(?P<x2>-?\d+),"
    r"y=(?P<y1>-?\d+)\.\.(?P<y2>-?\d+),"
    r"z=(?P<z1>-?\d+)\.\.(?P<z2>-?\d+)$",
    re.MULTILINE,
)


class Region:
    def __init__(self, x1, x2, y1, y2, z1, z2):
        self.x1 = int(x1)
        self.x2 = int(x2)
        self.y1 = int(y1)
        self.y2 = int(y2)
        self.z1 = int(z1)
        self.z2 = int(z2)

    @property
    def coords(self):
        return (self.x1, self.x2, self.y1, self.y2, self.z1, self.z2)

    @property
    def area(self):
        return (
            (self.x2 + 1 - self.x1)
            * (self.y2 + 1 - self.y1)
            * (self.z2 + 1 - self.z1)
        )

    @property
    def init_area(self):
        def clamp(x):
            return max(-50, min(x, 51))

        return (
            (clamp(self.x2 + 1) - clamp(self.x1))
            * (clamp(self.y2 + 1) - clamp(self.y1))
            * (clamp(self.z2 + 1) - clamp(self.z1))
        )

    def has_overlap(self, other):
        return (
            self.x2 >= other.x1
            and other.x2 >= self.x1
            and self.y2 >= other.y1
            and other.y2 >= self.y1
            and self.z2 >= other.z1
            and other.z2 >= self.z1
        )

    def clone(self, **kwargs):
        return Region(
            x1=kwargs.get("x1", self.x1),
            x2=kwargs.get("x2", self.x2),
            y1=kwargs.get("y1", self.y1),
            y2=kwargs.get("y2", self.y2),
            z1=kwargs.get("z1", self.z1),
            z2=kwargs.get("z2", self.z2),
        )

    def __sub__(self, other):
        if not self.has_overlap(other):
            return [self.clone()]

        new_regions = []

        if self.x1 < other.x1:
            new_regions.append(self.clone(x2=other.x1 - 1))
        if self.x2 > other.x2:
            new_regions.append(self.clone(x1=other.x2 + 1))

        if self.y1 < other.y1:
            new_regions.append(
                self.clone(
                    x1=max(self.x1, other.x1),
                    x2=min(self.x2, other.x2),
                    y2=other.y1 - 1,
                )
            )
        if self.y2 > other.y2:
            new_regions.append(
                self.clone(
                    x1=max(self.x1, other.x1),
                    x2=min(self.x2, other.x2),
                    y1=other.y2 + 1,
                )
            )

        if self.z1 < other.z1:
            new_regions.append(
                self.clone(
                    x1=max(self.x1, other.x1),
                    x2=min(self.x2, other.x2),
                    y1=max(self.y1, other.y1),
                    y2=min(self.y2, other.y2),
                    z2=other.z1 - 1,
                )
            )
        if self.z2 > other.z2:
            new_regions.append(
                self.clone(
                    x1=max(self.x1, other.x1),
                    x2=min(self.x2, other.x2),
                    y1=max(self.y1, other.y1),
                    y2=min(self.y2, other.y2),
                    z1=other.z2 + 1,
                )
            )

        return new_regions


class ReactorCore:
    def __init__(self):
        self.cubes = []

    def get_non_overlapping(self, region2):
        new_cubes = []

        for region1 in self.cubes:
            # Check has_overlap to avoid slower extend where possible
            if region1.has_overlap(region2):
                new_cubes.extend(region1 - region2)
            else:
                new_cubes.append(region1)

        return new_cubes

    def __isub__(self, region):
        self.cubes = self.get_non_overlapping(region)
        return self

    def __iadd__(self, region):
        self -= region
        self.cubes.append(region)
        return self

    def count(self):
        return sum(region.area for region in self.cubes)

    def count_init(self):
        return sum(region.init_area for region in self.cubes)


class RebootStep:
    def __init__(self, **kwargs):
        self.instruction = kwargs.pop("instruction")
        self.region = Region(**kwargs)


def parse_reboot_steps(input_text):
    for m in REGEX.finditer("\n".join(input_text)):
        yield RebootStep(**m.groupdict())
