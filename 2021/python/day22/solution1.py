#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .reactor import ReactorCore, parse_reboot_steps


def solve(input_text):
    core = ReactorCore()

    for step in parse_reboot_steps(input_text):
        if step.instruction == "on":
            core += step.region
        else:
            core -= step.region

    return core.count_init()


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
