#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest

from shared import get_input

from .. import solution1
from ..solution1 import solve


class TestSolution:
    @pytest.fixture
    def input_text1(self):
        yield get_input(solution1.__file__, "test_input1.txt")

    @pytest.fixture
    def input_text2(self):
        yield get_input(solution1.__file__, "test_input2.txt")

    @pytest.fixture
    def input_text3(self):
        yield get_input(solution1.__file__, "test_input3.txt")

    def test_solver1(self, input_text1):
        solution = solve(input_text1)

        assert solution == 39

    def test_solver2(self, input_text2):
        solution = solve(input_text2)

        assert solution == 590784

    def test_solver3(self, input_text3):
        solution = solve(input_text3)

        assert solution == 474140
