#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest

from shared import get_input

from .. import solution2
from ..solution2 import solve


class TestSolution:
    @pytest.fixture
    def input_text3(self):
        yield get_input(solution2.__file__, "test_input3.txt")

    def test_solver(self, input_text3):
        solution = solve(input_text3)

        assert solution == 2758514936282235
