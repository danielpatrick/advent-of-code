#!/usr/bin/env python
# -*- coding: utf-8 -*-

import heapq
from enum import Enum


class AnsiIntensity(Enum):
    NORMAL = 0
    BOLD = 1
    FAINT = 2


def displayable(char, colour=None, intensity=AnsiIntensity.NORMAL):
    i = f";{intensity.value}" if intensity != AnsiIntensity.NORMAL else ""
    col = 49 if colour is None else f"38;5;{colour}"

    return f"\033[{col}{i}m{char}\033[0m"


DISPLAY_MAP = str.maketrans(
    {
        "#": displayable("#", intensity=AnsiIntensity.FAINT),
        "A": displayable("A", 202, AnsiIntensity.BOLD),
        "B": displayable("B", 208, AnsiIntensity.BOLD),
        "C": displayable("C", 214, AnsiIntensity.BOLD),
        "D": displayable("D", 220, AnsiIntensity.BOLD),
        ".": displayable(".", intensity=AnsiIntensity.FAINT),
    }
)


class Burrow:
    HALLWAY = {0, 1, 2, 3, 4, 5, 6}
    HALLWAY_COLS = [1, 2, 4, 6, 8, 10, 11]
    ENERGY_MULTIPLIERS = [1, 10, 100, 1000]
    BANNED_SUBPATHS = (
        [1, 11, 2],
        [2, 11, 1],
        [2, 21, 3],
        [3, 21, 2],
        [3, 31, 4],
        [4, 31, 3],
        [4, 41, 5],
        [5, 41, 4],
    )

    # To set in subclass
    GRAPH = None
    ROOM_SIZE = None

    def __init__(self, s):
        self.objectives = {
            n: tuple((n + 1) * 10 + i for i in range(self.ROOM_SIZE, 0, -1))
            for n in range(4)
        }
        self.rooms = set(
            pos for room in self.objectives.values() for pos in room
        )
        self.rooms_map = {k: set(v) for k, v in self.objectives.items()}

        self.starting = self.create_starting(s)
        self.finishing = tuple(sorted(self.starting))

        self.ab_path_costs = self.find_path_costs()

    def create_starting(self, s):
        starting = [pair[1] for pair in sorted(self.parse_str(s))]

        return (
            tuple(sorted(starting[: self.ROOM_SIZE]))
            + tuple(sorted(starting[self.ROOM_SIZE : self.ROOM_SIZE * 2]))
            + tuple(sorted(starting[self.ROOM_SIZE * 2 : self.ROOM_SIZE * 3]))
            + tuple(sorted(starting[self.ROOM_SIZE * 3 :]))
        )

    def find_paths(self, path, cost=0):
        found_paths = []
        if cost > 0:
            found_paths.append((path, cost))

        for next_pos, next_cost in self.GRAPH[path[-1]].items():
            new_path = path[:] + [next_pos]

            if next_pos not in path and new_path not in self.BANNED_SUBPATHS:
                found_paths.extend(self.find_paths(new_path, cost + next_cost))

        return found_paths

    def find_path_costs(self):
        paths = {}

        for key in self.GRAPH:
            for path, cost in self.find_paths([key]):
                start = path[0]
                end = path[-1]

                if start in self.HALLWAY and end in self.HALLWAY:
                    continue

                if start in self.rooms and end in self.rooms:
                    continue

                if (start, end) not in paths or paths[(start, end)][-1] > cost:
                    paths[(start, end)] = (set(path[1:]), cost)

        return paths

    def can_leave_room(self, positions, i, target_room):
        room = self.rooms_map[target_room]

        if positions[i] not in room:
            return True

        first = target_room * self.ROOM_SIZE
        last = first + self.ROOM_SIZE
        other_pods = set(positions[:first] + positions[last:])

        return bool(other_pods & room)

    def updated_positions(self, positions, i, target):
        start = (i // self.ROOM_SIZE) * self.ROOM_SIZE
        end = start + self.ROOM_SIZE
        j = (i % self.ROOM_SIZE) + start

        return (
            positions[:start]
            + tuple(
                sorted(positions[start:j] + (target,) + positions[j + 1 : end])
            )
            + positions[end:]
        )

    def next_moves(self, positions):
        for i, pos in enumerate(positions):
            pod_type = i // self.ROOM_SIZE

            if pos in self.rooms:
                if not self.can_leave_room(positions, i, pod_type):
                    continue

                for end in self.HALLWAY:
                    path, units = self.ab_path_costs[(pos, end)]

                    if bool(set(positions) & path):
                        continue

                    cost = units * self.ENERGY_MULTIPLIERS[pod_type]
                    yield (cost, self.updated_positions(positions, i, end))
            else:
                first = pod_type * self.ROOM_SIZE
                last = first + self.ROOM_SIZE

                other_pods = set(positions[:first] + positions[last:])
                sibling_pods = set(positions[first:last])

                room = self.objectives[pod_type]

                for end in room:
                    if end in other_pods:
                        break

                    if end in sibling_pods:
                        continue

                    path, units = self.ab_path_costs[(pos, end)]

                    if bool(set(positions) & path):
                        continue

                    cost = units * self.ENERGY_MULTIPLIERS[pod_type]
                    yield (cost, self.updated_positions(positions, i, end))
                    break

    def estimated_remaining(self, positions):
        in_place = [0, 0, 0, 0]
        to_room = [0, 0, 0, 0]
        cost = 0

        for i, pos in enumerate(positions):
            n = i // self.ROOM_SIZE
            col = n * 2 + 3

            if pos in self.HALLWAY:
                x = self.HALLWAY_COLS[pos]
                to_room[n] += abs(col - x)
            else:
                x = (pos // 10) * 2 + 1

                if x == col:
                    in_place[n] += 1
                elif x != col:
                    to_room[n] += abs(col - x) + (pos % 10)

        for r in range(4):
            n = 4 - in_place[r]
            room_cost = to_room[r] + ((n * (n - 1)) // 2)

            cost += room_cost * self.ENERGY_MULTIPLIERS[r]

        return cost

    def find_lowest_cost(self):
        start = self.starting
        print("Start")
        self.print(start)

        queue = []
        heapq.heappush(queue, (self.estimated_remaining(start), 0, start))
        costs = {start: 0}

        while queue:
            estimate, cost, current = heapq.heappop(queue)

            if current == self.finishing:
                print("Finish")
                self.print(current)

                return cost

            for next_cost, positions in self.next_moves(current):
                next_cost += cost

                if positions not in costs or next_cost < costs[positions]:
                    costs[positions] = next_cost
                    heapq.heappush(
                        queue,
                        (
                            next_cost + self.estimated_remaining(positions),
                            next_cost,
                            positions,
                        ),
                    )

        raise Exception("No solution found")

    def _to_str(self, positions):
        result = [
            list("#############\n"),
            list("#...........#\n"),
            list("###.#.#.#.###\n"),
            *(list("  #.#.#.# #\n") for _ in range(self.ROOM_SIZE - 1)),
            list("  #########\n"),
        ]

        for i, pos in enumerate(positions):
            x = y = 0

            if pos in self.HALLWAY:
                y = 1
                x = self.HALLWAY_COLS[pos]
            else:
                x = (pos // 10) * 2 + 1
                y = pos % 10 + 1

            result[y][x] = ["A", "B", "C", "D"][i // self.ROOM_SIZE]

        return "".join("".join(line) for line in result).translate(DISPLAY_MAP)

    def print(self, positions):
        print(self._to_str(positions))

    def __str__(self):
        return self._to_str(self.starting)
