#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .burrow import Burrow

#  #  #  #  #  #  #  #  #  #  #  #  #  #
#
#  #  0  1     2     3     4     5  6  #
#
#  #  #  #  11 #  21 #  31 #  41 #  #  #
#
#        #  12 #  22 #  32 A  42 #
#
#        #  #  #  #  #  #  #  #  #


class SmallBurrow(Burrow):
    GRAPH = {
        0: {1: 1},
        1: {0: 1, 2: 2, 11: 2},
        2: {1: 2, 3: 2, 11: 2, 21: 2},
        3: {2: 2, 4: 2, 21: 2, 31: 2},
        4: {3: 2, 5: 2, 31: 2, 41: 2},
        5: {4: 2, 6: 1, 41: 2},
        6: {5: 1},
        11: {1: 2, 2: 2, 12: 1},
        12: {11: 1},
        21: {2: 2, 3: 2, 22: 1},
        22: {21: 1},
        31: {3: 2, 4: 2, 32: 1},
        32: {31: 1},
        41: {4: 2, 5: 2, 42: 1},
        42: {41: 1},
    }
    ROOM_SIZE = 2

    def parse_str(self, s):
        return [
            (s[2][3], 11),
            (s[3][3], 12),
            (s[2][5], 21),
            (s[3][5], 22),
            (s[2][7], 31),
            (s[3][7], 32),
            (s[2][9], 41),
            (s[3][9], 42),
        ]


def solve(input_text):
    b = SmallBurrow(input_text)

    return b.find_lowest_cost()


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
