#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .burrow import Burrow

#  #  #  #  #  #  #  #  #  #  #  #  #  #
#
#  #  0  1     2     3     4     5  6  #
#
#  #  #  #  11 #  21 #  31 #  41 #  #  #
#
#        #  12 #  22 #  32 #  42 #
#
#        #  13 #  23 #  33 #  43 #
#
#        #  14 #  24 #  34 #  44 #
#
#        #  #  #  #  #  #  #  #  #


class LargeBurrow(Burrow):
    GRAPH = {
        0: {1: 1},
        1: {0: 1, 2: 2, 11: 2},
        2: {1: 2, 3: 2, 11: 2, 21: 2},
        3: {2: 2, 4: 2, 21: 2, 31: 2},
        4: {3: 2, 5: 2, 31: 2, 41: 2},
        5: {4: 2, 6: 1, 41: 2},
        6: {5: 1},
        11: {1: 2, 2: 2, 12: 1},
        12: {11: 1, 13: 1},
        13: {12: 1, 14: 1},
        14: {13: 1},
        21: {2: 2, 3: 2, 22: 1},
        22: {21: 1, 23: 1},
        23: {22: 1, 24: 1},
        24: {23: 1},
        31: {3: 2, 4: 2, 32: 1},
        32: {31: 1, 33: 1},
        33: {32: 1, 34: 1},
        34: {33: 1},
        41: {4: 2, 5: 2, 42: 1},
        42: {41: 1, 43: 1},
        43: {42: 1, 44: 1},
        44: {43: 1},
    }
    ROOM_SIZE = 4

    def parse_str(self, s):
        return [
            (s[2][3], 11),
            ("D", 12),
            ("D", 13),
            (s[3][3], 14),
            (s[2][5], 21),
            ("C", 22),
            ("B", 23),
            (s[3][5], 24),
            (s[2][7], 31),
            ("B", 32),
            ("A", 33),
            (s[3][7], 34),
            (s[2][9], 41),
            ("A", 42),
            ("C", 43),
            (s[3][9], 44),
        ]


def solve(input_text):
    b = LargeBurrow(input_text)

    return b.find_lowest_cost()


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
