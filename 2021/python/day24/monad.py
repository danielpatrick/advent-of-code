#!/usr/bin/env python
# -*- coding: utf-8 -*-


def parse_instructions(input_text):
    result = []

    for i in range(14):
        result.append(
            (
                int(input_text[i * 18 + 4][6:]),
                int(input_text[i * 18 + 5][6:]),
                int(input_text[i * 18 + 15][6:]),
            )
        )

    return result


def find_valid_model_number(instructions, largest=True):
    limit = 9 if largest else 1
    f = min if largest else max
    num = 0
    stack = []

    for i, (a, b, c) in enumerate(instructions):
        if a == 1:
            stack.append((i, c))
        else:
            j, popped = stack.pop()
            num += f(limit, limit - b - popped) * 10 ** (13 - j)
            num += f(limit, limit + b + popped) * 10 ** (13 - i)

    return num
