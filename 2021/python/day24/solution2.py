#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .monad import find_valid_model_number, parse_instructions


def solve(input_text):
    instructions = parse_instructions(input_text)

    return find_valid_model_number(instructions, False)


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
