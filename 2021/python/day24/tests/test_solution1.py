#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest

from shared import get_input

from .. import solution1
from ..solution1 import solve


class TestSolution:
    @pytest.fixture
    def input_text(self):
        yield get_input(solution1.__file__, "input.txt")

    def test_solver(self, input_text):
        # There's not really a lot to test here
        assert isinstance(solve(input_text), int)
