#!/usr/bin/env python
# -*- coding: utf-8 -*-


class Grid:
    def __init__(self, lines):
        self.height = len(lines)
        self.width = len(lines[0])
        self.right_movers = set()
        self.down_movers = set()

        for y, row in enumerate(lines):
            for x, col in enumerate(row):
                if col == ">":
                    self.right_movers.add((x, y))
                elif col == "v":
                    self.down_movers.add((x, y))

    def idx(self, x, y):
        return self.width * y + x

    def right(self, x, y):
        return ((x + 1) % self.width, y)

    def down(self, x, y):
        return (x, (y + 1) % self.height)

    def move(self):
        return any(
            [
                self.move_dir(self.right_movers, self.right),
                self.move_dir(self.down_movers, self.down),
            ]
        )

    def move_dir(self, movers, move):
        to_remove = set()
        to_add = set()

        for (x, y) in movers:
            right = move(x, y)
            if not (right in self.right_movers or right in self.down_movers):
                to_remove.add((x, y))
                to_add.add(right)

        movers -= to_remove
        movers |= to_add

        return len(to_add) > 0

    def __str__(self):
        result = []

        for y in range(self.height):
            line = []
            for x in range(self.width):
                if (x, y) in self.right_movers:
                    line.append(">")
                elif (x, y) in self.down_movers:
                    line.append("v")
                else:
                    line.append(".")
            line.append("\n")
            result.append("".join(line))

        return "".join(result)


def solve(input_text):
    grid = Grid(input_text)

    count = 0

    while True:
        count += 1

        if not grid.move():
            break

    return count


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
