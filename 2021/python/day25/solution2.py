#!/usr/bin/env python
# -*- coding: utf-8 -*-


def solve(_):
    return "Finished!\n\nhttps://adventofcode.com/2021/"


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
