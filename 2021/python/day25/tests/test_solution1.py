#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest

from shared import get_input

from .. import solution1
from ..solution1 import Grid, solve


class TestSolution:
    @pytest.fixture
    def input_text(self):
        yield get_input(solution1.__file__, "test_input.txt")

    def test_solver(self, input_text):
        solution = solve(input_text)

        assert solution == 58

    def test_str(self, input_text):
        expected = (
            "v...>>.vv>\n"
            ".vv>>.vv..\n"
            ">>.>v>...v\n"
            ">>v>>.>.v.\n"
            "v>v.vv.v..\n"
            ">.>>..v...\n"
            ".vv..>.>v.\n"
            "v.v..>>v.v\n"
            "....v..v.>\n"
        )
        grid = Grid(input_text)

        assert str(grid) == expected

    def test_move(self, input_text):
        expected = (
            "....>.>v.>\n"
            "v.v>.>v.v.\n"
            ">v>>..>v..\n"
            ">>v>v>.>.v\n"
            ".>v.v...v.\n"
            "v>>.>vvv..\n"
            "..v...>>..\n"
            "vv...>>vv.\n"
            ">.v.v..v.v\n"
        )
        grid = Grid(input_text)
        grid.move()

        assert str(grid) == expected
