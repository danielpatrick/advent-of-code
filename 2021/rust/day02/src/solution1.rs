pub fn solve(input: &str) -> i32 {
    let (x, y) = input
        .lines()
        .map(|line| {
            let mut split = line.splitn(2, ' ');
            (
                split.next().unwrap(),
                split.next().unwrap().parse::<i32>().unwrap(),
            )
        })
        .fold((0, 0), |(x, y), (dir, n)| match dir {
            "forward" => (x + n, y),
            "down" => (x, y + n),
            "up" => (x, y - n),
            _ => panic!("Invalid direction: '{}'", dir),
        });

    x * y
}

#[cfg(test)]
mod tests {
    use super::solve;

    fn get_test_input() -> &'static str {
        include_str!("../../../inputs/day02/test_input.txt")
    }

    #[test]
    fn test_solve() {
        assert_eq!(solve(get_test_input()), 150);
    }
}
