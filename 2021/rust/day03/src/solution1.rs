pub fn solve(input: &str) -> usize {
    let length = input.lines().next().unwrap().len();

    let (gamma, epsilon) = (0..length)
        .map(|i| {
            input
                .chars()
                .filter(|c| c != &'\n')
                .skip(i)
                .step_by(length)
                .fold((0, 0), |(zeros, ones), n| match n {
                    '0' => (zeros + 1, ones),
                    '1' => (zeros, ones + 1),
                    c => panic!("Unexpected char: {}", c),
                })
        })
        .rev()
        .zip(0..) // used instead of enumerate because we need u32 for pow
        .fold((0, 0), |(gamma, epsilon), ((zeros, ones), i)| {
            let increase = 2_usize.pow(i);

            if zeros > ones {
                (gamma, epsilon + increase)
            } else {
                (gamma + increase, epsilon)
            }
        });

    gamma * epsilon
}

#[cfg(test)]
mod tests {
    use super::solve;

    fn get_test_input() -> &'static str {
        include_str!("../../../inputs/day03/test_input.txt")
    }

    #[test]
    fn test_solve() {
        assert_eq!(solve(get_test_input()), 198);
    }
}
