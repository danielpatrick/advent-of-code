fn calculate_rating<F>(lines: &[&str], i: usize, f: F) -> usize
where
    F: Fn(usize, usize) -> bool,
{
    if lines.is_empty() || i >= lines[0].len() {
        return 0;
    }

    let (zeroes, ones): (Vec<&str>, Vec<&str>) =
        lines.iter().partition(|line| match line.chars().nth(i) {
            Some('0') => true,
            Some('1') => false,
            c => panic!("Unexpected char: {:?}", c),
        });

    if f(zeroes.len(), ones.len()) {
        calculate_rating(&zeroes, i + 1, f)
    } else {
        calculate_rating(&ones, i + 1, f) + 2_usize.pow((lines[0].len() - i - 1) as u32)
    }
}

fn calculate_oxygen_rating(lines: &[&str]) -> usize {
    calculate_rating(lines, 0, |z, o| o > 0 && (z == 0 || z > o))
}

fn calculate_scrubber_rating(lines: &[&str]) -> usize {
    calculate_rating(lines, 0, |z, o| z > 0 && (o == 0 || o >= z))
}

pub fn solve(input: &str) -> usize {
    let lines: Vec<&str> = input.lines().collect();

    calculate_oxygen_rating(&lines) * calculate_scrubber_rating(&lines)
}

#[cfg(test)]
mod tests {
    use super::*;

    fn get_test_input() -> &'static str {
        include_str!("../../../inputs/day03/test_input.txt")
    }

    #[test]
    fn test_calculate_oxygen_rating() {
        let input = get_test_input();
        let lines = input.lines().collect::<Vec<_>>();

        assert_eq!(calculate_oxygen_rating(&lines), 23);
    }

    #[test]
    fn test_calculate_scrubber_rating() {
        let input = get_test_input();
        let lines = input.lines().collect::<Vec<_>>();

        assert_eq!(calculate_scrubber_rating(&lines), 10);
    }

    #[test]
    fn test_solve() {
        assert_eq!(solve(get_test_input()), 230);
    }
}
