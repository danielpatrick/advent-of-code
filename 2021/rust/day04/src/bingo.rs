use itertools::Itertools;
use std::collections::HashSet;
use std::fmt;

pub struct BingoCard {
    input: Vec<u8>,
    rows: Vec<HashSet<u8>>,
    cols: Vec<HashSet<u8>>,
    has_won: bool,
}

impl BingoCard {
    pub fn new(v: &[&str]) -> Self {
        let input = v
            .iter()
            .flat_map(|s| s.split_whitespace().map(|s| s.parse::<u8>().unwrap()))
            .collect::<Vec<_>>();

        let rows = input
            .chunks(5)
            .map(|row| row.iter().copied().collect())
            .collect();

        let cols = (0..5)
            .map(|i| input.chunks(5).map(|row| row[i]).collect())
            .collect();

        Self {
            input,
            rows,
            cols,
            has_won: false,
        }
    }

    pub fn remove_any_matches(&mut self, n: u8) -> bool {
        for row in &mut self.rows {
            if row.remove(&n) && row.is_empty() {
                self.has_won = true;
            }
        }

        for col in &mut self.cols {
            if col.remove(&n) && col.is_empty() {
                self.has_won = true;
            }
        }

        self.has_won
    }

    fn remaining_set(&self) -> HashSet<u8> {
        self.rows
            .iter()
            .flat_map(HashSet::iter)
            .copied()
            .collect::<HashSet<u8>>()
    }

    pub fn remaining_numbers(&self) -> Vec<u8> {
        let remaining = self.remaining_set();

        self.input
            .iter()
            .filter(|n| remaining.contains(n))
            .copied()
            .collect()
    }
}

impl fmt::Display for BingoCard {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let remaining = self.remaining_set();

        write!(
            f,
            "{}",
            self.input
                .chunks(5)
                .map(|line| {
                    line.iter()
                        .map(|n| {
                            if remaining.contains(n) {
                                format!(" {:>2} ", n)
                            } else {
                                format!("[{:>2}]", n)
                            }
                        })
                        .join(" ")
                        + "\n"
                })
                .collect::<String>()
        )
    }
}

pub fn parse_game_input(s: &str) -> (Vec<u8>, Vec<BingoCard>) {
    let mut iter = s.split("\n\n");
    let numbers = iter
        .next()
        .unwrap()
        .split(',')
        .map(|s| s.parse().unwrap())
        .collect();

    let cards = iter
        .map(|card_str| BingoCard::new(&card_str.lines().collect::<Vec<_>>()))
        .collect();

    (numbers, cards)
}

#[cfg(test)]
mod tests {
    use super::*;

    fn get_test_input() -> &'static str {
        include_str!("../../../inputs/day04/test_input.txt")
    }

    fn card_one_args() -> Vec<&'static str> {
        vec![
            "22 13 17 11  0",
            " 8  2 23  4 24",
            "21  9 14 16  7",
            " 6 10  3 18  5",
            " 1 12 20 15 19",
        ]
    }

    fn card_two_args() -> Vec<&'static str> {
        vec![
            " 3 15  0  2 22",
            " 9 18 13 17  5",
            "19  8  7 25 23",
            "20 11 10 24  4",
            "14 21 16 12  6",
        ]
    }

    fn card_three_args() -> Vec<&'static str> {
        vec![
            "14 21 17 24  4",
            "10 16 15  9 19",
            "18  8 23 26 20",
            "22 11 13  6  5",
            " 2  0 12  3  7",
        ]
    }

    #[test]
    fn test_parse_and_print() {
        #[rustfmt::skip]
        let expected_numbers = vec![
            7, 4, 9, 5, 11,
            17, 23, 2, 0, 14,
            21, 24, 10, 16, 13,
            6, 15, 25, 12, 22,
            18, 20, 8, 19, 3,
            26, 1,
        ];
        let expected_card1 = include_str!("../test_fixtures/card1.txt");
        let expected_card2 = include_str!("../test_fixtures/card2.txt");
        let expected_card3 = include_str!("../test_fixtures/card3.txt");

        let (numbers, cards) = parse_game_input(get_test_input());

        assert_eq!(numbers, expected_numbers);

        assert_eq!(format!("{}", cards[0]), expected_card1);
        assert_eq!(format!("{}", cards[1]), expected_card2);
        assert_eq!(format!("{}", cards[2]), expected_card3);
    }

    #[test]
    fn test_horizontal_win() {
        let mut card = BingoCard::new(&card_one_args());

        assert!(!card.remove_any_matches(1));
        assert!(!card.remove_any_matches(12));
        assert!(!card.remove_any_matches(20));
        assert!(!card.remove_any_matches(15));
        assert!(card.remove_any_matches(19));
    }

    #[test]
    fn test_vertical_win() {
        let mut card = BingoCard::new(&card_two_args());

        assert!(!card.remove_any_matches(22));
        assert!(!card.remove_any_matches(5));
        assert!(!card.remove_any_matches(23));
        assert!(!card.remove_any_matches(4));
        assert!(card.remove_any_matches(6));
    }

    #[test]
    fn test_diagonal_does_not_win() {
        let mut card = BingoCard::new(&card_three_args());

        assert!(!card.remove_any_matches(14));
        assert!(!card.remove_any_matches(16));
        assert!(!card.remove_any_matches(23));
        assert!(!card.remove_any_matches(6));
        assert!(!card.remove_any_matches(7));
    }

    #[test]
    fn test_still_won_on_next_remove() {
        let mut card = BingoCard::new(&card_one_args());

        card.remove_any_matches(1);
        card.remove_any_matches(12);
        card.remove_any_matches(20);
        card.remove_any_matches(15);
        card.remove_any_matches(19);

        assert!(card.remove_any_matches(19));
        assert!(card.remove_any_matches(100));
    }

    #[test]
    fn test_remaining_numbers() {
        let mut card = BingoCard::new(&card_two_args());
        let expected = vec![0, 2, 7, 8, 9, 11, 13, 15, 17, 18, 19, 20, 25];

        for n in [22, 5, 23, 4, 6, 3, 12, 16, 21, 14, 24, 10] {
            card.remove_any_matches(n);
        }

        let remaining = card
            .remaining_numbers()
            .iter()
            .sorted()
            .cloned()
            .collect::<Vec<u8>>();

        assert_eq!(remaining, expected);
    }
}
