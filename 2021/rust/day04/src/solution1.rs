use crate::bingo::parse_game_input;

pub fn solve(input: &str) -> usize {
    let (numbers, mut cards) = parse_game_input(input);

    numbers
        .iter()
        .find_map(|n| {
            for card in &mut cards {
                if card.remove_any_matches(*n) {
                    return Some(
                        card.remaining_numbers()
                            .iter()
                            .map(|n| *n as usize)
                            .sum::<usize>()
                            * *n as usize,
                    );
                }
            }

            None
        })
        .unwrap()
}

#[cfg(test)]
mod tests {
    use super::solve;

    fn get_test_input() -> &'static str {
        include_str!("../../../inputs/day04/test_input.txt")
    }

    #[test]
    fn test_solve() {
        assert_eq!(solve(get_test_input()), 4512);
    }
}
