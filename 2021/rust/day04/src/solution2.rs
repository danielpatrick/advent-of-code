use crate::bingo::parse_game_input;
use std::collections::HashSet;

fn find_last_winner(input: &str) -> Option<usize> {
    let (numbers, mut cards) = parse_game_input(input);
    let num_cards = cards.len();
    let mut winning_cards = HashSet::new();

    for n in numbers {
        for (i, card) in cards.iter_mut().enumerate() {
            if card.remove_any_matches(n) {
                winning_cards.insert(i);

                if winning_cards.len() == num_cards {
                    return Some(
                        card.remaining_numbers()
                            .iter()
                            .map(|n| *n as usize)
                            .sum::<usize>()
                            * n as usize,
                    );
                }
            }
        }
    }

    None
}

pub fn solve(input: &str) -> usize {
    find_last_winner(input).unwrap()
}

#[cfg(test)]
mod tests {
    use super::solve;

    fn get_test_input() -> &'static str {
        include_str!("../../../inputs/day04/test_input.txt")
    }

    #[test]
    fn test_solve() {
        assert_eq!(solve(get_test_input()), 1924);
    }
}
