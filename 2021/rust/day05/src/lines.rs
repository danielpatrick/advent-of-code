use std::cmp::Ordering::{Equal, Greater, Less};
use std::iter::{repeat, Iterator};

type Point = (usize, usize);

#[derive(Debug)]
pub struct Line {
    start: Point,
    end: Point,
}

impl Line {
    const fn new(start: Point, end: Point) -> Self {
        Self { start, end }
    }

    fn get_range(first: usize, second: usize) -> Box<dyn Iterator<Item = usize>> {
        match first.cmp(&second) {
            Less => Box::new(first..=second),
            Equal => Box::new(repeat(first)),
            Greater => Box::new((second..=first).rev()),
        }
    }

    pub fn get_points(&self) -> Vec<Point> {
        if self.start == self.end {
            return vec![self.start];
        }

        let xs = Self::get_range(self.start.0, self.end.0);
        let ys = Self::get_range(self.start.1, self.end.1);

        xs.zip(ys).collect()
    }

    pub const fn is_diagonal(&self) -> bool {
        self.start.0 != self.end.0 && self.start.1 != self.end.1
    }

    fn parse_xy(s: &str) -> Option<Point> {
        let mut iter = s.split(',');
        let a = iter.next()?.parse::<usize>().ok()?;
        let b = iter.next()?.parse::<usize>().ok()?;

        Some((a, b))
    }

    fn parse_two_points(s: &str) -> Option<(Point, Point)> {
        let mut iter = s.split(" -> ");

        Some((Self::parse_xy(iter.next()?)?, Self::parse_xy(iter.next()?)?))
    }
}

impl TryFrom<&str> for Line {
    type Error = &'static str;

    fn try_from(s: &str) -> Result<Self, Self::Error> {
        if let Some((start, end)) = Self::parse_two_points(s) {
            Ok(Self::new(start, end))
        } else {
            Err("Failed to parse Line from &str")
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_is_diagonal_true() {
        assert!(Line::new((7, 7), (9, 9)).is_diagonal());
        assert!(Line::new((9, 9), (7, 7)).is_diagonal());
        assert!(Line::new((9, 7), (7, 9)).is_diagonal());
        assert!(Line::new((7, 9), (9, 7)).is_diagonal());
    }

    #[test]
    fn test_is_diagonal_false() {
        assert!(!Line::new((5, 5), (5, 5)).is_diagonal());
        assert!(!Line::new((0, 9), (2, 9)).is_diagonal());
        assert!(!Line::new((7, 9), (5, 9)).is_diagonal());
        assert!(!Line::new((0, 3), (0, 5)).is_diagonal());
        assert!(!Line::new((7, 9), (7, 7)).is_diagonal());
    }

    #[test]
    fn test_get_points_single_point() {
        assert_eq!(Line::new((5, 5), (5, 5)).get_points(), vec![(5, 5)]);
    }
    #[test]
    fn test_get_points_horizontal() {
        assert_eq!(
            Line::new((0, 9), (2, 9)).get_points(),
            vec![(0, 9), (1, 9), (2, 9)]
        );
        assert_eq!(
            Line::new((7, 9), (5, 9)).get_points(),
            vec![(7, 9), (6, 9), (5, 9)]
        );
    }

    #[test]
    fn test_get_points_vertical() {
        assert_eq!(
            Line::new((0, 3), (0, 5)).get_points(),
            vec![(0, 3), (0, 4), (0, 5)]
        );
        assert_eq!(
            Line::new((7, 9), (7, 7)).get_points(),
            vec![(7, 9), (7, 8), (7, 7)]
        );
    }

    #[test]
    fn test_get_points_diagonal() {
        assert_eq!(
            Line::new((7, 7), (9, 9)).get_points(),
            vec![(7, 7), (8, 8), (9, 9)]
        );
        assert_eq!(
            Line::new((9, 9), (7, 7)).get_points(),
            vec![(9, 9), (8, 8), (7, 7)]
        );
        assert_eq!(
            Line::new((9, 7), (7, 9)).get_points(),
            vec![(9, 7), (8, 8), (7, 9)]
        );
        assert_eq!(
            Line::new((7, 9), (9, 7)).get_points(),
            vec![(7, 9), (8, 8), (9, 7)]
        );
    }
}
