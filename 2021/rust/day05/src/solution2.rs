use std::collections::HashSet;

use crate::lines::Line;

pub fn solve(input: &str) -> usize {
    let mut points = HashSet::new();
    let mut duplicates = HashSet::new();

    let iter = input
        .lines()
        .map(|line| line.try_into().unwrap())
        .flat_map(|line: Line| line.get_points());

    for point in iter {
        if !points.insert(point) {
            duplicates.insert(point);
        }
    }

    duplicates.len()
}

#[cfg(test)]
mod tests {
    use super::solve;

    fn get_test_input() -> &'static str {
        include_str!("../../../inputs/day05/test_input.txt")
    }

    #[test]
    fn test_solve() {
        assert_eq!(solve(get_test_input()), 12);
    }
}
