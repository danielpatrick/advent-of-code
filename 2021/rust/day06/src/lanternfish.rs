pub fn count_after(input: &str, after: usize) -> u64 {
    let mut state: [u64; 9] = [0; 9];

    for n in input
        .trim_end()
        .split(',')
        .map(|s| s.parse::<usize>().unwrap())
    {
        state[n] += 1;
    }

    for i in 0..after {
        state[(i + 7) % 9] += state[i % 9];
    }

    state.iter().sum()
}
