use crate::lanternfish::count_after;

pub fn solve(input: &str) -> u64 {
    count_after(input, 256)
}

#[cfg(test)]
mod tests {
    use super::solve;

    fn get_test_input() -> &'static str {
        include_str!("../../../inputs/day06/test_input.txt")
    }

    #[test]
    fn test_solve() {
        assert_eq!(solve(get_test_input()), 26984457539);
    }
}
