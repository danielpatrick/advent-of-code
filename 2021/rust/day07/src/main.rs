#![feature(int_abs_diff)]
use crossbeam::thread;
use std::env;

mod solution1;
mod solution2;

fn parse_arg() -> (bool, bool) {
    match env::args().nth(1).as_deref() {
        Some("1") => (true, false),
        Some("2") => (false, true),
        _ => (true, true),
    }
}

const fn get_input() -> &'static str {
    include_str!("../../../inputs/day07/input.txt")
}

fn maybe_print_solution(solution: Option<usize>, num: i32) {
    if let Some(solution) = solution {
        println!("Solution {}: {}", num, solution);
    }
}

fn main() {
    let input = get_input();
    let mut first_solution = None;
    let mut second_solution = None;

    let (execute_solution1, execute_solution2) = parse_arg();

    thread::scope(|s| {
        if execute_solution1 {
            s.spawn(|_| {
                first_solution = Some(solution1::solve(input));
            });
        }
        if execute_solution2 {
            s.spawn(|_| {
                second_solution = Some(solution2::solve(input));
            });
        }
    })
    .unwrap();

    maybe_print_solution(first_solution, 1);
    maybe_print_solution(second_solution, 2);
}
