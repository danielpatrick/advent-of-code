use itertools::Itertools;

pub fn solve(input: &str) -> usize {
    let numbers = input
        .trim_end()
        .split(',')
        .map(|s| s.parse().unwrap())
        .sorted()
        .collect::<Vec<usize>>();

    let target = numbers[numbers.len() / 2];

    numbers.iter().map(|n| target.abs_diff(*n)).sum()
}

#[cfg(test)]
mod tests {
    use super::solve;

    fn get_test_input() -> &'static str {
        include_str!("../../../inputs/day07/test_input.txt")
    }

    #[test]
    fn test_solve() {
        assert_eq!(solve(get_test_input()), 37);
    }
}
