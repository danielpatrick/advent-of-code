use itertools::Itertools;

pub fn solve(input: &str) -> usize {
    let numbers = input
        .trim_end()
        .split(',')
        .map(|s| s.parse().unwrap())
        .sorted()
        .collect::<Vec<usize>>();

    let median = numbers[numbers.len() / 2];
    let mean = numbers.iter().sum::<usize>() / numbers.len();

    (mean.min(median) - 1..=mean.max(median) + 1)
        .map(|target| {
            numbers
                .iter()
                .map(|n| {
                    let distance = target.abs_diff(*n);
                    distance * (distance + 1) / 2
                })
                .sum()
        })
        .min()
        .unwrap()
}

#[cfg(test)]
mod tests {
    use super::solve;

    fn get_test_input() -> &'static str {
        include_str!("../../../inputs/day07/test_input.txt")
    }

    #[test]
    fn test_solve() {
        assert_eq!(solve(get_test_input()), 168);
    }
}
