use itertools::Itertools;
use std::collections::{HashMap, HashSet};

type CharSetLengthMap = HashMap<usize, Vec<HashSet<char>>>;

enum Rule {
    Unique(usize),
    Superset(usize, usize),
    NotSuperset(usize, usize),
    Subset(usize, usize),
    NotSubset(usize, usize),
}

fn parse(input: &str) -> Vec<(Vec<String>, CharSetLengthMap)> {
    input
        .lines()
        .map(|line| {
            let mut iter = line.split(" | ");

            let unique = iter.next().unwrap().split_whitespace().fold(
                CharSetLengthMap::new(),
                |mut acc, s| {
                    acc.entry(s.len())
                        .or_default()
                        .push(s.chars().sorted().collect());
                    acc
                },
            );

            let items = iter
                .next()
                .unwrap()
                .split_whitespace()
                .map(|s| s.chars().sorted().collect())
                .collect();

            (items, unique)
        })
        .collect()
}

fn create_mapping(words: &CharSetLengthMap) -> HashMap<String, usize> {
    let mut mapping = [None; 10];
    let rules = [
        (1, Rule::Unique(2)),
        (7, Rule::Unique(3)),
        (4, Rule::Unique(4)),
        (8, Rule::Unique(7)),
        (3, Rule::Superset(5, 1)),
        (6, Rule::NotSuperset(6, 1)),
        (9, Rule::Superset(6, 4)),
        (5, Rule::Subset(5, 6)),
        (0, Rule::NotSuperset(6, 5)),
        (2, Rule::NotSubset(5, 9)),
    ];

    for (num, rule) in &rules {
        mapping[*num] = match rule {
            Rule::Unique(length) => Some(&words[length][0]),
            Rule::Superset(length, n) => words[length]
                .iter()
                .find(|item| item.is_superset(mapping[*n].unwrap())),
            Rule::NotSuperset(length, n) => words[length]
                .iter()
                .find(|item| !item.is_superset(mapping[*n].unwrap())),
            Rule::Subset(length, n) => words[length]
                .iter()
                .find(|item| item.is_subset(mapping[*n].unwrap())),
            Rule::NotSubset(length, n) => words[length]
                .iter()
                .find(|item| !item.is_subset(mapping[*n].unwrap())),
        };
    }

    mapping
        .iter()
        .enumerate()
        .map(|(i, v)| (v.unwrap().iter().sorted().collect(), i))
        .collect()
}

pub fn solve(input: &str) -> usize {
    let lines = parse(input);

    lines
        .iter()
        .map(|(items, char_counts)| {
            let mapping = create_mapping(char_counts);
            items
                .iter()
                .rev()
                .enumerate()
                .map(|(i, item)| mapping[item] * 10_usize.pow(i as u32))
                .sum::<usize>()
        })
        .sum()
}

#[cfg(test)]
mod tests {
    use super::solve;

    fn get_test_input() -> &'static str {
        include_str!("../../../inputs/day08/test_input.txt")
    }

    #[test]
    fn test_solve() {
        assert_eq!(solve(get_test_input()), 61229);
    }
}
