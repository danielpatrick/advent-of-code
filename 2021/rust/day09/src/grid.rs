use std::collections::BTreeSet;

pub struct Grid {
    heights: Vec<u8>,
    size: usize,
    width: usize,
}

impl Grid {
    pub fn new(s: &str) -> Self {
        let heights = s
            .lines()
            .flat_map(|line| line.chars().map(|c| c.to_digit(10).unwrap() as u8))
            .collect::<Vec<_>>();

        let size = heights.len();
        let width = size / s.lines().count();

        Self {
            heights,
            size,
            width,
        }
    }

    fn adjacent_positions(&self, pos: usize) -> Vec<usize> {
        let mut positions = Vec::new();

        if pos >= self.width {
            positions.push(pos - self.width);
        }
        if pos % self.width < (self.width - 1) {
            positions.push(pos + 1);
        }
        if pos < (self.size - self.width) {
            positions.push(pos + self.width);
        }
        if pos % self.width >= 1 {
            positions.push(pos - 1);
        }

        positions
    }

    fn adjacent_heights(&self, pos: usize) -> Vec<u8> {
        self.adjacent_positions(pos)
            .iter()
            .map(|pos| self.heights[*pos])
            .collect()
    }

    pub fn get_low_points(&self) -> Vec<u8> {
        self.heights
            .iter()
            .enumerate()
            .filter(|(pos, height)| self.adjacent_heights(*pos).iter().all(|h| h > height))
            .map(|(_, height)| *height)
            .collect()
    }

    fn map_basin(&self, not_checked: &mut BTreeSet<usize>, start: usize) -> u32 {
        if self.heights[start] == 9 {
            return 0;
        }

        let mut count = 1;

        for pos in self.adjacent_positions(start) {
            if not_checked.remove(&pos) {
                count += self.map_basin(not_checked, pos);
            }
        }

        count
    }

    pub fn find_basin_sizes(&self) -> Vec<u32> {
        let mut not_checked = (0..self.size).collect::<BTreeSet<usize>>();
        let mut basins = Vec::new();

        while !not_checked.is_empty() {
            let pos = not_checked.pop_first().unwrap();
            let size = self.map_basin(&mut not_checked, pos);

            if size != 0 {
                basins.push(size);
            }
        }

        basins
    }
}

#[cfg(test)]
mod tests {
    use super::Grid;
    use std::collections::BTreeSet;

    fn grid() -> Grid {
        Grid::new(include_str!("../../../inputs/day09/test_input.txt"))
    }

    #[test]
    fn test_adjacent_heights() {
        // Corners
        assert_eq!(grid().adjacent_heights(0), vec![1, 3]);
        assert_eq!(grid().adjacent_heights(9), vec![1, 1]);
        assert_eq!(grid().adjacent_heights(40), vec![8, 8]);
        assert_eq!(grid().adjacent_heights(49), vec![9, 7]);

        // Edges
        assert_eq!(grid().adjacent_heights(1), vec![9, 9, 2]);
        assert_eq!(grid().adjacent_heights(19), vec![0, 2, 2]);
        assert_eq!(grid().adjacent_heights(30), vec![9, 7, 9]);
        assert_eq!(grid().adjacent_heights(48), vec![8, 8, 6]);

        // Somewhere in the middle
        assert_eq!(grid().adjacent_heights(24), vec![8, 8, 8, 6]);
    }

    #[test]
    fn test_get_low_points() {
        assert_eq!(grid().get_low_points(), vec![1, 0, 5, 5]);
    }

    #[test]
    fn test_map_basin() {
        let mut not_checked = (0..50).collect::<BTreeSet<usize>>();
        let expected_not_checked1 = not_checked
            .difference(&BTreeSet::from([0, 1, 2, 10, 11, 20]))
            .cloned()
            .collect::<BTreeSet<_>>();
        let expected_not_checked2 = expected_not_checked1
            .difference(&BTreeSet::from([
                3, 5, 6, 7, 8, 9, 16, 18, 19, 29, 4, 15, 17, 26, 28, 39,
            ]))
            .cloned()
            .collect();

        let mut pos = not_checked.pop_first().unwrap();
        assert_eq!(grid().map_basin(&mut not_checked, pos), 3);
        assert_eq!(not_checked, expected_not_checked1);

        pos = not_checked.pop_first().unwrap();
        assert_eq!(grid().map_basin(&mut not_checked, pos), 0);
        pos = not_checked.pop_first().unwrap();
        assert_eq!(grid().map_basin(&mut not_checked, pos), 0);

        pos = not_checked.pop_first().unwrap();
        assert_eq!(grid().map_basin(&mut not_checked, pos), 9);
        assert_eq!(not_checked, expected_not_checked2);
    }

    #[test]
    fn test_find_basins() {
        assert_eq!(grid().find_basin_sizes(), vec![3, 9, 14, 9]);
    }
}
