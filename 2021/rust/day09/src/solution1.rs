use crate::grid::Grid;

pub fn solve(input: &str) -> u32 {
    Grid::new(input)
        .get_low_points()
        .iter()
        .map(|n| u32::from(*n) + 1)
        .sum::<u32>()
}

#[cfg(test)]
mod tests {
    use super::solve;

    fn get_test_input() -> &'static str {
        include_str!("../../../inputs/day09/test_input.txt")
    }

    #[test]
    fn test_solve() {
        assert_eq!(solve(get_test_input()), 15);
    }
}
