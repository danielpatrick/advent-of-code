use crate::grid::Grid;
use itertools::Itertools;

pub fn solve(input: &str) -> u32 {
    Grid::new(input)
        .find_basin_sizes()
        .iter()
        .sorted()
        .rev()
        .take(3)
        .product()
}

#[cfg(test)]
mod tests {
    use super::solve;

    fn get_test_input() -> &'static str {
        include_str!("../../../inputs/day09/test_input.txt")
    }

    #[test]
    fn test_solve() {
        assert_eq!(solve(get_test_input()), 1134);
    }
}
