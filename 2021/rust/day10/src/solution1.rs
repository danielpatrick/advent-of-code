use phf::phf_map;

const PAIRS: phf::Map<char, char> = phf_map! {
    ')' => '(',
    ']' => '[',
    '}' => '{',
    '>' => '<',
};

const POINTS: phf::Map<char, u64> = phf_map! {
    ')' => 3,
    ']' => 57,
    '}' => 1197,
    '>' => 25137,
};

fn chunk_validator(input: &str) -> u64 {
    let mut stack = Vec::new();

    input
        .chars()
        .find_map(|c| {
            if PAIRS.contains_key(&c) {
                if let Some(prev) = stack.pop() {
                    if prev != PAIRS[&c] {
                        return Some(POINTS[&c]);
                    }
                }
            } else {
                stack.push(c);
            }
            None
        })
        .unwrap_or(0)
}

pub fn solve(input: &str) -> u64 {
    input.lines().map(chunk_validator).sum()
}

#[cfg(test)]
mod tests {
    use super::{chunk_validator, solve};

    fn get_test_input() -> &'static str {
        include_str!("../../../inputs/day10/test_input.txt")
    }

    #[test]
    fn test_solve() {
        assert_eq!(solve(get_test_input()), 26397);
    }

    #[test]
    fn test_chunk_validator() {
        assert_eq!(chunk_validator("{([(<{}[<>[]}>{[]{[(<()>"), 1197);
        assert_eq!(chunk_validator("[[<[([]))<([[{}[[()]]]"), 3);
        assert_eq!(chunk_validator("[{[{({}]{}}([{[{{{}}([]"), 57);
        assert_eq!(chunk_validator("[<(<(<(<{}))><([]([]()"), 3);
        assert_eq!(chunk_validator("<{([([[(<>()){}]>(<<{{"), 25137);
    }
}
