use itertools::Itertools;
use phf::phf_map;

const PAIRS: phf::Map<char, char> = phf_map! {
    ')' => '(',
    ']' => '[',
    '}' => '{',
    '>' => '<',
};

const POINTS: phf::Map<char, u64> = phf_map! {
    '(' => 1,
    '[' => 2,
    '{' => 3,
    '<' => 4,
};

fn chunk_validator(input: &str) -> Option<u64> {
    let mut stack = Vec::new();

    for c in input.chars() {
        if PAIRS.contains_key(&c) {
            if let Some(prev) = stack.pop() {
                if prev != PAIRS[&c] {
                    return None;
                }
            }
        } else {
            stack.push(c);
        }
    }

    stack
        .iter()
        .rev()
        .map(|c| POINTS[c])
        .reduce(|a, b| a * 5 + b)
}

pub fn solve(input: &str) -> u64 {
    let scores = input
        .lines()
        .filter_map(chunk_validator)
        .sorted()
        .collect::<Vec<u64>>();

    scores[scores.len() / 2]
}

#[cfg(test)]
mod tests {
    use super::{chunk_validator, solve};

    fn get_test_input() -> &'static str {
        include_str!("../../../inputs/day10/test_input.txt")
    }

    #[test]
    fn test_solve() {
        assert_eq!(solve(get_test_input()), 288957);
    }

    #[test]
    fn test_chunk_validator_invalid() {
        assert_eq!(chunk_validator("{([(<{}[<>[]}>{[]{[(<()>"), None);
        assert_eq!(chunk_validator("[[<[([]))<([[{}[[()]]]"), None);
        assert_eq!(chunk_validator("[{[{({}]{}}([{[{{{}}([]"), None);
        assert_eq!(chunk_validator("[<(<(<(<{}))><([]([]()"), None);
        assert_eq!(chunk_validator("<{([([[(<>()){}]>(<<{{"), None);
    }

    #[test]
    fn test_chunk_validator_valid() {
        assert_eq!(chunk_validator("[({(<(())[]>[[{[]{<()<>>"), Some(288957));
        assert_eq!(chunk_validator("[(()[<>])]({[<{<<[]>>("), Some(5566));
        assert_eq!(chunk_validator("(((({<>}<{<{<>}{[]{[]{}"), Some(1480781));
        assert_eq!(chunk_validator("{<[[]]>}<{[{[{[]{()[[[]"), Some(995444));
        assert_eq!(chunk_validator("<{([{{}}[<[[[<>{}]]]>[]]"), Some(294));
    }
}
