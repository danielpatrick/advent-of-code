use std::collections::BTreeSet;

pub struct Grid {
    zero: i32,
    nine: i32,
    width: usize,
    height: usize,
    size: usize,
    octopuses: Vec<i32>,
}

impl Grid {
    pub fn new(s: &str) -> Self {
        let octopuses = s
            .lines()
            .flat_map(|line| line.chars().map(|c| c.to_digit(10).unwrap() as i32))
            .collect::<Vec<_>>();

        let size = octopuses.len();
        let height = s.lines().count();
        let width = size / height;

        Self {
            zero: 0,
            nine: 9,
            width,
            height,
            size,
            octopuses,
        }
    }

    pub fn step(&mut self) -> u32 {
        self.zero -= 1;
        self.nine -= 1;

        let mut flashed = BTreeSet::new();

        let count = (0..self.size)
            .map(|pos| self.check_flash(pos, &mut flashed))
            .sum();

        for pos in &flashed {
            self.octopuses[*pos] = self.zero;
        }

        count
    }

    fn check_flash(&mut self, pos: usize, flashed: &mut BTreeSet<usize>) -> u32 {
        if self.octopuses[pos] > self.nine && flashed.insert(pos) {
            1 + self
                .neighbours(pos)
                .iter()
                .map(|&pos| {
                    if self.octopuses[pos] <= self.nine {
                        self.octopuses[pos] += 1;
                        self.check_flash(pos, flashed)
                    } else {
                        0
                    }
                })
                .sum::<u32>()
        } else {
            0
        }
    }

    fn neighbours(&self, pos: usize) -> Vec<usize> {
        let x = pos % self.width;
        let y = pos / self.width;

        let north = y >= 1;
        let east = x < self.width - 1;
        let south = y < self.height - 1;
        let west = x >= 1;

        let mut res = Vec::new();

        if north {
            res.push(pos - self.width); // N

            if east {
                res.push(pos - self.width + 1); // NE
            }
        }
        if east {
            res.push(pos + 1); // E

            if south {
                res.push(pos + self.width + 1); // SE
            }
        }
        if south {
            res.push(pos + self.width); // S

            if west {
                res.push(pos + self.width - 1); // SW
            }
        }
        if west {
            res.push(pos - 1); // W

            if north {
                res.push(pos - self.width - 1); // NW
            }
        }

        res
    }
}

#[cfg(test)]
mod tests {
    use super::Grid;

    fn get_test_input() -> &'static str {
        include_str!("../../../inputs/day11/test_input.txt")
    }

    #[test]
    fn test_one_step() {
        let mut grid = Grid::new(get_test_input());

        assert_eq!(grid.step(), 0);
    }

    #[test]
    fn test_two_steps() {
        let mut grid = Grid::new(get_test_input());
        grid.step();

        assert_eq!(grid.step(), 35);
    }

    #[test]
    fn test_three_steps() {
        let mut grid = Grid::new(get_test_input());
        grid.step();
        grid.step();

        assert_eq!(grid.step(), 45);
    }

    #[test]
    fn test_four_steps() {
        let mut grid = Grid::new(get_test_input());
        grid.step();
        grid.step();
        grid.step();

        assert_eq!(grid.step(), 16);
    }

    #[test]
    fn test_ten_steps() {
        let mut grid = Grid::new(get_test_input());
        let result = (0..10).map(|_| grid.step()).sum::<u32>();

        assert_eq!(result, 204);
    }
}
