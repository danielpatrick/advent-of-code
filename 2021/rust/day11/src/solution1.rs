use crate::grid::Grid;

pub fn solve(input: &str) -> u32 {
    let mut grid = Grid::new(input);

    (0..100).map(|_| grid.step()).sum()
}

#[cfg(test)]
mod tests {
    use super::solve;

    fn get_test_input() -> &'static str {
        include_str!("../../../inputs/day11/test_input.txt")
    }

    #[test]
    fn test_solve() {
        assert_eq!(solve(get_test_input()), 1656);
    }
}
