use std::collections::HashMap;

pub type Rules<'a> = HashMap<&'a str, Vec<&'a str>>;

pub fn get_rules(input: &str) -> Rules {
    input.lines().fold(Rules::new(), |mut acc, line| {
        let mut iter = line.split('-');
        let first = iter.next().unwrap();
        let second = iter.next().unwrap();

        if first != "end" && second != "start" {
            acc.entry(first).or_default().push(second);
        }
        if first != "start" && second != "end" {
            acc.entry(second).or_default().push(first);
        }

        acc
    })
}

pub fn count_routes_recursive<'a>(
    rules: &'a Rules,
    mut path: Vec<&'a str>,
    current: &'a str,
    repeats_left: u8,
) -> usize {
    match current {
        "end" => {
            return 1;
        }
        "start" => {
            if !path.is_empty() {
                panic!("Cave::Start encountered when path is non-empty: {:?}", path);
            }
        }
        _ => path.push(current),
    }

    let mut results = 0;

    if let Some(targets) = rules.get(current) {
        for target in targets {
            let repeats = if target != &"end"
                && target.chars().all(char::is_lowercase)
                && path.contains(target)
            {
                if repeats_left == 0 {
                    continue;
                }
                1
            } else {
                0
            };

            results += count_routes_recursive(rules, path.clone(), target, repeats_left - repeats);
        }
    }

    results
}

pub fn count_routes(input: &str, allowed_repeats: u8) -> usize {
    let rules = get_rules(input);

    count_routes_recursive(&rules, Vec::new(), "start", allowed_repeats)
}
