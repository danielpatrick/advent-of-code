use crate::caves::count_routes;

pub fn solve(input: &str) -> usize {
    count_routes(input, 0)
}

#[cfg(test)]
mod tests {
    use super::solve;

    fn get_test_input1() -> &'static str {
        include_str!("../../../inputs/day12/test_input1.txt")
    }

    fn get_test_input2() -> &'static str {
        include_str!("../../../inputs/day12/test_input2.txt")
    }

    fn get_test_input3() -> &'static str {
        include_str!("../../../inputs/day12/test_input3.txt")
    }

    #[test]
    fn test_solve() {
        assert_eq!(solve(get_test_input1()), 10);
        assert_eq!(solve(get_test_input2()), 19);
        assert_eq!(solve(get_test_input3()), 226);
    }
}
