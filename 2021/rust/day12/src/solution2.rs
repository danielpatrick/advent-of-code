use crate::caves::count_routes;
//use crate::fastcaves::{count_routes, Rules};
//use crate::fastcaves::count_routes;

pub fn solve(input: &str) -> usize {
    count_routes(input, 1)
}

#[cfg(test)]
mod tests {
    use super::solve;

    fn get_test_input1() -> &'static str {
        include_str!("../../../inputs/day12/test_input1.txt")
    }

    fn get_test_input2() -> &'static str {
        include_str!("../../../inputs/day12/test_input2.txt")
    }

    fn get_test_input3() -> &'static str {
        include_str!("../../../inputs/day12/test_input3.txt")
    }

    #[test]
    fn test_solve() {
        assert_eq!(solve(get_test_input1()), 36);
        assert_eq!(solve(get_test_input2()), 103);
        assert_eq!(solve(get_test_input3()), 3509);
    }
}
