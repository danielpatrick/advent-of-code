use std::collections::BTreeSet;
use std::fmt;

pub struct OrigamiDots {
    dots: BTreeSet<(usize, usize)>,
}

impl OrigamiDots {
    pub fn fold(&mut self, fold: &Fold) {
        let replacements = self
            .dots
            .iter()
            .filter_map(|dot| match fold {
                Fold::Vertical(n) => {
                    if dot.0 > *n {
                        Some((*dot, (n * 2 - dot.0, dot.1)))
                    } else {
                        None
                    }
                }
                Fold::Horizontal(n) => {
                    if dot.1 > *n {
                        Some((*dot, (dot.0, n * 2 - dot.1)))
                    } else {
                        None
                    }
                }
            })
            .collect::<Vec<_>>();

        for &(old, new) in &replacements {
            self.dots.remove(&old);
            self.dots.insert(new);
        }
    }

    pub fn count_dots(&self) -> usize {
        self.dots.len()
    }
}

impl From<&str> for OrigamiDots {
    fn from(s: &str) -> Self {
        let dots = s
            .lines()
            .map(|line| {
                let mut iter = line.split(',');
                (
                    iter.next().unwrap().parse().unwrap(),
                    iter.next().unwrap().parse().unwrap(),
                )
            })
            .collect::<BTreeSet<(usize, usize)>>();

        Self { dots }
    }
}

impl fmt::Display for OrigamiDots {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let (min_x, max_x, min_y, max_y) = self
            .dots
            .iter()
            .map(|(x, y)| (*x, *x, *y, *y))
            .reduce(|(min_x, max_x, min_y, max_y), (x, _, y, _)| {
                (min_x.min(x), max_x.max(x), min_y.min(y), max_y.max(y))
            })
            .unwrap();

        let s = (min_y..=max_y)
            .flat_map(|y| {
                (min_x..=max_x)
                    .map(move |x| {
                        if self.dots.contains(&(x, y)) {
                            '#'
                        } else {
                            '.'
                        }
                    })
                    .chain(Some('\n'))
            })
            .collect::<String>();

        write!(f, "{}", s)
    }
}

pub enum Fold {
    Horizontal(usize),
    Vertical(usize),
}

impl From<&str> for Fold {
    fn from(s: &str) -> Self {
        let mut iter = s.split_whitespace().last().unwrap().split('=').rev();
        let n = iter.next().unwrap().parse::<usize>().unwrap();

        match iter.next().unwrap() {
            "x" => Self::Vertical(n),
            "y" => Self::Horizontal(n),
            c => panic!("Invalid axis {}", c),
        }
    }
}
