use crate::origami::OrigamiDots;

pub fn solve(input: &str) -> String {
    let mut iter = input.split("\n\n");
    let mut dots = OrigamiDots::from(iter.next().unwrap());
    let mut lines = iter.next().unwrap().lines();

    if let Some(line) = lines.next() {
        dots.fold(&line.into());
    }

    format!("{}", dots.count_dots())
}

#[cfg(test)]
mod tests {
    use super::solve;

    fn get_test_input() -> &'static str {
        include_str!("../../../inputs/day13/test_input.txt")
    }

    #[test]
    fn test_solve() {
        assert_eq!(solve(get_test_input()), "17");
    }
}
