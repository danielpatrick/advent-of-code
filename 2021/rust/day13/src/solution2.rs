use crate::origami::OrigamiDots;

pub fn solve(input: &str) -> String {
    let mut iter = input.split("\n\n");
    let mut dots = OrigamiDots::from(iter.next().unwrap());

    for line in iter.next().unwrap().lines() {
        dots.fold(&line.into());
    }

    format!("\n{}", dots)
}

#[cfg(test)]
mod tests {
    use super::solve;

    fn get_test_input() -> &'static str {
        include_str!("../../../inputs/day13/test_input.txt")
    }

    #[test]
    fn test_solve() {
        let expected = "\n\
            #####\n\
            #...#\n\
            #...#\n\
            #...#\n\
            #####\n\
        ";

        assert_eq!(solve(get_test_input()), expected);
    }
}
