use std::collections::BTreeMap;
use std::hash::Hash;
use std::ops::AddAssign;

use num::One;

#[derive(Default, Clone)]
pub struct Counter<K, V> {
    map: BTreeMap<K, V>,
}

impl<K, V> Counter<K, V>
where
    V: Ord + Copy,
{
    pub fn new() -> Self {
        Self {
            map: BTreeMap::new(),
        }
    }

    pub fn most_common(self) -> Vec<(K, V)> {
        let mut v = self.map.into_iter().collect::<Vec<_>>();

        v.sort_by_key(|k| k.1);

        v
    }
}

impl<K, V, const N: usize> From<[(K, V); N]> for Counter<K, V>
where
    K: Eq + Hash + Ord,
{
    fn from(arr: [(K, V); N]) -> Self {
        Self {
            map: BTreeMap::from(arr),
        }
    }
}

impl<K, V, const N: usize> From<[K; N]> for Counter<K, V>
where
    K: Eq + Hash + Ord,
    V: One + Default + AddAssign,
{
    fn from(arr: [K; N]) -> Self {
        let mut map = BTreeMap::new();

        for item in arr {
            *map.entry(item).or_default() += V::one();
        }
        Self { map }
    }
}

impl<K, V> AddAssign for Counter<K, V>
where
    K: Eq + Hash + Ord,
    V: Default + AddAssign,
{
    fn add_assign(&mut self, other: Self) {
        for (k, v) in other.map {
            let count = self.map.entry(k).or_default();
            *count += v;
        }
    }
}
