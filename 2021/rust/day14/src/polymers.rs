use std::collections::BTreeMap;

use crate::counter::Counter;

type Rules = BTreeMap<(char, char), char>;

type Cache = BTreeMap<usize, BTreeMap<(char, char), Counter<char, usize>>>;

fn parse(s: &str) -> (Vec<char>, Rules) {
    let mut iter = s.split("\n\n");
    let polymer = iter.next().unwrap().chars().collect();
    let rules = iter
        .next()
        .unwrap()
        .lines()
        .map(|line| {
            let mut iter = line.split(" -> ");
            let mut chars = iter.next().unwrap().chars();
            (
                (chars.next().unwrap(), chars.next().unwrap()),
                iter.next().unwrap().chars().next().unwrap(),
            )
        })
        .collect();

    (polymer, rules)
}

fn count_occurences(
    pair: (char, char),
    rules: &Rules,
    cache: &mut Cache,
    n: usize,
) -> Counter<char, usize> {
    if n == 0 {
        return cache
            .entry(0)
            .or_default()
            .entry(pair)
            .or_insert_with(|| Counter::from([pair.1]))
            .clone();
    }

    if let Some(counts) = cache.entry(n).or_default().get(&pair) {
        return counts.clone();
    }

    let mut counts = Counter::new();

    if let Some(&c) = rules.get(&pair) {
        counts += count_occurences((pair.0, c), rules, cache, n - 1);
        counts += count_occurences((c, pair.1), rules, cache, n - 1);
    }

    cache.entry(n).or_default().insert(pair, counts.clone());

    counts
}

pub fn find_count_diff_after(s: &str, n: usize) -> usize {
    let (polymer, rules) = parse(s);
    let mut cache = Cache::new();
    let mut counts = Counter::from([(polymer[0], 1)]);

    for i in 1..polymer.len() {
        counts += count_occurences((polymer[i - 1], polymer[i]), &rules, &mut cache, n);
    }

    let frequencies = counts.most_common();

    frequencies.last().unwrap().1 - frequencies.first().unwrap().1
}
