use crate::polymers::find_count_diff_after;

pub fn solve(input: &str) -> usize {
    find_count_diff_after(input, 40)
}

#[cfg(test)]
mod tests {
    use super::solve;

    fn get_test_input() -> &'static str {
        include_str!("../../../inputs/day14/test_input.txt")
    }

    #[test]
    fn test_solve() {
        assert_eq!(solve(get_test_input()), 2188189693529);
    }
}
