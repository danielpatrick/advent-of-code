use std::cmp::Reverse;
use std::collections::{BinaryHeap, HashMap};

pub type Point = (usize, usize);
pub type Position = usize;
pub type Risk = u32;

pub trait Grid {
    fn height(&self) -> usize;
    fn width(&self) -> usize;
    fn tile_risk(&self, pos: Position) -> Risk;

    fn neighbours(&self, pos: Position) -> Vec<(Position, Risk)> {
        let mut result = Vec::new();
        let (x, y) = self.point(&pos);

        if y < self.height() - 1 {
            let p = pos + self.width();
            result.push((p, self.tile_risk(p)));
        }
        if x < self.width() - 1 {
            let p = pos + 1;
            result.push((p, self.tile_risk(p)));
        }
        if y > 0 {
            let p = pos - self.width();
            result.push((p, self.tile_risk(p)));
        }
        if x > 0 {
            let p = pos - 1;
            result.push((p, self.tile_risk(p)));
        }

        result
    }

    fn parse_grid(s: &str) -> Vec<Risk> {
        s.lines()
            .flat_map(|line| line.chars().map(move |c| c.to_digit(10).unwrap() as Risk))
            .collect()
    }

    fn idx(&self, (x, y): &Point) -> Position {
        y * self.width() + x
    }

    fn point(&self, pos: &Position) -> Point {
        (pos % self.width(), pos / self.width())
    }

    fn target(&self) -> Position {
        self.idx(&(self.width() - 1, self.height() - 1))
    }

    fn find_path(&self) -> Vec<Position> {
        let start = 0;
        let target = self.target();

        let mut costs = HashMap::<Position, Risk>::new();
        let mut route = HashMap::<Position, Option<Position>>::new();
        let mut heap = BinaryHeap::new();

        costs.insert(start, 0);
        route.insert(start, None);
        heap.push(Reverse((0, start)));

        while let Some(Reverse((cost, position))) = heap.pop() {
            if position == target {
                break;
            }

            if cost > *costs.get(&position).unwrap_or(&u32::MAX) {
                continue;
            }

            for &(pos, edge_cost) in &self.neighbours(position) {
                let next_cost = cost + edge_cost;

                if next_cost < *costs.get(&pos).unwrap_or(&u32::MAX) {
                    heap.push(Reverse((next_cost, pos)));
                    route.insert(pos, Some(position));
                    costs.insert(pos, next_cost);
                }
            }
        }

        let mut current = target;
        let mut path = vec![current];

        while let Some(&Some(pos)) = route.get(&current) {
            path.push(pos);
            current = pos;
        }

        path.sort_unstable();

        path
    }

    fn path_risk(&self, path: &[Position]) -> Risk {
        path.iter().skip(1).map(|pos| self.tile_risk(*pos)).sum()
    }

    fn print_path(&self, path: &[Position]) {
        let bold = "\x1b[1m";
        let reset = "\x1b[0m";
        let height = self.height();
        let width = self.width();

        for y in 0..height {
            for x in 0..width {
                let pos = self.idx(&(x, y));
                let risk = self.tile_risk(pos);
                if path.contains(&pos) {
                    print!("{}{}{}", bold, risk, reset);
                } else {
                    print!("{}", risk);
                }
            }
            println!();
        }
    }
}
