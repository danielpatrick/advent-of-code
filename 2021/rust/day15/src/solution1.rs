use crate::grid::{Grid, Position, Risk};

pub struct BasicGrid {
    height: usize,
    width: usize,
    grid: Vec<Risk>,
}

impl BasicGrid {
    pub fn new(s: &str) -> Self {
        let grid = Self::parse_grid(s);
        let size = grid.len();
        let height = s.lines().count();
        let width = size / height;

        Self {
            height,
            width,
            grid,
        }
    }
}

impl Grid for BasicGrid {
    fn height(&self) -> usize {
        self.height
    }

    fn width(&self) -> usize {
        self.width
    }

    fn tile_risk(&self, pos: Position) -> Risk {
        self.grid[pos]
    }
}

pub fn solve(input: &str) -> u32 {
    let grid = BasicGrid::new(input);
    let path = grid.find_path();
    // grid.print_path(&path);

    grid.path_risk(&path)
}

#[cfg(test)]
mod tests {
    use super::solve;

    fn get_test_input() -> &'static str {
        include_str!("../../../inputs/day15/test_input.txt")
    }

    #[test]
    fn test_solve() {
        assert_eq!(solve(get_test_input()), 40);
    }
}
