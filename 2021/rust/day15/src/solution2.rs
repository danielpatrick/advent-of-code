use crate::grid::{Grid, Position, Risk};

pub struct LargeGrid {
    height: usize,
    width: usize,
    grid: Vec<Risk>,
}

impl LargeGrid {
    pub fn new(s: &str) -> Self {
        let h = s.lines().count();
        let height = h * 5;
        let w = s.lines().next().unwrap().chars().count();
        let width = w * 5;
        let grid = Self::parse_grid(width, height, w, h, s);

        Self {
            height,
            width,
            grid,
        }
    }

    fn parse_grid(width: usize, height: usize, w: usize, h: usize, s: &str) -> Vec<Risk> {
        let grid = <Self as Grid>::parse_grid(s);

        (0..(width * height))
            .map(|pos| {
                let (x, y) = (pos % width, pos / width);
                let mut risk = grid[(y % h) * w + (x % w)] as u32;
                risk += (x / w) as u32;
                risk += (y / h) as u32;
                if risk > 9 {
                    risk - 9
                } else {
                    risk
                }
            })
            .collect()
    }
}

impl Grid for LargeGrid {
    fn height(&self) -> usize {
        self.height
    }

    fn width(&self) -> usize {
        self.width
    }

    fn tile_risk(&self, pos: Position) -> Risk {
        self.grid[pos]
    }
}

pub fn solve(input: &str) -> u32 {
    let grid = LargeGrid::new(input);
    let path = grid.find_path();
    // grid.print_path(&path);

    grid.path_risk(&path)
}

#[cfg(test)]
mod tests {
    use super::solve;

    fn get_test_input() -> &'static str {
        include_str!("../../../inputs/day15/test_input.txt")
    }

    #[test]
    fn test_solve() {
        assert_eq!(solve(get_test_input()), 315);
    }
}
