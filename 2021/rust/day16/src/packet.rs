use bitvec::prelude::*;
use std::iter::{Product, Sum};

#[derive(Debug, PartialEq)]
pub enum Packet {
    Add(u8, Vec<Packet>),
    Mul(u8, Vec<Packet>),
    Min(u8, Vec<Packet>),
    Max(u8, Vec<Packet>),
    Val(u8, u64),
    Gtn(u8, Vec<Packet>),
    Ltn(u8, Vec<Packet>),
    Eql(u8, Vec<Packet>),
}

impl Packet {
    pub fn value(self) -> u64 {
        match self {
            Self::Add(_, v) => v.into_iter().sum(),
            Self::Mul(_, v) => v.into_iter().product(),
            Self::Min(_, v) => v.into_iter().map(Self::value).min().unwrap(),
            Self::Max(_, v) => v.into_iter().map(Self::value).max().unwrap(),
            Self::Val(_, n) => n,
            Self::Gtn(_, v) => {
                let mut iter = v.into_iter();
                if iter.next().unwrap().value() > iter.next().unwrap().value() {
                    1
                } else {
                    0
                }
            }
            Self::Ltn(_, v) => {
                let mut iter = v.into_iter();
                if iter.next().unwrap().value() < iter.next().unwrap().value() {
                    1
                } else {
                    0
                }
            }
            Self::Eql(_, v) => {
                let mut iter = v.into_iter();
                if iter.next().unwrap().value() == iter.next().unwrap().value() {
                    1
                } else {
                    0
                }
            }
        }
    }

    pub fn sum_versions(&self) -> u64 {
        match &self {
            Self::Val(version, _) => u64::from(*version),
            Self::Add(version, v)
            | Self::Mul(version, v)
            | Self::Min(version, v)
            | Self::Max(version, v)
            | Self::Gtn(version, v)
            | Self::Ltn(version, v)
            | Self::Eql(version, v) => v
                .iter()
                .fold(u64::from(*version), |acc, p| acc + p.sum_versions()),
        }
    }

    fn read_bits(v: &mut BitVec, n: usize) -> BitVec {
        v.drain((v.len() - n)..).collect()
    }

    fn read_bits_rev(v: &mut BitVec, n: usize) -> BitVec {
        v.drain((v.len() - n)..).rev().collect()
    }
}

impl Sum<Packet> for u64 {
    fn sum<I>(iter: I) -> Self
    where
        I: Iterator<Item = Packet>,
    {
        iter.map(Packet::value).sum()
    }
}

impl Product<Packet> for u64 {
    fn product<I>(iter: I) -> Self
    where
        I: Iterator<Item = Packet>,
    {
        iter.map(Packet::value).product()
    }
}

impl From<&str> for Packet {
    fn from(s: &str) -> Self {
        let mut v = s
            .trim_end()
            .chars()
            .flat_map(|c| match c {
                '0' => bitvec![0, 0, 0, 0],
                '1' => bitvec![0, 0, 0, 1],
                '2' => bitvec![0, 0, 1, 0],
                '3' => bitvec![0, 0, 1, 1],
                '4' => bitvec![0, 1, 0, 0],
                '5' => bitvec![0, 1, 0, 1],
                '6' => bitvec![0, 1, 1, 0],
                '7' => bitvec![0, 1, 1, 1],
                '8' => bitvec![1, 0, 0, 0],
                '9' => bitvec![1, 0, 0, 1],
                'A' => bitvec![1, 0, 1, 0],
                'B' => bitvec![1, 0, 1, 1],
                'C' => bitvec![1, 1, 0, 0],
                'D' => bitvec![1, 1, 0, 1],
                'E' => bitvec![1, 1, 1, 0],
                'F' => bitvec![1, 1, 1, 1],
                c => panic!("Unexpected hex char: '{}'", c),
            })
            .rev()
            .collect::<BitVec>();

        Self::from(&mut v)
    }
}

impl From<&mut BitVec> for Packet {
    fn from(v: &mut BitVec) -> Self {
        let version = Self::read_bits(v, 3).load::<u8>();
        let t = Self::read_bits(v, 3).load::<u8>();

        if t == 4 {
            let mut value = bitvec![];
            while v.pop().unwrap() {
                value.extend_from_bitslice(&Self::read_bits_rev(v, 4));
            }
            value.extend_from_bitslice(&Self::read_bits_rev(v, 4));
            value.reverse();

            Self::Val(version, value.load::<u64>())
        } else {
            let mut sub_packets = Vec::new();

            if v.pop().unwrap() {
                let num_packets = Self::read_bits(v, 11).load::<usize>();

                while sub_packets.len() < num_packets {
                    sub_packets.push(v.into());
                }
            } else {
                let num_bits = Self::read_bits(v, 15).load::<usize>();
                let mut sub_v = Self::read_bits(v, num_bits);

                while !sub_v.is_empty() {
                    sub_packets.push((&mut sub_v).into());
                }
            }

            match t {
                0 => Self::Add(version, sub_packets),
                1 => Self::Mul(version, sub_packets),
                2 => Self::Min(version, sub_packets),
                3 => Self::Max(version, sub_packets),
                5 => Self::Gtn(version, sub_packets),
                6 => Self::Ltn(version, sub_packets),
                7 => Self::Eql(version, sub_packets),
                _ => panic!("Unexpected packet type: '{}'", t),
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_literal() {
        let packet = Packet::from("D2FE28");

        assert_eq!(packet, Packet::Val(6, 2021));
    }

    #[test]
    fn test_parse_sub_packets_by_bit_limit() {
        let expected = Packet::Ltn(1, vec![Packet::Val(6, 10), Packet::Val(2, 20)]);
        let packet = Packet::from("38006F45291200");

        assert_eq!(packet, expected);
    }

    #[test]
    fn test_parse_sub_packets_by_packet_count() {
        let expected = Packet::Max(
            7,
            vec![Packet::Val(2, 1), Packet::Val(4, 2), Packet::Val(1, 3)],
        );
        let packet = Packet::from("EE00D40C823060");

        assert_eq!(packet, expected);
    }

    #[test]
    fn test_parse_nested() {
        let expected = Packet::Eql(
            4,
            vec![
                Packet::Add(2, vec![Packet::Val(2, 1), Packet::Val(4, 3)]),
                Packet::Mul(6, vec![Packet::Val(0, 2), Packet::Val(2, 2)]),
            ],
        );
        let packet = Packet::from("9C0141080250320F1802104A08");

        assert_eq!(packet, expected);
    }

    #[test]
    fn test_add_versions() {
        assert_eq!(Packet::from("8A004A801A8002F478").sum_versions(), 16);
        assert_eq!(
            Packet::from("620080001611562C8802118E34").sum_versions(),
            12
        );
        assert_eq!(
            Packet::from("C0015000016115A2E0802F182340").sum_versions(),
            23
        );
        assert_eq!(
            Packet::from("A0016C880162017C3686B18A3D4780").sum_versions(),
            31
        );
    }

    #[test]
    fn test_calculate_value() {
        assert_eq!(Packet::from("C200B40A82").value(), 3);
        assert_eq!(Packet::from("04005AC33890").value(), 54);
        assert_eq!(Packet::from("880086C3E88112").value(), 7);
        assert_eq!(Packet::from("CE00C43D881120").value(), 9);
        assert_eq!(Packet::from("D8005AC2A8F0").value(), 1);
        assert_eq!(Packet::from("F600BC2D8F").value(), 0);
        assert_eq!(Packet::from("9C005AC2F8F0").value(), 0);
        assert_eq!(Packet::from("9C0141080250320F1802104A08").value(), 1);
        assert_eq!(Packet::from("02008180210420C4200").value(), 10);
    }
}
