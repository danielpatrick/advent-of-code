use crate::packet::Packet;

pub fn solve(input: &str) -> u64 {
    Packet::from(input).sum_versions()
}

#[cfg(test)]
mod tests {
    use super::solve;

    fn get_test_input() -> &'static str {
        include_str!("../../../inputs/day16/test_input1.txt")
    }

    #[test]
    fn test_solve() {
        assert_eq!(solve(get_test_input()), 31);
    }

    #[test]
    fn test_solve_more_examples() {
        assert_eq!(solve("8A004A801A8002F478"), 16);
        assert_eq!(solve("620080001611562C8802118E34"), 12);
        assert_eq!(solve("C0015000016115A2E0802F182340"), 23);
        assert_eq!(solve("A0016C880162017C3686B18A3D4780"), 31);
    }
}
