use crate::packet::Packet;

pub fn solve(input: &str) -> u64 {
    Packet::from(input).value()
}

#[cfg(test)]
mod tests {
    use super::solve;

    fn get_test_input() -> &'static str {
        include_str!("../../../inputs/day16/test_input2.txt")
    }

    #[test]
    fn test_solve() {
        assert_eq!(solve(get_test_input()), 54);
    }

    #[test]
    fn test_solve_more_examples() {
        assert_eq!(solve("C200B40A82"), 3);
        assert_eq!(solve("04005AC33890"), 54);
        assert_eq!(solve("880086C3E88112"), 7);
        assert_eq!(solve("CE00C43D881120"), 9);
        assert_eq!(solve("D8005AC2A8F0"), 1);
        assert_eq!(solve("F600BC2D8F"), 0);
        assert_eq!(solve("9C005AC2F8F0"), 0);
        assert_eq!(solve("9C0141080250320F1802104A08"), 1);
        assert_eq!(solve("02008180210420C4200"), 10);
    }
}
