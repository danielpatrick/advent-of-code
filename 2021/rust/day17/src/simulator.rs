use lazy_static::lazy_static;
use num::integer::Roots;
use regex::Regex;
use std::mem::swap;

pub struct Target {
    pub x1: i32,
    pub x2: i32,
    pub y1: i32,
    pub y2: i32,
}

impl Target {
    pub const fn new(x1: i32, x2: i32, y1: i32, y2: i32) -> Self {
        Self { x1, x2, y1, y2 }
    }

    fn in_bounds(&self, x: i32, y: i32) -> bool {
        (self.x1..=self.x2).contains(&x) && (self.y1..=self.y2).contains(&y)
    }
}

#[derive(Debug, PartialEq)]
enum Ordering {
    Less,
    Equal(i32),
    Greater,
}

pub struct Simulation {
    target: Target,
}

impl Simulation {
    const fn new(target: Target) -> Self {
        Self { target }
    }

    fn does_hit(&self, mut dx: i32, mut dy: i32) -> Option<Ordering> {
        //
        //  (x1, y2)  (x2, y2)
        //      +--------+
        //      |        |
        //      |        |
        //      |        |
        //      +--------+
        //  (x1, y1)  (x2, y1)
        //
        let mut x = 0;
        let mut y = 0;
        let mut highest_y = 0;

        while y >= self.target.y1 {
            if x > self.target.x2 && y > self.target.y2 {
                // Above and further than the self
                return Some(Ordering::Greater);
            }

            highest_y = highest_y.max(y);

            if self.target.in_bounds(x, y) {
                return Some(Ordering::Equal(highest_y));
            }

            x += dx;
            y += dy;
            dx = 0.max(dx - 1);
            dy -= 1;
        }

        if x < self.target.x1 && y < self.target.y1 {
            // Lower than and short of the self
            return Some(Ordering::Less);
        }

        None
    }

    pub const fn highest_y(&self) -> i32 {
        let y = self.target.y1.abs() - 1;

        y * (y + 1) / 2
    }

    pub fn run(&self) -> i32 {
        let mut num_solutions = 0;

        // Assume y2 and y2 negative:
        //  - positive y1 and y2 requires bound of [int((2 * y1) ** 0.5), y2]
        //  - negative y1 and positive y2 has an infinite number of solutions
        // (because any dy greater than 0 will eventually reach zero)
        let mut start_dy = self.target.y1;
        let end_dy = self.target.y1.abs();

        // Assume x1 positive, because we can't move to the left
        let start_dx = (self.target.x1 * 2).sqrt();

        for dx in (start_dx..=self.target.x2).rev() {
            #[allow(clippy::mut_range_bound)]
            for dy in start_dy..end_dy {
                if let Some(ord) = self.does_hit(dx, dy) {
                    match ord {
                        Ordering::Less => {
                            start_dy += 1;
                        }
                        Ordering::Equal(_) => {
                            num_solutions += 1;
                        }
                        Ordering::Greater => {
                            break;
                        }
                    }
                }
            }
        }

        num_solutions
    }
}

impl From<&str> for Simulation {
    fn from(s: &str) -> Self {
        lazy_static! {
            static ref REGEX: Regex =
                Regex::new(r"^target area: x=([0-9-]+)..([0-9]+), y=([0-9-]+)..([0-9-]+)\n$")
                    .unwrap();
        }

        let captures = REGEX.captures(s).unwrap();
        let mut x1: i32 = captures[1].parse().unwrap();
        let mut x2: i32 = captures[2].parse().unwrap();
        let mut y1: i32 = captures[3].parse().unwrap();
        let mut y2: i32 = captures[4].parse().unwrap();

        if x1 > x2 {
            swap(&mut x1, &mut x2);
        }
        if y1 > y2 {
            swap(&mut y1, &mut y2);
        }

        Self::new(Target::new(x1, x2, y1, y2))
    }
}

#[cfg(test)]
mod tests {
    use super::{Ordering, Simulation, Target};

    fn get_test_input() -> &'static str {
        include_str!("../../../inputs/day17/test_input.txt")
    }

    #[test]
    fn test_highest_y() {
        let sim = Simulation::from(get_test_input());

        assert_eq!(sim.highest_y(), 45);
    }

    #[test]
    fn test_run() {
        let sim = Simulation::from(get_test_input());

        assert_eq!(sim.run(), 112);
    }

    #[test]
    fn test_does_hit() {
        let sim = Simulation::new(Target::new(20, 30, -10, -5));

        assert_eq!(sim.does_hit(7, 2), Some(Ordering::Equal(3)));
        assert_eq!(sim.does_hit(6, 3), Some(Ordering::Equal(6)));
        assert_eq!(sim.does_hit(9, 0), Some(Ordering::Equal(0)));
        assert_eq!(sim.does_hit(9, 2), Some(Ordering::Greater));
        assert_eq!(sim.does_hit(17, -4), None);
    }
}
