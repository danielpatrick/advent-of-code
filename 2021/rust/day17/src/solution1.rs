use crate::simulator::Simulation;

pub fn solve(input: &str) -> i32 {
    Simulation::from(input).highest_y()
}

#[cfg(test)]
mod tests {
    use super::solve;

    fn get_test_input() -> &'static str {
        include_str!("../../../inputs/day17/test_input.txt")
    }

    #[test]
    fn test_solve() {
        assert_eq!(solve(get_test_input()), 45);
    }
}
