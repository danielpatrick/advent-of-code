use serde::{Deserialize, Serialize};
use std::iter::Sum;
use std::mem;
use std::ops::AddAssign;

#[derive(Debug, PartialEq, Serialize, Deserialize, Clone)]
#[serde(untagged)]
pub enum Elem {
    Val(u16),
    Num(Box<Number>),
}

impl Elem {
    pub fn number(n: Number) -> Self {
        Self::Num(Box::new(n))
    }

    pub const fn value(v: u16) -> Self {
        Self::Val(v)
    }

    pub fn add_left(&mut self, right: u16) -> Option<u16> {
        match self {
            Elem::Val(v) => {
                *v += right;
                None
            }
            Elem::Num(n) => n.add_left(right),
        }
    }

    pub fn add_right(&mut self, left: u16) -> Option<u16> {
        match self {
            Elem::Val(v) => {
                *v += left;
                None
            }
            Elem::Num(n) => n.add_right(left),
        }
    }

    pub fn split(&mut self) -> bool {
        match self {
            &mut Self::Val(v) if v >= 10 => {
                *self = Self::number(Number::new(Self::value(v / 2), Self::value(v - v / 2)));
                true
            }
            Self::Val(_) => false,
            Self::Num(n) => n.split(),
        }
    }

    pub fn magnitude(&self) -> u16 {
        match &self {
            Self::Val(v) => *v,
            Self::Num(n) => n.magnitude(),
        }
    }
}

#[derive(Debug, PartialEq, Serialize, Deserialize, Clone)]
pub struct Number(Elem, Elem);

impl Number {
    pub const fn new(left: Elem, right: Elem) -> Self {
        Self(left, right)
    }

    pub fn explode(&mut self) -> bool {
        self.explode_rec(0).2
    }

    pub fn split(&mut self) -> bool {
        self.0.split() || self.1.split()
    }

    pub fn magnitude(&self) -> u16 {
        self.0.magnitude() * 3 + self.1.magnitude() * 2
    }

    fn explode_rec(&mut self, depth: usize) -> (Option<u16>, Option<u16>, bool) {
        let mut left = None;
        let mut right = None;
        let mut modified = false;

        if let Self(Elem::Val(l), Elem::Val(r)) = self {
            if depth >= 4 {
                return (Some(*l), Some(*r), true);
            }
        }
        if let Self(Elem::Num(n), other) = self {
            let (l, r, m) = n.explode_rec(depth + 1);

            if let Some(r) = r {
                right = other.add_left(r);

                if l.is_some() {
                    self.0 = Elem::Val(0);
                }
            } else {
                right = r;
            }

            left = l;
            modified = m;
        }
        if !modified {
            if let Self(other, Elem::Num(n)) = self {
                let (l, r, m) = n.explode_rec(depth + 1);

                if let Some(l) = l {
                    left = other.add_right(l);

                    if r.is_some() {
                        self.1 = Elem::Val(0);
                    }
                } else {
                    left = l;
                }

                right = r;
                modified = m;
            }
        }

        (left, right, modified)
    }

    fn add_left(&mut self, right: u16) -> Option<u16> {
        self.0.add_left(right)
    }

    fn add_right(&mut self, left: u16) -> Option<u16> {
        self.1.add_right(left)
    }

    fn reduce(&mut self) {
        while self.explode() || self.split() {}
    }
}

impl AddAssign for Number {
    fn add_assign(&mut self, other: Self) {
        let mut lhs = Self::new(Elem::value(0), Elem::number(other));

        mem::swap(&mut lhs.1, &mut self.1);
        mem::swap(&mut lhs.0, &mut self.0);

        self.0 = Elem::number(lhs);
    }
}

impl Sum<Self> for Number {
    fn sum<I>(iter: I) -> Self
    where
        I: Iterator<Item = Self>,
    {
        let mut ret: Option<Self> = None;

        for mut n in iter {
            if let Some(r) = &mut ret {
                *r += n;
                r.reduce();
            } else {
                n.reduce();
                ret = Some(n);
            }
        }

        ret.unwrap_or_else(|| Self::new(Elem::value(0), Elem::value(0)))
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use serde_json;

    fn get_test_input1() -> &'static str {
        include_str!("../../../inputs/day18/test_input1.txt")
    }

    fn get_test_input2() -> &'static str {
        include_str!("../../../inputs/day18/test_input2.txt")
    }

    #[test]
    fn test_from_str() {
        let expected = Number::new(
            Elem::value(1),
            Elem::number(Number::new(Elem::value(2), Elem::value(3))),
        );
        let result = serde_json::from_str::<Number>("[1, [2, 3]]");

        assert_eq!(result.unwrap(), expected);
    }

    #[test]
    fn test_to_string() {
        let expected = "[1,[2,3]]";
        let num = Number::new(
            Elem::value(1),
            Elem::number(Number::new(Elem::value(2), Elem::value(3))),
        );
        let result = serde_json::to_string(&num).unwrap();

        assert_eq!(&result, expected);
    }

    #[test]
    fn test_explode1() {
        let expected = String::from("[[[[0,9],2],3],4]");
        let mut number: Number = serde_json::from_str("[[[[[9, 8], 1], 2], 3], 4]").unwrap();

        assert!(number.explode());
        assert_eq!(serde_json::to_string(&number).unwrap(), expected);
    }

    #[test]
    fn test_explode2() {
        let expected = "[7,[6,[5,[7,0]]]]";
        let mut number: Number = serde_json::from_str("[7, [6, [5, [4, [3, 2]]]]]").unwrap();

        assert!(number.explode());
        assert_eq!(serde_json::to_string(&number).unwrap(), expected);
    }

    #[test]
    fn test_explode3() {
        let expected = "[[6,[5,[7,0]]],3]";
        let mut number: Number = serde_json::from_str("[[6, [5, [4, [3, 2]]]], 1]").unwrap();

        assert!(number.explode());
        assert_eq!(serde_json::to_string(&number).unwrap(), expected);
    }

    #[test]
    fn test_explode4() {
        let expected = "[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]";
        let mut number: Number =
            serde_json::from_str("[[3, [2, [1, [7, 3]]]], [6, [5, [4, [3, 2]]]]]").unwrap();

        assert!(number.explode());
        assert_eq!(serde_json::to_string(&number).unwrap(), expected);
    }

    #[test]
    fn test_explode5() {
        let expected = "[[3,[2,[8,0]]],[9,[5,[7,0]]]]";
        let mut number: Number =
            serde_json::from_str("[[3, [2, [8, 0]]], [9, [5, [4, [3, 2]]]]]").unwrap();

        assert!(number.explode());
        assert_eq!(serde_json::to_string(&number).unwrap(), expected);
    }

    #[test]
    fn test_explode_not_changed() {
        let expected = "[[[[0,9],2],3],4]";
        let mut number: Number = serde_json::from_str("[[[[0, 9], 2], 3], 4]").unwrap();

        assert!(!number.explode());
        assert_eq!(serde_json::to_string(&number).unwrap(), expected);
    }

    #[test]
    fn test_split1() {
        let expected = "[[[[0,7],4],[[7,8],[0,13]]],[1,1]]";
        let mut number: Number =
            serde_json::from_str("[[[[0, 7], 4], [15, [0, 13]]], [1, 1]]").unwrap();

        assert!(number.split());
        assert_eq!(serde_json::to_string(&number).unwrap(), expected);
    }

    #[test]
    fn test_split2() {
        let expected = "[[[[0,7],4],[[7,8],[0,[6,7]]]],[1,1]]";
        let mut number: Number =
            serde_json::from_str("[[[[0, 7], 4], [[7, 8], [0, 13]]], [1, 1]]").unwrap();

        assert!(number.split());
        assert_eq!(serde_json::to_string(&number).unwrap(), expected);
    }

    #[test]
    fn test_split_not_changed() {
        let expected = "[[[[1,7],4],[[7,8],[0,[6,7]]]],[1,1]]";
        let mut number: Number =
            serde_json::from_str("[[[[1, 7], 4], [[7, 8], [0, [6, 7]]]], [1, 1]]").unwrap();

        assert!(!number.split());
        assert_eq!(serde_json::to_string(&number).unwrap(), expected);
    }

    #[test]
    fn test_reduce_numbers_simple() {
        let expected = "[[[[0,7],4],[[7,8],[6,0]]],[8,1]]";
        let numbers = [
            serde_json::from_str("[[[[4, 3], 4], 4], [7, [[8, 4], 9]]]").unwrap(),
            serde_json::from_str("[1, 1]").unwrap(),
        ];
        let result = numbers.into_iter().sum::<Number>();

        assert_eq!(serde_json::to_string(&result).unwrap(), expected);
    }

    #[test]
    fn test_reduce_numbers_with_example_input1() {
        let expected = "[[[[6,6],[7,6]],[[7,7],[7,0]]],[[[7,7],[7,7]],[[7,8],[9,9]]]]";
        let numbers = get_test_input1()
            .lines()
            .map(serde_json::from_str)
            .map(|r| r.unwrap())
            .collect::<Vec<Number>>();
        let result = numbers.into_iter().sum::<Number>();

        assert_eq!(serde_json::to_string(&result).unwrap(), expected);
    }

    #[test]
    fn test_reduce_numbers_with_example_input2() {
        let expected = "[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]";
        let numbers = get_test_input2()
            .lines()
            .map(serde_json::from_str)
            .map(|r| r.unwrap())
            .collect::<Vec<Number>>();
        let result = numbers.into_iter().sum::<Number>();

        assert_eq!(serde_json::to_string(&result).unwrap(), expected);
    }

    #[test]
    fn test_magnitude1() {
        assert_eq!(
            serde_json::from_str::<Number>("[9, 1]")
                .unwrap()
                .magnitude(),
            29
        );
        assert_eq!(
            serde_json::from_str::<Number>("[1, 9]")
                .unwrap()
                .magnitude(),
            21
        );
        assert_eq!(
            serde_json::from_str::<Number>("[[9, 1], [1, 9]]")
                .unwrap()
                .magnitude(),
            129
        );
        assert_eq!(
            serde_json::from_str::<Number>("[[1, 2], [[3, 4], 5]]")
                .unwrap()
                .magnitude(),
            143
        );
        assert_eq!(
            serde_json::from_str::<Number>("[[[[0, 7], 4], [[7, 8], [6, 0]]], [8, 1]]")
                .unwrap()
                .magnitude(),
            1384
        );
        assert_eq!(
            serde_json::from_str::<Number>("[[[[1, 1], [2, 2]], [3, 3]], [4, 4]]")
                .unwrap()
                .magnitude(),
            445
        );
        assert_eq!(
            serde_json::from_str::<Number>("[[[[3, 0], [5, 3]], [4, 4]], [5, 5]]")
                .unwrap()
                .magnitude(),
            791
        );
        assert_eq!(
            serde_json::from_str::<Number>("[[[[5, 0], [7, 4]], [5, 5]], [6, 6]]")
                .unwrap()
                .magnitude(),
            1137
        );
    }
    #[test]
    fn test_magnitude2() {
        let number: Number = serde_json::from_str(
            "[[[[8, 7], [7, 7]], [[8, 6], [7, 7]]], [[[0, 7], [6, 6]], [8, 7]]]",
        )
        .unwrap();

        assert_eq!(number.magnitude(), 3488);
    }
}
