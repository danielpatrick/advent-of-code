use crate::snailfish::Number;

pub fn solve(input: &str) -> u16 {
    input
        .lines()
        .map(serde_json::from_str::<Number>)
        .map(Result::unwrap)
        .sum::<Number>()
        .magnitude()
}

#[cfg(test)]
mod tests {
    use super::solve;

    fn get_test_input1() -> &'static str {
        include_str!("../../../inputs/day18/test_input1.txt")
    }

    fn get_test_input2() -> &'static str {
        include_str!("../../../inputs/day18/test_input2.txt")
    }

    #[test]
    fn test_solve() {
        assert_eq!(solve(get_test_input1()), 4140);
        assert_eq!(solve(get_test_input2()), 3488);
    }
}
