use crate::snailfish::Number;
use itertools::Itertools;

pub fn solve(input: &str) -> u16 {
    input
        .lines()
        .map(serde_json::from_str::<Number>)
        .map(Result::unwrap)
        .permutations(2)
        .map(|nums| nums.into_iter().sum::<Number>().magnitude())
        .max()
        .unwrap()
}

#[cfg(test)]
mod tests {
    use super::solve;

    fn get_test_input() -> &'static str {
        include_str!("../../../inputs/day18/test_input1.txt")
    }

    #[test]
    fn test_solve() {
        assert_eq!(solve(get_test_input()), 3993);
    }
}
