use std::ops::{Add, Sub};

#[derive(Debug, PartialOrd, Ord, PartialEq, Eq, Hash, Clone, Copy)]
pub struct Coord {
    x: i32,
    y: i32,
    z: i32,
}

impl Coord {
    pub const fn new(x: i32, y: i32, z: i32) -> Self {
        Self { x, y, z }
    }

    pub const fn sum(&self) -> i32 {
        self.x + self.y + self.z
    }

    pub fn orientation(&self, idx: usize) -> Self {
        COORD_MAPPERS[idx](self)
    }
}

impl Sub for &Coord {
    type Output = Coord;

    fn sub(self, rhs: Self) -> Self::Output {
        Coord::new(self.x - rhs.x, self.y - rhs.y, self.z - rhs.z)
    }
}

impl Add for &Coord {
    type Output = Coord;

    fn add(self, rhs: Self) -> Self::Output {
        Coord::new(self.x + rhs.x, self.y + rhs.y, self.z + rhs.z)
    }
}

impl TryFrom<&str> for Coord {
    type Error = &'static str;

    fn try_from(s: &str) -> Result<Self, Self::Error> {
        let err = "Failed to parse str to Coord";

        let mut iter = s.split(',').map(|s| s.parse().or(Err(err)));

        Ok(Self::new(
            iter.next().ok_or(err)??,
            iter.next().ok_or(err)??,
            iter.next().ok_or(err)??,
        ))
    }
}

const COORD_MAPPERS: [fn(&Coord) -> Coord; 24] = [
    |&c| Coord::new(c.x, c.y, c.z),
    |&c| Coord::new(c.x, c.z, -c.y),
    |&c| Coord::new(c.x, -c.z, c.y),
    |&c| Coord::new(c.x, -c.y, -c.z),
    |&c| Coord::new(-c.x, c.z, c.y),
    |&c| Coord::new(-c.x, c.y, -c.z),
    |&c| Coord::new(-c.x, -c.y, c.z),
    |&c| Coord::new(-c.x, -c.z, -c.y),
    |&c| Coord::new(c.y, c.z, c.x),
    |&c| Coord::new(c.y, c.x, -c.z),
    |&c| Coord::new(c.y, -c.x, c.z),
    |&c| Coord::new(c.y, -c.z, -c.x),
    |&c| Coord::new(-c.y, c.x, c.z),
    |&c| Coord::new(-c.y, c.z, -c.x),
    |&c| Coord::new(-c.y, -c.z, c.x),
    |&c| Coord::new(-c.y, -c.x, -c.z),
    |&c| Coord::new(c.z, c.x, c.y),
    |&c| Coord::new(c.z, c.y, -c.x),
    |&c| Coord::new(c.z, -c.y, c.x),
    |&c| Coord::new(c.z, -c.x, -c.y),
    |&c| Coord::new(-c.z, c.y, c.x),
    |&c| Coord::new(-c.z, c.x, -c.y),
    |&c| Coord::new(-c.z, -c.x, c.y),
    |&c| Coord::new(-c.z, -c.y, -c.x),
];
