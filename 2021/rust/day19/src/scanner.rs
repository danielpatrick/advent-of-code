use crate::coord::Coord;
use itertools::Itertools;
use std::slice::Iter;

#[derive(Debug)]
pub struct Scanner {
    number: u8,
    beacons: Vec<Coord>,
}

impl Scanner {
    pub const fn new(number: u8, beacons: Vec<Coord>) -> Self {
        Self { number, beacons }
    }

    pub const fn number(&self) -> u8 {
        self.number
    }

    pub fn beacons(&self) -> Iter<Coord> {
        self.beacons.iter()
    }

    pub fn orientation(&self, idx: usize) -> Self {
        Self::new(
            self.number,
            self.beacons.iter().map(|b| b.orientation(idx)).collect(),
        )
    }

    pub fn moved(&self, location: &Coord) -> Self {
        Self::new(
            self.number,
            self.beacons.iter().map(|b| b + location).sorted().collect(),
        )
    }
}

impl TryFrom<&str> for Scanner {
    type Error = &'static str;

    fn try_from(s: &str) -> Result<Self, Self::Error> {
        let mut iter = s.lines();

        let number = iter
            .next()
            .unwrap()
            .split_whitespace()
            .nth(2)
            .unwrap()
            .parse()
            .or(Err("Failed to parse Scanner from str"))?;

        let beacons = iter
            .map(TryInto::try_into)
            .sorted()
            .collect::<Result<_, _>>()?;

        Ok(Self::new(number, beacons))
    }
}
