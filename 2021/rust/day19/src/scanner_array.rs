use crate::coord::Coord;
use crate::scanner::Scanner;
use itertools::Itertools;
use std::collections::{HashMap, VecDeque};

#[derive(Debug)]
pub struct ScannerArray {
    scanners: Vec<(Scanner, Coord)>,
}

impl ScannerArray {
    pub fn new(scanner: Scanner) -> Self {
        Self {
            scanners: vec![(scanner, Coord::new(0, 0, 0))],
        }
    }

    fn identify_overlap(
        &self,
        i: usize,
        potential: &Scanner,
        cache: &mut HashMap<u8, Vec<Scanner>>,
    ) -> Option<(Scanner, Coord)> {
        let n = potential.number();
        let cached = cache.entry(n).or_default();

        for j in 0..24 {
            if cached.len() < j + 1 {
                cached.push(potential.orientation(j));
            }

            if let Some(pos) = Self::find_offset(&self.scanners[i].0, &cached[j]) {
                let moved = cached[j].moved(&pos);
                cache.remove(&n); // We shouldn't need to reorientate this one anymore
                return Some((moved, pos));
            }
        }

        None
    }

    fn find_offset(scanner: &Scanner, potential: &Scanner) -> Option<Coord> {
        let mut counts: HashMap<Coord, usize> = HashMap::new();

        for p1 in scanner.beacons() {
            for p2 in potential.beacons() {
                let diff = p1 - p2;
                *counts.entry(diff).or_default() += 1;

                if counts[&diff] >= 12 {
                    return Some(diff);
                }
            }
        }
        None
    }

    fn place_scanners(&mut self, mut unplaced: VecDeque<Scanner>) {
        let mut cached_positions = HashMap::new();
        let mut placed = (0..self.scanners.len()).collect::<Vec<_>>();

        while !unplaced.is_empty() {
            let mut newly_placed = Vec::new();

            for i in &placed {
                for _ in 0..unplaced.len() {
                    let potential = unplaced.pop_front().unwrap();

                    if let Some((scanner, pos)) =
                        self.identify_overlap(*i, &potential, &mut cached_positions)
                    {
                        self.scanners.push((scanner, pos));
                        newly_placed.push(self.scanners.len() - 1);
                    } else {
                        unplaced.push_back(potential);
                    }
                }
            }

            placed = newly_placed;
        }
    }

    pub fn beacons(&self) -> Vec<Coord> {
        self.scanners
            .iter()
            .flat_map(|s| s.0.beacons())
            .copied()
            .unique()
            .collect()
    }

    pub fn scanner_positions(&self) -> Vec<Coord> {
        self.scanners.iter().map(|(_, p)| *p).collect()
    }
}

impl TryFrom<&str> for ScannerArray {
    type Error = &'static str;

    fn try_from(s: &str) -> Result<Self, Self::Error> {
        let mut unplaced: VecDeque<Scanner> = s
            .split("\n\n")
            .map(TryInto::try_into)
            .collect::<Result<_, _>>()?;
        let mut arr = Self::new(unplaced.pop_front().unwrap());

        arr.place_scanners(unplaced);

        Ok(arr)
    }
}
