use crate::scanner_array::ScannerArray;

pub fn solve(input: &str) -> usize {
    ScannerArray::try_from(input).unwrap().beacons().len()
}

#[cfg(test)]
mod tests {
    use super::solve;

    fn get_test_input() -> &'static str {
        include_str!("../../../inputs/day19/test_input.txt")
    }

    #[test]
    fn test_solve() {
        assert_eq!(solve(get_test_input()), 79);
    }
}
