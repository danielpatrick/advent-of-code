use crate::scanner_array::ScannerArray;
use itertools::Itertools;

pub fn solve(input: &str) -> usize {
    let positions = ScannerArray::try_from(input).unwrap().scanner_positions();

    positions
        .iter()
        .combinations(2)
        .map(|bs| (bs[1] - bs[0]).sum().abs())
        .max()
        .unwrap() as usize
}

#[cfg(test)]
mod tests {
    use super::solve;

    fn get_test_input() -> &'static str {
        include_str!("../../../inputs/day19/test_input.txt")
    }

    #[test]
    fn test_solve() {
        assert_eq!(solve(get_test_input()), 3621);
    }
}
