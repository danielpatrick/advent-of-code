use crate::image::InfiniteImage;
use bitvec::prelude::*;
use std::fmt;

pub struct Enhancer {
    algorithm: BitVec,
    image: InfiniteImage,
}

impl Enhancer {
    pub fn new(s: &str, image: InfiniteImage) -> Self {
        let algorithm = s
            .chars()
            .map(|c| match c {
                '.' => false,
                '#' => true,
                _ => panic!("Unexpected char '{}'", c),
            })
            .collect();

        Self { algorithm, image }
    }

    pub fn enhance(&mut self) {
        let height = self.image.height + 2;
        let width = self.image.width + 2;
        let default = if self.image.default {
            self.algorithm[511]
        } else {
            self.algorithm[0]
        };
        let bits = if default { 1 } else { 0 };
        let mut pixels = bitvec![bits; width * height];

        for y in 1..self.image.height - 1 {
            for x in 1..self.image.width - 1 {
                let pos1 = (y - 1) * self.image.height + x - 1;
                let pos2 = y * self.image.height + x - 1;
                let pos3 = (y + 1) * self.image.height + x - 1;

                let mut bv = self.image.pixels[pos1..=pos1 + 2].to_owned();
                bv.extend_from_bitslice(&self.image.pixels[pos2..=pos2 + 2]);
                bv.extend_from_bitslice(&self.image.pixels[pos3..=pos3 + 2]);

                bv.reverse();
                let i = bv.load::<usize>();
                pixels.set((y + 1) * width + x + 1, self.algorithm[i]);
            }
        }

        self.image = InfiniteImage::new(pixels, width, height, default);
    }

    pub fn count_pixels(&self) -> usize {
        self.image.count_pixels()
    }
}

impl fmt::Display for Enhancer {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.image.fmt(f)
    }
}
