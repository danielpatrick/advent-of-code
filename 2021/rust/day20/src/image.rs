use bitvec::prelude::*;
use std::fmt;

pub struct InfiniteImage {
    pub pixels: BitVec,
    pub width: usize,
    pub height: usize,
    pub default: bool,
}

impl InfiniteImage {
    pub fn new(pixels: BitVec, width: usize, height: usize, default: bool) -> Self {
        Self {
            pixels,
            width,
            height,
            default,
        }
    }

    pub fn count_pixels(&self) -> usize {
        self.pixels.count_ones()
    }
}

impl<'a> FromIterator<&'a str> for InfiniteImage {
    fn from_iter<T: IntoIterator<Item = &'a str>>(iter: T) -> Self {
        let mut width = None;
        let mut height = 4;
        let mut pixels: BitVec = iter
            .into_iter()
            .flat_map(|line| {
                let mut bv = bitvec![];
                if width.is_none() {
                    let w = line.len() + 4;
                    let blank_line = bitvec![0; w];

                    for _ in 0..2 {
                        bv.extend_from_bitslice(&blank_line);
                    }

                    width = Some(w);
                }
                height += 1;

                bv.extend_from_bitslice(&bitvec![0; 2]);
                bv.extend(line.chars().map(|c| match c {
                    '.' => false,
                    '#' => true,
                    _ => panic!("Unexpected char '{}'", c),
                }));
                bv.extend_from_bitslice(&bitvec![0; 2]);
                bv
            })
            .collect();

        let width = width.unwrap();
        let blank_line = bitvec![0; width];

        for _ in 0..2 {
            pixels.extend_from_bitslice(&blank_line);
        }

        Self {
            pixels,
            width,
            height,
            default: false,
        }
    }
}

impl fmt::Display for InfiniteImage {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let s = (0..self.height)
            .flat_map(|y| {
                self.pixels[y * self.width..(y + 1) * self.width]
                    .iter()
                    .map(|b| if *b { '#' } else { '.' })
                    .chain(Some('\n'))
            })
            .collect::<String>();

        s.fmt(f)
    }
}
