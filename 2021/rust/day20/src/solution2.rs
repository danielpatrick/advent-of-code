use crate::enhancer::Enhancer;

pub fn solve(input: &str) -> usize {
    let mut iter = input.lines();
    let mut enhancer = Enhancer::new(iter.next().unwrap(), iter.skip(1).collect());

    for _ in 0..50 {
        enhancer.enhance();
    }

    enhancer.count_pixels()
}

#[cfg(test)]
mod tests {
    use super::solve;

    fn get_test_input() -> &'static str {
        include_str!("../../../inputs/day20/test_input.txt")
    }

    #[test]
    fn test_solve() {
        assert_eq!(solve(get_test_input()), 3351);
    }
}
