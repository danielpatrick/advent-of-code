struct DeterministicDie {
    last_roll: u16,
}

impl DeterministicDie {
    pub const fn new() -> Self {
        Self { last_roll: 100 }
    }

    pub fn roll(&mut self) -> u16 {
        if self.last_roll == 100 {
            self.last_roll = 1;
        } else {
            self.last_roll += 1;
        }

        self.last_roll
    }
}

// (position, score)
type Player = (u16, u16);

fn move_player(player: &mut Player, die: &mut DeterministicDie) {
    player.0 += die.roll() + die.roll() + die.roll();
    player.0 %= 10;

    if player.0 == 0 {
        player.0 = 10;
    }

    player.1 += player.0;
}

fn parse_players(input: &str) -> (Player, Player) {
    let mut iter = input
        .lines()
        .map(|line| line.split_whitespace().last().unwrap().parse().unwrap());

    ((iter.next().unwrap(), 0), (iter.next().unwrap(), 0))
}

pub fn solve(input: &str) -> u64 {
    let (mut p1, mut p2) = parse_players(input);
    let mut num_rolls = 0;
    let mut die = DeterministicDie::new();

    loop {
        move_player(&mut p1, &mut die);
        num_rolls += 3;
        if p1.1 >= 1000 {
            break;
        }

        move_player(&mut p2, &mut die);
        num_rolls += 3;
        if p2.1 >= 1000 {
            break;
        }
    }

    num_rolls * u64::from(p1.1.min(p2.1))
}

#[cfg(test)]
mod tests {
    use super::solve;

    fn get_test_input() -> &'static str {
        include_str!("../../../inputs/day21/test_input.txt")
    }

    #[test]
    fn test_solve() {
        assert_eq!(solve(get_test_input()), 739785);
    }
}
