use std::collections::HashMap;

const POSSIBLE_ROLLS: [(u16, u16); 7] = [(3, 1), (4, 3), (5, 6), (6, 7), (7, 6), (8, 3), (9, 1)];

// (position, score)
type Player = (u16, u16);

fn parse_players(input: &str) -> (Player, Player) {
    let mut iter = input
        .lines()
        .map(|line| line.split_whitespace().last().unwrap().parse().unwrap());

    ((iter.next().unwrap(), 0), (iter.next().unwrap(), 0))
}

const fn move_player(p: Player, die_roll: u16) -> Player {
    let mut pos = p.0 + die_roll;
    pos %= 10;

    if pos == 0 {
        pos = 10;
    }

    (pos, p.1 + pos)
}

type Cache = HashMap<(Player, Player), (u64, u64)>;

fn roll(p1: Player, p2: Player, cache: &mut Cache) -> (u64, u64) {
    if p1.1 >= 21 {
        return (1, 0);
    }
    if p2.1 >= 21 {
        return (0, 1);
    }

    let key = (p1, p2);

    if let Some(&cached) = cache.get(&key) {
        return cached;
    }

    let result = POSSIBLE_ROLLS
        .iter()
        .fold((0, 0), |acc, (die_roll, count)| {
            let (two, one) = roll(p2, move_player(p1, *die_roll), cache);

            (
                acc.0 + one * u64::from(*count),
                acc.1 + two * u64::from(*count),
            )
        });

    cache.insert(key, result);

    result
}

pub fn solve(input: &str) -> u64 {
    let (p1, p2) = parse_players(input);

    let (one, two) = roll(p1, p2, &mut Cache::new());

    if one > two {
        one
    } else {
        two
    }
}

#[cfg(test)]
mod tests {
    use super::solve;

    fn get_test_input() -> &'static str {
        include_str!("../../../inputs/day21/test_input.txt")
    }

    #[test]
    fn test_solve() {
        assert_eq!(solve(get_test_input()), 444356092776315);
    }
}
