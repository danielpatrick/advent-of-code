use crate::region::Region;
use lazy_static::lazy_static;
use regex::Regex;

pub enum RebootInstruction {
    On(Region),
    Off(Region),
}

impl RebootInstruction {
    pub const fn region(&self) -> &Region {
        match self {
            Self::On(r) | Self::Off(r) => r,
        }
    }

    pub fn parse_many(s: &str) -> Vec<Self> {
        lazy_static! {
            static ref REGEX: Regex = Regex::new(
                    r"(?m)^(?P<instruction>on|off) x=(-?\d+)\.\.(-?\d+),y=(-?\d+)\.\.(-?\d+),z=(-?\d+)\.\.(-?\d+)$"
                )
                .unwrap();
        }

        REGEX
            .captures_iter(s)
            .map(|caps| {
                let region = Region::new(
                    caps[2].parse().unwrap()..=caps[3].parse().unwrap(),
                    caps[4].parse().unwrap()..=caps[5].parse().unwrap(),
                    caps[6].parse().unwrap()..=caps[7].parse().unwrap(),
                );

                match &caps[1] {
                    "on" => Self::On(region),
                    "off" => Self::Off(region),
                    other => panic!("Unexpected instruction: '{}'", other),
                }
            })
            .collect()
    }
}
