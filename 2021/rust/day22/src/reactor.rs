use crate::instruction::RebootInstruction;
use crate::region::Region;

pub struct Reactor {
    cubes: Vec<Region>,
}

impl Reactor {
    pub const fn new() -> Self {
        Self { cubes: Vec::new() }
    }

    pub fn reboot(&mut self, instructions: Vec<RebootInstruction>) {
        for instruction in instructions {
            let mut new_cubes: Vec<Region> = self
                .cubes
                .drain(0..)
                .flat_map(|region1| region1.remove_overlap(instruction.region()))
                .collect();

            if let RebootInstruction::On(region) = instruction {
                new_cubes.push(region);
            }

            self.cubes = new_cubes;
        }
    }

    pub fn count(&self) -> u64 {
        self.cubes.iter().map(Region::area).sum()
    }

    pub fn count_init(&self) -> u64 {
        self.cubes.iter().map(|c| c.area_within(-50, 51)).sum()
    }
}
