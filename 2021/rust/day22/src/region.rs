use std::ops::RangeInclusive;

#[derive(Debug, Clone)]
pub struct Region {
    x: RangeInclusive<i32>,
    y: RangeInclusive<i32>,
    z: RangeInclusive<i32>,
}

impl Region {
    pub const fn new(
        x: RangeInclusive<i32>,
        y: RangeInclusive<i32>,
        z: RangeInclusive<i32>,
    ) -> Self {
        Self { x, y, z }
    }

    pub fn area(&self) -> u64 {
        (self.x.end() + 1 - self.x.start()).abs() as u64
            * (self.y.end() + 1 - self.y.start()).abs() as u64
            * (self.z.end() + 1 - self.z.start()).abs() as u64
    }

    pub fn area_within(&self, start: i32, end: i32) -> u64 {
        ((self.x.end() + 1).clamp(start, end) - (*self.x.start()).clamp(start, end)).abs() as u64
            * ((self.y.end() + 1).clamp(start, end) - (*self.y.start()).clamp(start, end)).abs()
                as u64
            * ((self.z.end() + 1).clamp(start, end) - (*self.z.start()).clamp(start, end)).abs()
                as u64
    }

    fn has_overlap(&self, rhs: &Self) -> bool {
        self.x.end() >= rhs.x.start()
            && rhs.x.end() >= self.x.start()
            && self.y.end() >= rhs.y.start()
            && rhs.y.end() >= self.y.start()
            && self.z.end() >= rhs.z.start()
            && rhs.z.end() >= self.z.start()
    }

    pub fn remove_overlap(&self, rhs: &Self) -> Vec<Self> {
        if !self.has_overlap(rhs) {
            return vec![self.clone()];
        }

        let mut new_regions = Vec::new();

        if self.x.start() < rhs.x.start() {
            new_regions.push(Self::new(
                *self.x.start()..=rhs.x.start() - 1,
                *self.y.start()..=*self.y.end(),
                *self.z.start()..=*self.z.end(),
            ));
        }
        if self.x.end() > rhs.x.end() {
            new_regions.push(Self::new(
                rhs.x.end() + 1..=*self.x.end(),
                *self.y.start()..=*self.y.end(),
                *self.z.start()..=*self.z.end(),
            ));
        }

        if self.y.start() < rhs.y.start() {
            new_regions.push(Self::new(
                *self.x.start().max(rhs.x.start())..=*self.x.end().min(rhs.x.end()),
                *self.y.start()..=rhs.y.start() - 1,
                *self.z.start()..=*self.z.end(),
            ));
        }
        if self.y.end() > rhs.y.end() {
            new_regions.push(Self::new(
                *self.x.start().max(rhs.x.start())..=*self.x.end().min(rhs.x.end()),
                rhs.y.end() + 1..=*self.y.end(),
                *self.z.start()..=*self.z.end(),
            ));
        }

        if self.z.start() < rhs.z.start() {
            new_regions.push(Self::new(
                *self.x.start().max(rhs.x.start())..=*self.x.end().min(rhs.x.end()),
                *self.y.start().max(rhs.y.start())..=*self.y.end().min(rhs.y.end()),
                *self.z.start()..=rhs.z.start() - 1,
            ));
        }
        if self.z.end() > rhs.z.end() {
            new_regions.push(Self::new(
                *self.x.start().max(rhs.x.start())..=*self.x.end().min(rhs.x.end()),
                *self.y.start().max(rhs.y.start())..=*self.y.end().min(rhs.y.end()),
                rhs.z.end() + 1..=*self.z.end(),
            ));
        }

        new_regions
    }
}
