use crate::instruction::RebootInstruction;
use crate::reactor::Reactor;

pub fn solve(input: &str) -> u64 {
    let instructions = RebootInstruction::parse_many(input);
    let mut reactor = Reactor::new();
    reactor.reboot(instructions);

    reactor.count_init()
}

#[cfg(test)]
mod tests {
    use super::solve;

    fn get_test_input1() -> &'static str {
        include_str!("../../../inputs/day22/test_input1.txt")
    }

    fn get_test_input2() -> &'static str {
        include_str!("../../../inputs/day22/test_input2.txt")
    }

    fn get_test_input3() -> &'static str {
        include_str!("../../../inputs/day22/test_input3.txt")
    }

    #[test]
    fn test_solve() {
        assert_eq!(solve(get_test_input1()), 39);
        assert_eq!(solve(get_test_input2()), 590784);
        assert_eq!(solve(get_test_input3()), 474140);
    }
}
