use crate::instruction::RebootInstruction;
use crate::reactor::Reactor;

pub fn solve(input: &str) -> u64 {
    let instructions = RebootInstruction::parse_many(input);
    let mut reactor = Reactor::new();
    reactor.reboot(instructions);

    reactor.count()
}

#[cfg(test)]
mod tests {
    use super::solve;

    fn get_test_input() -> &'static str {
        include_str!("../../../inputs/day22/test_input3.txt")
    }

    #[test]
    fn test_solve() {
        assert_eq!(solve(get_test_input()), 2758514936282235);
    }
}
