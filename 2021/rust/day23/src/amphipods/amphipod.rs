use super::room::Room;

#[derive(Clone, Copy, PartialEq)]
pub enum Amphipod {
    Amber,
    Bronze,
    Copper,
    Desert,
}

impl Amphipod {
    pub const fn for_room(room: Room) -> Self {
        match room {
            Room::One => Self::Amber,
            Room::Two => Self::Bronze,
            Room::Three => Self::Copper,
            Room::Four => Self::Desert,
        }
    }
}

impl TryFrom<usize> for Amphipod {
    type Error = String;

    fn try_from(n: usize) -> Result<Self, Self::Error> {
        match n {
            0 => Ok(Self::Amber),
            1 => Ok(Self::Bronze),
            2 => Ok(Self::Copper),
            3 => Ok(Self::Desert),
            _ => Err(format!("Unexpected amphipod number: '{}'", n)),
        }
    }
}
