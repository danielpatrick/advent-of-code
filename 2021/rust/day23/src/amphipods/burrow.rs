use super::amphipod::Amphipod;
use super::graph::BurrowGraph;
use super::room::Room;
use super::state::AmphipodState;
use super::Location;
use itertools::Itertools;
use std::cmp::Reverse;
use std::collections::{BinaryHeap, HashMap};
use std::rc::Rc;

pub struct Burrow<const N: usize>
where
    [(); N * 4]:,
{
    starting: Rc<AmphipodState<N>>,
    finishing: Rc<AmphipodState<N>>,
    graph: BurrowGraph<N>,
}

impl<const N: usize> Burrow<N>
where
    [(); N * 4]:,
{
    const POD_ENERGY_COSTS: [usize; 4] = [1, 10, 100, 1000];

    pub fn new(starting: AmphipodState<N>) -> Self {
        Self {
            starting: Rc::new(starting),
            finishing: Rc::new(BurrowGraph::<N>::finishing_state()),
            graph: BurrowGraph::new(),
        }
    }

    fn amphipod_type_in_pos(state: &AmphipodState<N>, pos: Location) -> Option<Amphipod> {
        state
            .iter()
            .position(|p| *p == pos)
            .map(|i| Amphipod::try_from(i / N).unwrap())
    }

    fn enterable_room_pos(state: &AmphipodState<N>, r: Room) -> Option<Location> {
        let a = Amphipod::for_room(r);

        for i in (0..N).rev() {
            let pos = BurrowGraph::<N>::room_to_loc(r, i as u8);
            if let Some(amphipod) = Self::amphipod_type_in_pos(state, pos) {
                if amphipod != a {
                    return None;
                }
            } else {
                return Some(pos);
            }
        }

        None
    }

    /// can be an underestimate; must never be an overestimate
    fn estimated_cost_to_solve(state: &AmphipodState<N>) -> usize {
        let (num_to_move, to_entrance) = state.iter().enumerate().fold(
            ([4; 4], [0; 4]),
            |(mut num_to_move, mut to_entrance), (i, pos)| {
                let pod_num = i / N;
                let target_col = pod_num * 2 + 3;

                if pos.1 == 1 {
                    to_entrance[pod_num] += target_col.abs_diff(pos.0 as usize);
                } else if pos.0 as usize == target_col {
                    num_to_move[pod_num] -= 1;
                } else {
                    to_entrance[pod_num] +=
                        target_col.abs_diff(pos.0 as usize) + pos.1 as usize - 1;
                }

                (num_to_move, to_entrance)
            },
        );

        (0..4)
            .map(|r| {
                let n = num_to_move[r];

                Self::POD_ENERGY_COSTS[r] * (to_entrance[r] + (n * (n.max(1) - 1)) / 2)
            })
            .sum()
    }

    fn can_move_from_room(state: &AmphipodState<N>, i: usize) -> bool {
        let pos = state[i];
        let target_room = i / N;
        let (current_room, y) = BurrowGraph::<N>::loc_to_room(pos).unwrap();

        if current_room.number() as usize != target_room {
            true
        } else if y as usize == N - 1 {
            false
        } else {
            state
                .iter()
                .enumerate()
                .filter(|(i, _)| i / N != target_room) // ignore siblings
                .any(|(_, (x, _))| *x == pos.0)
        }
    }

    fn get_next_states(&self, state: &AmphipodState<N>) -> Vec<(Rc<AmphipodState<N>>, usize)> {
        state
            .iter()
            .enumerate()
            .filter_map(|(pod, pos)| {
                if BurrowGraph::<N>::is_hallway(*pos) {
                    let r = Room::new((pod / N) as u8).unwrap();

                    Self::enterable_room_pos(state, r)
                        .map(|target| (pod, self.graph.routes_from_corridor(*pos, target).unwrap()))
                } else if Self::can_move_from_room(state, pod) {
                    Some((pod, self.graph.routes_from_room(*pos).unwrap()))
                } else {
                    None
                }
            })
            .flat_map(|(pod, paths)| {
                let pod_multiplier = Self::POD_ENERGY_COSTS[pod / N];

                paths
                    .iter()
                    .filter(|(path, _)| path.iter().skip(1).all(|p| !state.contains(*p)))
                    .map(move |(path, cost)| {
                        (
                            Rc::new(state.moved(pod, *path.last().unwrap())),
                            *cost * pod_multiplier,
                        )
                    })
            })
            .collect()
    }

    pub fn find_lowest_cost(&self) -> Option<usize> {
        let mut costs = HashMap::<Rc<AmphipodState<N>>, usize>::new();
        let mut heap = BinaryHeap::new();

        costs.insert(self.starting.clone(), 0);
        heap.push(Reverse((
            Self::estimated_cost_to_solve(&self.starting),
            0,
            self.starting.clone(),
        )));

        while let Some(Reverse((_, cost, state))) = heap.pop() {
            if state == self.finishing {
                return Some(cost);
            }

            if cost > *costs.get(&state).unwrap_or(&usize::MAX) {
                continue;
            }

            for (new_state, c) in self.get_next_states(&state) {
                let new_cost = cost + c;

                if new_cost < *costs.get(&new_state).unwrap_or(&usize::MAX) {
                    heap.push(Reverse((
                        Self::estimated_cost_to_solve(&new_state) + new_cost,
                        new_cost,
                        new_state.clone(),
                    )));
                    costs.insert(new_state, new_cost);
                }
            }
        }

        None
    }

    fn _print_state(state: &AmphipodState<N>) {
        let mut output = vec![
            vec!["#############\n"],
            vec![
                "#", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", "#", "\n",
            ],
        ];

        for _ in 0..N {
            output.push(vec![
                "#", "#", "#", " ", "#", " ", "#", " ", "#", " ", "#", "#", "#", "\n",
            ]);
        }

        output.push(vec!["#############\n"]);

        for (i, (x, y)) in state.iter().enumerate() {
            output[*y as usize][*x as usize] = match i / N {
                0 => "A",
                1 => "B",
                2 => "C",
                _ => "D",
            };
        }

        println!("State: {:?}", state);
        println!(
            "{}",
            output
                .into_iter()
                .flat_map(std::iter::IntoIterator::into_iter)
                .join(""),
        );
    }
}

impl<const N: usize> From<&str> for Burrow<N>
where
    [(); N * 4]:,
{
    fn from(s: &str) -> Self {
        s.lines().collect()
    }
}

impl<'a, const N: usize> FromIterator<&'a str> for Burrow<N>
where
    [(); N * 4]:,
{
    fn from_iter<T: IntoIterator<Item = &'a str>>(iter: T) -> Self {
        Self::new(iter.into_iter().collect::<AmphipodState<N>>())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    // Size 2 rooms:
    //
    //   | 3  5  7  9
    // --+-----------
    // 2 | 7  9 11 13
    // 3 | 8 10 12 14

    fn create_burrow() -> Burrow<2> {
        let v = vec![
            "#############",
            "#...........#",
            "###B#C#B#D###",
            "  #A#D#C#A#",
            "  #########",
        ];
        Burrow::new(v.into_iter().collect())
    }

    fn create_big_burrow() -> Burrow<4> {
        let v = vec![
            "#############",
            "#...........#",
            "###B#C#B#D###",
            "  #D#C#B#A#",
            "  #D#B#A#C#",
            "  #A#D#C#A#",
            "  #########",
        ];
        Burrow::new(v.into_iter().collect())
    }

    #[test]
    fn test_can_move_from_room1() {
        #[rustfmt::skip]
        let state = AmphipodState::<2>::new(
            [(3, 3), (9, 3), (5, 2), (5, 3), (7, 2), (7, 3), (11, 1), (9, 2)]
        );

        assert!(!Burrow::can_move_from_room(&state, 0));
        assert!(!Burrow::can_move_from_room(&state, 2));
        assert!(!Burrow::can_move_from_room(&state, 3));
        assert!(!Burrow::can_move_from_room(&state, 4));
        assert!(!Burrow::can_move_from_room(&state, 5));
        assert!(Burrow::can_move_from_room(&state, 7));
    }

    #[test]
    fn test_can_move_from_room2() {
        #[rustfmt::skip]
        let state = AmphipodState::<2>::new(
            [(1, 1), (3, 3), (5, 3), (7, 2), (4, 1), (7, 3), (8, 1), (10, 1)]
        );

        assert!(!Burrow::can_move_from_room(&state, 1));
        assert!(!Burrow::can_move_from_room(&state, 2));
    }

    #[test]
    fn test_enterable_room_pos1() {
        #[rustfmt::skip]
        let state = AmphipodState::<2>::new(
            [(3, 2), (3, 3), (5, 2), (5, 3), (6, 1), (7, 3), (9, 3), (10, 1)],
        );
        assert_eq!(
            Burrow::enterable_room_pos(&state, Room::Three),
            Some((7, 2)),
        );
        assert_eq!(Burrow::enterable_room_pos(&state, Room::Four), Some((9, 2)));
    }

    #[test]
    fn test_enterable_room_pos2() {
        #[rustfmt::skip]
        let state = AmphipodState::<4>::new(
            [
                (2, 1), (3, 5), (9, 3), (9, 5), (1, 1), (5, 4), (10, 1), (11, 1),
                (5, 2), (5, 3), (7, 5), (9, 4), (3, 3), (3, 4), (5, 5), (9, 2),
            ]
        );

        assert_eq!(
            Burrow::enterable_room_pos(&state, Room::Three),
            Some((7, 4)),
        );
    }

    #[test]
    #[rustfmt::skip]
    fn test_get_next_states() {
        let state = AmphipodState::new(
            [(1, 1), (3, 3), (5, 3), (7, 2), (4, 1), (7, 3), (8, 1), (10, 1)],
        );
        let expected_state1 = Rc::new(AmphipodState::new(
            [(3, 2), (3, 3), (5, 3), (7, 2), (4, 1), (7, 3), (8, 1), (10, 1)],
        ));
        let expected_state2 = Rc::new(AmphipodState::new(
            [(1, 1), (3, 3), (5, 3), (7, 2), (4, 1), (7, 3), (9, 3), (10, 1)],
        ));
        let expected_state3 = Rc::new(AmphipodState::new(
            [(1, 1), (3, 3), (5, 3), (7, 2), (4, 1), (7, 3), (8, 1), (9, 3)],
        ));
        let expected_state4 = Rc::new(AmphipodState::new(
            [(1, 1), (3, 3), (5, 3), (6, 1), (4, 1), (7, 3), (8, 1), (10, 1)],
        ));
        let expected = vec![
            (expected_state1, 3),
            (expected_state4, 20),
            (expected_state2, 3000),
            (expected_state3, 3000),
        ];

        let b = create_burrow();

        assert_eq!(b.get_next_states(&state), expected);
    }

    #[test]
    fn test_find_lowest_cost() {
        assert_eq!(create_burrow().find_lowest_cost(), Some(12521));
        assert_eq!(create_big_burrow().find_lowest_cost(), Some(44169));
    }
}
