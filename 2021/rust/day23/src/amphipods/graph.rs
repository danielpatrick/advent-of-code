use super::room::Room;
use super::state::AmphipodState;
use super::Location;
use std::collections::BTreeMap;

pub type PathCost = (Vec<Location>, usize);

#[derive(Debug)]
pub struct BurrowGraph<const N: usize> {
    paths_from_rooms: BTreeMap<Location, Vec<PathCost>>,
    paths_to_rooms: BTreeMap<(Location, Location), Vec<PathCost>>,
}

impl<const N: usize> BurrowGraph<N> {
    const HALLWAY: [Location; 7] = [(1, 1), (2, 1), (4, 1), (6, 1), (8, 1), (10, 1), (11, 1)];

    pub fn new() -> Self {
        let mut paths_from_rooms: BTreeMap<Location, Vec<PathCost>> = BTreeMap::new();
        let mut paths_to_rooms: BTreeMap<(Location, Location), Vec<PathCost>> = BTreeMap::new();

        for r in 0..4 {
            let room = Room::new(r).unwrap();

            for i in 0..N as u8 {
                let mut paths = if i > 0 {
                    paths_from_rooms[&Self::room_to_loc(room, i - 1)].clone()
                } else {
                    Self::paths_from_room(room).to_vec()
                };

                let loc = Self::room_to_loc(room, i);

                for (path, cost) in &mut paths {
                    path.insert(0, loc);
                    *cost += 1;

                    let rev_path = path.iter().rev().copied().collect::<Vec<_>>();
                    paths_to_rooms.insert((rev_path[0], loc), vec![(rev_path, *cost)]);
                }

                paths_from_rooms.insert(loc, paths);
            }
        }

        Self {
            paths_from_rooms,
            paths_to_rooms,
        }
    }

    pub fn finishing_state() -> AmphipodState<N>
    where
        [(); N * 4]:,
    {
        (0..N * 4)
            .map(|i| Self::room_to_loc(Room::new((i / N) as u8).unwrap(), (i % N) as u8))
            .collect::<AmphipodState<N>>()
    }

    pub fn routes_from_room(&self, origin: Location) -> Option<&Vec<PathCost>> {
        self.paths_from_rooms.get(&origin)
    }

    pub fn routes_from_corridor(
        &self,
        origin: Location,
        target: Location,
    ) -> Option<&Vec<PathCost>> {
        self.paths_to_rooms.get(&(origin, target))
    }

    pub fn is_hallway(pos: Location) -> bool {
        Self::HALLWAY.contains(&pos)
    }

    pub fn loc_to_room(pos: Location) -> Option<(Room, u8)> {
        if pos.1 >= 2 {
            match pos.0 {
                3 => Some(Room::One),
                5 => Some(Room::Two),
                7 => Some(Room::Three),
                9 => Some(Room::Four),
                _ => None,
            }
            .map(|r| (r, pos.1 - 2))
        } else {
            None
        }
    }

    pub const fn room_to_loc(room: Room, pos: u8) -> Location {
        (room.number() * 2 + 3, pos + 2)
    }

    //   0123456789012
    // 0 #############
    // 1 #.. . . . ..#
    // 2 ###A#B#C#D###
    // 3   #A#B#C#D#
    // 4   #########
    fn paths_from_room(r: Room) -> [(Vec<Location>, usize); 7] {
        match r {
            Room::One => [
                (vec![(2, 1)], 1),
                (vec![(4, 1)], 1),
                (vec![(2, 1), (1, 1)], 2),
                (vec![(4, 1), (6, 1)], 3),
                (vec![(4, 1), (6, 1), (8, 1)], 5),
                (vec![(4, 1), (6, 1), (8, 1), (10, 1)], 7),
                (vec![(4, 1), (6, 1), (8, 1), (10, 1), (11, 1)], 8),
            ],
            Room::Two => [
                (vec![(4, 1)], 1),
                (vec![(6, 1)], 1),
                (vec![(4, 1), (2, 1)], 3),
                (vec![(6, 1), (8, 1)], 3),
                (vec![(4, 1), (2, 1), (1, 1)], 4),
                (vec![(6, 1), (8, 1), (10, 1)], 5),
                (vec![(6, 1), (8, 1), (10, 1), (11, 1)], 6),
            ],
            Room::Three => [
                (vec![(8, 1)], 1),
                (vec![(6, 1)], 1),
                (vec![(8, 1), (10, 1)], 3),
                (vec![(6, 1), (4, 1)], 3),
                (vec![(8, 1), (10, 1), (11, 1)], 4),
                (vec![(6, 1), (4, 1), (2, 1)], 5),
                (vec![(6, 1), (4, 1), (2, 1), (1, 1)], 6),
            ],
            Room::Four => [
                (vec![(10, 1)], 1),
                (vec![(8, 1)], 1),
                (vec![(10, 1), (11, 1)], 2),
                (vec![(8, 1), (6, 1)], 3),
                (vec![(8, 1), (6, 1), (4, 1)], 5),
                (vec![(8, 1), (6, 1), (4, 1), (2, 1)], 7),
                (vec![(8, 1), (6, 1), (4, 1), (2, 1), (1, 1)], 8),
            ],
        }
    }
}
