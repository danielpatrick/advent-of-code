mod amphipod;
mod burrow;
mod graph;
mod room;
mod state;

pub use burrow::Burrow;

pub type Location = (u8, u8);
