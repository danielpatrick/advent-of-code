#[derive(Debug, Clone, Copy)]
pub enum Room {
    One,
    Two,
    Three,
    Four,
}

impl Room {
    pub const fn new(number: u8) -> Option<Self> {
        match number {
            0 => Some(Self::One),
            1 => Some(Self::Two),
            2 => Some(Self::Three),
            3 => Some(Self::Four),
            _ => None,
        }
    }

    pub const fn number(self) -> u8 {
        match self {
            Self::One => 0,
            Self::Two => 1,
            Self::Three => 2,
            Self::Four => 3,
        }
    }
}
