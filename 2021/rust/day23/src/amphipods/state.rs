use super::Location;
use arrayvec::ArrayVec;
use itertools::Itertools;
use std::fmt;
use std::ops::Index;

#[derive(Hash, PartialEq, Eq, PartialOrd, Ord, Clone)]
pub struct AmphipodState<const N: usize>
where
    [(); N * 4]:,
{
    state: [Location; N * 4],
}

impl<const N: usize> AmphipodState<N>
where
    [(); N * 4]:,
{
    /// State must already be sorted within each N amphipods, ie
    ///
    ///     for i in 0..4 {
    ///         state[i * N..i * N + N].sort();
    ///     }
    ///
    pub const fn new(state: [Location; N * 4]) -> Self {
        Self { state }
    }

    pub fn moved(&self, pod: usize, target: Location) -> Self {
        let mut state = self.state;

        state[pod] = target;

        let start = (pod / N) * N;
        let end = start + N;

        state[start..end].sort_unstable();

        Self::new(state)
    }

    pub fn iter(&self) -> std::slice::Iter<Location> {
        self.state.iter()
    }

    pub fn contains(&self, pos: Location) -> bool {
        self.state.contains(&pos)
    }
}

impl<const N: usize> FromIterator<Location> for AmphipodState<N>
where
    [(); N * 4]:,
{
    fn from_iter<T: IntoIterator<Item = Location>>(iter: T) -> Self {
        Self::new(ArrayVec::from_iter(iter).into_inner().unwrap())
    }
}

impl<'a, const N: usize> FromIterator<&'a str> for AmphipodState<N>
where
    [(); N * 4]:,
{
    fn from_iter<T: IntoIterator<Item = &'a str>>(iter: T) -> Self {
        iter.into_iter()
            .enumerate()
            .skip(2)
            .flat_map(|(y, line)| {
                line.chars()
                    .enumerate()
                    .filter(|(_, c)| "ABCD".contains(*c))
                    .map(move |(x, c)| (c, (x, y)))
            })
            .sorted()
            .map(|(_, (x, y))| (x as u8, y as u8))
            .collect::<Self>()
    }
}

impl<const N: usize> Index<usize> for AmphipodState<N>
where
    [(); N * 4]:,
{
    type Output = Location;

    fn index(&self, idx: usize) -> &Self::Output {
        &self.state[idx]
    }
}

impl<const N: usize> fmt::Debug for AmphipodState<N>
where
    [(); N * 4]:,
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.state.fmt(f)
    }
}

impl<const N: usize> IntoIterator for AmphipodState<N>
where
    [(); N * 4]:,
{
    type Item = Location;
    type IntoIter = std::array::IntoIter<Self::Item, { N * 4 }>;

    fn into_iter(self) -> Self::IntoIter {
        self.state.into_iter()
    }
}
