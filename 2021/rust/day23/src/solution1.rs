use crate::amphipods::Burrow;

pub fn solve(input: &str) -> usize {
    let burrow = Burrow::<2>::from(input);

    burrow.find_lowest_cost().unwrap()
}

#[cfg(test)]
mod tests {
    use super::solve;

    fn get_test_input() -> &'static str {
        include_str!("../../../inputs/day23/test_input.txt")
    }

    #[test]
    fn test_solve() {
        assert_eq!(solve(get_test_input()), 12521);
    }
}
