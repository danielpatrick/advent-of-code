use crate::amphipods::Burrow;

pub fn solve(input: &str) -> usize {
    let mut lines = input.lines().collect::<Vec<_>>();
    let additional_lines = vec!["  #D#C#B#A#", "  #D#B#A#C#"];

    lines.insert(3, additional_lines[0]);
    lines.insert(4, additional_lines[1]);

    let burrow = lines.into_iter().collect::<Burrow<4>>();

    burrow.find_lowest_cost().unwrap()
}

#[cfg(test)]
mod tests {
    use super::solve;

    fn get_test_input() -> &'static str {
        include_str!("../../../inputs/day23/test_input.txt")
    }

    #[test]
    fn test_solve() {
        assert_eq!(solve(get_test_input()), 44169);
    }
}
