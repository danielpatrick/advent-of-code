pub enum Register {
    W,
    X,
    Y,
    Z,
}

impl TryFrom<&str> for Register {
    type Error = &'static str;

    fn try_from(s: &str) -> Result<Self, Self::Error> {
        let err = "Unable to parse Register from &str";

        match s {
            "w" => Ok(Self::W),
            "x" => Ok(Self::X),
            "y" => Ok(Self::Y),
            "z" => Ok(Self::Z),
            _ => Err(err),
        }
    }
}

pub enum Operand {
    Reg(Register),
    Value(i8),
}

impl Operand {
    pub const fn value(&self) -> Option<i8> {
        match self {
            Self::Reg(_) => None,
            Self::Value(n) => Some(*n),
        }
    }
}

impl TryFrom<&str> for Operand {
    type Error = &'static str;

    fn try_from(s: &str) -> Result<Self, Self::Error> {
        let err = "Unable to parse Operand from &str";

        if ["w", "x", "y", "z"].contains(&s) {
            Ok(Self::Reg(s.try_into()?))
        } else {
            Ok(Self::Value(s.parse().or(Err(err))?))
        }
    }
}

pub enum Instruction {
    Inp(Register),
    Add(Register, Operand),
    Mul(Register, Operand),
    Div(Register, Operand),
    Mod(Register, Operand),
    Eql(Register, Operand),
}

impl Instruction {
    pub const fn right(&self) -> Option<&Operand> {
        match self {
            Self::Inp(_) => None,
            Self::Add(_, op)
            | Self::Mul(_, op)
            | Self::Div(_, op)
            | Self::Mod(_, op)
            | Self::Eql(_, op) => Some(op),
        }
    }
}

impl TryFrom<&str> for Instruction {
    type Error = &'static str;

    fn try_from(s: &str) -> Result<Self, Self::Error> {
        let err = "Unable to parse Instruction from &str";

        let mut iter = s.split_whitespace();

        let op = iter.next().ok_or(err)?;
        let left: Register = iter.next().ok_or(err)?.try_into()?;
        let right = iter.next().ok_or(err);

        match op {
            "inp" => Ok(Self::Inp(left)),
            "add" => Ok(Self::Add(left, right?.try_into()?)),
            "mul" => Ok(Self::Mul(left, right?.try_into()?)),
            "div" => Ok(Self::Div(left, right?.try_into()?)),
            "mod" => Ok(Self::Mod(left, right?.try_into()?)),
            "eql" => Ok(Self::Eql(left, right?.try_into()?)),
            _ => Err(err),
        }
    }
}

pub fn parse_instructions(s: &str) -> Result<Vec<Instruction>, &'static str> {
    s.lines()
        .map(Instruction::try_from)
        .collect::<Result<_, _>>()
}
