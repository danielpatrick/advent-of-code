use crate::instruction::{Instruction, Operand};

pub fn highest_valid_model_number(instructions: &[Instruction]) -> u64 {
    valid_model_number(instructions, true)
}

pub fn lowest_valid_model_number(instructions: &[Instruction]) -> u64 {
    valid_model_number(instructions, false)
}

fn valid_model_number(instructions: &[Instruction], max: bool) -> u64 {
    let (limit, f): (i8, Box<dyn Fn(i8, i8) -> i8>) = if max {
        (9, Box::new(i8::min))
    } else {
        (1, Box::new(i8::max))
    };

    let mut num: u64 = 0;
    let mut stack = Vec::new();

    for (i, ins) in instructions.chunks(18).enumerate() {
        if let Instruction::Div(_, Operand::Value(1)) = ins[4] {
            stack.push((i, ins[15].right().unwrap().value().unwrap()));
        } else {
            let (j, popped) = stack.pop().unwrap();
            let b = ins[5].right().unwrap().value().unwrap();

            num += f(limit, limit - b - popped) as u64 * 10_u64.pow(13 - j as u32);
            num += f(limit, limit + b + popped) as u64 * 10_u64.pow(13 - i as u32);
        }
    }

    num
}
