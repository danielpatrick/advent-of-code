use crate::instruction::parse_instructions;
use crate::monad::lowest_valid_model_number;

pub fn solve(input: &str) -> u64 {
    let instructions = parse_instructions(input).unwrap();

    lowest_valid_model_number(&instructions)
}
