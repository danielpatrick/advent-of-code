use std::collections::HashSet;
use std::fmt;

type Location = (usize, usize);

struct Grid {
    height: usize,
    width: usize,
    south: HashSet<Location>,
    east: HashSet<Location>,
}

impl Grid {
    fn move_cucumbers<F>(cucumbers: &mut HashSet<Location>, other: &HashSet<Location>, f: F) -> bool
    where
        F: Fn(&Location) -> Location,
    {
        let mut has_moved = false;

        *cucumbers = cucumbers
            .iter()
            .map(|pos| {
                let new_pos = f(pos);

                if cucumbers.contains(&new_pos) || other.contains(&new_pos) {
                    *pos
                } else {
                    has_moved = true;
                    new_pos
                }
            })
            .collect();

        has_moved
    }

    fn tick(&mut self) -> bool {
        Self::move_cucumbers(&mut self.east, &self.south, |(x, y)| {
            ((x + 1) % self.width, *y)
        }) | Self::move_cucumbers(&mut self.south, &self.east, |(x, y)| {
            (*x, (y + 1) % self.height)
        })
    }
}

impl From<&str> for Grid {
    fn from(s: &str) -> Self {
        let mut height = 0;
        let mut width = 0;
        let mut south = HashSet::new();
        let mut east = HashSet::new();

        for (y, line) in s.lines().enumerate() {
            height += 1;
            width = line.len();

            for (x, c) in line.chars().enumerate() {
                match c {
                    '>' => {
                        east.insert((x, y));
                    }
                    'v' => {
                        south.insert((x, y));
                    }
                    _ => {}
                }
            }
        }

        Self {
            height,
            width,
            south,
            east,
        }
    }
}

impl fmt::Display for Grid {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let s = (0..self.height)
            .flat_map(|y| {
                (0..self.width)
                    .map(move |x| {
                        if self.east.contains(&(x, y)) {
                            '>'
                        } else if self.south.contains(&(x, y)) {
                            'v'
                        } else {
                            '.'
                        }
                    })
                    .chain(Some('\n'))
            })
            .collect::<String>();

        s.fmt(f)
    }
}

pub fn solve(input: &str) -> String {
    let mut grid = Grid::from(input);
    let mut count = 1;

    while grid.tick() {
        count += 1;
    }

    count.to_string()
}

#[cfg(test)]
mod tests {
    use super::{solve, Grid};

    fn get_test_input() -> &'static str {
        include_str!("../../../inputs/day25/test_input.txt")
    }

    #[test]
    fn test_solve() {
        assert_eq!(solve(get_test_input()), "58");
    }

    #[test]
    fn test_display() {
        let expected = "\
            v...>>.vv>\n\
            .vv>>.vv..\n\
            >>.>v>...v\n\
            >>v>>.>.v.\n\
            v>v.vv.v..\n\
            >.>>..v...\n\
            .vv..>.>v.\n\
            v.v..>>v.v\n\
            ....v..v.>\n";

        let grid = Grid::from(get_test_input());

        assert_eq!(format!("{}", grid), expected);
    }

    #[test]
    fn test_tick() {
        let expected = "\
            ....>.>v.>\n\
            v.v>.>v.v.\n\
            >v>>..>v..\n\
            >>v>v>.>.v\n\
            .>v.v...v.\n\
            v>>.>vvv..\n\
            ..v...>>..\n\
            vv...>>vv.\n\
            >.v.v..v.v\n";

        let mut grid = Grid::from(get_test_input());
        grid.tick();

        assert_eq!(format!("{}", grid), expected);
    }
}
