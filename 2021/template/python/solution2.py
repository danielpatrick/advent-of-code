#!/usr/bin/env python
# -*- coding: utf-8 -*-


def solve(input_text):
    return input_text + "?"


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
