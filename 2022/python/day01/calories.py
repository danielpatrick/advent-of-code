def parse_groups(input_list):
    current = []

    for line in input_list:
        if line == "":
            yield current
            current = []
        else:
            current.append(int(line))

    if current:
        yield current


def get_calories(input_list):
    return (sum(elf) for elf in parse_groups(input_list))
