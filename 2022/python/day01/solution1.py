#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .calories import get_calories


def solve(input_list):
    return max(get_calories(input_list))


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
