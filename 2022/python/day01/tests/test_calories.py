from ..calories import get_calories, parse_groups


def test_parse_groups():
    assert list(parse_groups(["1", "23", "", "5"])) == [[1, 23], [5]]
    assert list(parse_groups(["1", "23", "", "5", ""])) == [[1, 23], [5]]


def test_get_calories():
    assert list(get_calories(["1", "23", "", "5", "4"])) == [24, 9]
    assert list(get_calories(["1", "23", "", "5", "4", ""])) == [24, 9]
