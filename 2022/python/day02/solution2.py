#!/usr/bin/env python
# -*- coding: utf-8 -*-

VALUE = {
    "A": 0,
    "B": 1,
    "C": 2,
    "X": 0,
    "Y": 1,
    "Z": 2,
}


def score(a, b):
    if b == "X":
        return (VALUE[a] - 1) % 3 + 1

    if b == "Y":
        return VALUE[a] + 4

    if b == "Z":
        return (VALUE[a] + 1) % 3 + 7


def solve(input_list):
    return sum(score(*line.split()) for line in input_list)


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
