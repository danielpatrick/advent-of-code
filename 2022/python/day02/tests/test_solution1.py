#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest

from shared import get_input

from .. import solution1
from ..solution1 import score, solve


class TestSolution:
    @pytest.fixture
    def input_text(self):
        yield get_input(solution1.__file__, "test_input.txt")

    def test_score(self):
        assert score("A", "Y") == 8
        assert score("B", "X") == 1
        assert score("C", "Z") == 6
        assert score("C", "X") == 7
        assert score("B", "Z") == 9

    def test_solver(self, input_text):
        solution = solve(input_text)

        assert solution == 15
