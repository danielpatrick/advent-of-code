#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest

from shared import get_input

from .. import solution2
from ..solution2 import score, solve


class TestSolution:
    @pytest.fixture
    def input_text(self):
        yield get_input(solution2.__file__, "test_input.txt")

    def test_score(self):
        assert score("A", "Y") == 4
        assert score("B", "X") == 1
        assert score("C", "Z") == 7
        assert score("C", "X") == 2
        assert score("B", "Z") == 9

    def test_solver(self, input_text):
        solution = solve(input_text)

        assert solution == 12
