#!/usr/bin/env python
# -*- coding: utf-8 -*-


def find_common(contents):
    mid = len(contents) // 2

    return (set(contents[mid:]) & set(contents[:mid])).pop()


def priority(letter):
    if "a" <= letter <= "z":
        return ord(letter) - ord("a") + 1

    return ord(letter) - ord("A") + 1 + 26


def solve(input_list):
    return sum(priority(find_common(contents)) for contents in input_list)


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
