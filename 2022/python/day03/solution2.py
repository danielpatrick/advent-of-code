#!/usr/bin/env python
# -*- coding: utf-8 -*-


def chunk(items, n):
    for i in range(0, len(items), n):
        yield items[i : i + n]


def find_common(a, b, c):
    return (set(a) & set(b) & set(c)).pop()


def priority(letter):
    if "A" <= letter <= "Z":
        return ord(letter) - ord("A") + 1 + 26

    return ord(letter) - ord("a") + 1


def solve(input_list):
    return sum(
        priority(find_common(a, b, c)) for a, b, c in chunk(input_list, 3)
    )


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
