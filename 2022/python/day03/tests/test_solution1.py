#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest

from shared import get_input

from .. import solution1
from ..solution1 import find_common, priority, solve


class TestSolution:
    @pytest.fixture
    def input_list(self):
        yield get_input(solution1.__file__, "test_input.txt")

    def test_find_common(self):
        assert find_common("vJrwpWtwJgWrhcsFMMfFFhFp") == "p"
        assert find_common("jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL") == "L"
        assert find_common("PmmdzqPrVvPwwTWBwg") == "P"
        assert find_common("wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn") == "v"
        assert find_common("ttgJtRGJQctTZtZT") == "t"
        assert find_common("CrZsJsPPZsGzwwsLwLmpwMDw") == "s"

    def test_priority(self):
        assert priority("p") == 16
        assert priority("L") == 38
        assert priority("P") == 42
        assert priority("v") == 22
        assert priority("t") == 20
        assert priority("s") == 19

    def test_solver(self, input_list):
        solution = solve(input_list)

        assert solution == 157
