#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest

from shared import get_input

from .. import solution2
from ..solution2 import chunk, find_common, priority, solve


class TestSolution:
    @pytest.fixture
    def input_list(self):
        yield get_input(solution2.__file__, "test_input.txt")

    def test_chunk(self):
        assert list(chunk([1, 2, 3, 4, 5, 6], 2)) == [[1, 2], [3, 4], [5, 6]]
        assert list(chunk([1, 2, 3, 4, 5, 6], 3)) == [[1, 2, 3], [4, 5, 6]]
        assert list(chunk([1, 2, 3, 4, 5], 2)) == [[1, 2], [3, 4], [5]]

    def test_find_common1(self):
        group = (
            "vJrwpWtwJgWrhcsFMMfFFhFp",
            "jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL",
            "PmmdzqPrVvPwwTWBwg",
        )

        assert find_common(*group) == "r"

    def test_find_common2(self):
        group = (
            "wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn",
            "ttgJtRGJQctTZtZT",
            "CrZsJsPPZsGzwwsLwLmpwMDw",
        )

        assert find_common(*group) == "Z"

    def test_priority(self):
        assert priority("r") == 18
        assert priority("Z") == 52

    def test_solver(self, input_list):
        solution = solve(input_list)

        assert solution == 70
