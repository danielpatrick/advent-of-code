#!/usr/bin/env python
# -*- coding: utf-8 -*-


def parse_pair(line):
    a, b = line.split(",")

    return tuple(
        tuple(int(pos) for pos in assignment.split("-"))
        for assignment in (a, b)
    )
