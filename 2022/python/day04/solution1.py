#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .parser import parse_pair


def is_contained(a, b):
    """Return True if a contained by b."""
    a1, a2 = a
    b1, b2 = b

    return b1 <= a1 and b2 >= a2


def solve(input_list):
    pairs = (parse_pair(line) for line in input_list)

    return sum([is_contained(a, b) or is_contained(b, a) for a, b in pairs])


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
