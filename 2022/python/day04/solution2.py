#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .parser import parse_pair


def is_overlap(a, b):
    """Return True if a overlaps with b at any point."""
    return not (b[1] < a[0] or a[1] < b[0])


def solve(input_list):
    pairs = (parse_pair(line) for line in input_list)

    return sum([is_overlap(a, b) for a, b in pairs])


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
