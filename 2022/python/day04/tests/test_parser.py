#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest

from shared import get_input

from .. import parser
from ..parser import parse_pair


class TestParser:
    @pytest.fixture
    def input_list(self):
        yield get_input(parser.__file__, "test_input.txt")

    def test_parse_pair(self, input_list):
        assert parse_pair(input_list[0]) == ((2, 4), (6, 8))
        assert parse_pair(input_list[1]) == ((2, 3), (4, 5))
        assert parse_pair(input_list[2]) == ((5, 7), (7, 9))
        assert parse_pair(input_list[3]) == ((2, 8), (3, 7))
        assert parse_pair(input_list[4]) == ((6, 6), (4, 6))
        assert parse_pair(input_list[5]) == ((2, 6), (4, 8))
