#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest

from shared import get_input

from .. import solution1
from ..solution1 import is_contained, solve


class TestSolution:
    @pytest.fixture
    def input_list(self):
        yield get_input(solution1.__file__, "test_input.txt")

    def test_is_contained(self):
        assert is_contained((2, 4), (6, 8)) is False
        assert is_contained((6, 8), (2, 4)) is False
        assert is_contained((5, 7), (7, 9)) is False
        assert is_contained((3, 4), (2, 3)) is False
        assert is_contained((3, 6), (4, 5)) is False
        assert is_contained((4, 5), (3, 6)) is True
        assert is_contained((4, 4), (4, 5)) is True

    def test_solver(self, input_list):
        solution = solve(input_list)

        assert solution == 2
