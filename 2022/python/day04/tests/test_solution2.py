#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest

from shared import get_input

from .. import solution2
from ..solution2 import is_overlap, solve


class TestSolution:
    @pytest.fixture
    def input_list(self):
        yield get_input(solution2.__file__, "test_input.txt")

    def test_is_overlap(self):
        assert is_overlap((2, 4), (6, 8)) is False
        assert is_overlap((6, 8), (2, 4)) is False
        assert is_overlap((5, 7), (7, 9)) is True
        assert is_overlap((3, 4), (2, 3)) is True
        assert is_overlap((3, 6), (4, 5)) is True
        assert is_overlap((4, 5), (3, 6)) is True
        assert is_overlap((4, 4), (4, 5)) is True

    def test_solver(self, input_list):
        solution = solve(input_list)

        assert solution == 4
