#!/usr/bin/env python
# -*- coding: utf-8 -*-


def parse_input(input_list):
    mode = 0
    stacks = []
    instructions = []

    for line in input_list:
        if line == "":
            mode = 1
        elif mode == 0:
            for i, crate in enumerate(line[1::4]):
                if len(stacks) <= i:
                    stacks.append([])

                if crate[0].isalpha():
                    stacks[i].append(crate[0])
        else:
            _, n, _, start, _, end = line.split()
            instructions.append((int(n), int(start) - 1, int(end) - 1))

    for stack in stacks:
        stack.reverse()

    return (stacks, instructions)
