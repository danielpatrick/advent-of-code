#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .parser import parse_input


def solve(input_list):
    stacks, instructions = parse_input(input_list)

    for n, start, end in instructions:
        for _ in range(n):
            stacks[end].append(stacks[start].pop())

    return "".join(stack[-1] for stack in stacks)


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
