#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .parser import parse_input


def solve(input_list):
    stacks, instructions = parse_input(input_list)

    for n, start, end in instructions:
        i = len(stacks[start]) - n

        stacks[end].extend(stacks[start][i:])
        stacks[start] = stacks[start][:i]

    return "".join(stack[-1] for stack in stacks)


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
