#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest

from shared import get_input

from .. import parser
from ..parser import parse_input


class TestParser:
    @pytest.fixture
    def input_list(self):
        yield get_input(parser.__file__, "test_input.txt")

    def test_parser(self, input_list):
        expected_crates = [
            ["Z", "N"],
            ["M", "C", "D"],
            ["P"],
        ]
        expected_instructions = [
            (1, 1, 0),
            (3, 0, 2),
            (2, 1, 0),
            (1, 0, 1),
        ]
        expected = (expected_crates, expected_instructions)

        assert parse_input(input_list) == expected
