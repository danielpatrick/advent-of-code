def find_marker(buffer, n):
    """
    Find the location of the start-of-packet marker within the given buffer.

    Parameter n (int) indicates the required length of the marker, which
    is defined as consisting of n unique characters.
    """
    for i in range(len(buffer) - n + 1):
        if len(set(buffer[i : i + n])) == n:
            return i + n

    raise Exception(
        f"Invalid buffer: No consecutive unique chars of length {n} found."
    )
