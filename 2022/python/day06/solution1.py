#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .marker import find_marker


def solve(puzzle_input):
    return find_marker(puzzle_input, 4)


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
