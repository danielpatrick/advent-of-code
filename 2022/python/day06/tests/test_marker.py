#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest

from shared import get_input

from .. import marker
from ..marker import find_marker


class TestMarker:
    @pytest.fixture
    def puzzle_input(self):
        yield get_input(marker.__file__, "test_input.txt")

    def test_find_marker_length_4(self, puzzle_input):
        assert find_marker(puzzle_input, 4) == 7

    def test_find_marker_length_14(self, puzzle_input):
        assert find_marker(puzzle_input, 14) == 19
