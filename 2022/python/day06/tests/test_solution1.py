#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest

from shared import get_input

from .. import solution1
from ..solution1 import solve


class TestSolution:
    @pytest.fixture
    def puzzle_input(self):
        yield get_input(solution1.__file__, "test_input.txt")

    def test_solver(self, puzzle_input):
        assert solve(puzzle_input) == 7

    def test_solver2(self):
        puzzle_input = "bvwbjplbgvbhsrlpgdmjqwftvncz"
        assert solve(puzzle_input) == 5

    def test_solver3(self):
        puzzle_input = "nppdvjthqldpwncqszvftbrmjlhg"
        assert solve(puzzle_input) == 6

    def test_solver4(self):
        puzzle_input = "nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg"
        assert solve(puzzle_input) == 10

    def test_solver5(self):
        puzzle_input = "zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw"
        assert solve(puzzle_input) == 11
