#!/usr/bin/env python
# -*- coding: utf-8 -*-


class Node:
    def __init__(self, parent=None, size=None):
        self._parent = parent
        self._size = size
        self._children = {}

    @property
    def parent(self):
        return self._parent

    @property
    def children(self):
        return self._children


class DirBase(Node):
    def add_subdir(self, name):
        if name not in self._children:
            self._children[name] = Dir(self)

        return self._children[name]

    def add_file(self, name, size):
        if name not in self._children:
            self._children[name] = File(self, size)

        return self._children[name]

    @property
    def size(self):
        if self._size is None:
            self._size = sum(child.size for child in self._children.values())

        return self._size


class Dir(DirBase):
    def __init__(self, parent):
        if not isinstance(parent, DirBase):
            raise TypeError("Parent must be subclass of type DirBase")

        super().__init__(parent)


class Root(DirBase):
    def __init__(self):
        super().__init__()


class File(Node):
    def __init__(self, parent, size):
        if not isinstance(parent, DirBase):
            raise TypeError("Parent must be subclass of type DirBase")

        if not isinstance(size, int):
            raise TypeError("Size must be of type int")

        super().__init__(parent, size)

    @property
    def size(self):
        return self._size


def parse_input(puzzle_input):
    root = Root()
    current = root

    for line in puzzle_input:
        if line.startswith("$ cd"):
            change_path = line.split()[-1]

            if change_path == "/":
                current = root
            elif change_path == "..":
                current = current.parent
            else:
                current = current.add_subdir(change_path)
        elif not line.startswith("$ ls"):
            data, name = line.split()

            if data.isnumeric():
                current.add_file(name, int(data))
            else:
                current.add_subdir(name)

    return root


def calculate_sizes(puzzle_input):
    stack = [("/", parse_input(puzzle_input))]

    while stack:
        name, node = stack.pop()

        if isinstance(node, DirBase):
            yield (name, node.size)

            stack.extend(
                (f"{name if not name == '/' else ''}/{child_name}", child_node)
                for child_name, child_node in node.children.items()
            )
