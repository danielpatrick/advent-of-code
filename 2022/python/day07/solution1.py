#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .disk import calculate_sizes


def solve(puzzle_input):
    return sum(
        size for _, size in calculate_sizes(puzzle_input) if size <= 100000
    )


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
