#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .disk import calculate_sizes


def solve(puzzle_input):
    disk_size = 70000000
    required_space = 30000000

    sizes = dict(calculate_sizes(puzzle_input))
    to_delete = sizes["/"] - (disk_size - required_space)

    return min(size for size in sizes.values() if size >= to_delete)


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
