#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest

from shared import get_input

from .. import disk
from ..disk import Dir, File, Root, calculate_sizes, parse_input


class TestDisk:
    @pytest.fixture
    def puzzle_input(self):
        yield get_input(disk.__file__, "test_input.txt")

    def test_parse_input(self, puzzle_input):
        root = parse_input(puzzle_input)
        assert isinstance(root, Root)
        assert list(root.children.keys()) == ["a", "b.txt", "c.dat", "d"]

        a = root.children["a"]
        assert isinstance(a, Dir)
        assert list(a.children.keys()) == ["e", "f", "g", "h.lst"]

        e = a.children["e"]
        assert isinstance(e, Dir)
        assert list(e.children.keys()) == ["i"]
        assert isinstance(e.children["i"], File)
        assert e.children["i"].size == 584

        assert isinstance(a.children["f"], File)
        assert a.children["f"].size == 29116
        assert isinstance(a.children["g"], File)
        assert a.children["g"].size == 2557
        assert isinstance(a.children["h.lst"], File)
        assert a.children["h.lst"].size == 62596

        assert isinstance(root.children["b.txt"], File)
        assert root.children["b.txt"].size == 14848514

        assert isinstance(root.children["c.dat"], File)
        assert root.children["c.dat"].size == 8504156

        d = root.children["d"]
        assert isinstance(d, Dir)
        assert list(d.children.keys()) == ["j", "d.log", "d.ext", "k"]
        assert isinstance(d.children["j"], File)
        assert d.children["j"].size == 4060174
        assert isinstance(d.children["d.log"], File)
        assert d.children["d.log"].size == 8033020
        assert isinstance(d.children["d.ext"], File)
        assert d.children["d.ext"].size == 5626152
        assert isinstance(d.children["k"], File)
        assert d.children["k"].size == 7214296

    def test_calculate_sizes(self, puzzle_input):
        expected = {
            "/": 48381165,
            "/a/e": 584,
            "/a": 94853,
            "/d": 24933642,
        }

        assert dict(calculate_sizes(puzzle_input)) == expected
