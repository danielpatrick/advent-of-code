#!/usr/bin/env python
# -*- coding: utf-8 -*-

DIRECTIONS = {
    "UP": (-1, 0),
    "DOWN": (1, 0),
    "LEFT": (0, -1),
    "RIGHT": (0, 1),
}


def is_visible_dir(grid, y, x, direction):
    height = int(grid[y][x])
    dy, dx = DIRECTIONS[direction]
    y += dy
    x += dx

    while 0 <= y < len(grid) and 0 <= x < len(grid[y]):
        if int(grid[y][x]) >= height:
            return False

        y += dy
        x += dx

    return True


def is_visible(grid, y, x):
    return any(is_visible_dir(grid, y, x, d) for d in DIRECTIONS)


def solve(puzzle_input):
    count = 0

    for y in range(len(puzzle_input)):
        for x in range(len(puzzle_input[y])):
            if is_visible(puzzle_input, y, x):
                count += 1

    return count


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
