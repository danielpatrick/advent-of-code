#!/usr/bin/env python
# -*- coding: utf-8 -*-

from math import prod

DIRECTIONS = {
    "UP": (-1, 0),
    "DOWN": (1, 0),
    "LEFT": (0, -1),
    "RIGHT": (0, 1),
}


def count_visible(grid, y, x, direction):
    count = 0

    height = int(grid[y][x])
    dy, dx = DIRECTIONS[direction]
    y += dy
    x += dx

    while 0 <= y < len(grid) and 0 <= x < len(grid[y]):
        count += 1

        if int(grid[y][x]) >= height:
            break

        y += dy
        x += dx

    return count


def scenic_score(grid, y, x):
    return prod(count_visible(grid, y, x, d) for d in DIRECTIONS)


def solve(puzzle_input):
    highest = 0

    for y in range(len(puzzle_input)):
        for x in range(len(puzzle_input[y])):
            highest = max(highest, scenic_score(puzzle_input, y, x))

    return highest


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
