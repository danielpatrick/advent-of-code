#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest

from shared import get_input

from .. import solution1
from ..solution1 import is_visible, is_visible_dir, solve


class TestSolution:
    @pytest.fixture
    def puzzle_input(self):
        yield get_input(solution1.__file__, "test_input.txt")

    def test_is_visible_dir(self, puzzle_input):
        assert is_visible_dir(puzzle_input, 1, 1, "LEFT") is True
        assert is_visible_dir(puzzle_input, 1, 1, "UP") is True
        assert is_visible_dir(puzzle_input, 1, 1, "DOWN") is False
        assert is_visible_dir(puzzle_input, 1, 1, "RIGHT") is False

        assert is_visible_dir(puzzle_input, 1, 2, "UP") is True
        assert is_visible_dir(puzzle_input, 1, 2, "RIGHT") is True
        assert is_visible_dir(puzzle_input, 1, 2, "DOWN") is False
        assert is_visible_dir(puzzle_input, 1, 2, "LEFT") is False

        assert is_visible_dir(puzzle_input, 1, 3, "UP") is False
        assert is_visible_dir(puzzle_input, 1, 3, "RIGHT") is False
        assert is_visible_dir(puzzle_input, 1, 3, "DOWN") is False
        assert is_visible_dir(puzzle_input, 1, 3, "LEFT") is False

        assert is_visible_dir(puzzle_input, 2, 1, "UP") is False
        assert is_visible_dir(puzzle_input, 2, 1, "RIGHT") is True
        assert is_visible_dir(puzzle_input, 2, 1, "DOWN") is False
        assert is_visible_dir(puzzle_input, 2, 1, "LEFT") is False

        assert is_visible_dir(puzzle_input, 2, 2, "UP") is False
        assert is_visible_dir(puzzle_input, 2, 2, "RIGHT") is False
        assert is_visible_dir(puzzle_input, 2, 2, "DOWN") is False
        assert is_visible_dir(puzzle_input, 2, 2, "LEFT") is False

        assert is_visible_dir(puzzle_input, 2, 3, "UP") is False
        assert is_visible_dir(puzzle_input, 2, 3, "RIGHT") is True
        assert is_visible_dir(puzzle_input, 2, 3, "DOWN") is False
        assert is_visible_dir(puzzle_input, 2, 3, "LEFT") is False

        assert is_visible_dir(puzzle_input, 3, 1, "UP") is False
        assert is_visible_dir(puzzle_input, 3, 1, "RIGHT") is False
        assert is_visible_dir(puzzle_input, 3, 1, "DOWN") is False
        assert is_visible_dir(puzzle_input, 3, 1, "LEFT") is False

        assert is_visible_dir(puzzle_input, 3, 2, "UP") is False
        assert is_visible_dir(puzzle_input, 3, 2, "RIGHT") is False
        assert is_visible_dir(puzzle_input, 3, 2, "DOWN") is True
        assert is_visible_dir(puzzle_input, 3, 2, "LEFT") is True

        assert is_visible_dir(puzzle_input, 3, 3, "UP") is False
        assert is_visible_dir(puzzle_input, 3, 3, "RIGHT") is False
        assert is_visible_dir(puzzle_input, 3, 3, "DOWN") is False
        assert is_visible_dir(puzzle_input, 3, 3, "LEFT") is False

    def test_is_visible(self, puzzle_input):
        assert is_visible(puzzle_input, 1, 1) is True
        assert is_visible(puzzle_input, 1, 2) is True
        assert is_visible(puzzle_input, 1, 3) is False
        assert is_visible(puzzle_input, 2, 1) is True
        assert is_visible(puzzle_input, 2, 2) is False
        assert is_visible(puzzle_input, 2, 3) is True
        assert is_visible(puzzle_input, 3, 1) is False
        assert is_visible(puzzle_input, 3, 2) is True
        assert is_visible(puzzle_input, 3, 3) is False

    def test_solver(self, puzzle_input):
        assert solve(puzzle_input) == 21
