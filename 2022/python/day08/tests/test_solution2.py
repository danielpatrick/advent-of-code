#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest

from shared import get_input

from .. import solution2
from ..solution2 import count_visible, scenic_score, solve


class TestSolution:
    @pytest.fixture
    def puzzle_input(self):
        yield get_input(solution2.__file__, "test_input.txt")

    def test_count_visible(self, puzzle_input):
        assert count_visible(puzzle_input, 1, 2, "UP") == 1
        assert count_visible(puzzle_input, 1, 2, "LEFT") == 1
        assert count_visible(puzzle_input, 1, 2, "RIGHT") == 2
        assert count_visible(puzzle_input, 1, 2, "DOWN") == 2

        assert count_visible(puzzle_input, 3, 2, "UP") == 2
        assert count_visible(puzzle_input, 3, 2, "LEFT") == 2
        assert count_visible(puzzle_input, 3, 2, "DOWN") == 1
        assert count_visible(puzzle_input, 3, 2, "RIGHT") == 2

    def test_scenic_score(self, puzzle_input):
        assert scenic_score(puzzle_input, 1, 2) == 4
        assert scenic_score(puzzle_input, 3, 2) == 8

    def test_solver(self, puzzle_input):
        assert solve(puzzle_input) == 8
