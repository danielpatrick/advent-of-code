#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .rope_bridge import count_tail_moves


def solve(puzzle_input):
    return count_tail_moves(puzzle_input, 2)


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
