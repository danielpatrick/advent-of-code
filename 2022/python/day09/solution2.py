#!/usr/bin/env python
# -*- coding: utf-8 -*-

from itertools import pairwise

MOVE = {
    "U": (0, 1),
    "R": (1, 0),
    "D": (0, -1),
    "L": (-1, 0),
}


def parse_move(s):
    d, n = s.split()

    return (MOVE[d], int(n))


def is_touching(tail, head):
    return abs(tail[0] - head[0]) <= 1 and abs(tail[1] - head[1]) <= 1


def move_tail(tail, head):
    tx, ty = tail
    hx, hy = head

    if abs(hx - tx) >= 2:
        tx += (hx - tx) // abs(hx - tx)

        if abs(hy - ty) >= 1:
            ty += (hy - ty) // abs(hy - ty)

    elif abs(hy - ty) >= 2:
        ty += (hy - ty) // abs(hy - ty)

        if abs(hx - tx) >= 1:
            tx += (hx - tx) // abs(hx - tx)

    return (tx, ty)


def count_tail_moves(puzzle_input, length):
    visited = set((0, 0))
    knots = [[0, 0] for _ in range(length)]

    for line in puzzle_input:
        (dx, dy), n = parse_move(line)

        for _ in range(n):
            for i, (head, tail) in enumerate(pairwise(knots)):
                if i == 0:
                    head[0] += dx
                    head[1] += dy

                if not is_touching(tail, head):
                    x, y = move_tail(tail, head)
                    tail[0] = x
                    tail[1] = y

                    if i == length - 2:
                        visited.add(tuple(tail))

    return len(visited)


def solve(puzzle_input):
    return count_tail_moves(puzzle_input, 10)


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
