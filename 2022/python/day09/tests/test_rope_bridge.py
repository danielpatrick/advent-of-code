#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest

from shared import get_input

from .. import rope_bridge
from ..rope_bridge import count_tail_moves, is_touching, move_tail, parse_move


class TestRopeBridge:
    @pytest.fixture
    def puzzle_input(self):
        yield get_input(rope_bridge.__file__, "test_input.txt")

    @pytest.fixture
    def puzzle_input2(self):
        yield get_input(rope_bridge.__file__, "test_input2.txt")

    def test_parse_move(self):
        assert parse_move("R 4") == ((1, 0), 4)
        assert parse_move("U 4") == ((0, 1), 4)
        assert parse_move("L 3") == ((-1, 0), 3)
        assert parse_move("D 1") == ((0, -1), 1)

    def test_is_touching(self):
        assert is_touching((0, 0), (0, 0)) is True
        assert is_touching((0, 0), (1, 0)) is True
        assert is_touching((0, 0), (0, 1)) is True
        assert is_touching((0, 0), (1, 1)) is True
        assert is_touching((0, 0), (2, 0)) is False
        assert is_touching((0, 0), (0, 2)) is False

    def test_move_tail(self):
        assert move_tail((0, 0), (0, 0)) == (0, 0)
        assert move_tail((0, 0), (0, 4)) == (0, 1)
        assert move_tail((0, 0), (0, 4)) == (0, 1)
        assert move_tail((0, 0), (4, 4)) == (1, 1)

    def test_count_tail_moves(self, puzzle_input, puzzle_input2):
        assert count_tail_moves(puzzle_input, 2) == 13
        assert count_tail_moves(puzzle_input2, 10) == 36
