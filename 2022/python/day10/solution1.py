#!/usr/bin/env python
# -*- coding: utf-8 -*-


def total_increment(i, x):
    if (i - 20) % 40 == 0:
        return i * x

    return 0


def parse_addx(line):
    _, n = line.split()

    return int(n)


def solve(puzzle_input):
    i = 1
    x = 1
    total = 0

    for line in puzzle_input:
        if line == "noop":
            total += total_increment(i, x)

            i += 1
        else:
            total += total_increment(i, x)
            total += total_increment(i + 1, x)

            x += parse_addx(line)

            i += 2

    return total


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
