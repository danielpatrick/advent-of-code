#!/usr/bin/env python
# -*- coding: utf-8 -*-


def draw(sprite, crt):
    if len(crt) == 0 or len(crt[-1]) == 40:
        crt.append([])

    if len(crt[-1]) in (sprite - 1, sprite, sprite + 1):
        crt[-1].append("#")
    else:
        crt[-1].append(".")


def parse_addx(line):
    _, n = line.split()

    return int(n)


def format_crt(crt):
    return "".join("".join(line) + "\n" for line in crt)


def solve(puzzle_input):
    sprite = 1
    i = 1
    crt = []

    for line in puzzle_input:
        if line == "noop":
            draw(sprite, crt)

            i += 1
        else:
            draw(sprite, crt)
            draw(sprite, crt)

            sprite += parse_addx(line)

            i += 2

    return format_crt(crt)


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
