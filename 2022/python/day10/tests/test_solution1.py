#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest

from shared import get_input

from .. import solution1
from ..solution1 import parse_addx, solve, total_increment


class TestSolution:
    @pytest.fixture
    def puzzle_input(self):
        yield get_input(solution1.__file__, "test_input.txt")

    def test_parse_addx(self):
        assert parse_addx("addx -10") == -10
        assert parse_addx("addx -1") == -1
        assert parse_addx("addx 0") == 0
        assert parse_addx("addx 1") == 1
        assert parse_addx("addx 10") == 10

    def test_total_increment(self):
        assert total_increment(0, 10) == 0
        assert total_increment(19, 10) == 0
        assert total_increment(20, 10) == 200
        assert total_increment(40, 10) == 0
        assert total_increment(60, 10) == 600
        assert total_increment(80, 10) == 0

    def test_solver(self, puzzle_input):
        assert solve(puzzle_input) == 13140
