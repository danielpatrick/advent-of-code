#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest

from shared import get_input

from .. import solution2
from ..solution2 import draw, format_crt, parse_addx, solve


class TestSolution:
    @pytest.fixture
    def puzzle_input(self):
        yield get_input(solution2.__file__, "test_input.txt")

    def test_draw(self):
        crt = []

        draw(-2, crt)
        assert crt == [["."]]

        draw(0, crt)
        assert crt == [[".", "#"]]

        draw(2, crt)
        assert crt == [[".", "#", "#"]]

        draw(4, crt)
        assert crt == [[".", "#", "#", "#"]]

        draw(6, crt)
        assert crt == [[".", "#", "#", "#", "."]]

    def test_draw_new_row(self):
        crt = [["#" for _ in range(40)]]
        expected = [["#" for _ in range(40)], ["#"]]

        draw(0, crt)

        assert crt == expected

    def test_parse_addx(self):
        assert parse_addx("addx -10") == -10
        assert parse_addx("addx -1") == -1
        assert parse_addx("addx 0") == 0
        assert parse_addx("addx 1") == 1
        assert parse_addx("addx 10") == 10

    def test_format_crt(self):
        crt = [
            ["#", ".", ".", ".", "#", ".", "#", "#", "#", "#", "#"],
            ["#", ".", ".", ".", "#", ".", ".", ".", "#", ".", "."],
            ["#", "#", "#", "#", "#", ".", ".", ".", "#", ".", "."],
            ["#", ".", ".", ".", "#", ".", ".", ".", "#", ".", "."],
            ["#", ".", ".", ".", "#", ".", "#", "#", "#", "#", "#"],
        ]
        expected = (
            "#...#.#####\n"
            "#...#...#..\n"
            "#####...#..\n"
            "#...#...#..\n"
            "#...#.#####\n"
        )

        assert format_crt(crt) == expected

    def test_solver(self, puzzle_input):
        expected = (
            "##..##..##..##..##..##..##..##..##..##..\n"
            "###...###...###...###...###...###...###.\n"
            "####....####....####....####....####....\n"
            "#####.....#####.....#####.....#####.....\n"
            "######......######......######......####\n"
            "#######.......#######.......#######.....\n"
        )

        assert solve(puzzle_input) == expected
