RED := '\033[0;31m'
RESET := '\033[0m'

@_default:
  just --list

_echo_err +msg:
  #!/usr/bin/env bash
  echo -e >&2 "{{ RED }}ERROR{{ RESET }}: {{ msg }}"

_get_day d:
  #!/usr/bin/env bash
  [[ ! "{{ d }}" =~ ^(0?[1-9]|1[0-9]|2[0-5])$ ]] && just _echo_err "Invalid day: {{ d }}" && exit
  DAY=$([[ {{ d }} =~ ^[1-9]$ ]] && echo "0{{ d }}" || echo "{{ d }}")
  [[ ! -d "day${DAY}" ]] && just _echo_err "Dir does not exist: day${DAY}/" && exit

  echo "$DAY"

# Run (unoptimised) specified solution for given day
run d s="all":
  #!/usr/bin/env bash
  DAY="$(just _get_day {{ d }})" && [[ -z "$DAY" ]] && exit || cd "day$DAY"

  if [[ "{{ s }}" = "all" ]]; then
    echo "Solution for day $DAY..."
    cargo run
  else
    [[ ! "{{ s }}" =~ ^[1-2]$ ]] && just _echo_err "Invalid solution number: {{ s }}" && exit
    echo "Solution {{ s }} for day $DAY..."
    cargo run -- {{ s }}
  fi

alias day := run

# Build and run release version of specified solution for given day
run-release d s="all":
  #!/usr/bin/env bash
  DAY="$(just _get_day {{ d }})" && [[ -z "$DAY" ]] && exit || cd "day$DAY"

  if [[ "{{ s }}" = "all" ]]; then
    echo "Solution for day $DAY..."
    cargo run --release
  else
    [[ ! "{{ s }}" =~ ^[1-2]$ ]] && echo "Invalid solution number: {{ s }}" && exit
    echo "Solution {{ s }} for day $DAY..."
    cargo run --release -- {{ s }}
  fi

alias rrun := run-release

# Execute tests for given day
test d s="all":
  #!/usr/bin/env bash
  DAY="$(just _get_day {{ d }})" && [[ -z "$DAY" ]] && exit || cd "day$DAY"

  if [[ "{{ s }}" = "all" ]]; then
    echo "Running tests for day $DAY..."
    cargo test
  else
    [[ ! "{{ s }}" =~ ^[1-2]$ ]] && echo "Invalid solution number: {{ s }}" && exit
    echo "Running tests for solution {{ s }} of day $DAY..."
    cargo test solution{{ s }}::
  fi

# Format using rust fmt
format d:
  #!/usr/bin/env bash
  DAY="$(just _get_day {{ d }})" && [[ -z "$DAY" ]] && exit || cd "day$DAY"

  cargo fmt

# Static checks using clippy
clippy d:
  #!/usr/bin/env bash
  DAY="$(just _get_day {{ d }})" && [[ -z "$DAY" ]] && exit || cd "day$DAY"

  cargo clippy -- -D warnings
