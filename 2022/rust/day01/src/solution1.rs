pub fn solve(input: &str) -> i32 {
    input
        .split("\n\n")
        .map(|s| s.lines().map(|s| s.parse::<i32>().unwrap()).sum())
        .max()
        .unwrap()
}

#[cfg(test)]
mod tests {
    use super::solve;

    fn get_test_input() -> &'static str {
        include_str!("../../../inputs/day01/test_input.txt")
    }

    #[test]
    fn test_solve() {
        assert_eq!(solve(get_test_input()), 24000);
    }
}
