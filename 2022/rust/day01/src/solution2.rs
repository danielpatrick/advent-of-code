pub fn solve(input: &str) -> i32 {
    let mut calories = input
        .split("\n\n")
        .map(|s| s.lines().map(|s| s.parse::<i32>().unwrap()).sum())
        .collect::<Vec<_>>();

    calories.sort();

    calories.iter().rev().take(3).sum::<i32>()
}

#[cfg(test)]
mod tests {
    use super::solve;

    fn get_test_input() -> &'static str {
        include_str!("../../../inputs/day01/test_input.txt")
    }

    #[test]
    fn test_solve() {
        assert_eq!(solve(get_test_input()), 45000);
    }
}
