pub fn solve(input: &str) -> i32 {
    input
        .lines()
        .map(|l| {
            let mut chars = l.chars();
            let a = match chars.next().unwrap() {
                'A' => 0,
                'B' => 1,
                'C' => 2,
                _ => panic!("First char is not A, B or C"),
            };
            let b = match chars.last().unwrap() {
                'X' => 0,
                'Y' => 1,
                'Z' => 2,
                _ => panic!("Second char is not X, Y or Z"),
            };

            b * 3
                + 1
                + match b {
                    0 => (a - 1) % 3,
                    1 => a,
                    2 => (a + 1) % 3,
                    _ => panic!("Unexpected value for b"),
                }
        })
        .sum()
}

#[cfg(test)]
mod tests {
    use super::solve;

    fn get_test_input() -> &'static str {
        include_str!("../../../inputs/day02/test_input.txt")
    }

    #[test]
    fn test_solve() {
        assert_eq!(solve(get_test_input()), 12);
    }
}
