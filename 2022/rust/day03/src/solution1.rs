use std::collections::BTreeSet;

pub fn solve(input: &str) -> i32 {
    input
        .lines()
        .map(|s| {
            let n = s.len() / 2;

            let a = s.chars().take(n).collect::<BTreeSet<_>>();
            let b = s.chars().skip(n).collect::<BTreeSet<_>>();

            let ch = *a.intersection(&b).next().unwrap();

            if ('a'..='z').contains(&ch) {
                ch as i32 - 'a' as i32 + 1
            } else {
                ch as i32 - 'A' as i32 + 1 + 26
            }
        })
        .sum()
}

#[cfg(test)]
mod tests {
    use super::solve;

    fn get_test_input() -> &'static str {
        include_str!("../../../inputs/day03/test_input.txt")
    }

    #[test]
    fn test_solve() {
        assert_eq!(solve(get_test_input()), 157);
    }
}
