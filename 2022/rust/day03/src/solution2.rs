use itertools::Itertools;
use std::collections::BTreeSet;

pub fn solve(input: &str) -> i32 {
    input
        .lines()
        .chunks(3)
        .into_iter()
        .map(|mut lines| {
            let a = lines.next().unwrap().chars().collect::<BTreeSet<_>>();
            let b = lines.next().unwrap().chars().collect::<BTreeSet<_>>();
            let c = lines.next().unwrap().chars().collect::<BTreeSet<_>>();

            let ch = *(&a & &b).intersection(&c).next().unwrap();

            if ('a'..='z').contains(&ch) {
                ch as i32 - 'a' as i32 + 1
            } else {
                ch as i32 - 'A' as i32 + 1 + 26
            }
        })
        .sum()
}

#[cfg(test)]
mod tests {
    use super::solve;

    fn get_test_input() -> &'static str {
        include_str!("../../../inputs/day03/test_input.txt")
    }

    #[test]
    fn test_solve() {
        assert_eq!(solve(get_test_input()), 70);
    }
}
