fn parse_range(s: &str) -> (i32, i32) {
    let (a, b) = s.split_once('-').unwrap();

    (a.parse::<i32>().unwrap(), b.parse::<i32>().unwrap())
}

pub fn map_input(s: &str) -> impl Iterator<Item = ((i32, i32), (i32, i32))> + '_ {
    s.lines().map(|line| {
        let pair = line.split_once(',').unwrap();

        (parse_range(pair.0), parse_range(pair.1))
    })
}

#[cfg(test)]
mod tests {
    use super::map_input;

    fn get_test_input() -> &'static str {
        include_str!("../../../inputs/day04/test_input.txt")
    }

    #[test]
    fn test_map_input() {
        let expected = vec![
            ((2, 4), (6, 8)),
            ((2, 3), (4, 5)),
            ((5, 7), (7, 9)),
            ((2, 8), (3, 7)),
            ((6, 6), (4, 6)),
            ((2, 6), (4, 8)),
        ];

        assert_eq!(map_input(get_test_input()).collect::<Vec<_>>(), expected);
    }
}
