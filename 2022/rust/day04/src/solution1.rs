use crate::parser::map_input;

pub fn solve(input: &str) -> i32 {
    map_input(input)
        .filter(|((a, b), (c, d))| (c <= a && d >= b) || (a <= c && b >= d))
        .count() as i32
}

#[cfg(test)]
mod tests {
    use super::solve;

    fn get_test_input() -> &'static str {
        include_str!("../../../inputs/day04/test_input.txt")
    }

    #[test]
    fn test_solve() {
        assert_eq!(solve(get_test_input()), 2);
    }
}
