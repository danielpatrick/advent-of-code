use itertools::Itertools;

type Stack = Vec<char>;
type Instruction = (usize, usize, usize);

pub fn parse_input(input: &str) -> (Vec<Stack>, Vec<Instruction>) {
    let (stacks_text, instructions_text) = input.split_once("\n\n").unwrap();

    let mut stacks = Vec::new();

    for line in stacks_text.lines() {
        for (i, mut s) in (&line.chars().chunks(4)).into_iter().enumerate() {
            let c: char = s.nth(1).unwrap();
            if stacks.len() <= i {
                stacks.push(Vec::new());
            }

            if c.is_ascii_uppercase() {
                stacks[i].push(c)
            }
        }
    }

    stacks = stacks
        .iter()
        .map(|stack| stack.iter().rev().cloned().collect())
        .collect();

    let instructions = instructions_text
        .lines()
        .map(|line| {
            let mut words = line.split_whitespace();
            let n = words.nth(1).unwrap().parse::<usize>().unwrap();
            let from = words.nth(1).unwrap().parse::<usize>().unwrap() - 1;
            let to = words.nth(1).unwrap().parse::<usize>().unwrap() - 1;

            (n, from, to)
        })
        .collect();

    (stacks, instructions)
}

#[cfg(test)]
mod tests {
    use super::parse_input;

    fn get_test_input() -> &'static str {
        include_str!("../../../inputs/day05/test_input.txt")
    }

    #[test]
    fn test_parse_input() {
        let expected = (
            vec![vec!['Z', 'N'], vec!['M', 'C', 'D'], vec!['P']],
            vec![(1, 1, 0), (3, 0, 2), (2, 1, 0), (1, 0, 1)],
        );

        assert_eq!(parse_input(get_test_input()), expected);
    }
}
