use crate::parser::parse_input;

pub fn solve(input: &str) -> String {
    let (mut stacks, instructions) = parse_input(input);

    for (n, start, end) in instructions {
        let i = stacks[start].len() - n;
        let items = stacks[start].split_off(i);
        stacks[end].extend(items);
    }

    stacks.iter().filter_map(|stack| stack.last()).collect()
}

#[cfg(test)]
mod tests {
    use super::solve;

    fn get_test_input() -> &'static str {
        include_str!("../../../inputs/day05/test_input.txt")
    }

    #[test]
    fn test_solve() {
        assert_eq!(solve(get_test_input()), "MCD");
    }
}
