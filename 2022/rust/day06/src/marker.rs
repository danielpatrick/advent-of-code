use std::collections::HashSet;

pub fn find_marker(buffer: &str, n: usize) -> usize {
    let v = buffer.chars().collect::<Vec<char>>();

    for (i, window) in v.windows(n).enumerate() {
        if window.iter().cloned().collect::<HashSet<char>>().len() == n {
            return i + n;
        }
    }

    panic!("No marker found");
}

#[cfg(test)]
mod tests {
    use super::find_marker;

    fn get_test_input() -> &'static str {
        include_str!("../../../inputs/day06/test_input.txt")
    }

    #[test]
    fn test_find_marker() {
        assert_eq!(find_marker(get_test_input(), 4), 7);
        assert_eq!(find_marker(get_test_input(), 14), 19);
    }
}
