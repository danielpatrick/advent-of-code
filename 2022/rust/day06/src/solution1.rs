use crate::marker::find_marker;

pub fn solve(input: &str) -> usize {
    find_marker(input, 4)
}

#[cfg(test)]
mod tests {
    use super::solve;

    fn get_test_input() -> &'static str {
        include_str!("../../../inputs/day06/test_input.txt")
    }

    #[test]
    fn test_solve() {
        assert_eq!(solve(get_test_input()), 7);
        assert_eq!(solve("bvwbjplbgvbhsrlpgdmjqwftvncz"), 5);
        assert_eq!(solve("nppdvjthqldpwncqszvftbrmjlhg"), 6);
        assert_eq!(solve("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg"), 10);
        assert_eq!(solve("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw"), 11);
    }
}
