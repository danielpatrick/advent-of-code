pub fn solve(input: &str) -> String {
    input.trim().to_string() + "!"
}

#[cfg(test)]
mod tests {
    use super::solve;

    fn get_test_input() -> &'static str {
        include_str!("../../../inputs/dayDAY/test_input.txt")
    }

    #[test]
    fn test_solve() {
        assert_eq!(solve(get_test_input()), "lorem ipsum!");
    }
}
