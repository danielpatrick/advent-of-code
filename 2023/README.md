# Instructions

Requires
- [Just](https://github.com/casey/just)


## Create daily solution files

To start working on day 1, you can run:

```shell
just create 1
```

This will result in the following files and directories:

```
2021
├── inputs/
│  └── day01/
│     ├── input.txt
│     └── test_input.txt
├── python/
│  ├── day01/
│  │  ├── __init__.py
│  │  ├── solution1.py
│  │  ├── solution2.py
│  │  ├── test_solution1.py
│  │  └── test_solution2.py
│  ├── shared/
│  ├── .flake8
│  ├── .justfile
│  ├── README.md
│  ├── poetry.lock
│  ├── pyproject.toml
│  └── pytest.ini
├── rust/
│  ├── day01/
│  │  ├── src/
│  │  │  ├── main.rs
│  │  │  ├── solution1.rs
│  │  │  └── solution2.rs
│  │  ├── Cargo.lock
│  │  └── Cargo.toml
│  ├── .justfile
│  └── README.md
├── template/
├── .justfile
└── README.md
```

See `python/README.md` and `rust/README.md` for further info.
