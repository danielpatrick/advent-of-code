#!/usr/bin/env python
# -*- coding: utf-8 -*-


def first_digit(chars):
    try:
        return next(int(c) for c in chars if c.isdigit())
    except Exception:
        return None


def get_number(line):
    return first_digit(line) * 10 + first_digit(reversed(line))


def solve(puzzle_input):
    return sum(get_number(line) for line in puzzle_input)


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
