#!/usr/bin/env python
# -*- coding: utf-8 -*-

from enum import Enum

DIGITS = {
    "one": 1,
    "two": 2,
    "three": 3,
    "four": 4,
    "five": 5,
    "six": 6,
    "seven": 7,
    "eight": 8,
    "nine": 9,
}

DIGITS_REVERSED = {k[::-1]: v for k, v in DIGITS.items()}


class Direction(Enum):
    FORWARD = 1
    BACKWARD = -1


def get_digit(line, direction=Direction.FORWARD):
    digits_map = DIGITS if direction == Direction.FORWARD else DIGITS_REVERSED
    string = line[:: direction.value]

    for i, c in enumerate(string):
        if c.isdigit():
            return int(c)

        try:
            return next(
                digits_map[word]
                for word in digits_map
                if string[i : i + len(word)] == word
            )
        except Exception:  # nosec
            pass


def get_number(line):
    return get_digit(line) * 10 + get_digit(line, Direction.BACKWARD)


def solve(puzzle_input):
    return sum(get_number(line) for line in puzzle_input)


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
