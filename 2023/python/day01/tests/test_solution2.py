#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest

from shared import get_input

from .. import solution2
from ..solution2 import Direction, get_digit, solve


class TestSolution:
    @pytest.fixture
    def puzzle_input(self):
        yield get_input(solution2.__file__, "test_input_two.txt")

    def test_get_digit(self):
        assert get_digit("two1nine") == 2
        assert get_digit("eightwothree") == 8
        assert get_digit("abcone2threexyz") == 1
        assert get_digit("xtwone3four") == 2
        assert get_digit("4nineeightseven2") == 4
        assert get_digit("zoneight234") == 1
        assert get_digit("7pqrstsixteen") == 7
        assert get_digit("two1nine", Direction.BACKWARD) == 9
        assert get_digit("eightwothree", Direction.BACKWARD) == 3
        assert get_digit("abcone2threexyz", Direction.BACKWARD) == 3
        assert get_digit("xtwone3four", Direction.BACKWARD) == 4
        assert get_digit("4nineeightseven2", Direction.BACKWARD) == 2
        assert get_digit("zoneight234", Direction.BACKWARD) == 4
        assert get_digit("7pqrstsixteen", Direction.BACKWARD) == 6

    def test_solver(self, puzzle_input):
        assert solve(puzzle_input) == 281
