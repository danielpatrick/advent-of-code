#!/usr/bin/env python
# -*- coding: utf-8 -*-

AVAILABLE_CUBES = {
    "red": 12,
    "green": 13,
    "blue": 14,
}


def parse_cube(cube_str):
    a, b = cube_str.split(" ", 1)

    return (b, int(a))


def parse_cubes(cube_set_str):
    return dict(parse_cube(cube_str) for cube_str in cube_set_str.split(", "))


def game_number_if_possible(line):
    a, b = line.split(": ", 1)

    sets = [parse_cubes(cubes_str) for cubes_str in b.split("; ")]

    if all(
        all(AVAILABLE_CUBES.get(k, 0) >= v for k, v in cubes.items())
        for cubes in sets
    ):
        return int(a.split()[1])

    return 0


def solve(puzzle_input):
    return sum(game_number_if_possible(line) for line in puzzle_input)


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
