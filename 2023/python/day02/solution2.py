#!/usr/bin/env python
# -*- coding: utf-8 -*-

from math import prod as product


def game_power(line):
    required = {}

    a, b = line.split(": ", 1)

    for cubes_str in b.split("; "):
        for cube_str in cubes_str.split(", "):
            a, b = cube_str.split(" ", 1)

            required[b] = max(required.get(b, 0), int(a))

    return product(required.values())


def solve(puzzle_input):
    return sum(game_power(line) for line in puzzle_input)


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
