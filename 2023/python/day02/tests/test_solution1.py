#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest

from shared import get_input

from .. import solution1
from ..solution1 import game_number_if_possible, parse_cubes, solve


class TestSolution:
    @pytest.fixture
    def puzzle_input(self):
        yield get_input(solution1.__file__, "test_input.txt")

    def test_parse_cubes(self, puzzle_input):
        assert parse_cubes("3 blue, 4 red") == {"blue": 3, "red": 4}
        assert parse_cubes("1 red, 2 green, 6 blue") == {
            "red": 1,
            "green": 2,
            "blue": 6,
        }
        assert parse_cubes("2 green") == {"green": 2}

    def test_game_number_if_possible(self, puzzle_input):
        assert game_number_if_possible(puzzle_input[0]) == 1
        assert game_number_if_possible(puzzle_input[1]) == 2
        assert game_number_if_possible(puzzle_input[2]) == 0
        assert game_number_if_possible(puzzle_input[3]) == 0
        assert game_number_if_possible(puzzle_input[4]) == 5

    def test_solver(self, puzzle_input):
        assert solve(puzzle_input) == 8
