#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest

from shared import get_input

from .. import solution2
from ..solution2 import game_power, solve


class TestSolution:
    @pytest.fixture
    def puzzle_input(self):
        yield get_input(solution2.__file__, "test_input.txt")

    def test_game_power(self, puzzle_input):
        assert game_power(puzzle_input[0]) == 48
        assert game_power(puzzle_input[1]) == 12
        assert game_power(puzzle_input[2]) == 1560
        assert game_power(puzzle_input[3]) == 630
        assert game_power(puzzle_input[4]) == 36

    def test_solver(self, puzzle_input):
        assert solve(puzzle_input) == 2286
