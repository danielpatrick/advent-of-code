#!/usr/bin/env python
# -*- coding: utf-8 -*-


class Schematic:
    def __init__(self, s):
        self.height = len(s)
        self.width = len(s[0])
        self.grid = "".join(s)

    def get(self, x, y):
        return self.grid[x + y * self.width]

    def is_digit(self, x, y):
        return self.get(x, y).isdigit()

    def is_symbol(self, x, y):
        return not self.is_digit(x, y) and self.get(x, y) != "."

    def neighbours(self, row, start_x, end_x):
        if start_x > 0:
            yield (start_x - 1, row)

        if end_x + 1 < self.width:
            yield (end_x + 1, row)

        if row > 1:
            for x in range(max(start_x - 1, 0), min(end_x + 2, self.width)):
                yield (x, row - 1)

        if row + 1 < self.height:
            for x in range(max(start_x - 1, 0), min(end_x + 2, self.width)):
                yield (x, row + 1)

    def get_gears(self, row, start_x, end_x):
        return [
            (x, y)
            for (x, y) in self.neighbours(row, start_x, end_x)
            if self.is_symbol(x, y)
        ]

    def is_part_number(self, row, start_x, end_x):
        return any(
            self.is_symbol(x, y)
            for (x, y) in self.neighbours(row, start_x, end_x)
        )

    def get_part_number(self, row, start_x, end_x):
        return int(
            self.grid[
                row * self.width + start_x : row * self.width + end_x + 1
            ]
        )
