#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .schematic import Schematic


def solve(puzzle_input):
    schematic = Schematic(puzzle_input)

    part_number_sum = 0

    for y in range(schematic.height):
        start = None
        end = None

        for x in range(schematic.width + 1):
            if x != schematic.width and schematic.is_digit(x, y):
                if start is None:
                    start = x

                end = x
            else:
                if start is not None and schematic.is_part_number(
                    y, start, end
                ):
                    part_number_sum += schematic.get_part_number(y, start, end)

                start = None
                end = None

    return part_number_sum


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
