#!/usr/bin/env python
# -*- coding: utf-8 -*-

from math import prod as product

from .schematic import Schematic


def solve(puzzle_input):
    schematic = Schematic(puzzle_input)

    gears = {}

    for y in range(schematic.height):
        start = None
        end = None

        for x in range(schematic.width + 1):
            if x != schematic.width and schematic.is_digit(x, y):
                if start is None:
                    start = x

                end = x
            else:
                if start is not None:
                    for gear in schematic.get_gears(y, start, end):
                        if gear not in gears:
                            gears[gear] = []

                        gears[gear].append((y, start, end))

                start = None
                end = None

    return sum(
        product(schematic.get_part_number(*args) for args in parts)
        for parts in gears.values()
        if len(parts) >= 2
    )


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
