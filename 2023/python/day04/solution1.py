#!/usr/bin/env python
# -*- coding: utf-8 -*-


def get_points(line):
    winning, own = line.split(": ")[1].split(" | ")

    matches = len(set(winning.split()) & set(own.split()))

    if matches >= 1:
        return 2 ** (matches - 1)

    return 0


def solve(puzzle_input):
    return sum(get_points(line) for line in puzzle_input)


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
