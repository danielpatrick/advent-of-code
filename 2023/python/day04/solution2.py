#!/usr/bin/env python
# -*- coding: utf-8 -*-


def get_cards_won(line):
    card, numbers = line.split(": ")
    winning, own = numbers.split(" | ")

    matches = len(set(winning.split()) & set(own.split()))
    card_num = int(card.split()[1])

    return [card_num + n for n in range(matches)]


def solve(puzzle_input):
    cards_won = [get_cards_won(line) for line in puzzle_input]
    card_counts = [1 for _ in puzzle_input]

    for i in range(len(cards_won)):
        for card in cards_won[i]:
            card_counts[card] += card_counts[i]

    return sum(card_counts)


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
