#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest

from shared import get_input

from .. import solution1
from ..solution1 import get_points, solve


class TestSolution:
    @pytest.fixture
    def puzzle_input(self):
        yield get_input(solution1.__file__, "test_input.txt")

    def test_get_points(self, puzzle_input):
        assert get_points(puzzle_input[0]) == 8
        assert get_points(puzzle_input[1]) == 2
        assert get_points(puzzle_input[2]) == 2
        assert get_points(puzzle_input[3]) == 1
        assert get_points(puzzle_input[4]) == 0
        assert get_points(puzzle_input[5]) == 0

    def test_solver(self, puzzle_input):
        assert solve(puzzle_input) == 13
