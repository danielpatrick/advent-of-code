#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest

from shared import get_input

from .. import solution2
from ..solution2 import get_cards_won, solve


class TestSolution:
    @pytest.fixture
    def puzzle_input(self):
        yield get_input(solution2.__file__, "test_input.txt")

    def test_get_cards_won(self, puzzle_input):
        assert get_cards_won(puzzle_input[0]) == [1, 2, 3, 4]
        assert get_cards_won(puzzle_input[1]) == [2, 3]
        assert get_cards_won(puzzle_input[2]) == [3, 4]
        assert get_cards_won(puzzle_input[3]) == [4]
        assert get_cards_won(puzzle_input[4]) == []
        assert get_cards_won(puzzle_input[5]) == []

    def test_solver(self, puzzle_input):
        assert solve(puzzle_input) == 30
