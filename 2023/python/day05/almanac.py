#!/usr/bin/env python
# -*- coding: utf-8 -*-


class Almanac:
    def __init__(self, lines):
        self.maps = []
        current = []

        for i in range(len(lines) + 1):
            if i != len(lines) and lines[i] != "" and lines[i][0].isdigit():
                current.append(lines[i])
            elif len(current) > 0:
                self.maps.append(
                    [self.parse_mapping_line(line) for line in current]
                )
                current = []

    def parse_mapping_line(self, line):
        a, b, c = line.split()
        b = int(b)
        return (b, b + int(c), int(a) - b)

    def single_map(self, start, end, source_start, source_end, offset):
        mapped = []
        to_map = []

        if start >= source_start and end <= source_end:
            # inside range
            mapped.append((start + offset, end + offset))
        else:
            if start < source_start <= end:
                # some before start
                to_map.append((start, source_start))
            if start < source_end <= end:
                # some after end
                to_map.append((source_end, end))
            if start <= source_end and end >= source_start:
                # some overlap exists
                mapped.append(
                    (
                        max(start, source_start) + offset,
                        min(end, source_end) + offset,
                    )
                )
            else:
                to_map.append((start, end))

        return (mapped, to_map)

    def get_map_destinations(self, inputs, mapping):
        destinations = []
        not_mapped = [s for s in inputs]

        for source_start, source_end, offset in mapping:
            new_not_mapped = []

            for start, end in not_mapped:
                mapped, to_map = self.single_map(
                    start, end, source_start, source_end, offset
                )

                destinations.extend(mapped)
                new_not_mapped.extend(to_map)

            not_mapped = new_not_mapped

        destinations.extend(not_mapped)

        return destinations

    def get_seed_locations(self, seeds):
        destinations = [s for s in seeds]

        for m in self.maps:
            destinations = self.get_map_destinations(destinations, m)

        return destinations
