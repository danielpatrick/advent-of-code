#!/usr/bin/env python
# -*- coding: utf-8 -*-


from .almanac import Almanac


def get_seeds(puzzle_input):
    seed_line = [
        int(s) for s in puzzle_input[0].removeprefix("seeds: ").split()
    ]
    return [
        (seed_line[i * 2], sum(seed_line[i * 2 : (i + 1) * 2]) - 1)
        for i in range(len(seed_line) // 2)
    ]


def solve(puzzle_input):
    almanac = Almanac(puzzle_input[2:])
    seeds = get_seeds(puzzle_input)

    return min(loc[0] for loc in almanac.get_seed_locations(seeds))


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
