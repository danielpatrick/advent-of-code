#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest

from shared import get_input

from .. import almanac
from ..almanac import Almanac


class TestAlmanac:
    @pytest.fixture
    def puzzle_input(self):
        yield get_input(almanac.__file__, "test_input.txt")

    @pytest.mark.parametrize(
        "test_input,expected",
        [
            ((79, 93, 98, 100, -48), ([], [(79, 93)])),
            ((55, 68, 50, 98, 2), ([(57, 70)], [])),
            ((95, 105, 98, 100, 10), ([(108, 110)], [(95, 98), (100, 105)])),
        ],
    )
    def test_single_map(self, puzzle_input, test_input, expected):
        assert Almanac(puzzle_input).single_map(*test_input) == expected

    def test_get_map_destinations(self, puzzle_input):
        a = Almanac(puzzle_input)
        seeds = [(79, 93), (55, 68)]
        mapping = a.maps[0]
        expected = [(81, 95), (57, 70)]

        destinations = a.get_map_destinations(seeds, mapping)

        assert destinations == expected
