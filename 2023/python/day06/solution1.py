#!/usr/bin/env python
# -*- coding: utf-8 -*-

from math import prod as product

from .utils import num_ways


def solve(puzzle_input):
    durations = map(int, puzzle_input[0].split()[1:])
    distances = map(int, puzzle_input[1].split()[1:])

    return product(num_ways(d, r) for (d, r) in zip(durations, distances))


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
