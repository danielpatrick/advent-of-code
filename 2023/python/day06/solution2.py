#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .utils import num_ways


def solve(puzzle_input):
    duration = int("".join(puzzle_input[0].split()[1:]))
    distance = int("".join(puzzle_input[1].split()[1:]))

    return num_ways(duration, distance)


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
