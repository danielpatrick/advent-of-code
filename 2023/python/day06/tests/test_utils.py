#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest

from ..utils import num_ways


class TestSolution:
    @pytest.mark.parametrize(
        "duration,distance,expected",
        [
            (7, 9, 4),
            (15, 40, 8),
            (30, 200, 9),
        ],
    )
    def test_num_ways(self, duration, distance, expected):
        assert num_ways(duration, distance) == expected
