#!/usr/bin/env python
# -*- coding: utf-8 -*-

from math import ceil, floor
from operator import add, sub


def quadratic(duration, distance):
    return tuple(
        op(duration, (duration**2 - 4 * distance) ** 0.5) / 2
        for op in (sub, add)
    )


def num_ways(duration, distance):
    start, end = quadratic(duration, distance)

    return ceil(end - 1) - floor(start + 1) + 1
