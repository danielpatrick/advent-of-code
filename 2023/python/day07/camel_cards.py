#!/usr/bin/env python
# -*- coding: utf-8 -*-

from collections import Counter
from enum import Enum


class HandType(Enum):
    HighCard = 1
    OnePair = 2
    TwoPair = 3
    ThreeOfAKind = 4
    FullHouse = 5
    FourOfAKind = 6
    FiveOfAKind = 7


CARDS_WITH_JACKS = {k: v for v, k in enumerate("23456789TJQKA")}
CARDS_WITH_JOKERS = {k: v for v, k in enumerate("J23456789TQKA")}


class Hand:
    def __init__(self, line, *, jokers):
        cards, bid = line.split()

        self.bid = int(bid)

        if jokers:
            self.cards = [CARDS_WITH_JOKERS[c] for c in cards]
        else:
            self.cards = [CARDS_WITH_JACKS[c] for c in cards]

        self.hand_type = self._get_hand_type(self.cards, jokers=jokers)

    def _get_hand_type(self, cards, *, jokers):
        counter = Counter(cards)

        if jokers and 0 < counter[0] < 5:
            num_jokers = counter.pop(0)
            highest = counter.most_common(1)[0][0]

            counter[highest] += num_jokers

        counts = counter.most_common()

        if counts[0][1] == 5:
            return HandType.FiveOfAKind
        if counts[0][1] == 4:
            return HandType.FourOfAKind
        if counts[0][1] == 3:
            if counts[1][1] == 2:
                return HandType.FullHouse
            return HandType.ThreeOfAKind
        if counts[0][1] == 2:
            if counts[1][1] == 2:
                return HandType.TwoPair
            return HandType.OnePair

        return HandType.HighCard

    def _tuple(self):
        return (self.hand_type.value, *self.cards)

    def __lt__(self, other):
        return self._tuple() < other._tuple()


def calculate_winnings(puzzle_input, *, jokers):
    hands = sorted(Hand(line, jokers=jokers) for line in puzzle_input)

    return sum(rank * hand.bid for rank, hand in enumerate(hands, 1))
