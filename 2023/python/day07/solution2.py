#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .camel_cards import calculate_winnings


def solve(puzzle_input):
    return calculate_winnings(puzzle_input, jokers=True)


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
