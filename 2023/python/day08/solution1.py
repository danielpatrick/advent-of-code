#!/usr/bin/env python
# -*- coding: utf-8 -*-

ORDER_CHARS = {"L": 0, "R": 1}


def parse_line(line):
    k, v = line.split(" = ")

    return (k, tuple(v.strip("()").split(", ")))


def solve(puzzle_input):
    left_or_right = [ORDER_CHARS[c] for c in puzzle_input[0]]
    network = dict(parse_line(line) for line in puzzle_input[2:])
    element = "AAA"

    steps = 0

    while element != "ZZZ":
        pos = steps % len(left_or_right)

        element = network[element][left_or_right[pos]]
        steps += 1

    return steps


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
