#!/usr/bin/env python
# -*- coding: utf-8 -*-

from math import lcm

ORDER_CHARS = {"L": 0, "R": 1}


def parse_line(line):
    k, v = line.split(" = ")

    return (k, tuple(v.strip("()").split(", ")))


def solve(puzzle_input):
    left_or_right = [ORDER_CHARS[c] for c in puzzle_input[0]]
    network = dict(parse_line(line) for line in puzzle_input[2:])
    elements = [node for node in network if node[-1] == "A"]

    steps_required = None
    steps = 0

    while elements:
        remaining_elements = []
        pos = steps % len(left_or_right)

        for e in elements:
            if e[-1] == "Z":
                if steps_required is None:
                    steps_required = steps
                else:
                    steps_required = lcm(steps_required, steps)

            else:
                remaining_elements.append(network[e][left_or_right[pos]])

        elements = remaining_elements
        steps += 1

    return steps_required


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
