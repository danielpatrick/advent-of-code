#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest

from shared import get_input

from .. import utils
from ..utils import get_next_value, get_prev_value, parse_line


class TestSolution:
    @pytest.fixture
    def puzzle_input(self):
        yield get_input(utils.__file__, "test_input.txt")

    def test_get_prev_value(self, puzzle_input):
        assert get_prev_value(parse_line(puzzle_input[0])) == -3
        assert get_prev_value(parse_line(puzzle_input[1])) == 0
        assert get_prev_value(parse_line(puzzle_input[2])) == 5

    def test_get_next_value(self, puzzle_input):
        assert get_next_value(parse_line(puzzle_input[0])) == 18
        assert get_next_value(parse_line(puzzle_input[1])) == 28
        assert get_next_value(parse_line(puzzle_input[2])) == 68
