from enum import Enum
from operator import add, sub


class Position(Enum):
    Start = 0
    End = -1


def parse_line(line):
    return list(map(int, line.split()))


def get_value(values, pos: Position):
    if all(v == 0 for v in values):
        return 0

    op = sub if pos == Position.Start else add

    return op(
        values[pos.value],
        get_value(
            [values[i] - values[i - 1] for i in range(1, len(values))],
            pos,
        ),
    )


def get_prev_value(values):
    return get_value(values, Position.Start)


def get_next_value(values):
    return get_value(values, Position.End)
