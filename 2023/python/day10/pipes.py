#!/usr/bin/env python
# -*- coding: utf-8 -*-

TILE_TO_DIRS = {
    "|": [(0, -1), (0, 1)],
    "-": [(-1, 0), (1, 0)],
    "L": [(0, -1), (1, 0)],
    "J": [(0, -1), (-1, 0)],
    "7": [(0, 1), (-1, 0)],
    "F": [(0, 1), (1, 0)],
    ".": [],
}

DIRS_TO_TILE = {
    (0, 1): {"|", "L", "J"},
    (0, -1): {"|", "7", "F"},
    (1, 0): {"-", "J", "7"},
    (-1, 0): {"-", "L", "F"},
}


class Pipes:
    def __init__(self, lines):
        self.height = len(lines)
        self.width = len(lines[0])
        self.grid = "".join(lines)

        pos = self.grid.find("S")

        self.start = (pos % self.width, pos // self.width)

        start_tile = self.determine_start_tile()

        self.grid = self.grid[:pos] + start_tile + self.grid[pos + 1 :]

    def determine_start_tile(self):
        x, y = self.start

        joining = []

        for (dx, dy), tiles in DIRS_TO_TILE.items():
            if self.try_get(x + dx, y + dy) in tiles:
                joining.append((-dx, -dy))

        if len(joining) != 2:
            raise Exception(
                "Unable to find two joining items for start pos "
                f"{self.start}. Found: {joining}"
            )

        possible = list(DIRS_TO_TILE[joining[0]] & DIRS_TO_TILE[joining[1]])

        if len(possible) != 1:
            raise Exception(
                f"Expected 1 tile type to match start pos. Found: {possible}"
            )

        return possible[0]

    def get(self, x, y):
        return self.grid[y * self.width + x]

    def try_get(self, x, y):
        if 0 <= x < self.width and 0 <= y < self.height:
            return self.get(x, y)

    def get_moves(self, x, y, exclude=None):
        possible = [
            (x + dx, y + dy) for (dx, dy) in TILE_TO_DIRS[self.get(x, y)]
        ]

        if exclude is not None:
            possible.remove(exclude)

        return possible

    def map_pipe(self):
        pipe = [self.start]
        pos = pipe[-1]
        prev = None

        while True:
            prev, pos = pos, self.get_moves(*pos, prev)[0]

            if pos == self.start:
                break

            pipe.append(pos)

        return pipe

    def count_within_bounds(self):
        pipe_set = set(self.map_pipe())

        inside = 0

        for y in range(self.height):
            vertical_count = 0
            last_corner = None

            for x in range(self.width):
                if (x, y) in pipe_set:
                    char = self.get(x, y)

                    if (last_corner, char) in {("F", "J"), ("L", "7")}:
                        vertical_count += 1

                    if char in {"F", "L", "J", "7"}:
                        last_corner = char

                    elif char == "|":
                        vertical_count += 1
                elif vertical_count % 2 == 1:
                    inside += 1

        return inside
