#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .pipes import Pipes


def solve(puzzle_input):
    pipes = Pipes(puzzle_input)

    return (len(pipes.map_pipe()) + 1) // 2


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
