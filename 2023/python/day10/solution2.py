#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .pipes import Pipes


def solve(puzzle_input):
    pipes = Pipes(puzzle_input)

    return pipes.count_within_bounds()


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
