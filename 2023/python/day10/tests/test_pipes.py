#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest

from shared import get_input

from .. import pipes
from ..pipes import Pipes


class TestPipes:
    @pytest.fixture
    def puzzle_input1(self):
        yield get_input(pipes.__file__, "test_input1.txt")

    @pytest.fixture
    def puzzle_input2(self):
        yield get_input(pipes.__file__, "test_input2.txt")

    @pytest.fixture
    def puzzle_input3(self):
        yield get_input(pipes.__file__, "test_input3.txt")

    @pytest.fixture
    def puzzle_input4(self):
        yield get_input(pipes.__file__, "test_input4.txt")

    @pytest.fixture
    def puzzle_input5(self):
        yield get_input(pipes.__file__, "test_input5.txt")

    @pytest.fixture
    def puzzle_input6(self):
        yield get_input(pipes.__file__, "test_input6.txt")

    def test_pipes_init1(self, puzzle_input1):
        p = Pipes(puzzle_input1)

        assert p.get(*p.start) == "F"

    def test_pipes_init2(self, puzzle_input2):
        p = Pipes(puzzle_input2)

        assert p.get(*p.start) == "F"

    def test_map_pipe(self, puzzle_input1):
        p = Pipes(puzzle_input1)
        expected = [
            (1, 1),
            (2, 1),
            (3, 1),
            (1, 2),
            (3, 2),
            (1, 3),
            (2, 3),
            (3, 3),
        ]

        pipe_map = p.map_pipe()

        assert len(pipe_map) == len(expected)
        assert set(pipe_map) == set(expected)

    def test_count_within_bounds(
        self, puzzle_input3, puzzle_input4, puzzle_input5, puzzle_input6
    ):
        assert Pipes(puzzle_input3).count_within_bounds() == 4
        assert Pipes(puzzle_input4).count_within_bounds() == 4
        assert Pipes(puzzle_input5).count_within_bounds() == 8
        assert Pipes(puzzle_input6).count_within_bounds() == 10
