#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest

from shared import get_input

from .. import solution2
from ..solution2 import solve


class TestSolution:
    @pytest.fixture
    def puzzle_input3(self):
        yield get_input(solution2.__file__, "test_input3.txt")

    @pytest.fixture
    def puzzle_input4(self):
        yield get_input(solution2.__file__, "test_input4.txt")

    @pytest.fixture
    def puzzle_input5(self):
        yield get_input(solution2.__file__, "test_input5.txt")

    @pytest.fixture
    def puzzle_input6(self):
        yield get_input(solution2.__file__, "test_input6.txt")

    def test_solver(
        self, puzzle_input3, puzzle_input4, puzzle_input5, puzzle_input6
    ):
        assert solve(puzzle_input3) == 4
        assert solve(puzzle_input4) == 4
        assert solve(puzzle_input5) == 8
        assert solve(puzzle_input6) == 10
