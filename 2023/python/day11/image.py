#!/usr/bin/env python
# -*- coding: utf-8 -*-

from itertools import combinations


class Image:
    def __init__(self, lines):
        grid = "".join(lines)

        self.height = len(lines)
        self.width = len(lines[0])
        self.galaxies = [
            (i % self.width, i // self.width)
            for i in range(len(grid))
            if grid[i] == "#"
        ]

    def expand(self, num_empty):
        rows_without_galaxies = set(range(self.height)) - {
            y for (x, y) in self.galaxies
        }

        cols_without_galaxies = set(range(self.width)) - {
            x for (x, y) in self.galaxies
        }

        for i in range(len(self.galaxies)):
            (gx, gy) = self.galaxies[i]

            self.galaxies[i] = (
                gx + sum(num_empty for x in cols_without_galaxies if x < gx),
                gy + sum(num_empty for y in rows_without_galaxies if y < gy),
            )

        self.width += num_empty * len(cols_without_galaxies)
        self.height += num_empty * len(rows_without_galaxies)

    def distance_sum(self):
        return sum(
            abs(xa - xb) + abs(ya - yb)
            for (xa, ya), (xb, yb) in combinations(self.galaxies, 2)
        )
