#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .image import Image


def solve(puzzle_input):
    i = Image(puzzle_input)
    i.expand(1)

    return i.distance_sum()


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
