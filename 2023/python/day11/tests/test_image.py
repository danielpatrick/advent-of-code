#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest

from shared import get_input

from .. import image
from ..image import Image


class TestImage:
    @pytest.fixture
    def puzzle_input(self):
        yield get_input(image.__file__, "test_input.txt")

    @pytest.mark.parametrize(
        "num_empty,expected",
        [
            (1, 374),
            (9, 1_030),
            (99, 8_410),
        ],
    )
    def test_image_expand_and_distance_sum(
        self, num_empty, expected, puzzle_input
    ):
        i = Image(puzzle_input)
        i.expand(num_empty)

        assert i.distance_sum() == expected
