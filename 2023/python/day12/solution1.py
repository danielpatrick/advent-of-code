#!/usr/bin/env python
# -*- coding: utf-8 -*-


from .springs import num_arrangements, parse_line


def solve(puzzle_input):
    return sum(num_arrangements(*parse_line(line)) for line in puzzle_input)


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
