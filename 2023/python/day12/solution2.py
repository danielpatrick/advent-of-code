#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .springs import num_arrangements
from .springs import parse_line as simple_parse_line


def parse_line(line):
    springs, sizes = simple_parse_line(line)

    return (
        "?".join(springs for _ in range(5)),
        sizes * 5,
    )


def solve(puzzle_input):
    return sum(num_arrangements(*parse_line(line)) for line in puzzle_input)


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
