#!/usr/bin/env python
# -*- coding: utf-8 -*-


def arrangements(springs, sizes, spare, memo=None):
    if memo is None:
        memo = {}

    memo_key = (len(springs), tuple(sizes), spare)

    if memo_key in memo:
        return memo[memo_key]

    possible = 0

    if len(sizes) == 0:
        if spare == 0 or "#" not in springs:
            possible = 1
    else:
        for left_padding in range(spare + 1):
            length = left_padding + sizes[0]

            if "#" in springs[:left_padding]:
                continue

            if "." in springs[left_padding:length]:
                continue

            if len(sizes) > 1 and length < len(springs):
                if springs[length] == "#":
                    continue

                length += 1

            possible += arrangements(
                springs[length:],
                sizes[1:],
                spare - left_padding,
                memo,
            )

    memo[memo_key] = possible

    return possible


def num_arrangements(springs, sizes):
    num_spaces = len(springs)
    required_spaces = sum(sizes) + len(sizes) - 1

    return arrangements(springs, sizes, num_spaces - required_spaces)


def parse_line(line):
    springs, sizes = line.split()
    sizes = [int(s) for s in sizes.split(",")]

    return (springs, sizes)
