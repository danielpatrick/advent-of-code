#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest

from shared import get_input

from .. import springs
from ..springs import num_arrangements, parse_line


class TestSolution:
    @pytest.fixture
    def puzzle_input(self):
        yield get_input(springs.__file__, "test_input.txt")

    @pytest.mark.parametrize(
        "line_num,expected",
        [
            (0, 1),
            (1, 4),
            (2, 1),
            (3, 1),
            (4, 4),
            (5, 10),
        ],
    )
    def test_num_arrangements1(self, puzzle_input, line_num, expected):
        springs, sizes = parse_line(puzzle_input[line_num])

        assert num_arrangements(springs, sizes) == expected

    @pytest.mark.parametrize(
        "line_num,expected",
        [
            (0, 1),
            (1, 16384),
            (2, 1),
            (3, 16),
            (4, 2500),
            (5, 506250),
        ],
    )
    def test_num_arrangements2(self, puzzle_input, line_num, expected):
        springs, sizes = parse_line(puzzle_input[line_num])

        springs = "?".join(springs for _ in range(5))
        sizes *= 5

        assert num_arrangements(springs, sizes) == expected
