#!/usr/bin/env python
# -*- coding: utf-8 -*-


class Map:
    def __init__(self, lines):
        self.height = len(lines)
        self.width = len(lines[0])
        self.grid = "".join(lines)

    def count_differences(self, line, other):
        return sum(1 for a, b in zip(line, other) if a != b)

    def find_mirror(self, smudges, limit, *, length=None, step=None):
        for i in range(1, limit):
            possible_smudges = 0

            for j in range(min(limit - i, i)):
                start1 = i - j - 1
                end1 = None
                start2 = i + j
                end2 = None

                if length is not None:
                    start1 *= length
                    end1 = start1 + length
                    start2 *= length
                    end2 = start2 + length

                possible_smudges += self.count_differences(
                    self.grid[start1:end1:step],
                    self.grid[start2:end2:step],
                )

                if possible_smudges > smudges:
                    break

            if possible_smudges == smudges:
                return i

        return None

    def find_vertical_mirror(self, smudges):
        return self.find_mirror(smudges, self.width, step=self.width)

    def find_horizontal_mirror(self, smudges):
        return self.find_mirror(smudges, self.height, length=self.width)

    def find_mirror_score(self, smudges=0):
        vertical = self.find_vertical_mirror(smudges)

        if vertical is not None:
            return vertical

        horizontal = self.find_horizontal_mirror(smudges)

        if horizontal is not None:
            return horizontal * 100


def parse_input(puzzle_input):
    maps = []
    prev = None

    for i, line in enumerate(puzzle_input + [""]):
        if line == "":
            maps.append(Map(puzzle_input[prev:i]))
            prev = i + 1

    return maps
