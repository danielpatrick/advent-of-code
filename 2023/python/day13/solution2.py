#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .lava_island import parse_input


def solve(puzzle_input):
    maps = parse_input(puzzle_input)

    return sum(m.find_mirror_score(1) for m in maps)


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
