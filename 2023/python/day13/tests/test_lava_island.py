#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest

from shared import get_input

from .. import lava_island
from ..lava_island import parse_input


class TestSolution:
    @pytest.fixture
    def puzzle_input(self):
        yield get_input(lava_island.__file__, "test_input.txt")

    @pytest.mark.parametrize(
        "n,smudges,expected",
        [
            (0, 0, 5),
            (1, 0, None),
            (0, 1, None),
            (1, 1, None),
        ],
    )
    def test_map_find_vertical_mirror(
        self, puzzle_input, n, smudges, expected
    ):
        mirror_map = parse_input(puzzle_input)[n]

        assert mirror_map.find_vertical_mirror(smudges) == expected

    @pytest.mark.parametrize(
        "n,smudges,expected",
        [
            (0, 0, None),
            (1, 0, 4),
            (0, 1, 3),
            (1, 1, 1),
        ],
    )
    def test_map_find_horizontal_mirror(
        self, puzzle_input, n, smudges, expected
    ):
        mirror_map = parse_input(puzzle_input)[n]

        assert mirror_map.find_horizontal_mirror(smudges) == expected

    @pytest.mark.parametrize(
        "n,smudges,expected",
        [
            (0, 0, 5),
            (1, 0, 400),
            (0, 1, 300),
            (1, 1, 100),
        ],
    )
    def test_map_find_mirror_score(self, puzzle_input, n, smudges, expected):
        mirror_map = parse_input(puzzle_input)[n]

        assert mirror_map.find_mirror_score(smudges) == expected
