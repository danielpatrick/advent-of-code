#!/usr/bin/env python
# -*- coding: utf-8 -*-


class Platform:
    def __init__(self, s):
        self.height = len(s)
        self.width = len(s[0])

        self.rounded = {
            (x, y)
            for x in range(self.width)
            for y in range(self.height)
            if s[y][x] == "O"
        }

        self.moves = {
            (x, y): {} for x in range(self.width) for y in range(self.height)
        }

        for x in range(self.width):
            open_range = []

            for y in range(self.height + 1):
                if y == self.height or s[y][x] == "#":
                    for y in open_range:
                        self.moves[(x, y)][(0, 1)] = (x, open_range[-1])

                    open_range = []
                else:
                    open_range.append(y)
                    self.moves[(x, y)][(0, -1)] = (x, open_range[0])

        for y in range(self.height):
            open_range = []

            for x in range(self.width + 1):
                if x == self.width or s[y][x] == "#":
                    for x in open_range:
                        self.moves[(x, y)][(1, 0)] = (open_range[-1], y)

                    open_range = []
                else:
                    open_range.append(x)
                    self.moves[(x, y)][(-1, 0)] = (open_range[0], y)

    def tilt(self, dx, dy):
        new_rounded = set()

        for x, y in self.rounded:
            x, y = self.moves[(x, y)][(dx, dy)]

            while (x, y) in new_rounded:
                x -= dx
                y -= dy

            new_rounded.add((x, y))

        self.rounded = new_rounded

    def spin(self):
        for dx, dy in ((0, -1), (-1, 0), (0, 1), (1, 0)):
            self.tilt(dx, dy)

    def calculate_load(self):
        return sum(self.height - y for (_, y) in self.rounded)

    def _get_char(self, x, y):
        if y in self.empty_ranges_upwards[x]:
            if (x, y) in self.rounded:
                return "O"

            return "."

        return "#"

    def __repr__(self):
        return "".join(
            ("".join(self._get_char(x, y) for x in range(self.width)) + "\n")
            for y in range(self.height)
        )
