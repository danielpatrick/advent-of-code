#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .platform import Platform


def solve(puzzle_input):
    p = Platform(puzzle_input)
    p.tilt(0, -1)

    return p.calculate_load()


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
