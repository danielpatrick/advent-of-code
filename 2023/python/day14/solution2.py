#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .platform import Platform


def solve(puzzle_input):
    n = 1_000_000_000
    min_repeats = 4  # no idea what a good choice is here
    seen = []

    repeat_start = None
    repeat_loads = []

    p = Platform(puzzle_input)

    for i in range(n):
        p.spin()

        load = p.calculate_load()

        if repeat_start is not None:
            if i >= (repeat_start + 3 * len(repeat_loads)):
                break

            idx = (i - repeat_start) % len(repeat_loads)

            if repeat_loads[idx] != load:
                repeat_start = None
                repeat_loads = []
        else:
            try:
                idx = seen.index(load)
                if i - idx >= min_repeats:
                    repeat_start = i
                    repeat_loads = seen[idx:i]
            except ValueError:
                pass

        seen.append(load)

    idx = (n - repeat_start) % len(repeat_loads)

    return repeat_loads[idx - 1]


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
