#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest

from shared import get_input

from .. import platform
from ..platform import Platform


class TestSolution:
    @pytest.fixture
    def puzzle_input(self):
        yield get_input(platform.__file__, "test_input.txt")

    def test_calculate_load(self):
        puzzle_input = [
            "OOOO.#.O..",
            "OO..#....#",
            "OO..O##..O",
            "O..#.OO...",
            "........#.",
            "..#....#.#",
            "..O..#.O.O",
            "..O.......",
            "#....###..",
            "#....#....",
        ]

        assert Platform(puzzle_input).calculate_load() == 136
