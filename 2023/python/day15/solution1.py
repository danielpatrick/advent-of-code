#!/usr/bin/env python
# -*- coding: utf-8 -*-


def hash(word):
    result = 0

    for c in word:
        result += ord(c)
        result *= 17
        result %= 256

    return result


def solve(puzzle_input):
    return sum(hash(word) for word in puzzle_input.split(","))


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
