#!/usr/bin/env python
# -*- coding: utf-8 -*-


def hash(word):
    result = 0

    for c in word:
        result += ord(c)
        result *= 17
        result %= 256

    return result


def solve(puzzle_input):
    boxes = [[] for _ in range(256)]

    for step in puzzle_input.split(","):
        if step[-1] == "-":
            label = step[:-1]
            box = hash(label)

            # Remove lens with this label
            boxes[box] = [lens for lens in boxes[box] if lens[0] != label]
        else:
            label, length = step.split("=")
            box = hash(label)
            length = int(length)

            idx = next(
                (i for i, lens in enumerate(boxes[box]) if lens[0] == label),
                None,
            )

            if idx is None:
                # Append new lens as none with this label exists
                boxes[box].append((label, length))
            else:
                # Replace existing lens with same label
                boxes[box][idx] = (label, length)

    power = 0

    for i in range(256):
        for slot, (_, length) in enumerate(boxes[i], 1):
            power += (i + 1) * slot * length

    return power


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
