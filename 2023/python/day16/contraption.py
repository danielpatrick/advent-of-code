#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys

sys.setrecursionlimit(10_000)


UP = (0, -1)
RIGHT = (1, 0)
DOWN = (0, 1)
LEFT = (-1, 0)


DIRECTION_MAP = {
    (".", UP): [UP],
    (".", RIGHT): [RIGHT],
    (".", DOWN): [DOWN],
    (".", LEFT): [LEFT],
    ("/", UP): [RIGHT],
    ("/", RIGHT): [UP],
    ("/", DOWN): [LEFT],
    ("/", LEFT): [DOWN],
    ("\\", UP): [LEFT],
    ("\\", RIGHT): [DOWN],
    ("\\", DOWN): [RIGHT],
    ("\\", LEFT): [UP],
    ("|", UP): [UP],
    ("|", RIGHT): [DOWN, UP],
    ("|", DOWN): [DOWN],
    ("|", LEFT): [DOWN, UP],
    ("-", UP): [RIGHT, LEFT],
    ("-", RIGHT): [RIGHT],
    ("-", DOWN): [RIGHT, LEFT],
    ("-", LEFT): [LEFT],
}


class Contraption:
    def __init__(self, s):
        self.width = len(s[0])
        self.height = len(s)
        self.grid = "".join(s)

    def get(self, x, y):
        return self.grid[y * self.width + x]

    def in_bounds(self, x, y):
        return 0 <= x < self.width and 0 <= y < self.height

    def _energise(self, x, y, direction, energised):
        dx, dy = direction
        x, y = (x + dx, y + dy)

        if not self.in_bounds(x, y):
            return

        if (x, y, direction) in energised:
            return

        energised.add((x, y, direction))

        tile = self.get(x, y)

        for d in DIRECTION_MAP[(tile, direction)]:
            self._energise(x, y, d, energised)

    def count_energised(self, start=None, direction=RIGHT):
        energised = set()

        if start is None:
            start = (-1, 0)

        self._energise(*start, direction, energised)

        return len(set((x, y) for x, y, _ in energised))
