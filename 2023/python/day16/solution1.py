#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .contraption import Contraption


def solve(puzzle_input):
    c = Contraption(puzzle_input)

    return c.count_energised()


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
