#!/usr/bin/env python
# -*- coding: utf-8 -*-

from itertools import chain

from .contraption import DOWN, LEFT, RIGHT, UP, Contraption


def solve(puzzle_input):
    c = Contraption(puzzle_input)

    args_iter = chain(
        (((x, c.height), UP) for x in range(c.width)),
        (((-1, y), RIGHT) for y in range(c.height)),
        (((x, -1), DOWN) for x in range(c.width)),
        (((c.width, y), LEFT) for y in range(c.height)),
    )

    return max(c.count_energised(*args) for args in args_iter)


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
