#!/usr/bin/env python
# -*- coding: utf-8 -*-

import heapq


class CityMap:
    def __init__(self, s, min_moves, max_moves):
        self.min_moves = min_moves
        self.max_moves = max_moves

        self.grid = list(map(int, "".join(s)))
        self.height = len(s)
        self.width = len(s[0])

        self.start = (0, 0)
        self.target = (self.width - 1, self.height - 1)

    def get(self, x, y):
        return self.grid[y * self.width + x]

    def in_bounds(self, x, y):
        return 0 <= x < self.width and 0 <= y < self.height

    def edge(self, dx, dy, repeats, x, y, moves):
        new_x = x + dx * moves
        new_y = y + dy * moves

        if self.in_bounds(new_x, new_y):
            yield (
                (dx, dy, repeats + moves, new_x, new_y),
                sum(
                    self.get(x + dx * i, y + dy * i)
                    for i in range(1, moves + 1)
                ),
            )

    def edges_with_loss(self, dx, dy, repeats, x, y):
        prev_dir = (dx, dy)

        for dx, dy in ((1, 0), (0, 1), (-1, 0), (0, -1)):
            if prev_dir == (-dx, -dy):
                continue

            if prev_dir == (dx, dy):
                if repeats >= self.max_moves:
                    continue

                yield from self.edge(dx, dy, repeats, x, y, 1)
            else:
                yield from self.edge(dx, dy, 0, x, y, self.min_moves)

    def find_lowest_loss(self):
        queue = []
        heapq.heappush(queue, (0, 0, 0, 0, self.start[0], self.start[1]))
        seen = set()

        while queue:
            loss, dx, dy, repeats, x, y = heapq.heappop(queue)

            if (dx, dy, repeats, x, y) in seen:
                continue

            if (x, y) == self.target:
                return loss

            seen.add((dx, dy, repeats, x, y))

            for edge, next_loss in self.edges_with_loss(dx, dy, repeats, x, y):
                next_loss += loss

                heapq.heappush(queue, (next_loss, *edge))

        return None
