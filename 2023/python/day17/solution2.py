#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .city import CityMap


def solve(puzzle_input):
    return CityMap(puzzle_input, 4, 10).find_lowest_loss()


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
