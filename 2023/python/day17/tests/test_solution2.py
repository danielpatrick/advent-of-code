#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest

from shared import get_input

from .. import solution2
from ..solution2 import solve


class TestSolution:
    @pytest.fixture
    def puzzle_input1(self):
        yield get_input(solution2.__file__, "test_input.txt")

    @pytest.fixture
    def puzzle_input2(self):
        yield get_input(solution2.__file__, "test_input2.txt")

    def test_solver(self, puzzle_input1, puzzle_input2):
        assert solve(puzzle_input1) == 94
        assert solve(puzzle_input2) == 71
