#!/usr/bin/env python
# -*- coding: utf-8 -*-


from abc import ABC, abstractmethod


class LagoonBase(ABC):
    def __init__(self, s):
        self._interior_points = None
        self._area = None

        x = 0
        y = 0

        self.vertices = [(0, 0)]
        self.boundary_points = 0

        for line in s:
            dx, dy, dist = self.parse_line(line)

            x += dx * dist
            y += dy * dist

            self.vertices.append((x, y))
            self.boundary_points += dist

    @abstractmethod
    def parse_line(self, line):
        ...

    @property
    def interior_points(self):
        if self._interior_points is None:
            one = sum(
                self.vertices[i][0] * self.vertices[i + 1][1]
                for i in range(len(self.vertices) - 1)
            )
            two = sum(
                self.vertices[i][1] * self.vertices[i + 1][0]
                for i in range(len(self.vertices) - 1)
            )

            self._interior_points = abs(one - two) // 2

        return self._interior_points

    @property
    def area(self):
        if self._area is None:
            self._area = self.interior_points + (self.boundary_points // 2) + 1

        return self._area
