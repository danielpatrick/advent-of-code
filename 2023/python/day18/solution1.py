#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .lagoon import LagoonBase

DIRECTIONS = {
    "R": (1, 0),
    "D": (0, 1),
    "L": (-1, 0),
    "U": (0, -1),
}


class Lagoon(LagoonBase):
    def parse_line(self, line):
        d, dist, colour = line.split()

        dx, dy = DIRECTIONS[d]
        dist = int(dist)

        return (dx, dy, dist)


def solve(puzzle_input):
    lagoon = Lagoon(puzzle_input)

    return lagoon.area


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
