#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .lagoon import LagoonBase

DIRECTIONS = [(1, 0), (0, 1), (-1, 0), (0, -1)]


class Lagoon(LagoonBase):
    def parse_line(self, line):
        _, _, colour = line.split()
        colour = colour.strip("(#)")

        dx, dy = DIRECTIONS[int(colour[-1])]
        dist = int(colour[:-1], 16)

        return (dx, dy, dist)


def solve(puzzle_input):
    lagoon = Lagoon(puzzle_input)

    return lagoon.area


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
