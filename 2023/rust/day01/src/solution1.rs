pub fn solve(input: &str) -> u32 {
    input
        .lines()
        .map(|s| {
            let mut first: Option<u32> = None;
            let mut last: Option<u32> = None;

            let chars = s.chars().collect::<Vec<_>>();
            let len = chars.len() - 1;

            for i in 0..=len {
                first = first.or_else(|| chars[i].to_digit(10));
                last = last.or_else(|| chars[len - i].to_digit(10));
            }

            first.unwrap() * 10 + last.unwrap()
        })
        .sum()
}

#[cfg(test)]
mod tests {
    use super::solve;

    fn get_test_input() -> &'static str {
        include_str!("../../../inputs/day01/test_input.txt")
    }

    #[test]
    fn test_solve() {
        assert_eq!(solve(get_test_input()), 142);
    }
}
