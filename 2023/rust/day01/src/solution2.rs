use std::collections::HashMap;

use lazy_static::lazy_static;

lazy_static! {
    static ref DIGITS: HashMap<usize, HashMap<Vec<char>, u32>> = HashMap::from([
        (
            3,
            HashMap::from([
                (vec!['o', 'n', 'e'], 1),
                (vec!['t', 'w', 'o'], 2),
                (vec!['s', 'i', 'x'], 6),
            ])
        ),
        (
            4,
            HashMap::from([
                (vec!['f', 'o', 'u', 'r'], 4),
                (vec!['f', 'i', 'v', 'e'], 5),
                (vec!['n', 'i', 'n', 'e'], 9),
            ])
        ),
        (
            5,
            HashMap::from([
                (vec!['t', 'h', 'r', 'e', 'e'], 3),
                (vec!['s', 'e', 'v', 'e', 'n'], 7),
                (vec!['e', 'i', 'g', 'h', 't'], 8),
            ])
        ),
    ]);
}

fn get_digit(line: &[char], i: usize, forward: bool) -> Option<u32> {
    line[i].to_digit(10).or_else(|| {
        for (&len, map) in DIGITS.iter() {
            for (word, n) in map.iter() {
                let idx = if forward {
                    if i + len > line.len() {
                        continue;
                    }

                    i
                } else {
                    if i + 1 < len || i + 1 > line.len() {
                        continue;
                    }

                    i + 1 - len
                };

                if word == &line[idx..idx + len] {
                    return Some(*n);
                }
            }
        }

        None
    })
}

pub fn solve(input: &str) -> u32 {
    input
        .lines()
        .map(|s| {
            let mut first: Option<u32> = None;
            let mut last: Option<u32> = None;

            let chars = s.chars().collect::<Vec<_>>();
            let len = chars.len() - 1;

            for i in 0..=len {
                first = first.or_else(|| get_digit(&chars, i, true));
                last = last.or_else(|| get_digit(&chars, len - i, false));
            }

            first.unwrap() * 10 + last.unwrap()
        })
        .sum()
}

#[cfg(test)]
mod tests {
    use super::solve;

    fn get_test_input() -> &'static str {
        include_str!("../../../inputs/day01/test_input_two.txt")
    }

    #[test]
    fn test_solve() {
        assert_eq!(solve(get_test_input()), 281);
    }
}
