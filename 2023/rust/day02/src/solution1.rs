use phf::phf_map;

static AVAILABLE_CUBES: phf::Map<&'static str, u32> = phf_map! {
    "red" => 12,
    "green" => 13,
    "blue" => 14,
};

pub fn solve(input: &str) -> u32 {
    input
        .lines()
        .map(|line| {
            let (a, b) = line.split_once(": ").unwrap();

            let can_play = b.split("; ").all(|s| {
                s.split(", ").all(|s| {
                    let (count, colour) = s.split_once(' ').unwrap();
                    AVAILABLE_CUBES
                        .get(colour)
                        .is_some_and(|value| value >= &count.parse::<u32>().unwrap())
                })
            });
            if can_play {
                a.split_once(' ').unwrap().1.parse::<u32>().unwrap()
            } else {
                0
            }
        })
        .sum()
}

#[cfg(test)]
mod tests {
    use super::solve;

    fn get_test_input() -> &'static str {
        include_str!("../../../inputs/day02/test_input.txt")
    }

    #[test]
    fn test_solve() {
        assert_eq!(solve(get_test_input()), 8);
    }
}
