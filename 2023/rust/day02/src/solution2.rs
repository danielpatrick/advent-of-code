use std::cmp::max;
use std::collections::HashMap;

pub fn solve(input: &str) -> u32 {
    input
        .lines()
        .map(|line| {
            let (_, b) = line.split_once(": ").unwrap();

            let counts = b
                .split("; ")
                .fold(HashMap::<&str, u32>::new(), |mut acc, s| {
                    for s in s.split(", ") {
                        let (count, colour) = s.split_once(' ').unwrap();
                        let count = count.parse::<u32>().unwrap();

                        acc.entry(colour)
                            .and_modify(|e| *e = max(*e, count))
                            .or_insert(count);
                    }

                    acc
                });

            counts.values().product::<u32>()
        })
        .sum()
}

#[cfg(test)]
mod tests {
    use super::solve;

    fn get_test_input() -> &'static str {
        include_str!("../../../inputs/day02/test_input.txt")
    }

    #[test]
    fn test_solve() {
        assert_eq!(solve(get_test_input()), 2286);
    }
}
