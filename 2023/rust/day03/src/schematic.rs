use std::cmp::{max, min};

pub struct Schematic {
    grid: Vec<char>,
    pub height: usize,
    pub width: usize,
}

impl From<&str> for Schematic {
    fn from(s: &str) -> Self {
        let width = s.find('\n').unwrap();
        let grid = s.chars().filter(|&c| c != '\n').collect::<Vec<_>>();
        let height = grid.len() / width;

        Self {
            grid,
            width,
            height,
        }
    }
}

impl Schematic {
    fn get(&self, x: usize, y: usize) -> char {
        self.grid[x + y * self.width]
    }

    pub fn is_digit(&self, x: usize, y: usize) -> bool {
        self.get(x, y).is_ascii_digit()
    }

    fn get_digit(&self, x: usize, y: usize) -> Option<u32> {
        self.get(x, y).to_digit(10)
    }

    fn is_symbol(&self, x: usize, y: usize) -> bool {
        let c = self.get(x, y);

        !c.is_ascii_digit() && c != '.'
    }

    pub fn get_gears(&self, row: usize, start_x: usize, end_x: usize) -> Vec<(usize, usize)> {
        ((max(start_x, 1) - 1)..min(end_x + 2, self.width))
            .flat_map(|x| {
                ((max(row, 1) - 1)..min(row + 2, self.height))
                    .filter_map(move |y| self.is_symbol(x, y).then_some((x, y)))
            })
            .collect()
    }

    pub fn is_part_number(&self, row: usize, start_x: usize, end_x: usize) -> bool {
        !self.get_gears(row, start_x, end_x).is_empty()
    }

    pub fn get_part_number(&self, row: usize, start_x: usize, end_x: usize) -> u32 {
        (start_x..=end_x).fold(0, |acc, x| acc * 10 + self.get_digit(x, row).unwrap())
    }
}
