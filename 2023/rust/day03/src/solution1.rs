use crate::schematic::Schematic;

pub fn solve(input: &str) -> u32 {
    let schematic = Schematic::from(input);

    let mut positions = Vec::new();

    for y in 0..schematic.height {
        let mut start = None;
        let mut end = None;

        for x in 0..=schematic.width {
            if x != schematic.width && schematic.is_digit(x, y) {
                start.get_or_insert(x);
                end = Some(x);
            } else {
                if let (Some(start_x), Some(end_x)) = (start, end) {
                    if schematic.is_part_number(y, start_x, end_x) {
                        positions.push((y, start_x, end_x));
                    }
                }

                start = None;
                end = None;
            }
        }
    }

    positions
        .iter()
        .map(|(y, start, end)| schematic.get_part_number(*y, *start, *end))
        .sum()
}

#[cfg(test)]
mod tests {
    use super::solve;

    fn get_test_input() -> &'static str {
        include_str!("../../../inputs/day03/test_input.txt")
    }

    #[test]
    fn test_solve() {
        assert_eq!(solve(get_test_input()), 4361);
    }
}
