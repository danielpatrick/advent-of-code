use std::collections::HashMap;

use crate::schematic::Schematic;

pub fn solve(input: &str) -> u32 {
    let schematic = Schematic::from(input);

    #[allow(clippy::type_complexity)]
    let mut gears: HashMap<(usize, usize), Vec<(usize, usize, usize)>> = HashMap::new();

    for y in 0..schematic.height {
        let mut start = None;
        let mut end = None;

        for x in 0..=schematic.width {
            if x != schematic.width && schematic.is_digit(x, y) {
                start.get_or_insert(x);
                end = Some(x);
            } else {
                if let (Some(start_x), Some(end_x)) = (start, end) {
                    for gear in schematic.get_gears(y, start_x, end_x) {
                        gears.entry(gear).or_default().push((y, start_x, end_x))
                    }
                }

                start = None;
                end = None;
            }
        }
    }

    gears
        .values()
        .filter(|parts| parts.len() >= 2)
        .map(|parts| {
            parts
                .iter()
                .map(|(row, start_x, end_x)| schematic.get_part_number(*row, *start_x, *end_x))
                .product::<u32>()
        })
        .sum()
}

#[cfg(test)]
mod tests {
    use super::solve;

    fn get_test_input() -> &'static str {
        include_str!("../../../inputs/day03/test_input.txt")
    }

    #[test]
    fn test_solve() {
        assert_eq!(solve(get_test_input()), 467835);
    }
}
