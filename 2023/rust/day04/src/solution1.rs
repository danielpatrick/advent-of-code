use std::collections::HashSet;

fn create_set(s: &str) -> HashSet<&str> {
    s.split_whitespace().collect()
}

fn get_points(line: &str) -> u32 {
    let (winning, own) = line.split_once(": ").unwrap().1.split_once(" | ").unwrap();

    let matches = create_set(winning).intersection(&create_set(own)).count() as u32;

    if matches >= 1 {
        2u32.pow(matches - 1)
    } else {
        0
    }
}

pub fn solve(input: &str) -> u32 {
    input.lines().map(get_points).sum()
}

#[cfg(test)]
mod tests {
    use super::solve;

    fn get_test_input() -> &'static str {
        include_str!("../../../inputs/day04/test_input.txt")
    }

    #[test]
    fn test_solve() {
        assert_eq!(solve(get_test_input()), 13);
    }
}
