use std::collections::HashSet;

fn create_set(s: &str) -> HashSet<&str> {
    s.split_whitespace().collect()
}

fn get_cards_won(line: &str) -> Vec<usize> {
    let (card, numbers) = line.split_once(": ").unwrap();
    let card_number = card
        .split_once(' ')
        .unwrap()
        .1
        .trim()
        .parse::<usize>()
        .unwrap();

    let (winning, own) = numbers.split_once(" | ").unwrap();

    create_set(winning)
        .intersection(&create_set(own))
        .enumerate()
        .map(|(i, _)| card_number + i)
        .collect()
}

pub fn solve(input: &str) -> u32 {
    let cards = input.lines().map(get_cards_won).collect::<Vec<_>>();

    let num_cards = cards.len();

    let mut card_counts = vec![1; cards.len()];

    for i in 0..num_cards {
        for card in &cards[i] {
            card_counts[*card] += card_counts[i];
        }
    }

    return card_counts.iter().sum();
}

#[cfg(test)]
mod tests {
    use super::solve;

    fn get_test_input() -> &'static str {
        include_str!("../../../inputs/day04/test_input.txt")
    }

    #[test]
    fn test_solve() {
        assert_eq!(solve(get_test_input()), 30);
    }
}
