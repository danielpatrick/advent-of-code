use std::cmp::{max, min};

pub type NumRange = (i64, i64);
type MappingItem = (i64, i64, i64);
type Mapping = Vec<MappingItem>;

pub struct Almanac {
    maps: Vec<Mapping>,
}

impl From<&str> for Almanac {
    fn from(s: &str) -> Self {
        let maps = s
            .split("\n\n")
            .skip(1)
            .map(|s| {
                s.lines()
                    .filter(|line| line.chars().next().unwrap().is_ascii_digit())
                    .map(|line| {
                        let mut parts = line.split(' ');
                        let a = parts.next().unwrap().parse::<i64>().unwrap();
                        let b = parts.next().unwrap().parse::<i64>().unwrap();
                        let c = parts.next().unwrap().parse::<i64>().unwrap();

                        (b, b + c, a - b)
                    })
                    .collect::<Vec<_>>()
            })
            .collect::<Vec<_>>();

        Self { maps }
    }
}

impl Almanac {
    fn single_map(
        &self,
        input: NumRange,
        map_item: &MappingItem,
    ) -> (Vec<NumRange>, Vec<NumRange>) {
        let mut mapped = Vec::new();
        let mut to_map = Vec::new();

        if input.0 >= map_item.0 && input.1 <= map_item.1 {
            // inside range
            mapped.push((input.0 + map_item.2, input.1 + map_item.2));
        } else {
            if input.0 < map_item.0 && map_item.0 <= input.1 {
                // some before start
                to_map.push((input.0, map_item.0));
            }
            if input.0 < map_item.1 && map_item.1 <= input.1 {
                // some after end
                to_map.push((map_item.1, input.1));
            }
            if input.0 <= map_item.1 && input.1 >= map_item.0 {
                // some overlap exists
                mapped.push((
                    max(input.0, map_item.0) + map_item.2,
                    min(input.1, map_item.1) + map_item.2,
                ))
            } else {
                to_map.push(input)
            }
        }

        (mapped, to_map)
    }

    fn get_map_destinations(&self, inputs: Vec<(i64, i64)>, mapping: &Mapping) -> Vec<NumRange> {
        let mut destinations = Vec::new();
        let mut not_mapped = inputs.clone();

        for map_item in mapping {
            let mut new_not_mapped = Vec::new();

            for input in not_mapped {
                let (mapped, to_map) = self.single_map(input, map_item);

                destinations.extend(mapped);
                new_not_mapped.extend(to_map);
            }

            not_mapped = new_not_mapped;
        }

        destinations.extend(not_mapped);

        destinations
    }

    pub fn get_seed_locations(&self, inputs: Vec<(i64, i64)>) -> Vec<NumRange> {
        let mut destinations = inputs.clone();

        for m in &self.maps {
            destinations = self.get_map_destinations(destinations, m);
        }

        destinations
    }
}

#[cfg(test)]
mod tests {
    use super::Almanac;

    fn get_test_input() -> &'static str {
        include_str!("../../../inputs/day05/test_input.txt")
    }

    #[test]
    fn test_single_map() {
        let parameters = [
            (((79, 93), (98, 100, -48)), (vec![], vec![(79, 93)])),
            (((55, 68), (50, 98, 2)), (vec![(57, 70)], vec![])),
            (
                ((95, 105), (98, 100, 10)),
                (vec![(108, 110)], vec![(95, 98), (100, 105)]),
            ),
        ];

        let a = Almanac::from(get_test_input());

        for ((input, map), expected) in parameters {
            assert_eq!(a.single_map(input, &map), expected);
        }
    }

    #[test]
    fn test_get_map_destinations() {
        let a = Almanac::from(get_test_input());
        let seeds = vec![(79, 93), (55, 68)];
        let mapping = vec![(98, 100, -48), (50, 98, 2)];
        let expected = vec![(81, 95), (57, 70)];

        let destinations = a.get_map_destinations(seeds, &mapping);

        assert_eq!(destinations, expected);
    }
}
