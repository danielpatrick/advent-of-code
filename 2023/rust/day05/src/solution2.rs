use crate::almanac::{Almanac, NumRange};
use itertools::Itertools;

fn parse_seeds(input: &str) -> Vec<NumRange> {
    input
        .lines()
        .next()
        .unwrap()
        .strip_prefix("seeds: ")
        .unwrap()
        .split(' ')
        .map(|s| s.parse().unwrap())
        .tuples()
        .map(|(start, offset)| (start, start + offset - 1))
        .collect::<Vec<_>>()
}

pub fn solve(input: &str) -> i64 {
    let seeds = parse_seeds(input);
    let almanac = Almanac::from(input);

    almanac
        .get_seed_locations(seeds)
        .iter()
        .map(|loc| loc.0)
        .min()
        .unwrap()
}

#[cfg(test)]
mod tests {
    use super::solve;

    fn get_test_input() -> &'static str {
        include_str!("../../../inputs/day05/test_input.txt")
    }

    #[test]
    fn test_solve() {
        assert_eq!(solve(get_test_input()), 46);
    }
}
