use crate::utils::num_ways;

pub fn solve(input: &str) -> u64 {
    let mut mapped_lines = input.lines().map(|line| {
        line.split_ascii_whitespace()
            .skip(1)
            .map(|s| s.parse::<u64>().unwrap())
            .collect::<Vec<_>>()
    });

    let durations = mapped_lines.next().unwrap();
    let distances = mapped_lines.next().unwrap();

    durations
        .into_iter()
        .zip(distances)
        .map(|(duration, distance)| num_ways(duration, distance))
        .product()
}

#[cfg(test)]
mod tests {
    use super::solve;

    fn get_test_input() -> &'static str {
        include_str!("../../../inputs/day06/test_input.txt")
    }

    #[test]
    fn test_solve() {
        assert_eq!(solve(get_test_input()), 288);
    }
}
