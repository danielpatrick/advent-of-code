use crate::utils::num_ways;

pub fn solve(input: &str) -> u64 {
    let mut mapped_lines = input.lines().map(|line| {
        line.chars()
            .skip(11)
            .filter(char::is_ascii_digit)
            .map(|c| c.to_digit(10).unwrap() as u64)
            .fold(0, |acc, c| acc * 10 + c)
    });

    let duration = mapped_lines.next().unwrap();
    let distance = mapped_lines.next().unwrap();

    num_ways(duration, distance)
}

#[cfg(test)]
mod tests {
    use super::solve;

    fn get_test_input() -> &'static str {
        include_str!("../../../inputs/day06/test_input.txt")
    }

    #[test]
    fn test_solve() {
        assert_eq!(solve(get_test_input()), 71503);
    }
}
