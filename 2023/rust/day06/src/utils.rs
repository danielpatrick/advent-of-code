fn quadratic(duration: u64, distance: u64) -> (f64, f64) {
    let a = ((duration.pow(2) - 4 * distance) as f64).sqrt();

    (((duration as f64 - a) / 2.0), ((duration as f64 + a) / 2.0))
}

pub fn num_ways(duration: u64, distance: u64) -> u64 {
    let (start, end) = quadratic(duration, distance);

    (end - 1.0).ceil() as u64 - (start + 1.0).floor() as u64 + 1
}

#[cfg(test)]
mod tests {
    use super::num_ways;

    #[test]
    fn test_num_ways() {
        let params = [(7, 9, 4), (15, 40, 8), (30, 200, 9)];
        for (duration, distance, expected) in params {
            assert_eq!(num_ways(duration, distance), expected);
        }
    }
}
