use std::collections::HashMap;

pub enum JCardType {
    Joker,
    Jack,
}

#[derive(Clone, Copy, Hash, PartialEq, Eq, PartialOrd, Ord)]
enum Card {
    Joker = 0,
    One = 1,
    Two = 2,
    Three = 3,
    Four = 4,
    Five = 5,
    Six = 6,
    Seven = 7,
    Eight = 8,
    Nine = 9,
    Ten = 10,
    Jack = 11,
    Queen = 12,
    King = 13,
    Ace = 14,
}

#[derive(PartialEq, Eq, PartialOrd, Ord)]
enum HandType {
    HighCard = 1,
    OnePair = 2,
    TwoPair = 3,
    ThreeOfAKind = 4,
    FullHouse = 5,
    FourOfAKind = 6,
    FiveOfAKind = 7,
}

#[derive(PartialEq, Eq, PartialOrd, Ord)]
struct Hand<const JOKER: bool> {
    hand_type: HandType,
    cards: HandCards,
    bid: usize,
}

type HandCards = (Card, Card, Card, Card, Card);

impl<const JOKER: bool> From<&str> for Hand<JOKER> {
    fn from(s: &str) -> Self {
        let (cards, bid) = s.split_once(' ').unwrap();

        let mut cards_iter = cards.chars().map(|c| match c {
            '1' => Card::One,
            '2' => Card::Two,
            '3' => Card::Three,
            '4' => Card::Four,
            '5' => Card::Five,
            '6' => Card::Six,
            '7' => Card::Seven,
            '8' => Card::Eight,
            '9' => Card::Nine,
            'T' => Card::Ten,
            'J' => match JOKER {
                true => Card::Joker,
                false => Card::Jack,
            },
            'Q' => Card::Queen,
            'K' => Card::King,
            'A' => Card::Ace,
            _ => panic!("Invalid card {}", c),
        });

        let cards = (
            cards_iter.next().unwrap(),
            cards_iter.next().unwrap(),
            cards_iter.next().unwrap(),
            cards_iter.next().unwrap(),
            cards_iter.next().unwrap(),
        );

        Self {
            bid: bid.parse().unwrap(),
            hand_type: Self::get_hand_type(&cards),
            cards,
        }
    }
}

impl<const JOKER: bool> Hand<JOKER> {
    fn most_common(counts: &HashMap<Card, usize>) -> Vec<(Card, usize)> {
        let mut counts: Vec<_> = counts.iter().map(|(a, b)| (*a, *b)).collect();
        counts.sort_by(|a, b| a.1.cmp(&b.1).reverse());

        counts
    }

    fn get_hand_type(cards: &HandCards) -> HandType {
        let mut counts = HashMap::from([(cards.0, 1usize)]);
        *counts.entry(cards.1).or_default() += 1;
        *counts.entry(cards.2).or_default() += 1;
        *counts.entry(cards.3).or_default() += 1;
        *counts.entry(cards.4).or_default() += 1;

        if JOKER {
            let num_jokers = counts.remove(&Card::Joker).unwrap_or_default();
            if num_jokers == 5 {
                counts.insert(Card::Joker, 5);
            } else {
                let highest = Self::most_common(&counts)[0];
                counts.insert(highest.0, highest.1 + num_jokers);
            }
        }

        let most_common = Self::most_common(&counts);

        match most_common[0].1 {
            5 => HandType::FiveOfAKind,
            4 => HandType::FourOfAKind,
            3 => match most_common[1].1 {
                2 => HandType::FullHouse,
                _ => HandType::ThreeOfAKind,
            },
            2 => match most_common[1].1 {
                2 => HandType::TwoPair,
                _ => HandType::OnePair,
            },
            _ => HandType::HighCard,
        }
    }
}

fn calc_winnings<const JOKER: bool>(s: &str) -> usize {
    let mut hands = s.lines().map(Hand::<{ JOKER }>::from).collect::<Vec<_>>();

    hands.sort();

    hands.iter().enumerate().map(|(i, h)| (i + 1) * h.bid).sum()
}

pub fn calculate_winnings(s: &str, j: JCardType) -> usize {
    match j {
        JCardType::Jack => calc_winnings::<false>(s),
        JCardType::Joker => calc_winnings::<true>(s),
    }
}
