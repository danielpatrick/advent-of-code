use crate::camel_cards::{calculate_winnings, JCardType};

pub fn solve(input: &str) -> usize {
    calculate_winnings(input, JCardType::Jack)
}

#[cfg(test)]
mod tests {
    use super::solve;

    fn get_test_input() -> &'static str {
        include_str!("../../../inputs/day07/test_input.txt")
    }

    #[test]
    fn test_solve() {
        assert_eq!(solve(get_test_input()), 6440);
    }
}
