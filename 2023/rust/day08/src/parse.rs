use phf::phf_map;
use std::collections::HashMap;

type Network<'a> = HashMap<&'a str, (&'a str, &'a str)>;

static ORDER_CHARS: phf::Map<char, usize> = phf_map! {
    'L' => 0,
    'R' => 1,
};

fn parse_line(line: &str) -> (&str, (&str, &str)) {
    let (k, mut v) = line.split_once(" = ").unwrap();

    v = v.strip_prefix('(').unwrap();
    v = v.strip_suffix(')').unwrap();

    (k, v.split_once(", ").unwrap())
}

pub fn parse_input(input: &str) -> (Vec<usize>, Network) {
    let left_or_right = input
        .lines()
        .next()
        .unwrap()
        .chars()
        .map(|c| ORDER_CHARS[&c])
        .collect::<Vec<_>>();

    let network = input
        .lines()
        .skip(2)
        .map(parse_line)
        .collect::<HashMap<_, _>>();

    (left_or_right, network)
}
