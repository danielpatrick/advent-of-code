use crate::parse::parse_input;

pub fn solve(input: &str) -> usize {
    let (left_or_right, network) = parse_input(input);

    let mut element = "AAA";
    let mut steps = 0;

    while element != "ZZZ" {
        let pos = steps % left_or_right.len();

        element = match left_or_right[pos] {
            0 => network[element].0,
            _ => network[element].1,
        };

        steps += 1;
    }

    steps
}

#[cfg(test)]
mod tests {
    use super::solve;

    fn get_test_input1() -> &'static str {
        include_str!("../../../inputs/day08/test_input1.txt")
    }

    fn get_test_input2() -> &'static str {
        include_str!("../../../inputs/day08/test_input2.txt")
    }

    #[test]
    fn test_solve() {
        assert_eq!(solve(get_test_input1()), 2);
        assert_eq!(solve(get_test_input2()), 6);
    }
}
