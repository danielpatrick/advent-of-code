use crate::parse::parse_input;

fn gcd_of_two_numbers(a: usize, b: usize) -> usize {
    if b == 0 {
        return a;
    }
    gcd_of_two_numbers(b, a % b)
}

fn lcm(a: usize, b: usize) -> usize {
    a * b / gcd_of_two_numbers(a, b)
}

pub fn solve(input: &str) -> usize {
    let (left_or_right, network) = parse_input(input);

    let mut elements = network
        .keys()
        .filter(|s| s.ends_with('A'))
        .collect::<Vec<_>>();

    elements.iter_mut().fold(1, |acc, e| {
        let steps = (1..).find(|i| {
            let pos = (i - 1) % left_or_right.len();

            *e = match left_or_right[pos] {
                0 => &network[*e].0,
                _ => &network[*e].1,
            };

            e.ends_with('Z')
        });

        lcm(acc, steps.unwrap())
    })
}

#[cfg(test)]
mod tests {
    use super::solve;

    fn get_test_input() -> &'static str {
        include_str!("../../../inputs/day08/test_input3.txt")
    }

    #[test]
    fn test_solve() {
        assert_eq!(solve(get_test_input()), 6);
    }
}
