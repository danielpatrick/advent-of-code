use crate::utils::{get_next_value, parse_lines};

pub fn solve(input: &str) -> i32 {
    parse_lines(input).iter().map(|v| get_next_value(v)).sum()
}

#[cfg(test)]
mod tests {
    use super::solve;

    fn get_test_input() -> &'static str {
        include_str!("../../../inputs/day09/test_input.txt")
    }

    #[test]
    fn test_solve() {
        assert_eq!(solve(get_test_input()), 114);
    }
}
