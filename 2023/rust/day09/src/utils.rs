enum Position {
    Start = 0,
    End = -1,
}

pub fn parse_lines(input: &str) -> Vec<Vec<i32>> {
    input
        .lines()
        .map(|line| {
            line.split_ascii_whitespace()
                .map(|s| s.parse().unwrap())
                .collect()
        })
        .collect()
}

fn get_value(values: &[i32], pos: &Position) -> i32 {
    if values.iter().all(|v| v == &0) {
        return 0;
    }

    let v = get_value(
        &values.windows(2).map(|v| v[1] - v[0]).collect::<Vec<_>>(),
        pos,
    );

    match pos {
        Position::Start => values.first().unwrap() - v,
        Position::End => values.last().unwrap() + v,
    }
}

pub fn get_prev_value(values: &[i32]) -> i32 {
    get_value(values, &Position::Start)
}

pub fn get_next_value(values: &[i32]) -> i32 {
    get_value(values, &Position::End)
}

#[cfg(test)]
mod tests {
    use super::{get_next_value, get_prev_value, parse_lines};

    fn get_test_input() -> &'static str {
        include_str!("../../../inputs/day09/test_input.txt")
    }

    #[test]
    fn test_get_prev_value() {
        let lines = parse_lines(get_test_input());

        assert_eq!(get_prev_value(&lines[0]), -3);
        assert_eq!(get_prev_value(&lines[1]), 0);
        assert_eq!(get_prev_value(&lines[2]), 5);
    }

    #[test]
    fn test_get_next_value() {
        let lines = parse_lines(get_test_input());

        assert_eq!(get_next_value(&lines[0]), 18);
        assert_eq!(get_next_value(&lines[1]), 28);
        assert_eq!(get_next_value(&lines[2]), 68);
    }
}
