use lazy_static::lazy_static;
use std::collections::{HashMap, HashSet};
use std::ops::{Add, Neg};

#[derive(Clone, Copy, PartialEq, Eq, Hash)]
pub struct Coord(isize, isize);

impl Coord {
    fn new(x: isize, y: isize) -> Self {
        Self(x, y)
    }
}

#[derive(PartialEq, Eq, Hash)]
struct CoordOffset(isize, isize);

impl CoordOffset {
    fn new(x: isize, y: isize) -> Self {
        Self(x, y)
    }
}

impl Add<Coord> for &CoordOffset {
    type Output = Coord;

    fn add(self, other: Self::Output) -> Self::Output {
        Self::Output::new(other.0 + self.0, other.1 + self.1)
    }
}

impl Neg for &CoordOffset {
    type Output = CoordOffset;

    fn neg(self) -> Self::Output {
        Self::Output::new(-self.0, -self.1)
    }
}

lazy_static! {
    static ref TILE_TO_DIRS: HashMap<char, [CoordOffset; 2]> = HashMap::from([
        ('|', [CoordOffset(0, -1), CoordOffset(0, 1)]),
        ('-', [CoordOffset(-1, 0), CoordOffset(1, 0)]),
        ('L', [CoordOffset(0, -1), CoordOffset(1, 0)]),
        ('J', [CoordOffset(0, -1), CoordOffset(-1, 0)]),
        ('7', [CoordOffset(0, 1), CoordOffset(-1, 0)]),
        ('F', [CoordOffset(0, 1), CoordOffset(1, 0)]),
    ]);
    static ref DIRS_TO_TILE: HashMap<CoordOffset, HashSet<char>> = HashMap::from([
        (CoordOffset(0, 1), HashSet::from(['|', 'L', 'J'])),
        (CoordOffset(0, -1), HashSet::from(['|', '7', 'F'])),
        (CoordOffset(1, 0), HashSet::from(['-', 'J', '7'])),
        (CoordOffset(-1, 0), HashSet::from(['-', 'L', 'F'])),
    ]);
}

struct Grid<T> {
    height: isize,
    width: isize,
    grid: Vec<T>,
}

impl<T: Copy> Grid<T> {
    fn get(&self, pos: Coord) -> &T {
        &self.grid[(pos.1 * self.width + pos.0) as usize]
    }

    fn try_get(&self, pos: Coord) -> Option<&T> {
        (0 <= pos.0 && pos.0 < self.width && 0 <= pos.1 && pos.1 < self.height)
            .then(|| self.get(pos))
    }

    fn replace(&mut self, pos: Coord, new: T) {
        self.grid[(pos.1 * self.width + pos.0) as usize] = new;
    }
}

pub struct Pipes {
    grid: Grid<char>,
    start: Coord,
}

impl Pipes {
    fn determine_start_tile(grid: &Grid<char>, start: Coord) -> char {
        *DIRS_TO_TILE
            .iter()
            .filter_map(|(offset, tiles)| {
                if matches!(grid.try_get(offset + start), Some(t) if tiles.contains(t)) {
                    Some(DIRS_TO_TILE[&(-offset)].clone())
                } else {
                    None
                }
            })
            .reduce(|acc, set| (&acc & &set))
            .unwrap()
            .iter()
            .next()
            .unwrap()
    }

    fn get_moves(&self, pos: Coord, exclude: Option<Coord>) -> Vec<Coord> {
        TILE_TO_DIRS[self.grid.get(pos)]
            .iter()
            .map(|offset| offset + pos)
            .filter(|new_pos| !matches!(exclude, Some(x) if x == *new_pos))
            .collect()
    }

    pub fn map_pipe(&self) -> Vec<Coord> {
        let mut prev: Option<Coord> = None;
        let mut pipe = vec![self.start];
        let mut pos: Coord = *pipe.last().unwrap();

        loop {
            (prev, pos) = (Some(pos), self.get_moves(pos, prev)[0]);

            if pos == self.start {
                break;
            }

            pipe.push(pos);
        }

        pipe
    }

    pub fn count_within_bounds(&self) -> usize {
        let pipe_set: HashSet<Coord> = self.map_pipe().into_iter().collect();

        (0..self.grid.height).flat_map(|y| {
            let mut vertical_count = 0;
            let mut last_corner = '.';
            let pipe_set = &pipe_set; // avoid capture within closure

            (0..self.grid.width).filter(move |x| {
                let pos = Coord::new(*x, y);

                if pipe_set.contains(&pos) {
                    let c = *self.grid.get(pos);

                    if [('F', 'J'), ('L', '7')].contains(&(last_corner, c)) {
                        vertical_count += 1;
                    }

                    if ['F', 'L', 'J', '7'].contains(&c) {
                        last_corner = c;
                    } else if c == '|' {
                        vertical_count += 1;
                    }

                    false
                } else {
                    vertical_count % 2 == 1
                }
            })
        }).count()
    }
}

impl From<&str> for Pipes {
    fn from(s: &str) -> Self {
        let g = s.lines().flat_map(|line| line.chars()).collect::<Vec<_>>();
        let width = s.find('\n').unwrap() as isize;
        let height = g.len() as isize / width;
        let pos = g.iter().position(|c| c == &'S').unwrap() as isize;

        let mut grid = Grid {
            grid: g,
            width,
            height,
        };

        let start = Coord::new(pos % width, pos / width);
        let start_tile = Self::determine_start_tile(&grid, start);

        grid.replace(start, start_tile);
        Self { grid, start }
    }
}

#[cfg(test)]
mod tests {
    use super::Pipes;

    fn get_test_input3() -> &'static str {
        include_str!("../../../inputs/day10/test_input3.txt")
    }

    fn get_test_input4() -> &'static str {
        include_str!("../../../inputs/day10/test_input4.txt")
    }

    fn get_test_input5() -> &'static str {
        include_str!("../../../inputs/day10/test_input5.txt")
    }

    fn get_test_input6() -> &'static str {
        include_str!("../../../inputs/day10/test_input6.txt")
    }

    #[test]
    fn test_count_within_bounds() {
        assert_eq!(Pipes::from(get_test_input3()).count_within_bounds(), 4);
        assert_eq!(Pipes::from(get_test_input4()).count_within_bounds(), 4);
        assert_eq!(Pipes::from(get_test_input5()).count_within_bounds(), 8);
        assert_eq!(Pipes::from(get_test_input6()).count_within_bounds(), 10);
    }
}
