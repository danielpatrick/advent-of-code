use crate::pipes::Pipes;

pub fn solve(input: &str) -> usize {
    (Pipes::from(input).map_pipe().len() + 1) / 2
}

#[cfg(test)]
mod tests {
    use super::solve;

    fn get_test_input1() -> &'static str {
        include_str!("../../../inputs/day10/test_input1.txt")
    }

    fn get_test_input2() -> &'static str {
        include_str!("../../../inputs/day10/test_input2.txt")
    }

    #[test]
    fn test_solve() {
        assert_eq!(solve(get_test_input1()), 4);
        assert_eq!(solve(get_test_input2()), 8);
    }
}
