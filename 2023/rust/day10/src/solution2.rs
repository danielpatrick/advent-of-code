use crate::pipes::Pipes;

pub fn solve(input: &str) -> usize {
    Pipes::from(input).count_within_bounds()
}

#[cfg(test)]
mod tests {
    use super::solve;

    fn get_test_input3() -> &'static str {
        include_str!("../../../inputs/day10/test_input3.txt")
    }

    fn get_test_input4() -> &'static str {
        include_str!("../../../inputs/day10/test_input4.txt")
    }

    fn get_test_input5() -> &'static str {
        include_str!("../../../inputs/day10/test_input5.txt")
    }

    fn get_test_input6() -> &'static str {
        include_str!("../../../inputs/day10/test_input6.txt")
    }

    #[test]
    fn test_solve() {
        assert_eq!(solve(get_test_input3()), 4);
        assert_eq!(solve(get_test_input4()), 4);
        assert_eq!(solve(get_test_input5()), 8);
        assert_eq!(solve(get_test_input6()), 10);
    }
}
