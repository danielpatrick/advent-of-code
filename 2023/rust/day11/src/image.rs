use itertools::Itertools;
use std::collections::HashSet;

pub struct Image {
    galaxies: HashSet<(usize, usize)>,
    height: usize,
    width: usize,
}

impl From<&str> for Image {
    fn from(s: &str) -> Self {
        let width = s.find('\n').unwrap();
        let grid = s.replace('\n', "");
        let height = grid.len() / width;
        let galaxies = grid
            .chars()
            .enumerate()
            .filter_map(|(i, c)| {
                if c == '#' {
                    Some((i % width, i / width))
                } else {
                    None
                }
            })
            .collect();

        Self {
            galaxies,
            height,
            width,
        }
    }
}

impl Image {
    pub fn expand(&mut self, num_empty: usize) {
        let rows_without_galaxies = &(0..self.height).collect::<HashSet<_>>()
            - &self.galaxies.iter().map(|(_, y)| *y).collect();

        let cols_without_galaxies = &(0..self.width).collect::<HashSet<_>>()
            - &self.galaxies.iter().map(|(x, _)| *x).collect();

        self.galaxies = self
            .galaxies
            .iter()
            .map(|(x, y)| {
                (
                    x + cols_without_galaxies.iter().filter(|x_| x_ < &x).count() * num_empty,
                    y + rows_without_galaxies.iter().filter(|y_| y_ < &y).count() * num_empty,
                )
            })
            .collect();

        self.width += num_empty * cols_without_galaxies.len();
        self.height += num_empty * rows_without_galaxies.len();
    }

    pub fn distance_sum(&self) -> usize {
        self.galaxies
            .iter()
            .combinations(2)
            .map(|v| v[0].0.abs_diff(v[1].0) + v[0].1.abs_diff(v[1].1))
            .sum()
    }
}

#[cfg(test)]
mod tests {
    use super::Image;

    fn get_test_input() -> &'static str {
        include_str!("../../../inputs/day11/test_input.txt")
    }

    #[test]
    fn test_image_expand_and_distance_sum() {
        let num_empty_and_expected = [(1, 374), (9, 1_030), (99, 8_410)];
        for (num_empty, expected) in num_empty_and_expected {
            let mut i = Image::from(get_test_input());
            i.expand(num_empty);

            assert_eq!(i.distance_sum(), expected);
        }
    }
}
