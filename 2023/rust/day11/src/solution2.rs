use crate::image::Image;

pub fn solve(input: &str) -> usize {
    let mut i = Image::from(input);
    i.expand(999_999);

    i.distance_sum()
}

#[cfg(test)]
mod tests {
    use super::solve;

    fn get_test_input() -> &'static str {
        include_str!("../../../inputs/day11/test_input.txt")
    }

    #[test]
    fn test_solve() {
        assert_eq!(solve(get_test_input()), 82000210);
    }
}
