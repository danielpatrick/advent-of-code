use crate::springs::{num_arrangements, parse_line};

pub fn solve(input: &str) -> usize {
    input
        .lines()
        .map(parse_line)
        .map(|(springs, sizes)| num_arrangements(springs, &sizes))
        .sum()
}

#[cfg(test)]
mod tests {
    use super::solve;

    fn get_test_input() -> &'static str {
        include_str!("../../../inputs/day12/test_input.txt")
    }

    #[test]
    fn test_solve() {
        assert_eq!(solve(get_test_input()), 21);
    }
}
