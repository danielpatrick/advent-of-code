use std::collections::HashMap;

fn arrangements(
    springs: &[char],
    sizes: &[usize],
    spare: usize,
    memo: &mut HashMap<String, usize>,
) -> usize {
    let memo_key = format!("{} {:?} {}", springs.len(), sizes, spare);

    if memo.contains_key(&memo_key) {
        return memo[&memo_key];
    }

    let mut possible = 0;

    if sizes.is_empty() {
        if spare == 0 || !springs.contains(&'#') {
            possible = 1;
        }
    } else {
        for left_padding in 0..=spare {
            let mut length = left_padding + sizes[0];

            if springs[..left_padding].contains(&'#') {
                continue;
            }

            if springs[left_padding..length].contains(&'.') {
                continue;
            }

            if sizes.len() > 1 && length < springs.len() {
                if springs[length] == '#' {
                    continue;
                }

                length += 1;
            }

            possible += arrangements(&springs[length..], &sizes[1..], spare - left_padding, memo);
        }
    }

    memo.insert(memo_key, possible);

    possible
}

pub fn num_arrangements(springs: &str, sizes: &[usize]) -> usize {
    let springs = springs.chars().collect::<Vec<_>>();
    let spaces = springs.len() + 1 - sizes.iter().sum::<usize>() - sizes.len();

    arrangements(&springs, sizes, spaces, &mut HashMap::new())
}

pub fn parse_line(line: &str) -> (&str, Vec<usize>) {
    let (springs, sizes) = line.split_once(' ').unwrap();
    let sizes = sizes
        .split(',')
        .map(&str::parse::<usize>)
        .map(Result::unwrap)
        .collect::<Vec<_>>();

    (springs, sizes)
}

#[cfg(test)]
mod tests {
    use super::num_arrangements;

    #[test]
    fn test_num_arrangements() {
        let inputs_and_expected = [
            ("???.###", vec![1, 1, 3], 1),
            (".??..??...?##.", vec![1, 1, 3], 4),
            ("?#?#?#?#?#?#?#?", vec![1, 3, 1, 6], 1),
            ("????.#...#...", vec![4, 1, 1], 1),
            ("????.######..#####.", vec![1, 6, 5], 4),
            ("?###????????", vec![3, 2, 1], 10),
        ];

        for (springs, sizes, expected) in inputs_and_expected {
            assert_eq!(num_arrangements(springs, &sizes), expected);
        }
    }
}
