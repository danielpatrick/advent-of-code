pub struct Map {
    height: usize,
    width: usize,
    grid: Vec<char>,
}

impl From<&str> for Map {
    fn from(s: &str) -> Self {
        let grid = s.lines().flat_map(|line| line.chars()).collect::<Vec<_>>();
        let width = s.find('\n').unwrap();
        let height = grid.len() / width;

        Self {
            height,
            width,
            grid,
        }
    }
}

enum Orientation {
    Vertical,
    Horizontal,
}

impl Map {
    fn find_mirror(
        &self,
        smudges: usize,
        limit: usize,
        orientation: &Orientation,
    ) -> Option<usize> {
        for i in 1..limit {
            let mut possible_smudges = 0;

            for j in 0..i.min(limit - i) {
                let (slice1, slice2) = match orientation {
                    Orientation::Horizontal => {
                        let start1 = (i - j - 1) * self.width;
                        let start2 = (i + j) * self.width;

                        (
                            self.grid[start1..(start1 + self.width)].to_vec(),
                            self.grid[start2..(start2 + self.width)].to_vec(),
                        )
                    }
                    Orientation::Vertical => {
                        let start1 = i - j - 1;
                        let start2 = i + j;

                        (
                            self.grid[start1..]
                                .iter()
                                .step_by(self.width)
                                .copied()
                                .collect::<Vec<_>>(),
                            self.grid[start2..]
                                .iter()
                                .step_by(self.width)
                                .copied()
                                .collect::<Vec<_>>(),
                        )
                    }
                };

                possible_smudges += slice1.iter().zip(&slice2).filter(|(a, b)| a != b).count();

                if possible_smudges > smudges {
                    break;
                }
            }

            if possible_smudges == smudges {
                return Some(i);
            }
        }

        None
    }

    fn find_vertical_mirror(&self, smudges: usize) -> Option<usize> {
        self.find_mirror(smudges, self.width, &Orientation::Vertical)
    }

    fn find_horizontal_mirror(&self, smudges: usize) -> Option<usize> {
        self.find_mirror(smudges, self.height, &Orientation::Horizontal)
    }

    pub fn find_mirror_score(&self, smudges: usize) -> usize {
        self.find_vertical_mirror(smudges)
            .unwrap_or_else(|| self.find_horizontal_mirror(smudges).unwrap() * 100)
    }
}

pub fn parse_input(puzzle_input: &str) -> Vec<Map> {
    puzzle_input.split("\n\n").map(Map::from).collect()
}

#[cfg(test)]
mod tests {
    use super::parse_input;

    fn get_test_input() -> &'static str {
        include_str!("../../../inputs/day13/test_input.txt")
    }

    #[test]
    fn test_map_find_vertical_mirror() {
        let maps = parse_input(get_test_input());
        let input_and_expected = [(0, 0, Some(5)), (1, 0, None), (0, 1, None), (1, 1, None)];

        for (n, smudges, expected) in input_and_expected {
            assert_eq!(maps[n].find_vertical_mirror(smudges), expected);
        }
    }

    #[test]
    fn test_map_find_horizontal_mirror() {
        let maps = parse_input(get_test_input());
        let input_and_expected = [
            (0, 0, None),
            (1, 0, Some(4)),
            (0, 1, Some(3)),
            (1, 1, Some(1)),
        ];

        for (n, smudges, expected) in input_and_expected {
            assert_eq!(maps[n].find_horizontal_mirror(smudges), expected);
        }
    }

    #[test]
    fn test_map_find_mirror_score() {
        let maps = parse_input(get_test_input());
        let input_and_expected = [(0, 0, 5), (1, 0, 400), (0, 1, 300), (1, 1, 100)];

        for (n, smudges, expected) in input_and_expected {
            assert_eq!(maps[n].find_mirror_score(smudges), expected);
        }
    }
}
