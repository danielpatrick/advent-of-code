use crate::lava_island::parse_input;

pub fn solve(input: &str) -> usize {
    parse_input(input)
        .iter()
        .map(|m| m.find_mirror_score(1))
        .sum()
}

#[cfg(test)]
mod tests {
    use super::solve;

    fn get_test_input() -> &'static str {
        include_str!("../../../inputs/day13/test_input.txt")
    }

    #[test]
    fn test_solve() {
        assert_eq!(solve(get_test_input()), 400);
    }
}
