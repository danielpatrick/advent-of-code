use std::collections::{HashMap, HashSet};

#[derive(Debug, PartialEq, Eq, Hash)]
pub enum Direction {
    Up,
    Down,
    Left,
    Right,
}

#[derive(Debug, PartialEq, Eq)]
pub struct Platform {
    height: usize,
    width: usize,
    rounded: HashSet<(usize, usize)>,
    moves: HashMap<(usize, usize), HashMap<Direction, (usize, usize)>>,
}

impl Platform {
    pub fn tilt(&mut self, dir: &Direction) {
        let mut new_rounded = HashSet::new();

        for (x, y) in &self.rounded {
            let (mut x, mut y) = self.moves[&(*x, *y)][dir];

            while new_rounded.contains(&(x, y)) {
                (x, y) = match dir {
                    Direction::Up => (x, y + 1),
                    Direction::Down => (x, y - 1),
                    Direction::Left => (x + 1, y),
                    Direction::Right => (x - 1, y),
                }
            }

            new_rounded.insert((x, y));
        }

        self.rounded = new_rounded;
    }

    pub fn calculate_load(&self) -> usize {
        self.rounded.iter().map(|(_, y)| self.height - y).sum()
    }

    pub fn spin(&mut self) {
        for d in [
            Direction::Up,
            Direction::Left,
            Direction::Down,
            Direction::Right,
        ] {
            self.tilt(&d);
        }
    }
}

impl From<&str> for Platform {
    fn from(s: &str) -> Self {
        let grid = s.lines().flat_map(|line| line.chars()).collect::<Vec<_>>();
        let width = s.find('\n').unwrap();
        let height = grid.len() / width;

        let mut rounded = HashSet::new();

        for x in 0..width {
            for y in 0..height {
                if grid[width * y + x.to_owned()] == 'O' {
                    rounded.insert((x, y));
                }
            }
        }

        let mut moves: HashMap<(usize, usize), HashMap<Direction, (usize, usize)>> = HashMap::new();

        for x in 0..width {
            let mut open_range = Vec::new();

            for y in 0..=height {
                if y == height || grid[y * width + x] == '#' {
                    for y in &open_range {
                        moves
                            .entry((x, *y))
                            .or_default()
                            .insert(Direction::Down, (x, *open_range.last().unwrap()));
                    }

                    open_range.clear();
                } else {
                    open_range.push(y);
                    moves
                        .entry((x, y))
                        .or_default()
                        .insert(Direction::Up, (x, open_range[0]));
                }
            }
        }

        for y in 0..height {
            let mut open_range = Vec::new();

            for x in 0..=width {
                if x == width || grid[y * width + x] == '#' {
                    for x in &open_range {
                        moves
                            .entry((*x, y))
                            .or_default()
                            .insert(Direction::Right, (*open_range.last().unwrap(), y));
                    }

                    open_range.clear();
                } else {
                    open_range.push(x);
                    moves
                        .entry((x, y))
                        .or_default()
                        .insert(Direction::Left, (open_range[0], y));
                }
            }
        }

        Self {
            height,
            width,
            rounded,
            moves,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST_INPUT: &str = include_str!("../../../inputs/day14/test_input.txt");
    const AFTER_TILT_UP: &str = "OOOO.#.O..\n\
         OO..#....#\n\
         OO..O##..O\n\
         O..#.OO...\n\
         ........#.\n\
         ..#....#.#\n\
         ..O..#.O.O\n\
         ..O.......\n\
         #....###..\n\
         #....#....\n";

    #[test]
    fn test_tilt() {
        let mut p = Platform::from(TEST_INPUT);
        p.tilt(&Direction::Up);

        assert_eq!(p, Platform::from(AFTER_TILT_UP));
    }

    #[test]
    fn test_calculate_load() {
        assert_eq!(Platform::from(AFTER_TILT_UP).calculate_load(), 136);
    }
}
