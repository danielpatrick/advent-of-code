use crate::platform::{Direction, Platform};

pub fn solve(input: &str) -> usize {
    let mut p = Platform::from(input);

    p.tilt(&Direction::Up);

    p.calculate_load()
}

#[cfg(test)]
mod tests {
    use super::solve;

    const TEST_INPUT: &str = include_str!("../../../inputs/day14/test_input.txt");

    #[test]
    fn test_solve() {
        assert_eq!(solve(TEST_INPUT), 136);
    }
}
