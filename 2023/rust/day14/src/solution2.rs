use crate::platform::Platform;

pub fn solve(input: &str) -> usize {
    let n = 1_000_000_000;
    let min_repeats = 4;
    let mut seen = Vec::new();

    let mut repeat: Option<(usize, Vec<usize>)> = None;

    let mut platform = Platform::from(input);

    for i in 0..n {
        platform.spin();

        let load = platform.calculate_load();

        if let Some((start, ref loads)) = repeat {
            if i >= (start + 3 * loads.len()) {
                break;
            }

            let idx = (i - start) % loads.len();

            if loads[idx] != load {
                repeat = None;
            }
        } else if let Some(idx) = seen.iter().position(|l| l == &load) {
            if i - idx >= min_repeats {
                repeat = Some((i, seen[idx..i].to_vec()));
            }
        }

        seen.push(load);
    }

    let repeat = repeat.unwrap();
    let idx = (n - repeat.0) % repeat.1.len();

    repeat.1[idx - 1]
}

#[cfg(test)]
mod tests {
    use super::solve;

    const TEST_INPUT: &str = include_str!("../../../inputs/day14/test_input.txt");

    #[test]
    fn test_solve() {
        assert_eq!(solve(TEST_INPUT), 64);
    }
}
