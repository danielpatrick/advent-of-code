fn hash(s: &str) -> usize {
    s.chars().fold(0, |acc, c| ((acc + c as usize) * 17) % 256)
}

pub fn solve(input: &str) -> usize {
    let mut boxes: Vec<Vec<(&str, usize)>> = vec![vec![]; 256];

    for step in input.trim_end().split(',') {
        if let Some(label) = step.strip_suffix('-') {
            let box_num = hash(label);

            // Remove lens with this label
            boxes[box_num].retain(|(lens_label, _)| lens_label != &label);
        } else {
            let (label, length) = step.split_once('=').unwrap();
            let box_num = hash(label);
            let length = length.parse::<usize>().unwrap();

            let idx = boxes[box_num].iter().position(|lens| lens.0 == label);

            if let Some(idx) = idx {
                // Replace existing lens with same label
                boxes[box_num][idx] = (label, length);
            } else {
                // Append new lens as none with this label exists
                boxes[box_num].push((label, length));
            }
        }
    }

    boxes
        .iter()
        .enumerate()
        .flat_map(|(i, lenses)| {
            lenses
                .iter()
                .enumerate()
                .map(move |(slot, (_, length))| (i + 1) * (slot + 1) * length)
        })
        .sum()
}

#[cfg(test)]
mod tests {
    use super::solve;

    const TEST_INPUT: &str = include_str!("../../../inputs/day15/test_input.txt");

    #[test]
    fn test_solve() {
        assert_eq!(solve(TEST_INPUT), 145);
    }
}
