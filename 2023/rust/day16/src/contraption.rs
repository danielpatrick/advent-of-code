use std::collections::HashSet;

#[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
pub enum Direction {
    Up,
    Right,
    Down,
    Left,
}

pub struct Contraption {
    pub height: usize,
    pub width: usize,
    grid: Vec<char>,
}

impl Contraption {
    fn map(&self, pos: (usize, usize), direction: Direction) -> Vec<Direction> {
        match (self.grid[pos.1 * self.width + pos.0], direction) {
            ('/', Direction::Up) | ('\\', Direction::Down) => vec![Direction::Right],
            ('/', Direction::Right) | ('\\', Direction::Left) => vec![Direction::Up],
            ('/', Direction::Down) | ('\\', Direction::Up) => vec![Direction::Left],
            ('/', Direction::Left) | ('\\', Direction::Right) => vec![Direction::Down],
            ('|', Direction::Right | Direction::Left) => vec![Direction::Down, Direction::Up],
            ('-', Direction::Up | Direction::Down) => vec![Direction::Right, Direction::Left],
            _ => vec![direction],
        }
    }

    fn energise(
        &self,
        pos: (usize, usize),
        direction: Direction,
        energised: &mut HashSet<((usize, usize), Direction)>,
    ) {
        if !energised.insert((pos, direction)) {
            return;
        }

        for d in self.map(pos, direction) {
            let (x, y) = match (pos, d) {
                ((0, _), Direction::Left) | ((_, 0), Direction::Up) => continue,
                ((x, _), Direction::Right) if x + 1 == self.width => continue,
                ((_, y), Direction::Down) if y + 1 == self.height => continue,
                ((x, y), Direction::Up) => (x, y - 1),
                ((x, y), Direction::Right) => (x + 1, y),
                ((x, y), Direction::Down) => (x, y + 1),
                ((x, y), Direction::Left) => (x - 1, y),
            };

            self.energise((x, y), d, energised);
        }
    }

    pub fn count_energised(&self, pos: (usize, usize), direction: Direction) -> usize {
        let mut energised = HashSet::new();

        self.energise(pos, direction, &mut energised);

        energised
            .iter()
            .fold(HashSet::new(), |mut acc, (pos, _)| {
                acc.insert(pos);
                acc
            })
            .len()
    }
}

impl TryFrom<&str> for Contraption {
    type Error = &'static str;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        let grid = value
            .lines()
            .flat_map(|line| line.chars())
            .collect::<Vec<_>>();

        value
            .find('\n')
            .map_or(Err("No newline found in value"), |width| {
                let height = grid.len() / width;

                Ok(Self {
                    height,
                    width,
                    grid,
                })
            })
    }
}

#[cfg(test)]
mod tests {
    use super::{Contraption, Direction};

    const TEST_INPUT: &str = include_str!("../../../inputs/day16/test_input.txt");

    #[test]
    fn test_count_energised() {
        let c = Contraption::try_from(TEST_INPUT).unwrap();

        assert_eq!(c.count_energised((0, 0), Direction::Right), 46);
    }
}
