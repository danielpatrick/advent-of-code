use crate::contraption::{Contraption, Direction};

pub fn solve(input: &str) -> usize {
    let c = Contraption::try_from(input).unwrap();

    c.count_energised((0, 0), Direction::Right)
}

#[cfg(test)]
mod tests {
    use super::solve;

    const TEST_INPUT: &str = include_str!("../../../inputs/day16/test_input.txt");

    #[test]
    fn test_solve() {
        assert_eq!(solve(TEST_INPUT), 46);
    }
}
