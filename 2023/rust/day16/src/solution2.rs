use crate::contraption::{Contraption, Direction};

pub fn solve(input: &str) -> usize {
    let c = Contraption::try_from(input).unwrap();

    (0..c.width)
        .map(|x| (x, 0, Direction::Down))
        .chain((0..c.width).map(|x| (x, c.height - 1, Direction::Up)))
        .chain((0..c.height).map(|y| (0, y, Direction::Right)))
        .chain((0..c.height).map(|y| (c.width - 1, y, Direction::Left)))
        .map(|(x, y, d)| c.count_energised((x, y), d))
        .max()
        .unwrap()
}

#[cfg(test)]
mod tests {
    use super::solve;

    const TEST_INPUT: &str = include_str!("../../../inputs/day16/test_input.txt");

    #[test]
    fn test_solve() {
        assert_eq!(solve(TEST_INPUT), 51);
    }
}
