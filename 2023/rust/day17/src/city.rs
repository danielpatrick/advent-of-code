use std::cmp::Reverse;
use std::collections::{BinaryHeap, HashSet};

#[derive(Clone, Copy, PartialEq, Eq, Hash, PartialOrd, Ord, Debug)]
enum Direction {
    Up,
    Right,
    Down,
    Left,
}

const DIRECTIONS: [Direction; 4] = [
    Direction::Right,
    Direction::Down,
    Direction::Left,
    Direction::Up,
];

pub struct CityMap {
    height: usize,
    width: usize,
    grid: Vec<usize>,
    start: (usize, usize),
    target: (usize, usize),
}

type Edge = (Direction, usize, usize, usize);

impl CityMap {
    fn get(&self, x: usize, y: usize) -> usize {
        self.grid[y * self.width + x]
    }

    const fn new_pos(
        &self,
        x: usize,
        y: usize,
        dir: Direction,
        moves: usize,
    ) -> Option<(usize, usize)> {
        match dir {
            Direction::Right if x + moves < self.width => Some((x + moves, y)),
            Direction::Down if y + moves < self.height => Some((x, y + moves)),
            Direction::Left if x >= moves => Some((x - moves, y)),
            Direction::Up if y >= moves => Some((x, y - moves)),
            _ => None,
        }
    }

    fn edge_with_loss(
        &self,
        dir: Direction,
        repeats: usize,
        x: usize,
        y: usize,
        moves: usize,
    ) -> Option<(Edge, usize)> {
        self.new_pos(x, y, dir, moves).map(|(new_x, new_y)| {
            (
                (dir, repeats + moves, new_x, new_y),
                (1..=moves)
                    .map(|m| self.new_pos(x, y, dir, m).unwrap())
                    .map(|(x, y)| self.get(x, y))
                    .sum(),
            )
        })
    }

    fn edges_with_loss(
        &self,
        dir: Direction,
        repeats: usize,
        x: usize,
        y: usize,
        min_moves: usize,
        max_moves: usize,
    ) -> Vec<(Edge, usize)> {
        DIRECTIONS
            .iter()
            .enumerate()
            .filter_map(|(i, new_dir)| {
                if dir == DIRECTIONS[(i + 2) % 4] {
                    None
                } else if dir == *new_dir && repeats > 0 {
                    if repeats >= max_moves {
                        None
                    } else {
                        self.edge_with_loss(*new_dir, repeats, x, y, 1)
                    }
                } else {
                    self.edge_with_loss(*new_dir, 0, x, y, min_moves)
                }
            })
            .collect()
    }

    pub fn find_lowest_loss(&self, min_moves: usize, max_moves: usize) -> Option<usize> {
        let mut seen = HashSet::new();
        let mut heap = BinaryHeap::new();

        heap.push(Reverse((
            0,
            (Direction::Right, 0, self.start.0, self.start.1),
        )));

        while let Some(Reverse((loss, edge))) = heap.pop() {
            let (dir, repeats, x, y) = edge;

            if seen.contains(&edge) {
                continue;
            }

            if (x, y) == self.target {
                return Some(loss);
            }

            seen.insert(edge);

            for (next_edge, next_loss) in
                self.edges_with_loss(dir, repeats, x, y, min_moves, max_moves)
            {
                heap.push(Reverse((loss + next_loss, next_edge)));
            }
        }

        None
    }
}

impl TryFrom<&str> for CityMap {
    type Error = &'static str;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        let grid = value
            .lines()
            .flat_map(|line| line.chars().map(|c| c.to_digit(10).unwrap() as usize))
            .collect::<Vec<_>>();

        value
            .find('\n')
            .map_or(Err("No newline found in value"), |width| {
                let height = grid.len() / width;

                Ok(Self {
                    height,
                    width,
                    grid,
                    start: (0, 0),
                    target: (width - 1, height - 1),
                })
            })
    }
}

#[cfg(test)]
mod tests {
    use super::CityMap;

    const TEST_INPUT1: &str = include_str!("../../../inputs/day17/test_input.txt");
    const TEST_INPUT2: &str = include_str!("../../../inputs/day17/test_input2.txt");

    #[test]
    fn test_solve() {
        assert_eq!(
            CityMap::try_from(TEST_INPUT1)
                .unwrap()
                .find_lowest_loss(1, 3)
                .unwrap(),
            102
        );
        assert_eq!(
            CityMap::try_from(TEST_INPUT1)
                .unwrap()
                .find_lowest_loss(4, 10)
                .unwrap(),
            94
        );
        assert_eq!(
            CityMap::try_from(TEST_INPUT2)
                .unwrap()
                .find_lowest_loss(4, 10)
                .unwrap(),
            71
        );
    }
}
