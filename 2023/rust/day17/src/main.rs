use crossbeam::thread;
use std::env;

mod city;
mod solution1;
mod solution2;

fn parse_arg() -> (bool, bool) {
    match env::args().nth(1).as_deref() {
        Some("1") => (true, false),
        Some("2") => (false, true),
        _ => (true, true),
    }
}

fn maybe_print_solution(solution: Option<usize>, num: i8) {
    if let Some(solution) = solution {
        println!("Solution {num}: {solution}");
    }
}

fn main() {
    const INPUT: &str = include_str!("../../../inputs/day17/input.txt");

    let mut first_solution = None;
    let mut second_solution = None;

    let (execute_solution1, execute_solution2) = parse_arg();

    thread::scope(|s| {
        if execute_solution1 {
            s.spawn(|_| {
                first_solution = Some(solution1::solve(INPUT));
            });
        }
        if execute_solution2 {
            s.spawn(|_| {
                second_solution = Some(solution2::solve(INPUT));
            });
        }
    })
    .unwrap();

    maybe_print_solution(first_solution, 1);
    maybe_print_solution(second_solution, 2);
}
