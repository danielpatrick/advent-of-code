use crate::city::CityMap;

pub fn solve(input: &str) -> usize {
    CityMap::try_from(input)
        .unwrap()
        .find_lowest_loss(1, 3)
        .unwrap()
}

#[cfg(test)]
mod tests {
    use super::solve;

    const TEST_INPUT: &str = include_str!("../../../inputs/day17/test_input.txt");

    #[test]
    fn test_solve() {
        assert_eq!(solve(TEST_INPUT), 102);
    }
}
