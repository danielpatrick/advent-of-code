use crate::city::CityMap;

pub fn solve(input: &str) -> usize {
    CityMap::try_from(input)
        .unwrap()
        .find_lowest_loss(4, 10)
        .unwrap()
}

#[cfg(test)]
mod tests {
    use super::solve;

    const TEST_INPUT1: &str = include_str!("../../../inputs/day17/test_input.txt");
    const TEST_INPUT2: &str = include_str!("../../../inputs/day17/test_input2.txt");

    #[test]
    fn test_solve() {
        assert_eq!(solve(TEST_INPUT1), 94);
        assert_eq!(solve(TEST_INPUT2), 71);
    }
}
