use std::cell::RefCell;

pub enum Direction {
    Right,
    Down,
    Left,
    Up,
}

pub struct Lagoon {
    vertices: Vec<(usize, usize)>,
    boundary_points: usize,
    interior_points: RefCell<Option<usize>>,
    area: RefCell<Option<usize>>,
}

impl Lagoon {
    fn interior_points(&self) -> usize {
        *self.interior_points.borrow_mut().get_or_insert_with(|| {
            let (one, two) = self
                .vertices
                .windows(2)
                .map(|w| (w[0].0 * w[1].1, w[0].1 * w[1].0))
                .reduce(|acc, new| (acc.0 + new.0, acc.1 + new.1))
                .unwrap();

            one.abs_diff(two) / 2
        })
    }

    pub fn area(&self) -> usize {
        *self
            .area
            .borrow_mut()
            .get_or_insert_with(|| self.interior_points() + self.boundary_points / 2 + 1)
    }
}

impl<I> From<I> for Lagoon
where
    I: IntoIterator<Item = (Direction, usize)>,
{
    fn from(value: I) -> Self {
        let mut x = 0;
        let mut y = 0;

        let mut vertices = vec![(0, 0)];
        let mut boundary_points = 0;

        for (dir, dist) in value {
            (x, y) = match dir {
                Direction::Right => (x + dist, y),
                Direction::Down => (x, y + dist),
                Direction::Left => (x - dist, y),
                Direction::Up => (x, y - dist),
            };

            vertices.push((x, y));
            boundary_points += dist;
        }

        Self {
            vertices,
            boundary_points,
            interior_points: RefCell::new(None),
            area: RefCell::new(None),
        }
    }
}
