use crate::lagoon::{Direction, Lagoon};

pub fn solve(input: &str) -> usize {
    let iter = input.lines().map(|line| {
        let dir = match line.chars().next().unwrap() {
            'R' => Direction::Right,
            'D' => Direction::Down,
            'L' => Direction::Left,
            'U' => Direction::Up,
            c => panic!("Oh no '{c}' doesn't look like a direction"),
        };

        let dist = line
            .split_ascii_whitespace()
            .nth(1)
            .unwrap()
            .parse()
            .unwrap();

        (dir, dist)
    });

    Lagoon::from(iter).area()
}

#[cfg(test)]
mod tests {
    use super::solve;

    const TEST_INPUT: &str = include_str!("../../../inputs/day18/test_input.txt");

    #[test]
    fn test_solve() {
        assert_eq!(solve(TEST_INPUT), 62);
    }
}
