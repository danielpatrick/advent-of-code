use crate::lagoon::{Direction, Lagoon};

pub fn solve(input: &str) -> usize {
    let iter = input.lines().map(|line| {
        let colour = line.split_ascii_whitespace().nth(2).unwrap();

        let dir = match colour.chars().nth(7).unwrap() {
            '0' => Direction::Right,
            '1' => Direction::Down,
            '2' => Direction::Left,
            '3' => Direction::Up,
            c => panic!("Oh no '{c}' doesn't look like a direction"),
        };

        let dist = usize::from_str_radix(colour.get(2..=6).unwrap(), 16).unwrap();

        (dir, dist)
    });

    Lagoon::from(iter).area()
}

#[cfg(test)]
mod tests {
    use super::solve;

    const TEST_INPUT: &str = include_str!("../../../inputs/day18/test_input.txt");

    #[test]
    fn test_solve() {
        assert_eq!(solve(TEST_INPUT), 952408144115);
    }
}
