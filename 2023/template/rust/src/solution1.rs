pub fn solve(input: &str) -> String {
    input.trim().to_string() + "!"
}

#[cfg(test)]
mod tests {
    use super::solve;

    const TEST_INPUT: &str = include_str!("../../../inputs/dayDAY/test_input.txt");

    #[test]
    fn test_solve() {
        assert_eq!(solve(TEST_INPUT), "lorem ipsum!");
    }
}
