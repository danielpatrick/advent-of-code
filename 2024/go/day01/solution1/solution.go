package solution1

import (
	"slices"
	"strconv"
	"strings"
)

func absDiff(one int, two int) int {
	if one > two {
		return one - two
	}

	return two - one
}

func Solve(inputText string) (string, error) {
	lines := strings.Split(strings.TrimSpace(inputText), "\n")

	first := make([]int, len(lines))
	second := make([]int, len(lines))

	for idx, line := range lines {
		parts := strings.Split(line, "   ")

		first[idx], _ = strconv.Atoi(parts[0])
		second[idx], _ = strconv.Atoi(parts[1])
	}

	slices.Sort(first)
	slices.Sort(second)

	sum := 0

	for idx := range len(lines) {
		sum += absDiff(first[idx], second[idx])
	}

	return strconv.Itoa(sum), nil
}
