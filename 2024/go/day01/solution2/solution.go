package solution2

import (
	"strconv"
	"strings"
)

func Solve(inputText string) (string, error) {
	lines := strings.Split(strings.TrimSpace(inputText), "\n")

	first := make([]int, len(lines))
	second := make([]int, len(lines))

	for idx, line := range lines {
		parts := strings.Split(line, "   ")

		first[idx], _ = strconv.Atoi(parts[0])
		second[idx], _ = strconv.Atoi(parts[1])
	}

	counts := make(map[int]int)

	for _, n := range second {
		if _, ok := counts[n]; ok {
			counts[n] += 1
		} else {
			counts[n] = 1
		}
	}

	sum := 0

	for _, n := range first {
		sum += n * counts[n]
	}

	return strconv.Itoa(sum), nil
}
