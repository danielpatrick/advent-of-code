package checker

func checkSequence(arr []int, compare func(a, b int) bool) bool {
	if len(arr) < 2 {
		return true
	}

	for i := range len(arr) - 1 {
		if !compare(arr[i], arr[i+1]) {
			return false
		}
	}

	return true
}

func isAscending(arr []int) bool {
	return checkSequence(arr, func(a, b int) bool { return a < b })
}

func isDescending(arr []int) bool {
	return checkSequence(arr, func(a, b int) bool { return a > b })
}

func hasNoGapsOverFour(arr []int) bool {
	return checkSequence(arr, func(a, b int) bool {
		diff := a - b

		return -3 <= diff && diff <= 3
	})
}

func CheckReports(arr []int) bool {
	return (isAscending(arr) || isDescending(arr)) && hasNoGapsOverFour(arr)
}
