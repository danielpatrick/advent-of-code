package checker

import (
	"testing"
)

func TestValid(t *testing.T) {
	inputs := [][]int{{7, 6, 4, 2, 1}, {1, 3, 6, 7, 9}}

	for _, input := range inputs {
		if !CheckReports(input) {
			t.Fatalf(`CheckReports("%v") = false, expected true`, input)
		}
	}
}

func TestInvalid(t *testing.T) {
	inputs := [][]int{
		{1, 2, 7, 8, 9},
		{9, 7, 6, 2, 1},
		{1, 3, 2, 4, 5},
		{8, 6, 4, 4, 1},
	}

	for _, input := range inputs {

		if CheckReports(input) {
			t.Fatalf(`CheckReports("%v") = true, expected false`, input)
		}
	}
}
