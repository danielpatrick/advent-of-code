package main

import (
	"day02/solution1"
	"day02/solution2"
	"fmt"
	"os"
)

type solverFn func(string) (string, error)

type Result struct {
	Value *string
	Error error
}

func parseArg() (bool, bool) {
	if len(os.Args) > 1 {
		if os.Args[1] == "1" {
			return true, false
		} else if os.Args[1] == "2" {
			return false, true
		}
	}

	return true, true
}

func maybeSolve(shouldSolve bool, solver solverFn, inputText string, ch chan<- Result) {
	if shouldSolve {
		s, err := solver(inputText)
		ch <- Result{&s, err}
	}

	close(ch)
}

func maybePrintSolution(solution Result, num int) {
	if solution.Error != nil {
		fmt.Printf("Solver %v failed with error: %s", num, solution.Error)
	} else if solution.Value != nil {
		fmt.Printf("Solution %v: %s\n", num, *solution.Value)
	}
}

func main() {
	inputBytes, _ := os.ReadFile("../../inputs/day02/input.txt")
	inputText := string(inputBytes)

	executeSolution1, executeSolution2 := parseArg()

	firstSolution := make(chan Result)
	secondSolution := make(chan Result)

	go maybeSolve(executeSolution1, solution1.Solve, inputText, firstSolution)
	go maybeSolve(executeSolution2, solution2.Solve, inputText, secondSolution)

	maybePrintSolution(<-firstSolution, 1)
	maybePrintSolution(<-secondSolution, 2)

	fmt.Println("Process completed")
}
