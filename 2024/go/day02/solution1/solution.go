package solution1

import (
	"day02/checker"
	"strconv"
	"strings"
)

func Solve(inputText string) (string, error) {
	lines := strings.Split(strings.TrimSuffix(inputText, "\n"), "\n")

	count := 0

	for _, line := range lines {
		arr := make([]int, 0)

		for _, s := range strings.Split(line, " ") {
			i, _ := strconv.Atoi(s)
			arr = append(arr, i)
		}

		if checker.CheckReports(arr) {
			count += 1
		}
	}

	return strconv.Itoa(count), nil
}
