package solution2

import (
	"day02/checker"
	"strconv"
	"strings"
)

func arrayWithout(arr []int, idx int) []int {
	result := make([]int, len(arr)-1)
	copy(result, arr[:idx])

	for i, n := range arr[idx+1:] {
		result[idx+i] = n
	}

	return result
}

func checkReports(arr []int) bool {
	for i := range len(arr) {
		if checker.CheckReports(arrayWithout(arr, i)) {
			return true
		}
	}

	return false
}

func Solve(inputText string) (string, error) {
	lines := strings.Split(strings.TrimSuffix(inputText, "\n"), "\n")

	count := 0

	for _, line := range lines {
		arr := make([]int, 0)

		for _, s := range strings.Split(line, " ") {
			i, _ := strconv.Atoi(s)
			arr = append(arr, i)
		}

		if checkReports(arr) {
			count += 1
		}
	}

	return strconv.Itoa(count), nil
}
