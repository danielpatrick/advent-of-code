package solution1

import (
	"regexp"
	"strconv"
)

func Solve(inputText string) (string, error) {
	r := regexp.MustCompile(`mul\((\d+),(\d+)\)`)

	total := 0

	for _, match := range r.FindAllStringSubmatch(inputText, -1) {
		intA, _ := strconv.Atoi(match[1])
		intB, _ := strconv.Atoi(match[2])
		total += intA * intB
	}

	return strconv.Itoa(total), nil
}
