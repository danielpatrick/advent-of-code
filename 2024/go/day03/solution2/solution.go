package solution2

import (
	"regexp"
	"strconv"
)

func Solve(inputText string) (string, error) {
	r := regexp.MustCompile(`(?:(do)\(\))|(?:(don't)\(\))|(?:(mul)\((\d+),(\d+)\))`)

	total := 0
	enabled := true

	for _, match := range r.FindAllStringSubmatch(inputText, -1) {
		switch {
		case match[1] == "do":
			enabled = true
		case match[2] == "don't":
			enabled = false
		case match[3] == "mul" && enabled:
			intA, _ := strconv.Atoi(match[4])
			intB, _ := strconv.Atoi(match[5])
			total += intA * intB
		}
	}

	return strconv.Itoa(total), nil
}
