#!/usr/bin/env python
# -*- coding: utf-8 -*-

from collections import Counter


def parse_line(line):
    return tuple(int(word) for word in line.split())


def solve(puzzle_input):
    one, two = zip(*[parse_line(line) for line in puzzle_input])
    counts = Counter(two)

    return sum(counts[n] * n for n in one)


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
