# -*- coding: utf-8 -*-


def check_reports(reports):
    prev = None
    direction = None

    for report in reports:
        if prev is None:
            prev = report
            continue

        diff = report - prev
        prev = report

        if diff == 0:
            return False

        if abs(diff) > 3:
            return False

        direction_ = diff // abs(diff)

        if direction is None:
            direction = direction_
            continue

        if direction_ != direction:
            return False

        direction = direction_

    return True
