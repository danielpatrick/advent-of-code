#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .report_checker import check_reports


def solve(puzzle_input):
    return sum(
        check_reports([int(s) for s in line.split()]) for line in puzzle_input
    )


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
