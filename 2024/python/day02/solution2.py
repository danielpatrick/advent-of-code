#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .report_checker import check_reports


def is_safe(line):
    reports = [int(s) for s in line.split()]

    return any(
        check_reports(reports[:i] + reports[i + 1 :])
        for i in range(len(reports))
    )


def solve(puzzle_input):
    return sum(is_safe(line) for line in puzzle_input)


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
