#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest

from shared import get_input

from .. import report_checker
from ..report_checker import check_reports


class TestSolution:
    @pytest.fixture
    def puzzle_input(self):
        yield get_input(report_checker.__file__, "test_input.txt")

    def test_check_reports(self, puzzle_input):
        assert check_reports(map(int, puzzle_input[0].split())) is True
        assert check_reports(map(int, puzzle_input[1].split())) is False
        assert check_reports(map(int, puzzle_input[2].split())) is False
        assert check_reports(map(int, puzzle_input[3].split())) is False
        assert check_reports(map(int, puzzle_input[4].split())) is False
        assert check_reports(map(int, puzzle_input[5].split())) is True
