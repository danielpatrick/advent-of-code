#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest

from shared import get_input

from .. import solution2
from ..solution2 import is_safe, solve


class TestSolution:
    @pytest.fixture
    def puzzle_input(self):
        yield get_input(solution2.__file__, "test_input.txt")

    def test_is_safe(self, puzzle_input):
        assert is_safe(puzzle_input[0]) is True
        assert is_safe(puzzle_input[1]) is False
        assert is_safe(puzzle_input[2]) is False
        assert is_safe(puzzle_input[3]) is True
        assert is_safe(puzzle_input[4]) is True
        assert is_safe(puzzle_input[5]) is True

    def test_is_safe_first_report_unsafe(self):
        assert is_safe("5 6 4 2 1") is True
        assert is_safe("10 6 4 2 1") is True
        assert is_safe("2 6 7 8 9") is True
        assert is_safe("5 6 4 2 1") is True

    def test_is_safe_first_report_unsafe_plus_another(self):
        assert is_safe("5 6 7 4 1") is False
        assert is_safe("10 6 4 2 3") is False
        assert is_safe("2 6 7 8 13") is False

    def test_is_safe_special_cases(self):
        assert is_safe("8 6 7 4 1") is True
        assert is_safe("9 7 7 6 5") is True
        assert is_safe("5 6 7 9 8") is True

    def test_solver(self, puzzle_input):
        assert solve(puzzle_input) == 4
