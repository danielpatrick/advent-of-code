#!/usr/bin/env python
# -*- coding: utf-8 -*-


import re

PATTERN = re.compile(r"mul\((\d+),(\d+)\)")


def solve(puzzle_input):
    if isinstance(puzzle_input, list):
        puzzle_input = "\n".join(puzzle_input)

    return sum(int(a) * int(b) for a, b in PATTERN.findall(puzzle_input))


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
