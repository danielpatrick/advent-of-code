#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re

# fmt: off
PATTERN = re.compile(
    r"(?:(do)\(\))|"
    r"(?:(don't)\(\))|"
    r"(?:(mul)\((\d+),(\d+)\))"
)
# fmt: on


def solve(puzzle_input):
    if isinstance(puzzle_input, list):
        puzzle_input = "\n".join(puzzle_input)

    total = 0
    enabled = True

    for do, dont, mul, a, b in PATTERN.findall(puzzle_input):
        if do == "do":
            enabled = True
        elif dont == "don't":
            enabled = False
        elif mul == "mul" and enabled:
            total += int(a) * int(b)

    return total


if __name__ == "__main__":
    from shared import run_solver

    run_solver(solve, __file__)
