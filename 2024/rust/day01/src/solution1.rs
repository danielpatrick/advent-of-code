pub fn solve(input: &str) -> String {
    let (mut one, mut two): (Vec<_>, Vec<_>) = input
        .lines()
        .map(|s| s.split_once("   ").unwrap())
        .map(|(a, b)| (a.parse::<i32>().unwrap(), b.parse::<i32>().unwrap()))
        .unzip();

    one.sort_unstable();
    two.sort_unstable();

    one.into_iter()
        .zip(two)
        .map(|(a, b)| (a - b).abs())
        .sum::<i32>()
        .to_string()
}

#[cfg(test)]
mod tests {
    use super::solve;

    const TEST_INPUT: &str = include_str!("../../../inputs/day01/test_input.txt");

    #[test]
    fn test_solve() {
        assert_eq!(solve(TEST_INPUT), "11");
    }
}
