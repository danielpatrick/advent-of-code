use std::collections::HashMap;

pub fn solve(input: &str) -> String {
    let (one, two): (Vec<_>, Vec<_>) = input
        .lines()
        .map(|s| s.split_once("   ").unwrap())
        .map(|(a, b)| (a.parse::<i32>().unwrap(), b.parse::<i32>().unwrap()))
        .unzip();

    let counts = two.iter().fold(HashMap::new(), |mut counts, n| {
        counts.entry(n).and_modify(|c| *c += 1).or_insert(1);
        counts
    });

    one.iter()
        .map(|n| counts.get(n).copied().unwrap_or(0) * n)
        .sum::<i32>()
        .to_string()
}

#[cfg(test)]
mod tests {
    use super::solve;

    const TEST_INPUT: &str = include_str!("../../../inputs/day01/test_input.txt");

    #[test]
    fn test_solve() {
        assert_eq!(solve(TEST_INPUT), "31");
    }
}
