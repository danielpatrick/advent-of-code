pub fn check_reports(reports: &[u32]) -> bool {
    return (reports.is_sorted_by(|a, b| a < b) || reports.is_sorted_by(|a, b| a > b))
        && reports
            .windows(2)
            .map(|slice| slice[0].abs_diff(slice[1]))
            .all(|n| n > 0 && n <= 3);
}

#[cfg(test)]
mod tests {
    use super::check_reports;

    #[test]
    fn test_check_reports() {
        assert_eq!(check_reports(&[7, 6, 4, 2, 1]), true);
        assert_eq!(check_reports(&[1, 2, 7, 8, 9]), false);
        assert_eq!(check_reports(&[9, 7, 6, 2, 1]), false);
        assert_eq!(check_reports(&[1, 3, 2, 4, 5]), false);
        assert_eq!(check_reports(&[8, 6, 4, 4, 1]), false);
        assert_eq!(check_reports(&[1, 3, 6, 7, 9]), true);
    }
}
