use crate::checker::check_reports;

pub fn solve(input: &str) -> String {
    input
        .lines()
        .filter(|line| {
            let reports = line
                .split_whitespace()
                .map(|s| s.parse::<u32>().unwrap())
                .collect::<Vec<_>>();

            check_reports(&reports)
        })
        .count()
        .to_string()
}

#[cfg(test)]
mod tests {
    use super::solve;

    const TEST_INPUT: &str = include_str!("../../../inputs/day02/test_input.txt");

    #[test]
    fn test_solve() {
        assert_eq!(solve(TEST_INPUT), "2");
    }
}
