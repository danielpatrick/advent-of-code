use crate::checker::check_reports;

fn is_safe(reports: &[u32]) -> bool {
    (0..reports.len()).any(|i| {
        let mut r = reports[0..i].to_vec();
        r.extend_from_slice(&reports[i + 1..]);
        check_reports(&r)
    })
}

pub fn solve(input: &str) -> String {
    input
        .lines()
        .filter(|line| {
            let reports = line
                .split_whitespace()
                .map(|s| s.parse::<u32>().unwrap())
                .collect::<Vec<_>>();

            is_safe(&reports)
        })
        .count()
        .to_string()
}

#[cfg(test)]
mod tests {
    use super::solve;

    const TEST_INPUT: &str = include_str!("../../../inputs/day02/test_input.txt");

    #[test]
    fn test_solve() {
        assert_eq!(solve(TEST_INPUT), "4");
    }
}
