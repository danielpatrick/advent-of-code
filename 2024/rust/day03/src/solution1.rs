use regex::Regex;

pub fn solve(input: &str) -> String {
    let regex = Regex::new(r"mul\((\d+),(\d+)\)").unwrap();

    regex
        .captures_iter(input)
        .map(|c| c.extract().1)
        .map(|m: [&str; 2]| m.iter().map(|s| s.parse::<i32>().unwrap()).product::<i32>())
        .sum::<i32>()
        .to_string()
}

#[cfg(test)]
mod tests {
    use super::solve;

    const TEST_INPUT: &str = include_str!("../../../inputs/day03/test_input.txt");

    #[test]
    fn test_solve() {
        assert_eq!(solve(TEST_INPUT), "161");
    }
}
