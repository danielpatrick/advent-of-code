use regex::Regex;

pub fn solve(input: &str) -> String {
    let regex =
        Regex::new(r"(?:(do)\(()()\))|(?:(don't)\(()()\))|(?:(mul)\((\d+),(\d+)\))").unwrap();

    let (total, _) = regex.captures_iter(input).map(|c| c.extract().1).fold(
        (0, true),
        |(acc, enabled), m: [&str; 1 | 3]| match (enabled, m[0]) {
            (true, "mul") => (
                acc + m[1].parse::<i32>().unwrap() * m[2].parse::<i32>().unwrap(),
                true,
            ),
            (false, "do") => (acc, true),
            (true, "don't") => (acc, false),
            _ => (acc, enabled),
        },
    );

    total.to_string()
}

#[cfg(test)]
mod tests {
    use super::solve;

    const TEST_INPUT: &str = include_str!("../../../inputs/day03/test_input.txt");

    #[test]
    fn test_solve() {
        assert_eq!(solve(TEST_INPUT), "48");
    }
}
