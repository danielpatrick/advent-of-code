package solution1

import (
	"strings"
)

func Solve(inputText string) (string, error) {
	return strings.TrimSpace(inputText) + "!", nil
}
