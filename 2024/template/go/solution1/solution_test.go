package solution1

import (
	"os"
	"testing"
)

var testInputBytes, _ = os.ReadFile("../../../inputs/dayDAY/test_input.txt")
var testInput = string(testInputBytes)

func TestSolve(t *testing.T) {
	expected := "lorem ipsum!"
	result, err := Solve(testInput)

	if result != expected || err != nil {
		t.Fatalf(`Solve("%s") = %q, %v, expected '%s', nil`, testInput, result, err, expected)
	}
}
