# Advent of Code Solutions

Spoilers within (also terrible code).


## Template

Requires

- [Just](https://github.com/casey/just)

Create year dir using:

```shell
just create 2021
```

This will result in the following directory structure:

```
2021/
├── inputs/
├── python/
├── rust/
├── template/
├── .justfile
└── README.md
```

See the `README.md` within the new folder for further info.
